<?php
// $Id$
/**
 * @file
 * Functions for league home tab
 */

/**
 * display content of league home tab
 */
function league_org_tab_home($node) {
  // Set bread crumb
  league_set_bread_crumb ($node);
  drupal_set_title('');

  $org = $node->nid;

  $body = $node->body;
  if(isset($body['und'])) {
    $content['body']['#content'] = $body['und']['0']['safe_value'];
  } else {
    $content['body']['#content'] = ' ';
  }
  $content['body']['#theme'] = 'league_divs_render';
  $content['body']['#div'] = 'body';

  return $content;
}

function theme_league_divs_render($variables) {
  $element = $variables['element'];
  $output = '<div id=home-';
  $output .= $element['#div'];
  $output .= '>';
  if(isset($element['#title'])) {
    $title = $element['#title'];
    $output .= '<h3>';
    $output .= $title;
    $output .= '</h3>';
  }
  $output .= $element['#content'];
  $output .= '</div>';
  return $output;
}

function league_org_get_recent_fixtures($org, $limit = 4) {

  $data = array();

  $query = db_select('league_fixture', 'f');
  $query->addField('f', 'date', 'date');
  $query->innerJoin('league_competition', 'comp', 'comp.cid = f.cid');
  $query->innerJoin('league_event', 'le', 'le.eid = comp.eid');
  $query->condition('le.org', $org);
  // node
  $query->innerJoin('node', 'n', 'n.nid = f.nid');
  $query->addField('n', 'changed', 'updateTime');
  $query->addField('n', 'nid', 'nid');
  // group by for handling fixtures with 2 mini matches
  $query->groupBy('f.nid');
  $query->innerJoin('league_match', 'm', 'm.fid = f.nid');
  // home team and score
  $query->innerJoin('node', 'hn', 'hn.nid = f.home_team');
  $query->addField('hn', 'title', 'homeTeam');
  $query->addExpression('sum(home_score)', 'homeScore');
  // away team and score
  $query->innerJoin('node', 'an', 'an.nid = f.away_team');
  $query->addField('an', 'title', 'awayTeam');
  $query->addExpression('sum(away_score)', 'awayScore');
  // Only include results where there is a none-zero score
  $query->where('m.home_score!=0 or m.away_score!=0');
  // Only include fixtures within the last 30 days
  $lastMonth = time() - (30 * 24 * 60 * 60);
  $query->where("n.changed>$lastMonth");

  $query->range(0, $limit);
  $query->orderBy('updateTime', 'DESC');
//dsm((string)$query);
  
  $result = $query->execute();

  foreach ( $result as $row ) {
    $data[$row->nid]['homeTeam'] = $row->homeTeam;
    $data[$row->nid]['awayTeam'] = $row->awayTeam;
    $data[$row->nid]['homeScore'] = league_score2points($row->homeScore);
    $data[$row->nid]['awayScore'] = league_score2points($row->awayScore);
    $data[$row->nid]['date'] = date('D jS M Y',$row->date);

    $comment = league_get_fixture_comment($row->nid);
    $data[$row->nid]['press'] = $comment;
  }
  return $data;
}

function league_org_recent_fixtures($org) {
  $content = ' ';

  $latest = league_org_get_recent_fixtures($org);

  foreach ($latest as $nid => $fixture) {
    $content .= '<b>' . $fixture['homeTeam'] . ' </b> &nbsp;';
    $content .= $fixture['homeScore'] . " - " . $fixture['awayScore'];
    $content .= '&nbsp; <b>' . $fixture['awayTeam'] . '</b> &nbsp;' . $fixture['date'] . '<br />';
    $length = min(strlen($fixture['press']), 90);
    if ($length > 1 ) {
      $content .= substr($fixture['press'], 0, $length);
      $content .= "<br />";
    }
    $content .= l('Read more ...', "node/$nid");
    $content .= "<p>";
  }
  //$content .= "</div>";
  return $content;
}

/**
 * Get the first comment for a fixture.
 */
function league_get_fixture_comment($nid) {
  $query = db_select('comment', 'c');
  $query->condition('c.nid', $nid);
  $query->innerJoin('field_data_comment_body', 'f', 'f.entity_id = c.cid');
  $query->addField('f', 'comment_body_value', 'comment');
  $query->range(0, 1);

  $comment = $query->execute()->fetchField();
  return $comment;
}

