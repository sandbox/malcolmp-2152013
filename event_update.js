// Javascript for the event settings page
//

// when the document is loaded ..
(function($){
$(document).ready(function() {
// Function to set fields visible depending on event type.
function setVisibility(e) {
  // Get event type
  type_field = $("#edit-type").get(0);
  type = type_field.value;
  var i;
  for (i = 0; i < conditional_fields.length; i++) {
    field = conditional_fields[i].field;
    // If event type is not one for this field ...
    if(conditional_fields[i].types.indexOf(type) == -1) {
      // Make field invisible.
      $(field).get(0).style.display = "none";
    } else {
      // Make field visible.
      $(field).get(0).style.display = "";
    }
  } 
}
// Code to run after page load
  setVisibility();
  $("#edit-type").get(0).onchange = setVisibility;
});
})(jQuery);
