<?php
// $Id$
/**
 * @file
 * Administration page callbacks for chessdb module
 */

function league_admin_settings($form, &$form_state) {

  // Checkbox options

  $settings = array(
    'mailadmin' => t('Send all emails to admin for testing purposes'),
    'orgtitle' => t('Override the site title with the organisation'),
  );
  // Get current settings
  $default = league_get_settings();

  $form['settings'] = array(
      '#title' => t('League Settings'),
      '#type' => 'fieldset',
      '#attributes' => array('class' => array('settings-fieldset') ),
  );

  $form['settings']['flags'] = array(
    '#title' => t('Settings'),
    '#type' => 'checkboxes',
    '#options' => $settings,
    '#default_value' => $default,
  );

  // Club field to use for venue:
  $default_club_field = variable_get('league_club_venue_field', 'nid');
  $club_fields = league_admin_club_fields();

  // Selector field setting
  $form['settings']['field'] = array(
    '#title' => 'Club Venue Field',
    '#type' => 'select',
    '#default_value' => $default_club_field,
    '#options' => $club_fields,
  );

  $limits = league_get_limits();

  // Maximum number of fixtures
  $form['settings']['max_fixtures'] = array(
      '#type' => 'textfield',
      '#title' => t('Maximum number of fixtures per event'),
      '#default_value' => $limits['max_fixtures'],
      '#size' => 10,
  );

  // Submit Button
  $form['settings']['submit'] = array(
    '#value' => t('Save Settings'),
    '#type' => 'submit',
  );

  // Required to do file uploads
  $form['#attributes'] = array('enctype' => "multipart/form-data");

  return $form;
}

function league_admin_club_fields() {
  // Setup lists of club fields to populate drop downs.
  $options = array('nid' => 'nid');
  $instances = field_info_instances();
  $fields = $instances['node']['club'];
  foreach ($fields as $field_name => $instance) {
    $label = $instance['label'];
    $options[$field_name] = $label;
  }
  return $options;
}

/**
 *  Action after the admin settings form is submitted,
 *  with the submit button
 */

function league_admin_settings_submit($form, &$form_state) {

  $settings = $form_state['values']['flags'];
  variable_set('league_settings', $settings);
  $field = $form_state['values']['field'];
  variable_set('league_club_venue_field', $field);
  $limits = league_get_limits();
  $max_fixtures = $form_state['values']['max_fixtures'];
  $limits['max_fixtures'] = $max_fixtures;
  variable_set('league_limits', $limits);
  drupal_set_message("League Settings Saved $field $max_fixtures");
}
