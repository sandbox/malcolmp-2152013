// Javascript for creating a team
//
(function($){
function setTeamName() {
// Function to set a default team name
// Use JQuery to get the club selector object.
  selector = $("#edit-club").get(0);
// Get the currently selected option
  option = selector.options[selector.selectedIndex];
  nextTeam = next_teams[option.value];
// Use JQuery to find the text input field and set its value
  $("#edit-name").get(0).value = nextTeam;

}
  // when the document is loaded ..
  $(document).ready(function() {
    // set the default team name from the club
    setTeamName();
    // set an event handler so it changes if the club changes
    $("#edit-club").get(0).onchange = function() { setTeamName();}
  })
})(jQuery);
