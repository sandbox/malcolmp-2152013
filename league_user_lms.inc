<?php
/**
 * @file
 * User LMS tab.
 */

/**
 * User LMS tab.
 */
function league_user_tab_lms($user) {
  // Set breadcrumb
  // league_set_bread_crumb ($node);
  // Set title
  $title = 'User LMS Access';
  drupal_set_title($title);

  $content['details']['#type'] = 'markup';
  $user_text = league_user_lms_details($user);
  $content['details']['#markup'] = '<div>' . $user_text . '</div>';

  $content['table']['#table'] = league_user_lms_access($user);
  $content['table']['#theme'] = 'league_table';

  $content['clubs']['#table'] = league_user_lms_clubs($user);
  $content['clubs']['#theme'] = 'league_table';

  return $content;
}

/**
 *  Get users details.
 */
function league_user_lms_details($user) {
  $last_login = date('Y-m-d H:i',$user->login);
  $real_name = league_user_value($user, 'name');
  $mobile = league_user_value($user, 'mobile');
  $phone = league_user_value($user, 'phone');
  $roles = ' '; 
  foreach( $user->roles as $role ) {
    $roles .= " " . $role;
  }
  $text = "$real_name last logged in $last_login roles: $roles phone: $mobile $phone ";
  return $text;
}

/**
 *  Get users organisations.
 */
function league_user_lms_access($user) {

  $uid = $user->uid;
  $header = array('Organisation', 'Owner', 'Season', 'Event', 'Owner', 'Team Captain');
  $rows = array();
  // Orgs for the user.
  $query = db_select('user_tools_owner', 'ur');
  $query->condition('ur.uid', $user->uid);
  $query->condition('ur.level', array('O','U'), 'IN');
  $query->condition('ur.type', 'O');
  $query->innerJoin('node', 'n', 'n.nid = ur.id');
  $query->addField('ur', 'id', 'org');
  $query->addField('n', 'title', 'title');
  // If user is org owner
  $query->leftJoin('user_tools_owner', 'owner', "owner.uid = $uid and owner.id=n.nid and owner.level='O' and owner.type='O'");
  $query->addField('owner', 'uid', 'owner');
  // Active seasons
  $query->leftJoin('league_event', 'le', 'le.org = n.nid');
  $query->condition('le.lms', 'A');
  // Events in the seasons
  $query->leftJoin('league_competition', 'comp', 'comp.eid = le.eid');
  // event name and id
  $query->innerJoin('node', 'e', 'e.nid = comp.cid');
  $query->addField('e', 'title', 'event');
  $query->addField('e', 'nid', 'eventid');
  // Season name and id
  $query->addField('le', 'name', 'season');
  $query->addField('le', 'eid', 'eid');
  // Events user is owner of
  $query->leftJoin('user_tools_owner', 'eo', "eo.uid = $uid and eo.type='C' and eo.id=e.nid");
  $query->addField('eo', 'uid', 'eo');
  // Teams in the event that user is captain of
  $query->leftJoin('field_data_field_event', 'te', 'te.field_event_nid = e.nid');
  $query->leftJoin('field_data_field_captain', 'cap', "cap.field_captain_value = $uid and cap.entity_id=te.entity_id");
  // Team name
  $query->leftJoin('node', 'nt', "nt.nid = cap.entity_id");
  $query->addField('nt', 'title', 'team');
  // Remove dupes where not captain of team.
  $query->distinct();
  // execute query
  $result = $query->execute();
  foreach( $result as $row) {
    $org = l($row->title,"node/$row->org/home");
    $owner = 'N';
    if(isset($row->owner)) {
      $owner = 'Y';
    }
    $event = $row->event;
    $season = $row->season;
    $eowner = 'N';
    if(isset($row->eo)) {
      $eowner = 'Y';
    }
    $team = $row->team;
    $rows[] = array($org, $owner, $season, $event, $eowner, $team);
  }

  $table['data'] = $rows;
  $table['header'] = $header;
  return $table;
}

/**
 *  Get users clubs.
 */
function league_user_lms_clubs($user=0) {

  $uid = $user->uid;
  $header = array('Club', 'Owner', 'Secretary');
  $rows = array();

  $query = db_select('node', 'n');
  $query->condition('n.type', 'club');
  $query->addField('n', 'title', 'club');
  $query->addField('n', 'nid', 'nid');
  // If user is club owner
  $query->leftJoin('user_tools_owner', 'owner', "owner.uid = $uid and owner.id=n.nid");
  $query->addField('owner', 'uid', 'owner');
  // If user is club secretary
  $query->leftJoin('field_data_field_contact_user', 'sec', "sec.field_contact_user_uid = $uid and sec.entity_id=n.nid");
  $query->addField('sec', 'field_contact_user_uid', 'sec');
  $query->where('sec.field_contact_user_uid is not null or owner.uid is not null');
  // execute query
  $result = $query->execute();
  foreach( $result as $row) {
    $club = l($row->club,"league/club/$row->nid/0");
    $owner = 'N';
    if(isset($row->owner)) {
      $owner = 'Y';
    }
    $sec = 'N';
    if(isset($row->sec)) {
      $sec = 'Y';
    }
    $rows[] = array($club, $owner, $sec);
  }
  $table['data'] = $rows;
  $table['header'] = $header;
  return $table;
}
