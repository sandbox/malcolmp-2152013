<?php
// $Id$
/**
 * @file
 * Functions for league fixture tab
 */

/**
 * display content of league fixture tab
 */
function league_fixture_view_tab($node) {
  // Set bread crumb
  league_set_bread_crumb ($node);
  // Set title
  drupal_set_title(league_fixture_title($node));
  
  $content = node_view($node);
  return $content;
}
