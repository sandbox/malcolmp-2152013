<?php
/**
 * @file
 * Functions for remove club from org
 */

/**
 * Remove a club from an org
 */
function league_club_remove_org($club, $org, $goto=TRUE) {
  $teams = league_teams_for_club($club, $org);
  $nteams = count($teams);
  if($nteams > 0) {
    drupal_set_message("You can't remove the club while teams exist. You could make the club inactive instead", 'error');
  } else {
    // Load club node
    $node = node_load($club);
    // Get player list (before deleting league_cluborg record! )
    $lid = league_player_object_list($club,$org);
    // Remove the league_club_org record
    $query = db_delete('league_club_org');
    $query->condition('cid', $node->nid);
    $query->condition('org', $org);
    $status = $query->execute();
    // Remove any player list for that org
    $references = league_player_list_references($lid);
    if(count($references) == 0 ) {
      if(league_player_list_delete($lid) ) {
        drupal_set_message("Player List $lid deleted");
      }
    }
    drupal_set_message("Club removed from organisation");
  }
  if($goto) {
    drupal_goto("node/$org/home");
  }
}
