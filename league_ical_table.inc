<?php
/**
 * @file
 * League table as ical.
 */

/**
 * Create table as ical
 */
function league_ical_table($table) {

  $data = $table['data']; // Actual table data

  // send HTTP headers
  $fileName = "fixtures.ics";
  drupal_add_http_header('Content-Type', 'text/calendar;charset=utf-8');
  drupal_add_http_header('Content-Disposition', 'attachment; filename="' . $fileName . '"');
  drupal_add_http_header('Cache-Control',  'max-age=0');
  print league_ical_header();
  foreach ($data as $card ) {
    $summary = $card[0] . ' v ' . $card[2];
    if($card[3] != 'Not Set') {
      print league_ical_event($summary, $card[5], $card[3], $card[4], $card[6]);
    }
  }
  print league_ical_footer();
  exit;
}

function league_ical_header() {
  $header = <<<HEADER
BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//ZContent.net//Zap Calendar 1.0//EN
CALSCALE:GREGORIAN
METHOD:PUBLISH";
HEADER;
  return $header;
}

function league_ical_footer() {
  $footer = "
END:VCALENDAR";
  return $footer;
}

function league_ical_event($summary, $nid, $date, $time, $venue) {
//watchdog('league', "league_ical_event $summary $nid $date $time, $venue");
  $uid=$nid;
  global $base_url;
  $url="$base_url/node/$nid";
  $timestamp=date("Ymd\THis\Z", time());
  $event = "
BEGIN:VEVENT
SUMMARY:$summary
UID:$uid@ecflms.org.uk
SEQUENCE:0
STATUS:CONFIRMED
TRANSP:TRANSPARENT
DTSTAMP:$timestamp
DTSTART:$date
DTEND:$date
TIME:$time
CATEGORIES:Chess
LOCATION:$venue
URL:$url
END:VEVENT";
  return $event;
}
