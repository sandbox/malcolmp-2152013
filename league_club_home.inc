<?php
// $Id$
/**
 * @file
 * Functions for league home tab
 */

/**
 * display content of club settings tab
 */
function league_club_settings_tab($node) {
  // Set title
  drupal_set_title($node->title);

  module_load_include('inc', 'node', 'node.pages');
  $form = drupal_get_form('club_node_form', $node);
  return $form;
}

/**
 * display content of league home tab
 */
function league_club_home_tab($node) {
  // Set bread crumb
  league_set_bread_crumb ($node);
  // Set title
  drupal_set_title($node->title);

  $node->content = array();
  // Build fields content for the node
  field_attach_prepare_view('node',
                            array($node->nid => $node),
                             'full');
  $node->content += field_attach_view('node', $node, 'full');

  // Add the club owners
  $rows = league_object_owner_rows($node->nid, 'B');
  $node->content['table'] = array(
    '#theme' => 'table',
    '#weight' => 1,
    '#rows'  => $rows,
  );

  return $node->content;
}

function league_cubrecent_fixtures($org) {
  $content = ' ';

  $latest = league_org_get_recent_fixtures($org);

  foreach ($latest as $nid => $fixture) {
    $content .= '<b>' . $fixture['homeTeam'] . " " . $fixture['homeScore'] . " - " . $fixture['awayScore'] . " " . $fixture['awayTeam'] . "</b><br />";
    $length = min(strlen($fixture['press']), 90);
    if ($length > 1 ) {
      $content .= substr($fixture['press'], 0, $length);
      $content .= "<br />";
    }
    $content .= l('Read more ...', "node/$nid");
    $content .= "<p>";
  }
  $content .= "</div>";
  return $content;
}

/**
 * Get the first comment for a fixture.
 */
function league_club_get_fixture_comment($nid) {
  $query = db_select('comment', 'c');
  $query->condition('c.nid', $nid);
  $query->innerJoin('field_data_comment_body', 'f', 'f.entity_id = c.cid');
  $query->addField('f', 'comment_body_value', 'comment');
  $query->range(0, 1);

  $comment = $query->execute()->fetchField();
  return $comment;
}

