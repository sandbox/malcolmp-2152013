<?php
/**
 * @file
 * Views files.
 */

/*******************************************************************************
 * Hook Functions (Drupal)
 ******************************************************************************/

/**
 * Implements hook_views_data().
 */
function league_views_data()
{

  $data = array();

  $data['league_club_org']['table']['group'] = t('Club Org');

  $data['league_club_org']['table']['base'] = array(
        'title' => t('Club Org'),
        'help' => t('Contains records we want exposed to Views.'),
  );

  // The org field
  $data['league_club_org']['org'] = array(
      'title' => t('Organisation ID'),
      'help' => t('The clubs org id.'),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_node_nid',
        'numeric' => TRUE,
        'validate type' => 'nid',
    ),
  );

  // The Status field
  $data['league_club_org']['status'] = array(
      'title' => t('Status'),
      'help' => t('Club status within org.'),
      'field' => array(
        'handler' => 'views_handler_field',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
  );

  // The list field
  $data['league_club_org']['list'] = array(
      'title' => t('Player List ID'),
      'help' => t('The player list id.'),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
  );

  $data['league_club_org']['table']['join'] = array(
    'node' => array(
        'left_field' => 'nid',
        'field' => 'cid',
    ),
  );

  // The Node ID field
  $data['league_club_org']['cid'] = array(
    'title' => t('Club Node ID'),
    'help' => t('The club node ID.'),
    'field' => array(
        'handler' => 'views_handler_field_node',
    ),
    'sort' => array(
        'handler' => 'views_handler_sort',
    ),
    'filter' => array(
        'handler' => 'views_handler_filter_numeric',
    ),
    'relationship' => array(
        'base' => 'node',
        'field' => 'cid',
        'handler' => 'views_handler_relationship',
        'label' => t('Node'),
    ),
    'argument' => array(
        'handler' => 'views_handler_argument_node_nid',
        'numeric' => TRUE,
        'validate type' => 'nid',
    ),
  );

  return $data;
}
