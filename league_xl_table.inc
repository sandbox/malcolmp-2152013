<?php
/**
 * @file
 * League table as xl.
 */

// Library for writing XL files
// $exlib = libraries_get_path('vendor');
//require_once "$exlib/autoload.php";
use \PhpOffice\PhpSpreadsheet\Spreadsheet;
use \PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
 * Create table as Excel
 */
function league_xl_table($table) {

  // Create new PHPExcel object
  $objPHPExcel = new Spreadsheet();

  // Create a workbook
  $objPHPExcel->setActiveSheetIndex(0);

  // Creating header worksheet
  $worksheet = $objPHPExcel->getActiveSheet();
  $worksheet->setTitle('Table');

  $headings = $table['header']; // Headers

  $row = 1;
  $cell = 1;
  foreach ($headings as $heading ) {
    if(is_array($heading)) {
      $label = $heading['data'];
    } else {
      $label = $heading;
    }
    $worksheet->SetCellValue([$cell, $row], $label);
    $cell++;
  }

  $data = $table['data']; // Actual table data
  $row++;
  foreach ($data as $card ) {
    $cell = 1;
    foreach ($headings as $value => $col) {
      $worksheet->SetCellValue([$cell, $row], $card[$value]);
      $cell++;
    }
    $row++;
  }

  // send HTTP headers
  $fileName = "table.xls";
  drupal_add_http_header('Content-Type', 'application/vnd.ms-excel');
  drupal_add_http_header('Content-Disposition', 'attachment; filename="' . $fileName . '"');
  drupal_add_http_header('Cache-Control',  'max-age=0');
  $objWriter = new \PhpOffice\PhpSpreadsheet\Writer\Xls($objPHPExcel);
  $objWriter->save("php://output");
  exit;
}
