<?php
/**
 * @file
 * Function to Theme the list of a users fixtures
 */

/**
 * Theme the my fixtures tab
 */
function theme_league_my_fixtures($variables) {
  $element = $variables['element'];
  $clubs = $element['#clubs'];
  $user = $element['#user'];
  $node = $element['#node'];

  $output = '<div class=league-frame>';
  if($user != 0) {
    global $user;
    $output .= "Welcome user <strong>" . $user->name . "</strong>. ";
    $org = $node->nid;
    if (count($clubs) == 0 ) {
      $owner = $node->uid;
      $link = l('contact the organisation owner', "user/$owner/contact");
      $output .= "You don't have access to update match results for any club, please $link ";
    } else {
      $output .= "You have access to update match results for the following clubs: ";
      foreach ($clubs as $club => $nid) {
          $output .= l($club, "league/club/$nid/$org/fixtures") . ", ";
      }
      $output .= "A list of recent fixtures is shown below that have not had any score entered yet and are for one of the clubs listed above. Pick on the one you want to update. To see all the fixtures for a club, pick on its name above.";
    }
  } else {
      $output .= "You need to login to see a list of fixtures that you are allowed to update ";
  }
  $output .= '</div>';

  return $output;
}

