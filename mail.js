// Javascript for creating a team
//
(function($){
  // when the document is loaded ..
  $(document).ready(function() {
    // add a function to the checkAll check box that flips all the others
    $("#checkAll").change(function () {
     $("[id^=edit-check]").attr('checked', $(this).attr("checked"));
   });
  });
})(jQuery);
