<?php
/**
 * @file
 * Functions for theming league table settings form
 */

// Theme function for league table ordering settings.
function theme_league_competition_table_edit($variables) {
  $form = $variables['form'];
 
  // Create an array of table rows
  $rows = array();
  foreach (element_children($form) as $sort) {
    // Row for the column (dragable)
    $form[$sort]['weight']['#attributes']['class'] = array('sorts-order-weight');
    $form[$sort]['label']['#attributes']['class'] = array('sorts-order-label');
    $form[$sort]['enabled']['#attributes']['class'] = array('sorts-order-enabled');
    $row = array(
        'data' => array(
          array('class' => array('event-cross')),
            drupal_render($form[$sort]['label']),
            drupal_render($form[$sort]['enabled']),
            drupal_render($form[$sort]['weight']),
          ),
          'class' => array('draggable','tabledrag-leaf',''),
    );
    $rows[] = $row;
  }
 
  // Table header
  $header = array('Pick cross and drag to order columns', t('Column'), t('Enabled'), t('Weight') );
  $output = ' ';
  $output .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'sorts-order')));
  $output .= drupal_render_children($form);
 
  drupal_add_tabledrag('sorts-order',         // table id
                       'order',                // operation
                       'sibling',
                       'sorts-order-weight'); // Class of weights column
 
  return $output;
}
