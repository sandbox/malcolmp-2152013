<?php
/**
* @file
* Functions for creating the club settings form.
*/

// Club Settings page.
function league_club_org_settings_tab($node,$org) {
  // Set Breadcrumb
  league_set_bread_crumb ($node);

  return drupal_get_form('league_club_org_settings_form', $node,$org);
}

// Club org settings form
function league_club_org_settings_form($form, &$form_state,$node,$org) {

  $form_state['league_org'] = $org;
  $form_state['league_club'] = $node->nid;
  $form['buttons'] = array();
  $form['buttons']['#weight'] = 200;
  $form['buttons']['remove'] = array(
    '#value' => t('Remove Club From Organisation'),
    '#type' => 'submit',
  );

  return $form;
}

/**
*  Remove club button pressed.
*/
function league_club_org_settings_form_submit($form, &$form_state) {
  $org = $form_state['league_org'];
  $club = $form_state['league_club'];
  drupal_goto("league/dclub/$club/$org");
}
