// Javascript for creating a team
//
function setTeamName() {
// Function to set a default team name
// Use JQuery to get the club selector object.
  selector = $("#edit-field-club-nid-nid").get(0);
// Get the currently selected option
  option = selector.options[selector.selectedIndex];
// Use JQuery to find the text input field and set its value
  $("#edit-title").get(0).value = option.text + " 1";

}
if (Drupal.jsEnabled) {
  // when the document is loaded ..
  $(document).ready(function() {
    // set the default team name from the club
    setTeamName();
    // set an event handler so it changes if the club changes
    $("#edit-field-club-nid-nid").get(0).onchange = function() { setTeamName();}
  })
}
