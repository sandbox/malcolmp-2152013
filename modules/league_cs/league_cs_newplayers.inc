<?php
/**
 * @file
 * Team Captains.
 */

/**
 * Team Captains.
 */
function league_cs_reports_tab_newplayers($node,$option='list',$pid=0,$ecf=' ') {
  // Set breadcrumb
  league_set_bread_crumb ($node);
  // Set title
  $title = $node->title . ' - New Players';
  drupal_set_title($title);

  switch($option) {
    case 'list' : $content = league_cs_newplayers_list($node->nid);
                  break;
    case 'search':$content = league_cs_newplayers_search($node->nid, $pid);
                  break;
    case 'confirm':$content = league_cs_newplayers_confirm($node->nid, $pid, $ecf);
                  break;
    default :  $content = "<h3>Invalid option</h3>";
  }

  return $content;
}

function league_cs_newplayers_list($org) {
  $content['title']['#markup'] = "<h3>These players have no PNUM code specified, or the PNUM they have is not valid (possibly it has changed). Pick to search for them in the Chess Scotland list</h3>";
  $content['title']['#type'] = 'markup';
  $content['newplayers']['#table'] = league_cs_no_grading_code($org);
  $content['newplayers']['#theme'] = 'league_table';
  return $content;
}

function league_cs_newplayers_search($org, $pid) {
  // Get players name.
  $player = league_player_load($pid);
  $search = $player->lastname . ', ' . substr($player->firstname,0,1);
  $details = $player->firstname . ' ' . $player->lastname;

  // Get list of matching Chess Scotland players
  $players = csdata_get_players($player->lastname, substr($player->firstname,0,1));

  // Display as a table with links
  $header = array('Name', 'Grade', 'Sex', 'PNUM', 'Club' );
  $rows = array();
  foreach($players as $eplayer) {
    $ecf = $eplayer['pid'];
    if(isset($eplayer['grade']) ) {
      $grade = $eplayer['grade'];
    } else {
      $grade = ' ';
    }
    $sex =   $eplayer['sex'];
    $club =   $eplayer['club'];
    $name = l($eplayer['name'],"node/$org/league/newplayers/confirm/$pid/$ecf");
    $rows[] = array( $name, $grade, $sex, $ecf, $club );
  }

  $content['title']['#markup'] = "<h3>Pick which of the below players in the Chess Scotland list is: $details </h3>";
  $content['title']['#type'] = 'markup';
  $table['data'] = $rows;
  $table['header'] = $header;
  $content['newplayers']['#table'] = $table;
  $content['newplayers']['#theme'] = 'league_table';
  return $content;
}

function league_cs_newplayers_confirm($org, $pid, $ecf) {
  // Check access
  if (!league_check_access($org)) {
    drupal_set_message('You do not have access to do this','warning');
    return('Access Denied');
  }
  // Get confirmation form
  $content['form'] = drupal_get_form('league_cs_newplayers_confirm_form', $pid, $ecf,$org);
  return $content;
}

function league_cs_newplayers_replace_table($league_player,$ecf_player,$new_pid=0) {
  
  // Get player name
  $league_name = l($league_player->lastname . ', ' . $league_player->firstname, "league_player/$league_player->pid");
  $ecf_name = $ecf_player['lastname'] . ', ' . $ecf_player['firstname'];
  if($new_pid>0) {
    $new_player = league_player_load($new_pid);
    $new_name = l($new_player->lastname . ', ' . $new_player->firstname,"league_player/$new_pid");
  }

  // Table headings
  $content = '<table>';
  $content .= '<tr><th>Details</th><th>New Player</th><th>Chess Scotland Player</th>';
  if($new_pid>0) {
    $content .= '<th>Existing Player</th></tr>';
  } else {
    $content .= '</tr>';
  }

  // Player Name
  $content .= "<tr><th>Name</th><td>$league_name</td><td>$ecf_name</td>";
  if($new_pid>0) {
    $content .= "<td>$new_name</td></tr>";
  } else {
    $content .= '</tr>';
  }

  // Player Sex
  $content .= '<tr><th>Sex</th><td>' . $league_player->sex;
  $content .= '</td><td>' . $ecf_player['sex'] . '</td>';
  if($new_pid>0) {
    $content .= '<td>' . $new_player->sex . '</td></tr>';
  } else {
    $content .= '</tr>';
  }

  // Player Dob
  $content .= '<tr><th>DOB</th><td>' . league_player_get_dob($league_player->dob);
  $content .= '</td><td>' . league_player_get_dob($ecf_player['dob']) . '</td>';
  if($new_pid>0) {
    $content .= '<td>' . league_player_get_dob($new_player->dob) . '</td></tr>';
  } else {
    $content .= '</tr>';
  }

  // Player Club
  if($league_player->club > 0) {
    $league_club = league_club_value($league_player->club, 'title');
  } else {
    $league_club = ' ';
  }
  $content .= '<tr><th>Club</th><td>' . $league_club;
  $content .= '</td><td>' . $ecf_player['club'] . '</td>';
  if($new_pid>0) {
    if($new_player->club > 0) {
      $new_club = league_club_value($new_player->club,'title');
    } else {
      $new_club = ' ';
    }
    $content .= '<td>' . $new_club . '</td></tr>';
  } else {
    $content .= '</tr>';
  }

  // Player PNUM
  $league_code = ' ';
  if(isset($league_player->ecf_code)) {
    $league_code = $league_player->ecf_code;
  }
  $content .= '<tr><th>PNUM</th><td>' . $league_code;
  $content .= '</td><td>' . $ecf_player['ecfCode'] . '</td>';
  if($new_pid>0) {
    $new_code = ' ';
    if(isset($new_player->ecf_code)) {
      $new_code = $new_player->ecf_code;
    }
    $content .= '<td>' . $new_code . '</td></tr>';
  } else {
    $content .= '</tr>';
  }

  // Table footer
  $content .= '</table>';

  return $content;
}
/**
 * Get the results of the player given by the pid.
 */
function league_cs_newplayers_results($pid) {
  $whites = league_cs_newplayers_results_colour($pid,'white','black');
  $blacks = league_cs_newplayers_results_colour($pid,'black','white');
  $results = array_merge($whites, $blacks);
  $header = array('Match', 'Colour', 'Opponent', 'Result', 'RID');
  $table['data'] = $results;
  $table['header'] = $header;

  return $table;
}

/**
 * Get the results of the player given by the pid for the given colour
 */
function league_cs_newplayers_results_colour($pid,$colour,$opponent) {
  $query = db_select('league_chess_result', 'r');
  $query->condition("r.$colour", $pid);
  $query->innerJoin('league_match', 'm', 'm.mid = r.mid');
  $query->innerJoin('node', 'n', 'n.nid = m.fid');
  $query->addField('r', 'result', 'result');
  $query->addField('r', 'rid', 'rid');
  $query->addField('n', 'title', 'title');
  $query->addField('n', 'nid', 'nid');
  $query->innerJoin('league_player', 'p', "p.pid = r.$opponent");
  $query->addField('p', 'firstname', 'firstname');
  $query->addField('p', 'lastname', 'lastname');

  $rows = array();
  //dsm((string)$query);
  $queryResult = $query->execute();
  foreach ( $queryResult as $row) {
    $opponent = $row->lastname . ', ' . $row->firstname;
    $result = $row->result;
    $nid = $row->nid;
    $rid = $row->rid;
    $title = l($row->title, "node/$nid");

    $rows[] = array($title, $colour, $opponent, $result, $rid);
  }
  return $rows;
}

/**
 * Get players with a matching PNUM code in the org.
 */
function league_cs_get_by_ecfcode($ecfcode, $org) {
  $query = db_select('league_player', 'p');
  $query->condition('p.org', $org);
  $query->condition('p.ecf_code', $ecfcode);
  $query->addField('p', 'pid', 'pid');
  //dsm((string)$query);
  $queryResult = $query->execute();

  $pids = array();
  foreach ( $queryResult as $row ) {
    $pids[] = $row->pid;
  }
  return $pids;
}

/**
 * List players in the org who don't have a valid grading code.
 */

function league_cs_no_grading_code($org) {

  $rows = array();
  $header = array('Name', 'PNUM', 'Player List');
  $lorg = csdata_get_player_org();

  // Players in the org
  $query = db_select('league_player', 'p');
  $query->fields('p', array('pid', 'firstname', 'lastname', 'ecf_code'));
  $query->condition('p.org', $org);
  // Player list they are in
  $query->leftJoin('league_player_player', 'lpp', "lpp.pid = p.pid");
  // Player list they are in
  $query->leftJoin('league_player_list', 'l', "l.lid = lpp.lid");
  $query->addField('l', 'name', 'list');
  // Check the grading list
  $query->leftJoin('player_list_player', 'm', "m.org=$lorg AND m.pid = p.ecf_code");
  $query->where('m.pid is null');
  $query->orderBy('firstname', 'ASC');
  $query->orderBy('lastname', 'ASC');

  //dsm((string)$query);
  $queryResult = $query->execute();

  foreach ( $queryResult as $row ) {
    $pid = $row->pid;
    $name = l("$row->firstname $row->lastname","node/$org/league/newplayers/search/$pid");
    $ecfcode = $row->ecf_code;
    $list = $row->list;
    $rows[] = array($name, $ecfcode, $list);
  }

  $table['data'] = $rows;
  $table['header'] = $header;

  return $table;
}

/**
*  Form for confirming identity of player 
*/
function league_cs_newplayers_confirm_form($form, &$form_state,$pid,$ecf,$org) {
  // Get Local Player details
  $league_player = league_player_load($pid);
  $league_name = $league_player->lastname . ', ' . $league_player->firstname;
  // Get ecf player details
  $ecf_player = csdata_get_player($ecf);
  $ecf_name = $ecf_player['lastname'] . ', ' . $ecf_player['firstname'];
  // Set up heading
  $form['heading']['#type'] = 'markup';
  $form['heading']['#markup'] = "<h3>Confirm replacement of $league_name ($pid) $ecf_name( $ecf ) </h3>";

  $form_state['league_ecf_pid'] = $pid;
  $form_state['league_ecf_code'] = $ecf;
  $form_state['league_ecf_org'] = $org;

  $form['buttons'] = array();
  $form['buttons']['#weight'] = 100;
  $form['buttons']['submit'] = array(
     '#value' => t('Replace'),
     '#type'  => 'submit',
     '#weight' => 5,
  );
  $form['buttons']['cancel'] = array(
     '#value' => t('Cancel'),
     '#type'  => 'submit',
     '#submit'  => array('league_cs_newplayers_cancel'),
     '#weight' => 15,
  );
  // Display table to say what will happen:
  $existing = league_cs_get_by_ecfcode($ecf, $org);
  // If there is an existing player in the org with this PNUM
  if(count($existing) > 0) {
  //   if there are some results, change them to be the existing pid
    $text = "The player on the left will be removed and all results assigned to the player on the right:";
    $text .= league_cs_newplayers_replace_table($league_player,$ecf_player,$existing[0]);
    $form['explain']['#type'] = 'markup';
    $form['explain']['#markup'] = $text;
    $form['results']['#table'] = league_cs_newplayers_results($pid);
    $form['results']['#theme'] = 'league_table';

    $form_state['league_ecf_newpid'] = $existing[0];
  } else {
  // If there is no existing player, just set the ecf code
    $text = "The player on the left will be assigned the PNUM of the player on the right.";
    $text .= league_cs_newplayers_replace_table($league_player,$ecf_player);
    $form['explain']['#type'] = 'markup';
    $form['explain']['#markup'] = $text;

    $form_state['league_ecf_newpid'] = 0;
  }

  return $form;
}

/**
*  Action after the form is submitted.
*/
function league_cs_newplayers_confirm_form_submit($form, &$form_state) {
  $pid = $form_state['league_ecf_pid'];
  $ecf = $form_state['league_ecf_code'];
  $org = $form_state['league_ecf_org'];
  $new_pid =  $form_state['league_ecf_newpid'];

  if (!league_check_access($org)) {
    drupal_set_message('You do not have access to do this','warning');
    $form_state['redirect'] = "node/$org/league/newplayers";
  } else {

    // If there is an existing player in the org with this PNUM
    if($new_pid > 0) {
      $table = $form['results']['#table'];
      $rows = $table['data'];
      // If there are some results, change them to be the existing pid
      foreach($rows as $row) {
        $colour = $row[1];
        $rid = $row[4];
        league_cs_result_change_player($rid, $new_pid, $colour);
      }
      // Delete this player
      drupal_set_message("Delete player $pid");
      league_player_delete($pid);
    } else {
      // If there is no existing player, just set the ecf code
      drupal_set_message("Set player $pid PNUM to $ecf");
      $player = league_player_load($pid);
      $player->ecf_code = $ecf;
      league_player_save($player);
    }

    // Clear players with no grade cache
    $cache_name = "league_chess_count_no_grade_players_$org";
    cache_clear_all($cache_name, 'cache');

    $form_state['redirect'] = "node/$org/league/newplayers";
  }
}

function league_cs_result_change_player($rid, $pid, $colour) {
  drupal_set_message("Replacement $colour in result $rid with $pid");
  db_update('league_chess_result')
    ->fields(array($colour => $pid))
    ->condition('rid', $rid, '=')
    ->execute();
}

/**
*  Action after cancel button pressed.
*/
function league_cs_newplayers_cancel($form, &$form_state) {
  drupal_set_message("Replacement Cancelled");
  $org = $form_state['league_ecf_org'];

  $form_state['redirect'] = "node/$org/league/newplayers";
}
