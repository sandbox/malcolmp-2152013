<?php
// $Id$

// Event admin tab
function league_cs_grade_refresh_tab($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);

  $org = $node->nid;
  $content = array();

  // Form for specifying grade refresh.
  $content['form'] = drupal_get_form('league_cs_grade_refresh_form', $node);

  return $content;
}

/**
 *  Form for admin tab.
 */
function league_cs_grade_refresh_form($form, &$form_state, $node) {

  $form_state['league_node'] = $node;

  $form['grades'] = array(
    '#title' => t('Zero Local Grades'),
    '#type'  => 'fieldset',
    '#weight'  => 100,
  );

  $grade_text = league_cs_last_refresh_text($node);

  $form['grades']['text'] = array(
   '#type'  => 'markup',
   '#markup'  => $grade_text,
  );

  $form['grades']['refresh'] = array(
    '#value' => t('Remove Local Grades'),
    '#type'  => 'submit',
    '#submit' => array('league_cs_event_grades_form_submit'),
  );

  $typeOptions = array(
   'local' => t('Players with local grades who now have a Chess Scotland grade'),
  );

  $form['grades']['types'] = array(
   '#title'  => t('Grades to update'),
   '#type'  => 'radios',
   '#default_value'  => 'local',
   '#options'  => $typeOptions,
  );

  $status_text = league_cs_status_text($node);
  $form['status'] = array(
   '#type'  => 'markup',
   '#markup'  => $status_text,
   '#weight'  => 300,
  );

  return $form;
}

function league_cs_event_grades_form_submit($form, &$form_state) {
  $node = $form_state['league_node'];
  $type = $form_state['values']['types'];
  $org = $node->nid;
  if (!league_check_access($org)) {
    drupal_set_message('You do not have access to do this','warning');
    return;
  }

  league_cs_refresh_org_grades_local($org, $type);
  drupal_set_message("Local grades refreshed for $node->title ");

  // Clear field cache to changes are seen when view/edit league_player
  cache_clear_all("*", 'cache_field', TRUE);
}

function league_cs_refresh_org_grades_local($org, $type) {
  $lorg = csdata_get_player_org();
  $rlid = csdata_get_rating_list_id('standard');
  if($type == 'local') {
    $cat = "rpa.cat!='*'";
  } else {
    $cat = "rpa.cat NOT IN ('*','F')";
  }
  // Blank out local grades (not CAT *) 
  $result = db_query("update league_player p join player_list_player m on m.pid=p.ecf_code and p.org=$org AND p.chess_rating>0 join rating_player rpa ON rpa.rlid=$rlid and $cat and rpa.rating!=0 set p.chess_rating=0", array() );
  $rows = $result->rowCount();
  drupal_set_message("$rows grades updated");
  league_cs_set_last_refresh($org);
}

// Set local grades to published grade if different to grade
function league_cs_set_local_grades_pub($org, $type='local') {
  $lorg = csdata_get_player_org();
  if($type == 'local') {
    $cat = "rpa.cat!='*'";
  } else {
    $cat = "rpa.cat NOT IN ('*','F')";
  }
  // Standard grades.
  $rlid = csdata_get_rating_list_id('standard');
  $result = db_query("update league_player p join rating_player rpa ON rpa.rlid=$rlid and rpa.pid=p.ecf_code and $cat set p.chess_rating=rpa.rating WHERE p.org=$org", array() );
  $rows_std = $result->rowCount();
  // Rapid grades.
  $rlid = csdata_get_rating_list_id('allegro');
  $result = db_query("update league_player p join rating_player rpa ON rpa.rlid=$rlid and rpa.pid=p.ecf_code and $cat set p.chess_rapid=rpa.rating WHERE p.org=$org", array() );
  $rows_rpd = $result->rowCount();
  drupal_set_message("$rows_std standard grades updated. $rows_rpd rapid grades updated.");
}
