<?php
/**
 * @file
 * Views files.
 */

/*******************************************************************************
 * Hook Functions (Drupal)
 ******************************************************************************/

/**
 * Implements hook_views_data().
 */
function league_event_views_data()
{

  $data = array();

  $data['league_event']['table']['group'] = t('League Event');

  $data['league_event']['table']['base'] = array(
        'title' => t('League Event'),
        'help' => t('Contains records we want exposed to Views.'),
  );

  // The Event ID field
  $data['league_event']['eid'] = array(
    'title' => t('League Event ID'),
    'help' => t('The league event ID.'),
    'field' => array(
        'handler' => 'views_handler_field_node',
    ),
    'sort' => array(
        'handler' => 'views_handler_sort',
    ),
    'filter' => array(
        'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
        'handler' => 'views_handler_argument_node_nid',
        'numeric' => TRUE,
        'validate type' => 'nid',
    ),
  );

  // The Status field
  $data['league_event']['lms'] = array(
      'title' => t('Status'),
      'help' => t('Events status within org.'),
      'field' => array(
        'handler' => 'views_handler_field',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
  );

  // The Name field
  $data['league_event']['name'] = array(
      'title' => t('Event Name'),
      'help' => t('Events name.'),
      'field' => array(
        'handler' => 'views_handler_field',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
  );

  // The order field
  $data['league_event']['order'] = array(
      'title' => t('display Order'),
      'help' => t('The order events are listed.'),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
  );

  $data['league_event']['table']['join'] = array(
    'node' => array(
        'left_field' => 'nid',
        'field' => 'org',
    ),
  );

  // The org field
  $data['league_event']['org'] = array(
      'title' => t('Organisation ID'),
      'help' => t('The events org id.'),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
    'relationship' => array(
        'base' => 'node',
        'field' => 'eid',
        'handler' => 'views_handler_relationship',
        'label' => t('Node'),
    ),
    'argument' => array(
        'handler' => 'views_handler_argument_node_nid',
        'numeric' => TRUE,
        'validate type' => 'nid',
    ),
  );

  return $data;
}
