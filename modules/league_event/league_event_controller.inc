<?php
/**
 * @file
 * League Event controller
 */

class LeagueEventController extends DrupalDefaultEntityController {
  public function create($org) {
    return (object) array(
      'eid' => '',
      'org' => $org,
      'name' => '',
      'lms' => 'I',
      'display_order' => 0,
    );
  }

  public function save($event) {
    $transaction = db_transaction();

    try {
      $event->is_new = empty($event->eid);
      if($event->is_new) {
        drupal_write_record('league_event', $event);
        $op = 'insert';
      } else {
        drupal_write_record('league_event', $event, 'eid');
        $op = 'update';
      }
      // Save fields
      $function = 'field_attach_' . $op;
      $function('league_event', $event);

      module_invoke_all('entity_' . $op, $event, 'league_event');
      unset($event->is_new);

      db_ignore_slave();

      return $event;
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('league_event', $e, NULL, WATCHDOG_ERROR);
      return FALSE;
    }
  }

  public function delete($eids) {
    if(!empty($eids)) {
      $events = $this->load($eids, array());
      $transaction = db_transaction();

      try {
        db_delete('league_event')
          ->condition('eid', $eids, 'IN')
          ->execute();

        foreach($events as $eid => $event) {
          field_attach_delete('league_event', $event);
        }

        db_ignore_slave();
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('league_event', $e, NULL, WATCHDOG_ERROR);
        return FALSE;
      }

      module_invoke_all('entity_delete', $event, 'league_event');

      cache_clear_all();
      $this->resetCache();
    }
    
    return TRUE;
  }
}

