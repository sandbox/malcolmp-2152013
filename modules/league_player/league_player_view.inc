<?php
/**
* @file
* Functions for league player options.
*/

function league_player_view_page($player, $view_mode = 'full') {
  // Set breadcrumb
  league_player_set_bread_crumb($player);

  $pid = $player->pid;
  $player->content = array();

  // Build fields content
  field_attach_prepare_view('league_player',
                            array($player->pid => $player),
                             $view_mode);

  $rows = array();
  $rows[] = array('Player ID', $player->pid);
  $rows[] = array('First Name', $player->firstname);
  $rows[] = array('Last Name', $player->lastname);
  $rows[] = array('Sex', $player->sex);
  $rows[] = array('User ID', $player->uid);
//$club = league_player_view_format_club($player);
//$rows[] = array('Club (used for individual events)', $club);
  $dob = league_player_view_format_dob($player);
  $rows[] = array('DOB', $dob);
  $lists = league_player_lists_in($player->pid);
  if(count($lists) == 0) {
    $rows[] = array('Player lists', 'None');
  } else {
    $rows[] = array('Player lists', implode(',',$lists));
  }

  // Let other modules add rows
  $other = module_invoke_all('league_player_view_fields', $player);
  $rows = array_merge($rows, $other);
 
  $player->content['table'] = array(
    '#theme' => 'table',
    '#rows'  => $rows,
  );

  $player->content += field_attach_view('league_player', $player, $view_mode);
 
  if($player->uid > 0) {
    $user = user_load($player->uid);
    $picture = array(
       '#account' => $user,
       '#theme' => 'user_picture',
    );
    $div = array();
    $div['header']['#type'] = 'markup';
    $div['header']['#markup'] = '<div id="player-picture">';
    $div['picture'] = $picture;
    $div['footer']['#type'] = 'markup';
    $div['footer']['#markup'] = '</div>';
    $player->content['picture'] = $div;
  }

  return $player->content;
}

function league_player_view_format_dob($player) {
  // Date of Birth only visible to logged in users
  if (league_check_access($player->org) ) {
    if(is_null($player->dob)) {
      $dob = 'unknown';
    } else {
      $dob = date('Y-m-d',$player->dob);
    }
  } else {
    $dob = '****';
  }
  return $dob;
}

function league_player_view_format_club($player) {
  $club = 'Not Set';
  if(isset($player->club) && $player->club != 0) {
    $clubNode = node_load($player->club);
    if(is_object($clubNode)) {
      $club = $clubNode->title;
    }
  }
  return $club;
}
