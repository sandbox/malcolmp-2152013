<?php
// $Id$
/**
 * @file
 * Administration page callbacks for chessdb module
 */

function league_player_admin_settings($form, &$form_state) {

  // Checkbox options

  $settings = array(
    'ownlist' => t('Allow clubs to update their own player lists'),
  );
  // Get current settings
  $default = league_player_get_settings();
  $form['flags'] = array(
    '#title' => t('Settings'),
    '#type' => 'checkboxes',
    '#options' => $settings,
    '#default_value' => $default,
  );

  // Submit Button
  $form['submit'] = array(
    '#value' => t('Save Settings'),
    '#type' => 'submit',
  );

  return $form;
}

/**
 *  Action after the admin settings form is submitted,
 *  with the submit button
 */

function league_player_admin_settings_submit($form, &$form_state) {

  $settings = $form_state['values']['flags'];
  variable_set('league_player_settings', $settings);
  drupal_set_message("League Player Settings Saved");
}
