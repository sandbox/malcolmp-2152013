<?php
/**
 * @file
 * League Player controller
 */

class LeaguePlayerController extends DrupalDefaultEntityController {
  public function create($org) {
    return (object) array(
      'pid' => '',
      'org' => $org,
      'dob' => null,
      'sex' => '',
      'lastname' => '',
      'firstname' => '',
      'club' => 0,
      'uid' => 0,
    );
  }

  public function save($player) {
    $transaction = db_transaction();

    try {
      $player->is_new = empty($player->pid);
      if($player->is_new) {
        drupal_write_record('league_player', $player);
        $op = 'insert';
      } else {
        drupal_write_record('league_player', $player, 'pid');
        $op = 'update';
      }
      // Save fields
      $function = 'field_attach_' . $op;
      $function('league_player', $player);

      module_invoke_all('entity_' . $op, $player, 'league_player');
      unset($player->is_new);

      db_ignore_slave();

      return $player;
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('league_player', $e, NULL, WATCHDOG_ERROR);
      return FALSE;
    }
  }

  public function delete($pids) {
    if(!empty($pids)) {
      $players = $this->load($pids, array());
      $transaction = db_transaction();

      try {
        db_delete('league_player')
          ->condition('pid', $pids, 'IN')
          ->execute();

        foreach($players as $pid => $player) {
          field_attach_delete('league_player', $player);
        }

        db_ignore_slave();
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('league_player', $e, NULL, WATCHDOG_ERROR);
        return FALSE;
      }

      module_invoke_all('entity_delete', $player, 'league_player');

      cache_clear_all();
      $this->resetCache();
    }
    
    return TRUE;
  }
}

