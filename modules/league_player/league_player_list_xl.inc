<?php
/**
* @file
* Functions for getting league player list in xl
*/

/**
*  $list   - list object
*
*/
function league_player_list_xl($list) {
  $_GET['format'] = 'xl';
  $path = drupal_get_path('module', 'league');
  require_once("$path/league_xl_table.inc");
  $table = league_player_get_org_players($list->lid, 'list');
  league_xl_table($table);
}
