<?php
// $Id$
/**
 * @file
 * Administration page callbacks for chessdb module
 */

function league_player_page() {

  $item = menu_get_item();
  $links = system_admin_menu_block($item);

  $items = array();
  foreach($links as $link) {
    $items[] = l($link['title'], $link['href'], $item['localized_options']).
              ': ' . filter_xss_admin($link['description']);
  }

  return theme('item_list', array('items' => $items));
}

