<?php
/**
* @file
* Functions for user lookup.
*/

function league_player_lookup_user($js=NULL, $pid=0) {
  if(isset($_POST['lastname']) && isset($_POST['firstname']) ) {
    $lastname = $_POST['lastname'];
    $firstname = $_POST['firstname'];
    watchdog('league_player', "league_player_lookup_user $pid $js $lastname");
    $name = $lastname;
  } else {
    $name = ' ';
  }
    if ($js) {
      ctools_include('ajax');
      ctools_include('modal');

    $form_state = array(
      'ajax' => TRUE,
      'title' => t('Pick player from the user list'),
      'league_player_name' => $name,
      'league_player_pid' => $pid,
    );

    // Use ctools to generate ajax instructions for the browser to create
    // a form in a modal popup.
    $output = ctools_modal_form_wrapper('league_player_user_lookup_form', $form_state);

    // If the form has been submitted, there may be additional instructions
    // such as dismissing the modal popup.
    if (!empty($form_state['ajax_commands'])) {
      $output = $form_state['ajax_commands'];
    }

    // Return the ajax instructions to the browser via ajax_render().
    print ajax_render($output);
    drupal_exit();
  }
  else {
    return drupal_get_form('league_player_user_lookup_form', $name);
  }
}


/**
*  Form for user lookup.
*/
function league_player_user_lookup_form($form, &$form_state ) {

  $name = $form_state['league_player_name'];
  $pid = $form_state['league_player_pid'];
  if (strlen($name) < 2) {
    $debug = ' ';
//  $debug = print_r($_POST, true);
    $link_table = "<h2>ERROR: You must specify name to lookup user </h2>$debug";
  } else {
    $link_table = league_player_user_select_list($pid,$name);
  }

  $form['users'] = array(
     '#markup' => "<div id=user-select-table>$link_table</div>",
     '#type'  => 'markup',
  );

  $form['cancel'] = array(
     '#value' => t('Cancel'),
     '#type'  => 'submit',
     '#weight' => 5,
     '#submit'  => array('league_player_user_lookup_form_cancel'),
  );

  return $form;
}

function league_player_user_lookup_form_cancel($form, &$form_state) {
  $form_state['ajax_commands'][] = ctools_modal_command_dismiss();
}

function league_player_user_lookup_form_submit($form, &$form_state) {
  // Tell the browser to close the modal.
  $pid = $form_state['league_player_pid'];
  $player = league_player_load($pid);
  $player->uid=0;
  league_player_save($player);
  $form_state['ajax_commands'][] = ctools_modal_command_dismiss();
  $form_state['ajax_commands'][] = array
  (
    // This command is defined in league_player.js
    // It puts the User selected into the original form
    'command' => 'insertPlayerUser',
    'uid' => 0,
    'uname' => 'none',
    'name' => 'Not Set',
  );
}

function league_player_user_select_list($pid,$name=NULL) {
  // Get list of users matching lastname.
  $rows = array();
  $query = db_select('field_data_field_name', 'n');
  if($name != NULL ) {
    $query->condition('n.field_name_value', '%'.$name, 'LIKE');
  }
  $query->addField('n', 'field_name_value', 'name');
  $query->innerJoin('users', 'u', 'u.uid = n.entity_id');
  $query->addField('u', 'uid', 'uid');
  $query->addField('u', 'name', 'uname');

  //dsm((string)$query);
  $queryResult = $query->execute();

  foreach ( $queryResult as $row ) {
    $uid = $row->uid;
    $uname = $row->uname;
    $name = l($row->name,
              "league_player/ajax/lookup/$pid/$uid",
              array('attributes' => array('class' => array('use-ajax'))) 
            );
//  $name = l($row->name,"league_player/nojs/lookup/$pid/$uid");
//  drupal_set_message("league_player_user_select_list $uname $uid $name ");
    $rows[] = array( $name, $uname, $uid);
  }

  if(count($rows) > 0 ) {
    $output = "<p>Select the user to associate with player from list below. Pressing Cancel will unset the user.<p>";
    // Create table
    $header = array('Name', 'User Name', 'User ID');
    // Players into table
    $output .= theme('table', array('header' =>$header,
                                    'rows'   => $rows));
  } else {
    $output = "<p>No users with name $name. Press <b>Cancel</b> to return to player form";
  }
  // Render the page contents.
  return $output;
}
