<?php
/**
 * @file
 * Functions for theming the display of fixture generation checks
 */

/**
 * Theme fixture generation checks.
 */
function theme_league_generate_check($variables) {

  $element = $variables['element'];
  $fdata = $element['#data'];

  $html = "<h2>Fixture Checking </h2>";
  foreach($fdata as $set => $fixtures) {
    $html .= "<h3>$set</h3>";
    if(count($fixtures) >0 ) {
      $html .= '<ul>';
      foreach($fixtures as $fixture) {
        $event_name = league_event_value($fixture['event'], 'title');
        $home_team = league_team_value($fixture['home'], 'title');
        $away_team = league_team_value($fixture['away'], 'title');
        $date = date('Y-m-d', $fixture['date']);
        $html .= '<li>';
        $html .= "$event_name : $home_team v $away_team $date";
      }
      $html .= '</ul>';
    }
  }

  return $html;
}

