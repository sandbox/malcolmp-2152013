<?php
// $Id$
/**
 * @file
 * Functions for theme dates
 */

function theme_league_generate_dates_header($variables) {
  $element = $variables['element'];
  $node = $element['#node'];
  $home = ' ';
  if($node->type == 'club') {
    $home = 'home';
  }

  $output = '<div class=league-frame>';
  $output .= '<h2>'. $node->title. " - dates with no $home fixtures</h2>";
  $output .= "<table border>";
  $output .= "<tr><th>Description</th><th>Start date</th><th>End Date</th><th>Delete</th></tr>\n";

  return $output;
}

function theme_league_generate_dates_footer($variables) {
  $element = $variables['element'];
  $node = $element['#node'];

  $output = '</table></div>';

  return $output;
}
