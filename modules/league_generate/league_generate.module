<?php
/**
 * @file
 * League Generate Module.
 */

/*******************************************************************************
 * Hook Functions (Drupal)
 ******************************************************************************/
/**
 * Implements hook_theme().
 */
function league_generate_theme() {
  $items = array(
    'league_generate_dates_header' => array(
      'render element' => 'element',
      'file' => 'league_generate_theme_dates.inc',
    ),
    'league_generate_dates_footer' => array(
      'render element' => 'element',
      'file' => 'league_generate_theme_dates.inc',
    ),
    'league_generate_teams_header' => array(
      'render element' => 'element',
      'file' => 'league_generate_theme_teams.inc',
    ),
    'league_generate_teams_footer' => array(
      'render element' => 'element',
      'file' => 'league_generate_theme_teams.inc',
    ),
     'league_generate_constraints' => array(
      'render element' => 'element',
      'file' => 'league_generate_constraint_theme.inc',
    ),
     'league_generate_check' => array(
      'render element' => 'element',
      'file' => 'league_generate_check_theme.inc',
    ),

  );
  return $items;
}


/**
 * Implements hook_node_delete().
 */

function league_generate_node_delete($node) {
  // Delete dates for an org, club, team
  if($node->type == 'org' || $node->type == 'club' || $node->type == 'team') {
    $query = db_delete('league_generate_dates');
    $query->condition('entity_id', $node->nid);
    $query->execute();
  }
}

/**
 * Implements hook_menu().
 */
function league_generate_menu() {
  $items = array();

  // team node tab - fixture generation dates
  $items['league_team/%node/dates'] = array(
    'title' => 'Avoid Dates',
    'page callback' => 'league_generate_dates_tab',
    'page arguments' => array(1),
    'access callback' => 'league_view_tab_permission',
    'access arguments' => array(1,'team'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'league_generate_dates.inc',
    'weight' => 50,
  );

  // team node tab - teams to avoid clashes with
  $items['league_team/%node/teams'] = array(
    'title' => 'Avoid Teams',
    'page callback' => 'league_generate_teams_tab',
    'page arguments' => array(1),
    'access callback' => 'league_view_tab_permission',
    'access arguments' => array(1,'team'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'league_generate_teams.inc',
    'weight' => 50,
  );

  // Club within org - Generate tab.
  $items['league/club/%node/%/club/generate'] = array(
    'type' => MENU_LOCAL_TASK,
    'title' => 'Fixture Generation',
    'page callback' => 'league_generate_dates_tab',
    'page arguments' => array(2),
    'access arguments' => array('league view'),
    'file' => 'league_generate_dates.inc',
    'weight' => 30,
  );


  // admin tab - generate fixtures
  $items['node/%node/league/fixtures/generate'] = array(
    'title' => 'Generate',
    'page callback' => 'league_admin_tab_generate',
    'page arguments' => array(1),
    'access callback' => 'league_view_tab_permission',
    'access arguments' => array(1,'organisation'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'league_generate.inc',
    'weight' => 12,
  );

  // admin tab - generate fixtures - new
  $items['node/%node/league/fixtures/gennew'] = array(
    'title' => 'Generate New',
    'page callback' => 'league_admin_tab_gennew',
    'page arguments' => array(1),
    'access callback' => 'league_view_tab_permission',
    'access arguments' => array(1,'organisation'),
//  'access arguments' => array('league administrator'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'league_generate.inc',
    'weight' => 12,
  );

  // admin tab - fixture generation dates
  $items['node/%node/league/fixtures/gendates'] = array(
    'title' => 'Dates',
    'page callback' => 'league_generate_dates_tab',
    'page arguments' => array(1),
    'access callback' => 'league_view_tab_permission',
    'access arguments' => array(1,'organisation'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'league_generate_dates.inc',
    'weight' => 15,
  );

  // admin tab - fixture generation constraints
  $items['node/%node/league/fixtures/constraints'] = array(
    'title' => 'Check',
    'page callback' => 'league_admin_tab_fixture_check',
    'page arguments' => array(1),
    'access callback' => 'league_view_tab_permission',
    'access arguments' => array(1,'organisation'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'league_generate.inc',
    'weight' => 18,
  );

  // tab on event node to generate fixtures
  $items['league_comp/%node/gfixtures'] = array(
    'title' => 'Generate Fixtures',
    'page callback' => 'league_event_tab_generate',
    'page arguments' => array(1),
    'access callback' => 'league_event_edit_tab_permission',
    'access arguments' => array(1,array('L')),
    'type' => MENU_LOCAL_TASK,
    'weight' => 30,
    'file' => 'league_generate.inc',
  );

  return $items;
} // function league_generate_menu()

/**
 * Get the dates for which no fixtures will be generated.
 */
function league_generate_get_dates($node) {
//drupal_set_message(" league_generate_get_dates nid: $node->nid");
  $results = array();
  $query = db_select('league_generate_dates', 'd');
  $query->fields('d', array('did', 'start', 'end', 'description' ));
  $query->condition('d.entity_id', $node->nid);
  $query->orderBy('d.did', 'ASC');
  $result = $query->execute();
//dsm((string)$query);
  $date=0;
  foreach ( $result as $row) {
    $results[$date]['did'] = $row->did;
    if(isset($row->start) ) {
      $results[$date]['start'] = date('Y-m-d', $row->start);
    } else {
      $results[$date]['start'] = ' ';
    }
    if(isset($row->end) ) {
      $results[$date]['end'] = date('Y-m-d', $row->end);
    } else {
      $results[$date]['end'] = ' ';
    }
    $results[$date]['description'] = $row->description;
    $date++;
  }
  return $results;
}

