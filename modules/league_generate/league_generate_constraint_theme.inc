<?php
/**
 * @file
 * Functions for theming the display of fixture generation constraints
 */

/**
 * Theme fixture generation constraints.
 */
function theme_league_generate_constraints($variables) {

  $element = $variables['element'];
  $fdata = $element['#data'];
  $node = $fdata['node'];
  $sid = $fdata['sid'];
  $sdate = $fdata['sdate'];
  $edate = $fdata['edate'];
  $data = $fdata['data'];

  $html = "<h2>Fixture Generation Constraints</h2>";
  $event_count = count($data->events);
  if($event_count == 0) {
    $html .= "No events";
  } else {
    $html .= "Events:";
    foreach($data->events as $eid => $event) {
      $html .= $event['name'] . ',';
    }
    $org_dates = $data->org_dates;
    if(count($org_dates) > 0) {
      $html .= '<p>Organisation dates:<ul>';
      foreach($org_dates as $org_date) {
        $html .= '<li>';
        $html .= $org_date['description'];
        $html .= ' from ';
        $html .= $org_date['start'];
        $html .= ' to ';
        $html .= $org_date['end'];
      }
      $html .= '</ul>';
    }
    $html .= "<p>Clubs<ul>";
    foreach($data->venue_limits as $club => $limit) {
      $club_name = league_club_value($club, 'title');
      $html .= "<li>$club_name ($club) - venue limit = $limit";
      if( isset($data->venue_dates[$club]) ) {
        $html .= "<p>Existing fixture dates: ";
        foreach($data->venue_dates[$club] as $date => $count) {
          $date = date('Y-m-d', $date);
          $html .= " $date ($count), ";
        }
      }
      if( isset($data->venue_unavailable[$club]) ) {
        $html .= "<p>Venue unavailable: ";
        foreach($data->venue_unavailable[$club] as $date ) {
          $date = date('Y-m-d', $date);
          $html .= " $date, ";
        }
      }
    }
    $html .= "</ul><p>Teams:<ul>";
    foreach($data->teams as $eid => $teams) {
      $event_name = league_event_value($eid, 'title');
      $html .= "<li>$event_name ($eid) Teams:<ul>";
      foreach($teams as $team) {
        $team_name = league_team_value($team, 'title');
        $html .= "<li>$team_name ($team):";
        $avoids = $data->avoid_teams[$team];
        if(count($avoids) > 0) {
          $html .= '<p>Teams to avoid<ul>';
          foreach($avoids as $avoid) {
            $avoid_name = league_team_value($avoid, 'title');
            $avoid_event = league_team_value($avoid, 'event');
            $avoid_event_name = league_event_value($avoid_event, 'title');
            $avoid_season = league_event_value($avoid_event, 'season');
            $season = league_event_load($avoid_season);
            $season_name = $season->name;
            $avoid_org = league_event_value($avoid_event, 'org');
            $avoid_orgname = league_org_value($avoid_org, 'title');
            $html .= "<li>$avoid_name ($avoid | $avoid_event_name | $season_name | $avoid_orgname) , ";
          }
          $html .= '</ul>';
        }
        $team_dates = $data->team_dates[$team];
        if(count($team_dates) > 0) {
          $html .= '<p>Team dates:';
          foreach($team_dates as $team_date) {
            $html .= '<p>';
            $html .= $team_date['description'];
            $html .= ' from ';
            $html .= $team_date['start'];
            $html .= ' to ';
            $html .= $team_date['end'];
          }
        }
      }
      $html .= "</ul>";
    }
    $html .= "</ul>";
  }

  return $html;
}
