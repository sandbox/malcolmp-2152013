<?php

/**
 * @file
 * Functions for chessdb matchcard options.
 */
  
/**
 * Fixture matchcard edit tab.
 */
function league_generate_dates_tab ($node, $org=0) {
  // Set bread crumb
  league_set_bread_crumb ($node);
  // Set title
  drupal_set_title($node->title);

  // Load dates 
  $dates = league_generate_get_dates($node);

  $content = array();
  $content['header']['#node'] = $node;
  $content['header']['#theme'] = 'league_generate_dates_header';

  // Create dates edit form
  $content['form'] = drupal_get_form('league_generate_dates_form', $node, $dates);

  $content['footer']['#node'] = $node;
  $content['footer']['#theme'] = 'league_generate_dates_footer';

  return $content;
}

/**
*  Form to edit dates.
*/
function league_generate_dates_form($form, &$form_state,$node,$dates) {

  $ndates = count($dates);

  // Values to pass through:
  //  Number of boards
  $form['ndates'] = array(
   '#value' => $ndates,
   '#type'  => 'value',
  );
  $form['nid'] = array(
   '#value' => $node->nid,
   '#type'  => 'value',
  );

  $odd = 'even';
  // For each date
  for($dateNo=0; $dateNo< $ndates; $dateNo++)
  {
    if($odd == 'odd') {
      $odd = 'even';
    } else {
      $odd = 'odd';
    }

    $form['description'.$dateNo] = array(
      '#type' => 'textfield',
      '#default_value' => $dates[$dateNo]['description'],
      '#size' => 20,
      '#prefix' => "<tr class=$odd><td>",
      '#suffix' => "</td>",
    );

    // Make it readonly if they don't have access
    if (!league_check_access($node->nid)) {
      $form['description'.$dateNo]['#attributes']['readonly'] = 'readonly';
    }

    // Date if they have access, otherwise readonly field
    if (league_check_access($node->nid)) {
      $form['start'.$dateNo] = array(
       '#type'  => 'date_popup',
       '#date_format'  => 'Y-m-d',
       '#date_label_position'  => 'within',
       '#default_value' => $dates[$dateNo]['start'],
       '#size' => 10,
       '#prefix' => "<td>",
       '#suffix' => "</td>",
      );
    } else {
      $form['start'.$dateNo] = array(
        '#type' => 'textfield',
        '#default_value' => $dates[$dateNo]['start'],
        '#size' => 10,
        '#prefix' => "<td>",
        '#suffix' => "</td>",
        '#attributes' => array('readonly' => 'readonly'),
      );
    }

    if (league_check_access($node->nid)) {
      $form['end'.$dateNo] = array(
       '#type'  => 'date_popup',
       '#date_format'  => 'Y-m-d',
       '#date_label_position'  => 'within',
       '#default_value' => $dates[$dateNo]['end'],
       '#size' => 10,
       '#prefix' => "<td>",
       '#suffix' => "</td>",
      );
    } else {
      $form['end'.$dateNo] = array(
        '#type' => 'textfield',
        '#default_value' => $dates[$dateNo]['end'],
        '#size' => 10,
        '#prefix' => "<td>",
        '#suffix' => "</td>",
        '#attributes' => array('readonly' => 'readonly'),
      );
    }
    
    $form['did'.$dateNo] = array(
     '#value' => $dates[$dateNo]['did'],
     '#type'  => 'value',
    );
  
    $form['del'.$dateNo] = array(
       '#type'  => 'checkbox',
    );
    $form['del'.$dateNo]['#prefix'] = "<td>";
    $form['del'.$dateNo]['#suffix'] = "</td></tr>";
  }

  $form['submit'] = array(
   '#value' => t('Save'),
   '#type'  => 'submit',
   '#prefix' => '<tr><td colspan=4>',
  );

  $form['adddate'] = array(
   '#value' => t('Add Date'),
   '#type'  => 'submit',
  );
  $form['deldate'] = array(
   '#value' => t('Delete Dates'),
   '#type'  => 'submit',
   '#suffix' => '</td></tr>',
  );

  return $form;
}

/**
*  Validate the dates form.
*/
function league_generate_dates_form_validate($form, &$form_state) {
  $ndates = $form_state['values']['ndates'];

  // Validate the dates
  for($dateNo=0; $dateNo< $ndates; $dateNo++)
  {
    $start = $form_state['values']['start'.$dateNo];
    if(!isset($start)) {
      form_set_error('start'.$dateNo, 'Start date must not be blank');
    }
    $end = $form_state['values']['end'.$dateNo];
    if(isset($end)) {
      $start_date = strtotime($start);
      $end_date = strtotime($end);
      if($start_date > $end_date) {
        $fieldName = 'end'.$dateNo;
        form_set_error($fieldName, 'End date must be greater than start date');
      }
    }
  }
}

/**
*  Action after the form is submitted.
*  For updating dates when fixtures should not be generated
*/
function league_generate_dates_form_submit($form, &$form_state) {
  $ndates = $form_state['values']['ndates'];
  $op = $form_state['values']['op'];
  $nid = $form_state['values']['nid'];
  $node = node_load($nid);
  if (!league_check_access($node->nid)) {
    drupal_set_message("You do not have access to do this", 'error');
    return;
  }

  // Store the updating user 
  // Get the results
  for($dateNo=0; $dateNo< $ndates; $dateNo++)
  {
    $start = $form_state['values']['start'.$dateNo];
    $start_date = strtotime($start);
    $end = $form_state['values']['end'.$dateNo];
    if(isset($end)) {
      $end_date = strtotime($end);
    } else {
      $end_date = NULL;
    }
    $description = $form_state['values']['description'.$dateNo] ;
    $did = $form_state['values']['did'.$dateNo] ;
    // If its a new date, write new record
    if($did == 0 ) {
      league_generate_write_date(0, $nid, $start_date, $end_date, $description);
    } else { 
    // Otherwise update the existing record
      league_generate_write_date($did, $nid, $start_date, $end_date, $description);
    }
  }
  // Add dates if that button was pressed
  if( $op == 'Add Date' ) {
    league_generate_write_date(0, $nid, NULL, NULL, ' ');
  }

  // Delete dates if the delete button was pressed
  if( $op == 'Delete Dates' ) {
    for($dateNo=0; $dateNo< $ndates; $dateNo++)
    {
      $del = $form_state['values']['del'.$dateNo] ;
      if($del == 1) {
        $did = $form_state['values']['did'.$dateNo] ;
        league_generate_delete_date($did);
      }
    }
  }
  drupal_set_message('Dates updated');
}

/**
*  Write a new date to the database.
*/
function league_generate_write_date($did, $entity_id, $start, $end, $description ) {

  // Now store the results
  $record = (object) array(
    'did' => $did,
    'entity_id' => $entity_id,
    'start' => $start,
    'end' => $end,
    'description' => $description,
  );

  if($did == 0) {
//  drupal_set_message("New date did $did entity_id $entity_id start $start end $end description $description");
    drupal_write_record('league_generate_dates', $record);
  } else {
//  drupal_set_message("Update date did $did entity_id $entity_id start $start end $end description $description");
    $record->did = $did;
    drupal_write_record('league_generate_dates', $record, 'did');
 }
}

/**
 * Delete date from an entity by did
 */
function league_chess_delete_result($did) {
  $nrecs = db_delete('league_generate_dates')
  ->condition('did' , $did)
  ->execute();
  return $nrecs;
}

/**
 * Delete date from entity by did
 */
function league_generate_delete_date($did) {
  $nrecs = db_delete('league_generate_dates')
  ->condition('did' , $did)
  ->execute();
  return $nrecs;
}

