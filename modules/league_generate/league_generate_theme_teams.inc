<?php
// $Id$
/**
 * @file
 * Functions for theme teams to avoid
 */

function theme_league_generate_teams_header($variables) {
  $element = $variables['element'];
  $node = $element['#node'];

  $output = '<div class=league-frame>';
  $output .= '<h2>'. $node->title. " - teams to avoid when generating fixtures</h2>";
  $output .= "<table border>";
  $output .= "<tr><th>Team</th><th>Avoid</th></tr>\n";

  return $output;
}

function theme_league_generate_teams_footer($variables) {
  $output = '</table></div>';
  return $output;
}
