<?php

/**
 * @file
 * Functions for teams to avoid when generating fixtures
 */
  
/**
 * Avoid Teams tab.
 */
function league_generate_teams_tab ($node, $org=0) {
  // Set bread crumb
  league_set_bread_crumb ($node);
  // Set title
  drupal_set_title($node->title);

  // The teams club
  $team = $node->nid;
  $club = league_team_value($team, 'club');
  // Teams for this club from any organisation for events in 
  // active or new seasons.
  $teams = league_teams_for_club($club, 0, Array('A', 'N'));
  // Remove this team from the array.
  $this_team = array_search($team, $teams);
  unset($teams[$team]);

  $content = array();
  $content['header']['#node'] = $node;
  $content['header']['#theme'] = 'league_generate_teams_header';

  // Create teams avoid form
  $content['form'] = drupal_get_form('league_generate_teams_form', $node, $teams);

  $content['footer']['#node'] = $node;
  $content['footer']['#theme'] = 'league_generate_teams_footer';

  return $content;
}

/**
*  Form to select teams to avoid fixtures with.
*/
function league_generate_teams_form($form, &$form_state, $node, $teams) {

  $avoids = league_team_value($node, 'avoid');

  // Values to pass through:
  $form['nid'] = array(
   '#value' => $node->nid,
   '#type'  => 'value',
  );
  $form_state['teams'] = $teams;

  $odd = 'even';

  // For each team
  foreach($teams as $nid => $team) {
    if($odd == 'odd') {
      $odd = 'even';
    } else {
      $odd = 'odd';
    }

    $form['name'.$nid] = array(
      '#type' => 'markup',
      '#markup' => $teams[$nid],
      '#prefix' => "<tr class=$odd><td>",
      '#suffix' => "</td>",
    );

    // Default value
    if( in_array($nid , $avoids) ) {
      $checked = 1;
    } else {
      $checked = 0;
    }
    // Checkbox if they have access, otherwise readonly field
    if (league_check_access($node->nid)) {
      $form['check'.$nid] = array(
        '#type'  => 'checkbox',
        '#default_value' => $checked,
        '#prefix' => "<td>",
        '#suffix' => "</td></tr>",
      );
    } else {
      $labels = array(0 => 'No', 1 => 'Yes');
      $form['check'.$nid] = array(
        '#type' => 'markup',
        '#markup' => $labels[$checked],
        '#prefix' => "<td>",
        '#suffix' => "</td></tr>",
      );
    }
  }

  $form['submit'] = array(
   '#value' => t('Save'),
   '#type'  => 'submit',
   '#prefix' => '<tr><td colspan=4>',
  );

  return $form;
}

/**
*  Action after the form is submitted.
*  For updating dates when fixtures should not be generated
*/
function league_generate_teams_form_submit($form, &$form_state) {
  $nid = $form_state['values']['nid'];
  $node = node_load($nid);
  if (!league_check_access($node->nid)) {
    drupal_set_message("You do not have access to do this", 'error');
    return;
  }

  $avoids = array();
  $teams = $form_state['teams'];
  // Get the results
  foreach($teams as $nid => $team) {
    $cb = $form_state['values']['check'.$nid] ;
    // If its a new date, write new record
    if($cb == 1 ) {
      $avoids[] = array('value' => $nid);
    }
  }
  $node->field_teams['und'] = $avoids;
  node_save($node);
  drupal_set_message('Teams saved');
}
