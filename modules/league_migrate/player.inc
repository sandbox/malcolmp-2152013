<?php

/**
 * Common mappings for the Drupal 6 players migrations.
 */
class MigrateDestinationPlayer extends MigrateDestinationEntity {
  static public function getKeySchema() {
    return array(
      'pid' => array(
        'type' => 'int', 
        'unsigned' => TRUE, 
        'description' => 'ID of Player',
      ),
    );
  }

  public function __construct() {
    parent::__construct('league_player', 'league_player', array());
  }

  public function fields($migration = NULL) {
    $fields = array();
    // Core Fields.
    $fields['pid'] = t('Player ID');
    $fields['org'] = t('Organisation nid');
    $fields['dob'] = t('Date Of Birth');
    $fields['sex'] = t('Sex');
    $fields['firstname'] = t('First Name');
    $fields['lastname'] = t('Last Name');
    $fields['club'] = t('Club');

    // Then add in anything provided by handlers
    $fields += migrate_handler_invoke_all('Entity', 'fields', $this->entityType, $this->bundle, $migration);

    return $fields;
  }

  /**
   * Delete a batch of players at once.
   *
   * @param $nids
   *  Array of players IDs to be deleted.
   */
  public function bulkRollback(array $pids) {
    migrate_instrument_start('league_player_delete_multiple');
    $this->prepareRollback($pids);
    league_player_delete_multiple($pids);
    $this->completeRollback($pids);
    migrate_instrument_stop('league_player_delete_multiple');
  }

  /**
   * Import a single player.
   *
   * @param $node
   *  Node object to build. Prefilled with any fields mapped in the Migration.
   * @param $row
   *  Raw source data object - passed through to prepare/complete handlers.
   * @return array
   *  Array of key fields (pid only in this case) of the player that was saved if
   *  successful. FALSE on failure.
   */
  public function import(stdClass $player, stdClass $row) {
    // Updating previously-migrated content?
    $migration = Migration::currentMigration();

    // Invoke migration prepare handlers
    $this->prepare($player, $row);

    migrate_instrument_start('league_player_save');
    league_player_save($player);
    migrate_instrument_stop('league_player_save');

    if (isset($player->pid)) {
      $this->numCreated++;
      $return = array($player->pid);
    }
    else {
      $return = FALSE;
    }

    $this->complete($player, $row);
    return $return;
  }
}

class LeaguePlayerMigration extends DrupalMigration {
// TODO - should exclude DEFAULT and NEWPLAY
  protected function query() {
    $query = Database::getConnection('default', $this->sourceConnection)
             ->select('chessdb_player', 'p')
             ->fields('p', array('ecfCode', 'org', 'grade', 'dob', 'sex', 'name', 'ecfClub', 'mid'));
    return $query;
  }

  public function __construct(array $arguments) {
    parent::__construct($arguments);

    // Get any CCK/core fields attached to users.
    $this->sourceFields += $this->version->getSourceFields(
      'league_player', 'league_player');

    // Select players from D6 database as source.
    $this->source = new MigrateSourceSQL($this->query(), $this->sourceFields,
      NULL, $this->sourceOptions);
    // We migrate into players.
    $this->destination = new MigrateDestinationPlayer();

    // We instantiate the MigrateMap
    $this->map = new MigrateSQLMap($this->machineName,
        array(
          'ecfCode' => array('type' => 'char',
                          'length' => 7,
                          'not null' => TRUE,
                       ),
          'org' => array( 'type' => 'int', 
                          'unsigned' => TRUE, 
                          'description' => 'nid of org',
                       ),
        ),
        MigrateDestinationPlayer::getKeySchema()
      );

    // Finally we add simple field mappings 

    $this->addFieldMapping('field_ecf_code', 'ecfCode');
    $this->addFieldMapping('field_membership_number', 'mid');
    $this->addFieldMapping('sex', 'sex');
    $this->addFieldMapping('dob', 'dob');
    $this->addFieldMapping('field_rating', 'grade');
    $this->addFieldMapping('firstname', 'name')
       ->callbacks(array($this, 'getFirstName'));
    $this->addFieldMapping('lastname', 'name')
       ->callbacks(array($this, 'getLastName'));
    $this->addFieldMapping('org', 'org')
         ->sourceMigration('LeagueOrg');

    // ecfClub is hacked in prepare row to be the club node
    $this->addFieldMapping('club', 'ecfClub');

    // Things not to migrate:
    $this->addUnmigratedDestinations(array('field_rapid_rating', ));
  }

  protected function getFirstName($value) {
    $names = explode(",", $value);
    $value = ltrim($names[1]);
    return $value;
  }

  protected function getLastName($value) {
    $names = explode(",", $value);
    $value = ltrim($names[0]);
    return $value;
  }

  protected function getClub($value) {
    $names = explode(",", $value);
    $value = ltrim($names[0]);
    return $value;
  }

  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    $club = league_ecf_club_for_ecf_code($row->org, $row->ecfClub);
    $row->ecfClub = $club;
    $dob = $row->dob;
    if(isset($dob) && substr($dob,0,4) == '0000') {
      $row->dob = null;
    } else {
      $row->dob = strtotime($dob);
    }
    return TRUE;
  }
}
