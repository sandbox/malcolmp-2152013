<?php

/**
 * Common mappings for the Drupal 6 user migrations.
 */
class LeagueUserMigration extends DrupalUser6Migration {
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->addFieldMapping('field_name', 'profile_name');
    $this->addFieldMapping('field_phone', 'profile_phone');
    $this->addFieldMapping('field_mobile', 'profile_mobile');
    $this->addFieldMapping('field_address', 'profile_address');
  }
}
