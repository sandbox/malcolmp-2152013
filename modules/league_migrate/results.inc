<?php
/**
 * @file
 * Classes for migrating results into Drupal.
 */

/**
 * Class for migrating results into Drupal (Team events)
 */
class LeagueResultsMigration extends DrupalMigration {

  protected $sourceType;
  protected $destinationType;
  protected $nodeMigration;

  /**
   * @param array $arguments
   */
  public function __construct(array $arguments) {
    if (!empty($arguments['node_migration'])) {
      $this->nodeMigration = $arguments['node_migration'];
      $this->dependencies[] = $this->nodeMigration;
    }

    parent::__construct($arguments);

    // Create our three main objects - source, destination, and map

    // $this->source = new MigrateSourceSQL($this->query());
    $this->source = new MigrateSourceSQL($this->query(),
                  $this->sourceFields, NULL, $this->sourceOptions);

    $table_name = 'league_chess_result';
    $this->destination = new MigrateDestinationTable($table_name);

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'id' => array('type' => 'int',
                      'unsigned' => TRUE,
                      'not null' => TRUE,
                      'description' => 'Result ID',
                      'alias' => 'r',
                      ),
      ),
      MigrateDestinationTable::getKeySchema($table_name)
    );

    /**
     * Setup field mappings.
     */

    if (!empty($this->nodeMigration)) {
      $this->addFieldMapping('fid', 'nid')
           ->sourceMigration($this->nodeMigration);
    }
    else {
      $this->addFieldMapping('fid', 'nid');
    }

    $this->addFieldMapping('org', 'org')
         ->sourceMigration('LeagueOrg');
    $this->addFieldMapping('white', 'whiteEcfCode');
    $this->addFieldMapping('black', 'blackEcfCode');
    $this->addFieldMapping('board', 'board');
    $this->addFieldMapping('result', 'result');
    $this->addFieldMapping('color', 'colour');
//  $this->addFieldMapping('fid', 'fixtureId');
    $this->addFieldMapping('whiteGrade', 'whiteGrade');
    $this->addFieldMapping('blackGrade', 'blackGrade');

  }
  protected function query() {
    //TODO - get latest version of results only - ignore history
    //         (makes it easier as vid=nid)
    $query = Database::getConnection('default', $this->sourceConnection)
             ->select('chessdb_result', 'r')
             ->fields('r', array('id', 'board', 'result', 'fixtureId', 'whiteEcfCode', 'blackEcfCode', 'colour' ));
    //TODO vid ???
    $query->innerJoin('node', 'n', 'n.vid=r.fixtureId');
    $query->addField('n', 'nid', 'nid');
    $query->innerJoin('chessdb_fixture', 'f', 'f.id=n.vid');
    $query->innerJoin('content_field_org', 'org', 'org.nid=f.eventnid');
    $query->addField('org', 'field_org_nid', 'org');
    $query->leftJoin('ecfdata_master', 'w', 'w.REF=r.whiteEcfCode');
    $query->addField('w', 'GRADE', 'whiteGrade');
    $query->leftJoin('ecfdata_master', 'b', 'b.REF=r.blackEcfCode');
    $query->addField('b', 'GRADE', 'blackGrade');
    $query->condition('f.homeTeam', 0, '!=');
    $query->condition('f.awayTeam', 0, '!=');

    return $query;
  }

  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    $row->org = $this->mapOrg($row->org);
    $row->fid = $this->mapFixture($row->nid);
    $white = $this->getPlayer($row->org, $row->whiteEcfCode);
    $row->whiteEcfCode = $white;
    $black = $this->getPlayer($row->org, $row->blackEcfCode);
    $row->blackEcfCode = $black;
    return TRUE;
  }

  public function getPlayer($org, $ecfCode) {
    if( $ecfCode == 'NEWPLAY' ) {
      $player = -1;
    } else {
      if( $ecfCode == 'DEFAULT' ) {
        $player = -2;
      } else {
        if( $ecfCode == 'HALFBYE' ) {
          $player = -3;
        } else {
          if( $ecfCode == 'FULLBYE' ) {
            $player = -4;
          } else {
            $player = league_ecf_player_for_ecf_code($org, $ecfCode);
          }
        }
      }
    }
    return $player;
  }

  public function mapOrg($org) {
    $query = db_select('migrate_map_leagueorg', 'm');
    $query->condition('m.sourceid1', $org);
    $query->addField('m', 'destid1', 'org');
    $result = $query->execute();
    foreach( $result as $row) {
      $value = $row->org;
      return $value;
    }
  }

  public function mapFixture($fid) {
    $query = db_select('migrate_map_leaguefixture', 'm');
    $query->condition('m.sourceid1', $fid);
    $query->addField('m', 'destid1', 'fid');
    $result = $query->execute();
    foreach( $result as $row) {
      $value = $row->fid;
      return $value;
    }
    return $fid;
  }

  /**
   * Implementation of Migration::createStub().
   * @todo: if possible, implement this :)
   *
   * @param $migration
   * @return array|bool
   */
/*
  protected function createStub($migration) {
      return FALSE;
  }
 */
}

/**
 * Class for migrating results into Drupal (Team events)
 */
class LeagueResultsNTMigration extends DrupalMigration {

  protected $sourceType;
  protected $destinationType;
  protected $nodeMigration;

  /**
   * @param array $arguments
   */
  public function __construct(array $arguments) {
    if (!empty($arguments['node_migration'])) {
      $this->nodeMigration = $arguments['node_migration'];
      $this->dependencies[] = $this->nodeMigration;
    }

    parent::__construct($arguments);

    // Create our three main objects - source, destination, and map

    // $this->source = new MigrateSourceSQL($this->query());
    $this->source = new MigrateSourceSQL($this->query(),
                  $this->sourceFields, NULL, $this->sourceOptions);

    $table_name = 'league_chess_result';
    $this->destination = new MigrateDestinationTable($table_name);

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'id' => array('type' => 'int',
                      'unsigned' => TRUE,
                      'not null' => TRUE,
                      'description' => 'Result ID',
                      'alias' => 'r',
                      ),
      ),
      MigrateDestinationTable::getKeySchema($table_name)
    );

    /**
     * Setup field mappings.
     */

    if (!empty($this->nodeMigration)) {
      $this->addFieldMapping('fid', 'nid')
           ->sourceMigration($this->nodeMigration);
    }
    else {
      $this->addFieldMapping('fid', 'nid');
    }

    $this->addFieldMapping('org', 'org')
         ->sourceMigration('LeagueOrg');
    $this->addFieldMapping('white', 'whiteEcfCode');
    $this->addFieldMapping('black', 'blackEcfCode');
    $this->addFieldMapping('board', 'board');
    $this->addFieldMapping('result', 'result');
    $this->addFieldMapping('color', 'colour');
//  $this->addFieldMapping('fid', 'fixtureId');
    $this->addFieldMapping('whiteGrade', 'whiteGrade');
    $this->addFieldMapping('blackGrade', 'blackGrade');

  }
  protected function query() {
    //TODO - get latest version of results only - ignore history
    //         (makes it easier as vid=nid)
    $query = Database::getConnection('default', $this->sourceConnection)
             ->select('chessdb_result', 'r')
             ->fields('r', array('id', 'board', 'result', 'fixtureId', 'whiteEcfCode', 'blackEcfCode', 'colour' ));
    //TODO vid ???
    $query->innerJoin('node', 'n', 'n.vid=r.fixtureId');
    $query->addField('n', 'nid', 'nid');
    $query->innerJoin('chessdb_fixture', 'f', 'f.id=n.vid');
    $query->innerJoin('content_field_org', 'org', 'org.nid=f.eventnid');
    $query->addField('org', 'field_org_nid', 'org');
    $query->leftJoin('ecfdata_master', 'w', 'w.REF=r.whiteEcfCode');
    $query->addField('w', 'GRADE', 'whiteGrade');
    $query->leftJoin('ecfdata_master', 'b', 'b.REF=r.blackEcfCode');
    $query->addField('b', 'GRADE', 'blackGrade');
    $query->condition('f.homeTeam', 0);
    $query->condition('f.awayTeam', 0);

    return $query;
  }

  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    $row->org = $this->mapOrg($row->org);
    $row->fid = $this->mapFixture($row->nid);
    $white = $this->getPlayer($row->org, $row->whiteEcfCode);
    $row->whiteEcfCode = $white;
    $black = $this->getPlayer($row->org, $row->blackEcfCode);
    $row->blackEcfCode = $black;
    return TRUE;
  }

  public function getPlayer($org, $ecfCode) {
    if( $ecfCode == 'NEWPLAY' ) {
      $player = -1;
    } else {
      if( $ecfCode == 'DEFAULT' ) {
        $player = -2;
      } else {
        if( $ecfCode == 'HALFBYE' ) {
          $player = -3;
        } else {
          if( $ecfCode == 'FULLBYE' ) {
            $player = -4;
          } else {
            $player = league_ecf_player_for_ecf_code($org, $ecfCode);
          }
        }
      }
    }
    return $player;
  }

  public function mapOrg($org) {
    $query = db_select('migrate_map_leagueorg', 'm');
    $query->condition('m.sourceid1', $org);
    $query->addField('m', 'destid1', 'org');
    $result = $query->execute();
    foreach( $result as $row) {
      $value = $row->org;
      return $value;
    }
  }

  public function mapFixture($fid) {
    $query = db_select('migrate_map_leaguefixturent', 'm');
    $query->condition('m.sourceid1', $fid);
    $query->addField('m', 'destid1', 'fid');
    $result = $query->execute();
    foreach( $result as $row) {
      $value = $row->fid;
      return $value;
    }
    watchdog('league_migrate', "mapFixture nt fid $fid no mapping ");
    return $fid;
  }

  /**
   * Implementation of Migration::createStub().
   * @todo: if possible, implement this :)
   *
   * @param $migration
   * @return array|bool
   */
/*
  protected function createStub($migration) {
      return FALSE;
  }
 */
}
