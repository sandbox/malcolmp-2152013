<?php

/**
 * Common mappings for the Drupal 6 node migrations.
 */
abstract class LeagueNodeMigration extends DrupalNode6Migration {
  public function __construct(array $arguments) {
    parent::__construct($arguments);

//  $this->addFieldMapping('field_legacy_nid', 'nid')
//       ->description('We have a common field to save the D6 nid');
  }
}

class LeagueOrgMigration extends LeagueNodeMigration {
  public function __construct(array $arguments) {
    // Add any other data we're pulling into the source row, before the parent
    // constructor.
    // $this->sourceFields['lead_graphic'] = 'Lead graphic string from D6';
    // $this->sourceFields['summary'] = 'Summary string from D6';

    parent::__construct($arguments);

    $this->addFieldMapping('field_users', 'field_users')
         ->sourceMigration('LeagueUser');
    $this->addFieldMapping('field_current_season', 'field_season');
    $this->addFieldMapping('field_auto_locking', 'field_auto_locking');
    $this->addFieldMapping('field_avoid_date', 'field_avoid_date');

    // Things not to migrate:
    $this->addUnmigratedDestinations(array('field_events', 'field_clubs', 'field_website', 'field_fixture_revisions' ));

  }
}

class LeagueClubMigration extends LeagueNodeMigration {
  public function __construct(array $arguments) {

    parent::__construct($arguments);

    $this->addFieldMapping('field_user', 'field_users')
         ->sourceMigration('LeagueUser');
    $this->addFieldMapping('field_ecf_club_code', 'field_club_code');
    $this->addFieldMapping('field_org', 'field_org')
         ->sourceMigration('LeagueOrg');
    $this->addFieldMapping('field_contact', 'field_contact');

    // Things not to migrate:
    $this->addUnmigratedDestinations(array('field_player_list', ));

  }

}

class LeagueEventMigration extends LeagueNodeMigration {
  public function __construct(array $arguments) {

    parent::__construct($arguments);

    $this->addFieldMapping('field_user', 'field_users')
         ->sourceMigration('LeagueUser');
    $this->addFieldMapping('field_org', 'field_org')
         ->sourceMigration('LeagueOrg');
    $this->addFieldMapping('field_boards', 'field_boards');
    //TODO can we calculate from the start date ?
    //$this->addFieldMapping('field_event_season', 'field_start_date');
    $this->addFieldMapping('field_event_season', 'field_season_computed');
    $this->addFieldMapping('field_ntimes', 'field_ntimes');
    $this->addFieldMapping('field_rate', 'field_rate');
    $this->addFieldMapping('field_type', 'field_type');

    // Things not to migrate:
    $this->addUnmigratedDestinations(array('field_player_list', ));

  }



}

class LeagueTeamMigration extends LeagueNodeMigration {
  public function __construct(array $arguments) {

    parent::__construct($arguments);

    $this->addFieldMapping('field_club', 'field_club')
         ->sourceMigration('LeagueClub');
    $this->addFieldMapping('field_event', 'field_event')
         ->sourceMigration('LeagueEvent');
// Don't really care about importing these 
//  $this->addFieldMapping('field_teams', 'field_team_avoid');
    $this->addFieldMapping('field_avoid_date', 'field_avoid_date');
    $this->addFieldMapping('field_night', 'field_night');
    $this->addFieldMapping('field_captain', 'field_captain')
         ->sourceMigration('LeagueUser');

  }
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
//  Skip teams where the club or event is not set.
    if(!isset($row->field_club) || $row->field_club == 0) {
      return FALSE;
    }
    if(!isset($row->field_event) || $row->field_event == 0) {
      return FALSE;
    }
    return TRUE;
  }

}

class LeagueFixtureMigration extends LeagueNodeMigration {
  protected function query() {
    $query = Database::getConnection('default', $this->sourceConnection)
          ->select('chessdb_fixture', 'f');
    $query->innerJoin('node', 'n', 'n.nid=f.nid');
    $query->innerJoin('node_revisions', 'nr', 'n.vid=nr.vid');
    $query->addField('n', 'nid', 'nid');
    $query->addField('n', 'vid', 'vid');
    $query->addField('n', 'language', 'language');
    // Create title from home v away
    $query->addExpression('concat(f.homeTeam, :v, f.awayTeam)', 'title', array(':v' => ' v ' ) ); 
    $query->addField('f', 'lastUpdater', 'uid');
    $query->addField('n', 'status', 'status');
    $query->addField('n', 'created', 'created');
    $query->addField('n', 'changed', 'changed');
    $query->addExpression(':comment', 'comment', array(':comment' =>2) ); 
    $query->addField('n', 'promote', 'promote');
    $query->addField('n', 'moderate', 'moderate');
    $query->addField('n', 'sticky', 'sticky');
    $query->addField('n', 'tnid', 'tnid');
    $query->addField('n', 'translate', 'translate');
    $query->addField('nr', 'body', 'body');
    $query->addField('nr', 'teaser', 'teaser');
    $query->addField('nr', 'format', 'format');
    $query->addField('f', 'round', 'round');
    $query->addField('f', 'eventnid', 'eventnid');
    $query->addField('f', 'homeTeam', 'homeTeam');
    $query->addField('f', 'awayTeam', 'awayTeam');
    $query->addField('f', 'date', 'date');
    $query->addField('f', 'homeScore', 'homeScore');
    $query->addField('f', 'awayScore', 'awayScore');
    $query->addField('f', 'locked', 'locked');
    $query->addField('f', 'verifier', 'verifier');
    $query->addField('f', 'tableNo', 'tableNo');
    $query->innerJoin('node', 'h', 'h.nid=f.homeTeam');
    $query->addField('h', 'title', 'homeTeamName');
    $query->innerJoin('node', 'a', 'a.nid=f.awayTeam');
    $query->addField('a', 'title', 'awayTeamName');
 
    return $query;
  }
  public function __construct(array $arguments) {

    parent::__construct($arguments);

    $this->addFieldMapping('field_event', 'eventnid')
         ->sourceMigration('LeagueEvent');
    $this->addFieldMapping('field_away_team', 'awayTeam')
         ->sourceMigration('LeagueTeam');
    $this->addFieldMapping('field_home_team', 'homeTeam')
         ->sourceMigration('LeagueTeam');
    $this->addFieldMapping('field_round', 'round');
    $this->addFieldMapping('field_away_score', 'awayScore');
    $this->addFieldMapping('field_home_score', 'homeScore');
    $this->addFieldMapping('field_locked', 'locked');
    $this->addFieldMapping('field_verifier', 'verifier');
         ->sourceMigration('LeagueUser');
    $this->addFieldMapping('field_table_number', 'tableNo');

    $this->addFieldMapping('field_date', 'date');

    // Things not to migrate:
    //$this->addUnmigratedDestinations(array('body', ));

    //TODO results are loaded into the node - but how to process them?
    // need to add their gradings and default the colour

  }
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    return TRUE;
  }

  // Preprocess the node before saving.
  public function prepare($entity, $row) {
    // Date wasn't being set if I didn't do this.
    $date = $row->date;
    $entity->field_date['und'][0]['value'] = $date;
    // Set node title from team names
    $home = $row->homeTeamName;
    $away = $row->awayTeamName;
    $entity->title = "$home v $away";
  }

}
class LeagueFixtureNoTeamMigration extends LeagueNodeMigration {
  protected function query() {
    $query = Database::getConnection('default', $this->sourceConnection)
          ->select('chessdb_fixture', 'f');
    $query->innerJoin('node', 'n', 'n.nid=f.nid');
    $query->innerJoin('node_revisions', 'nr', 'n.vid=nr.vid');
    $query->addField('n', 'nid', 'nid');
    $query->addField('n', 'vid', 'vid');
    $query->addField('n', 'language', 'language');
    // Create title from home v away
    $query->addExpression('concat(:v, f.round )', 'title', array(':v' => 'Round ' ) ); 
    $query->addField('f', 'lastUpdater', 'uid');
    $query->addField('n', 'status', 'status');
    $query->addField('n', 'created', 'created');
    $query->addField('n', 'changed', 'changed');
    $query->addExpression(':comment', 'comment', array(':comment' =>0) ); 
    $query->addField('n', 'promote', 'promote');
    $query->addField('n', 'moderate', 'moderate');
    $query->addField('n', 'sticky', 'sticky');
    $query->addField('n', 'tnid', 'tnid');
    $query->addField('n', 'translate', 'translate');
    $query->addField('nr', 'body', 'body');
    $query->addField('nr', 'teaser', 'teaser');
    $query->addField('nr', 'format', 'format');
    $query->addField('f', 'round', 'round');
    $query->addField('f', 'eventnid', 'eventnid');
    $query->addField('f', 'date', 'date');
    $query->addField('f', 'homeScore', 'homeScore');
    $query->addField('f', 'awayScore', 'awayScore');
    $query->addField('f', 'locked', 'locked');
    $query->addField('f', 'verifier', 'verifier');
    $query->addField('f', 'tableNo', 'tableNo');
    $query->condition('f.homeTeam', 0);
    $query->condition('f.awayTeam', 0);
 
    return $query;
  }
  public function __construct(array $arguments) {

    parent::__construct($arguments);

    $this->addFieldMapping('field_event', 'eventnid')
         ->sourceMigration('LeagueEvent');
    $this->addFieldMapping('field_round', 'round');
    $this->addFieldMapping('field_away_score', 'awayScore');
    $this->addFieldMapping('field_home_score', 'homeScore');
    $this->addFieldMapping('field_locked', 'locked');
    $this->addFieldMapping('field_verifier', 'verifier');
    $this->addFieldMapping('field_table_number', 'tableNo');

    $this->addFieldMapping('field_date', 'date');

  }
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    return TRUE;
  }

  // Preprocess the node before saving.
  public function prepare($entity, $row) {
    // Date wasn't being set if I didn't do this.
    $date = $row->date;
    $entity->field_date['und'][0]['value'] = $date;
    // Set node title from round.
    $round = $row->round;
    $entity->title = "Round $round";
  }

}

// Use a different query to get press comments
class LeagueFixtureCommentMigration extends DrupalComment6Migration {
  protected function query() {
    $query = Database::getConnection('default', $this->sourceConnection)
          ->select('chessdb_fixture', 'c');
    $query->addField('c', 'nid', 'cid');
    // Hard code pid to be 1
    $query->addExpression(':pid', 'pid', array(':pid' =>0) ); 
    $query->addField('c', 'nid', 'nid');
    $query->addField('c', 'lastUpdater', 'uid');
    // Hard code subject
    $query->addExpression(':sub', 'subject', array(':sub' =>'Press/Admin Comment') ); 
    $query->addField('c', 'pressComment', 'comment');
    $query->addExpression(':host', 'hostname', array(':host' =>'localhost') ); 
    $query->addField('c', 'updateTime', 'timestamp');
    $query->addExpression(':status', 'status', array(':status' =>0) ); 
    $query->addExpression(':thread', 'thread', array(':thread' =>'01/') ); 
    $query->innerJoin('users', 'u', 'u.uid=c.lastUpdater');
    $query->addField('u', 'name', 'name');
    $query->addField('u', 'mail', 'mail');
    $query->addExpression(':homepage', 'homepage', array(':homepage' =>null) ); 
    $query->addExpression(':format', 'format', array(':format' =>1) ); 
 
    $query->where('length(pressComment) >0');
    //$string = (string)$query;
    //watchdog('league_migrate', "Comment query $string");
    return $query;
  }
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    $row->timestamp = strtotime($row->timestamp);
    return TRUE;
  }
}
