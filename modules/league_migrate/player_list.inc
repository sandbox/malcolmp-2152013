<?php

/**
 * Common mappings for the Drupal 6 players migrations.
 */
class MigrateDestinationPlayerList extends MigrateDestinationEntity {
  static public function getKeySchema() {
    return array(
      'lid' => array(
        'type' => 'int', 
        'unsigned' => TRUE, 
        'description' => 'ID of Player List',
      ),
    );
  }

  public function __construct() {
    parent::__construct('league_player_list', 'league_player_list', array());
  }

  public function fields($migration = NULL) {
    $fields = array();
    // Core Fields.
    $fields['lid'] = t('List ID');
    $fields['org'] = t('Organisation nid');
    $fields['name'] = t('List Name');

    // Then add in anything provided by handlers
    //$fields += migrate_handler_invoke_all('Entity', 'fields', $this->entityType, $this->bundle, $migration);

    return $fields;
  }

  /**
   * Delete a batch of player lists at once.
   *
   * @param $nids
   *  Array of players IDs to be deleted.
   */
  public function bulkRollback(array $lids) {
    migrate_instrument_start('league_player_list_delete_multiple');
    $this->prepareRollback($lids);
    //Delete the lists (this will remove the players first)
    league_player_list_delete_multiple($lids);
    $this->completeRollback($lids);
    migrate_instrument_stop('league_player_list_delete_multiple');
  }

  /**
   * Import a single player list.
   *
   * @param $node
   *  Node object to build. Prefilled with any fields mapped in the Migration.
   * @param $row
   *  Raw source data object - passed through to prepare/complete handlers.
   * @return array
   *  Array of key fields (lid only in this case) of the player that was saved if
   *  successful. FALSE on failure.
   */
  public function import(stdClass $list, stdClass $row) {
    // Updating previously-migrated content?
    $migration = Migration::currentMigration();

    // Invoke migration prepare handlers
    $this->prepare($list, $row);

    migrate_instrument_start('league_player_list_save');
    $list = league_player_list_save($list);
    // Get players from existing list.
    $players = $migration->getPlayers($list, $row);
    // Add all the players to the list.
    $migration->addPlayers($list->lid, $list->org, $players);
    migrate_instrument_stop('league_player_list_save');

    if (isset($list->lid)) {
      $this->numCreated++;
      $return = array($list->lid);
    }
    else {
      $return = FALSE;
    }

    $this->complete($list, $row);
    return $return;
  }
}

class LeaguePlayerListMigration extends DrupalMigration {
  protected function query() {
    $query = Database::getConnection('default', $this->sourceConnection)
             ->select('content_field_player_list', 'f');
    $query->innerJoin('node', 'c', 'c.nid = f.nid');
    $query->condition('c.type', 'club');
    $query->innerJoin('chessdb_list', 'l', 'l.nid = f.field_player_list_nid');
    $query->addField('f', 'field_player_list_nid', 'nid');
    $query->addField('l', 'org', 'org');
    $query->addField('c', 'title', 'name');
    //Temporary hack for testing
    //$query->range(0, 1);
    return $query;
  }

  public function getPlayers($list, $row) {
    $newList = $list->lid;
    $oldList = $row->nid;
    $query = Database::getConnection('default', $this->sourceConnection)
             ->select('chessdb_listPlayer', 'p');
    $query->condition('p.nid', $oldList);
    $query->addField('p', 'ecfCode', 'ecfCode');
    //Temporary hack for testing
    //$query->range(0, 1);
    $result = $query->execute();
    $players = array();
    foreach ( $result as $row) {
      $players[] = $row->ecfCode;
    }
    return $players;
  }

  // Get a players pid. given ecfCode and org
  public function getPid($ecfCode,$org) {
    $pid = 0;
    
    $query = db_select('field_data_field_ecf_code', 'e');
    $query->addField('e', 'entity_id', 'pid');
    $query->condition('e.field_ecf_code_value', $ecfCode);
    $query->innerJoin('league_player', 'p', "p.org=$org");
    $query->where('p.pid=e.entity_id');
    $result = $query->execute();
    foreach( $result as $row) {
      $pid = $row->pid;
      return $pid;
    }
    return $pid;
  }

  public function addPlayers($lid, $org, $players) {
    foreach($players as $ecfCode) {
      if(isset($ecfCode) && strlen($ecfCode) > 6) {
        $pid = $this->getPid($ecfCode,$org);
        if($pid !=0 ) {
          league_player_add_to_list($pid, $lid);
        }
      }
    }
  }

  public function __construct(array $arguments) {
    parent::__construct($arguments);

    // Get any CCK/core fields attached to users.
    $this->sourceFields += $this->version->getSourceFields(
      'league_player_list', 'league_player_list');

    // Select player lists from D6 database as source.
    $this->source = new MigrateSourceSQL($this->query(), $this->sourceFields,
      NULL, $this->sourceOptions);
    // We migrate into player lists.
    $this->destination = new MigrateDestinationPlayerList();

    // We instantiate the MigrateMap
    $this->map = new MigrateSQLMap($this->machineName,
        array(
          'nid' => array( 'type' => 'int', 
                          'unsigned' => TRUE, 
                          'description' => 'list id',
                       ),
        ),
        MigrateDestinationPlayerList::getKeySchema()
      );

    // Finally we add simple field mappings 

    $this->addFieldMapping('name', 'name');
    $this->addFieldMapping('org', 'org')
         ->sourceMigration('LeagueOrg');

  }

  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    $name = $row->name;
    if(isset($name) && strlen($name) > 1 && strlen($name) < 21 ) {
      $row->name = $name;
    } else {
      $row->name = "NOT SET";
    }
    return TRUE;
  }

}
