<?php
/**
 * @file
 * league_demo_feature.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function league_demo_feature_user_default_roles() {
  $roles = array();

  // Exported role: football.
  $roles['football'] = array(
    'name' => 'football',
    'weight' => 3,
  );

  // Exported role: table tennis.
  $roles['table tennis'] = array(
    'name' => 'table tennis',
    'weight' => 4,
  );

  return $roles;
}
