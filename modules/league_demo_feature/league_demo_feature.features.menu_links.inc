<?php
/**
 * @file
 * league_demo_feature.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function league_demo_feature_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_admin:menutoken/519bca8cd839f
  $menu_links['main-menu_admin:menutoken/519bca8cd839f'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'menutoken/519bca8cd839f',
    'router_path' => 'menutoken/%',
    'link_title' => 'Admin',
    'options' => array(
      'menu_token_link_path' => 'node/[league:org]/league',
      'menu_token_data' => array(),
      'menu_token_options' => array(
        'clear' => 0,
      ),
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_admin:menutoken/519bca8cd839f',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -43,
    'customized' => 1,
  );
  // Exported menu link: main-menu_fixtures:menutoken/519bca8cca44b
  $menu_links['main-menu_fixtures:menutoken/519bca8cca44b'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'menutoken/519bca8cca44b',
    'router_path' => 'menutoken/%',
    'link_title' => 'Fixtures',
    'options' => array(
      'menu_token_link_path' => 'node/[league:org]/ofixtures',
      'menu_token_data' => array(),
      'menu_token_options' => array(
        'clear' => 0,
      ),
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_fixtures:menutoken/519bca8cca44b',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: main-menu_home:menutoken/519bca8cc9575
  $menu_links['main-menu_home:menutoken/519bca8cc9575'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'menutoken/519bca8cc9575',
    'router_path' => 'menutoken/%',
    'link_title' => 'Home',
    'options' => array(
      'menu_token_link_path' => 'node/[league:org]/home',
      'menu_token_data' => array(),
      'menu_token_options' => array(
        'clear' => 0,
      ),
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_home:menutoken/519bca8cc9575',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: main-menu_league-tables:menutoken/519bca8ccadeb
  $menu_links['main-menu_league-tables:menutoken/519bca8ccadeb'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'menutoken/519bca8ccadeb',
    'router_path' => 'menutoken/%',
    'link_title' => 'League Tables',
    'options' => array(
      'menu_token_link_path' => 'node/[league:org]/tables',
      'menu_token_data' => array(),
      'menu_token_options' => array(
        'clear' => 0,
      ),
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_league-tables:menutoken/519bca8ccadeb',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: main-menu_logout:user/logout
  $menu_links['main-menu_logout:user/logout'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'user/logout',
    'router_path' => 'user/logout',
    'link_title' => 'Logout',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_logout:user/logout',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -40,
    'customized' => 1,
  );
  // Exported menu link: main-menu_previous-seasons:menutoken/519bca8cd2486
  $menu_links['main-menu_previous-seasons:menutoken/519bca8cd2486'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'menutoken/519bca8cd2486',
    'router_path' => 'menutoken/%',
    'link_title' => 'Previous Seasons',
    'options' => array(
      'menu_token_link_path' => 'node/[league:org]/previous',
      'menu_token_data' => array(),
      'menu_token_options' => array(
        'clear' => 0,
      ),
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_previous-seasons:menutoken/519bca8cd2486',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Exported menu link: main-menu_reports:menutoken/519bca8cda410
  $menu_links['main-menu_reports:menutoken/519bca8cda410'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'menutoken/519bca8cda410',
    'router_path' => 'menutoken/%',
    'link_title' => 'Reports',
    'options' => array(
      'menu_token_link_path' => 'node/[league:org]/reports',
      'menu_token_data' => array(),
      'menu_token_options' => array(
        'clear' => 0,
      ),
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_reports:menutoken/519bca8cda410',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -44,
    'customized' => 1,
  );
  // Exported menu link: main-menu_team-captains:menutoken/519bca8cd1a96
  $menu_links['main-menu_team-captains:menutoken/519bca8cd1a96'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'menutoken/519bca8cd1a96',
    'router_path' => 'menutoken/%',
    'link_title' => 'Team Captains',
    'options' => array(
      'menu_token_link_path' => 'node/[league:org]/captains',
      'menu_token_data' => array(),
      'menu_token_options' => array(
        'clear' => 0,
      ),
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_team-captains:menutoken/519bca8cd1a96',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Admin');
  t('Fixtures');
  t('Home');
  t('League Tables');
  t('Logout');
  t('Previous Seasons');
  t('Reports');
  t('Team Captains');


  return $menu_links;
}
