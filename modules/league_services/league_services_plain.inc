<?php

/**
 * @file
 * Functions for league services as plain html to include in other web sites
 */

function league_services_plain_html ($type, $org, $parm) {
  $valid = TRUE;
  $data = array('name' => $parm, 'org' => $org);
  switch($type) {
    case 'table' : $output = league_service_table($data);
                   break;
    case 'club'  : $output = league_service_club($data);
                   break;
    case 'event' : $output = league_service_event($data);
                   break;
    case 'match' : $output = league_service_match($data);
                   break;
    default :      $valid = FALSE;
                   $error = "type of $type not valid, must be table, event, match or club";
  }

  $node = node_load($org);
  if(is_object($node)) {
    if($node->type == 'organisation') {
      $orgname = $node->title;
    } else {
      $error = "node $org of wrong type. Not a valid organisation";
      $valid = FALSE;
    }
  } else {
    $error = "node $org is not a node. Not a valid organisation";
    $valid = FALSE;
  }

  if(!$valid) {
    $content['error']['#type'] = 'markup';
    $content['error']['#markup'] = "<h2>ERROR type of $type not valid, must be table, event, match or club</h2>";
    return $content;
  }

  if(isset($output[0]) && isset($output[0]['title']) ) {
    if($type == 'match') {
      $content['title']['#type'] = 'markup';
      $content['title']['#markup'] = "<h1>ECF LMS - $orgname - $parm</h1>";
      $count=0;
      foreach($output as $table) {
        $content[$count]['#table'] = $table;
        $content[$count]['#theme'] = 'league_table';
        $count++;
      }
    } else {
      $table = $output[0];
      $title = $table['title'];
      $content['title']['#type'] = 'markup';
      $content['title']['#markup'] = "<h1>ECF LMS - $orgname</h1><h2>$title</h2>";

      // It would be better to check the type of event here.
      // The data returned is different for knockouts.
      if(isset($table['data']) ) {
        $content['table']['#table'] = $table;
        $content['table']['#theme'] = 'league_table';
      } else {
        $content['table']['#data'] = $table;
        $content['table']['#theme'] = 'league_knockout_table';
      }
    }
  } else {
    $content['newp']['#type'] = 'markup';
    $content['newp']['#markup'] = "<h2>ERROR no data for event. Is event name correct ?</h2>";
  }
  return $content;
}
