// Javascript for calling league_service
//
function Lmsjs(url) {
  this.league_base_url = url;
}
Lmsjs.prototype = {
  contructor: Lmsjs,
  //
  // Convert json data from service to html
  //
  leagueHtml:function(json) {
    html = ' ';
    for(var i=0; i<json.length; i++) {
      html += this.leagueHtmlTable(json[i]);
    }
    return html;
  },
  //
  // Convert json data from service to html
  //
  leagueHtmlTable:function (json) {
    header = json.header;
    data = json.data;
    title = json.title;
    html = '<h2>' + title + '</h2>';
    html += '<table border>';
    // Header
    html += '<tr>';
    for(var i=0; i<header.length; i++) {
      html += '<th>' + header[i] + '</th>';
    }
    html += '</tr>';
    // Data
    for(var i=0; i<data.length; i++) {
      row = data[i];
      html += '<tr>';
      // Assume first item is a heading
      html += '<th>' + row[0] + '</th>';
      for(var j=1; j<row.length; j++) {
        html += '<td>' + row[j] + '</td>';
      }
      html += '</tr>';
    }
    html += '</table>';
    return html;
  },
  leagueRequest:function (lms, url, org, eventName, selector) {
    // Send an AJAX request to the server
    $.ajax({
       url: url,
       type: "POST",
       crossDomain: true,
       data: { org: org, name : eventName },
       dataType: "json",          // Type of content expected response
       error: function (xhr, ajaxOptions, thrownError) {
         alert('ajax error status:' + xhr.status);
         alert('ajax error:' + thrownError);
       },
       success: function(data) {
       //alert('Ajax 2 returned '+data);
         html = lms.leagueHtml(data, eventName);
         $(selector).html(html);
       }
    });
  },
  leagueResults:function (org, eventName, selector) {
    url = this.league_base_url +  "match";
    this.leagueRequest(this, url, org, eventName, selector);
  },
  leagueTable:function (org, eventName, selector) {
    url = this.league_base_url +  "table";
    this.leagueRequest(this, url, org, eventName, selector);
  },
  leagueClub:function (org, eventName, selector) {
    url = this.league_base_url +  "club";
    this.leagueRequest(this, url, org, eventName, selector);
  },
  leagueEvent:function (org, eventName, selector) {
    url = this.league_base_url +  "event";
    this.leagueRequest(this, url, org, eventName, selector);
  }
}
