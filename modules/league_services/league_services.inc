<?php
// $Id$
/**
 * @file
 * League services.
 */

/**
 * Implements hook_services_resources().
 */
 
function league_services_services_resources() {
  $resources = array (
     'league' => array(
       'actions' => array(
         'table' => array(
           'help' => 'League Table',
           'callback' => 'league_service_table',
           'access callback' => 'league_services_access',
           'args' => array(
             array(
               'name' => 'name',
               'type' => 'string',
               'optional' => FALSE,
               'source' => 'data',
               'description' => t('Name of the event.'),
             ),
           ),
         ),
         'club' => array(
           'help' => 'Club Fixtures',
           'callback' => 'league_service_club',
           'access callback' => 'league_services_access',
           'args' => array(
             array(
               'name' => 'ecfcode',
               'type' => 'string',
               'optional' => FALSE,
               'source' => 'data',
               'description' => t('Club ecfcode.'),
             ),
           ),
         ),
         'event' => array(
           'help' => 'Event Fixtures',
           'callback' => 'league_service_event',
           'access callback' => 'league_services_access',
           'args' => array(
             array(
               'name' => 'name',
               'type' => 'string',
               'optional' => FALSE,
               'source' => 'data',
               'description' => t('Event name.'),
             ),
           ),
         ),
         'match' => array(
           'help' => 'Match Results',
           'callback' => 'league_service_match',
           'access callback' => 'league_services_access',
           'args' => array(
             array(
               'name' => 'name',
               'type' => 'string',
               'optional' => FALSE,
               'source' => 'data',
               'description' => t('Event name.'),
             ),
           ),
         ),
       ),
     ),
  );
  return $resources;
}
