<?php
/**
 * @file
 * Players with no grade in player lists
 */

/**
 * Players with local grades
 */
function league_chess_reports_nograde($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);
  // Set title
  $settings = league_chess_national_settings();
  $nat_name = $settings['national_rating_name'];
  $title = $node->title . " - Players with no grades - $nat_name or Local";
  drupal_set_title($title);

  $content['newly']['#table'] = league_chess_no_graded_players($node);
  $content['newly']['#theme'] = 'league_table';

  // Ensure if a player is picked on, the Back to list tab works.
  $_SESSION['league_player_list_url'] = $_GET['q'];

  return $content;
}

function league_chess_no_graded_players_query($org) {
  $settings = league_chess_national_settings();
  $rlid = $settings['standard_rlid'];

  $query = db_select('league_player', 'p');
  $query->fields('p', array('pid', 'firstname', 'lastname', 'chess_rating', 'ecf_code'));
  // In a list in the organisaton
  $query->innerJoin('league_player_player', 'lpp', 'lpp.pid = p.pid');
  $query->innerJoin('league_player_list', 'lpl', 'lpl.lid = lpp.lid');
  $query->condition('lpl.org', $org);
  // players without local grades
  $query->where("p.chess_rating IS NULL OR p.chess_rating=0");
  // Check the grading list.
  $query->leftjoin('rating_player', 'rp', "rp.pid = p.ecf_code AND rp.rlid=$rlid");
  $query->where("rp.CAT IS NULL OR rp.CAT='P'");
  $query->addField('rp', 'rating', 'grade');
  $query->addField('rp', 'cat', 'cat');

  return $query;
}

/**
 * Get players with no grades.
 */

function league_chess_no_graded_players($node) {

  $settings = league_chess_national_settings();
  $nat_code = $settings['national_rating_code'];
  $nat_name = $settings['national_rating_name'];
  $org = $node->nid;
  $header = array();
  $header[] = array('data' => 'First Name', 'field' => 'firstname' );
  $header[] = array('data' => 'Last Name', 'field' => 'lastname' );
  $header[] = array('data' => 'Local Grade', 'field' => 'chess_rating' );
  $header[] = array('data' => $nat_code, 'field' => 'ecf_code' );
  $header[] = array('data' => $nat_name . ' Grade', 'field' => 'grade' );
  $header[] = array('data' => 'Cat', 'field' => 'cat', 'sort'=>'desc' );

  $rows = array();

  $query = league_chess_no_graded_players_query($org);

  // Make table sortable
  $query = $query->extend('TableSort')->orderByHeader($header);
//drupal_set_message(print_r((string)$query,TRUE));

  //dsm((string)$query);
  $queryResult = $query->execute();

  foreach ( $queryResult as $row ) {
    $pid = $row->pid;
    $firstname = l("$row->firstname","league_player/$pid");
    $lastname = $row->lastname;
    $lgrade = $row->chess_rating;
    $ecfcode = $row->ecf_code;
    $grade = $row->grade;
    $cat = $row->cat;
    $rows[] = array($firstname, $lastname, $lgrade, $ecfcode, $grade, $cat);
  }

  $table['data'] = $rows;
  $table['header'] = $header;

  return $table;
}

function league_chess_count_no_grade_players($org) {
  $cache_name = "league_chess_count_no_grade_players_$org";
  $num_rows = &drupal_static($cache_name);
  if (!isset($num_rows)) {
    if( $cache = cache_get($cache_name)) {
      $num_rows = $cache->data;
    } else {
      $query = league_chess_no_graded_players_query($org);
      //dsm((string)$query);
      $num_rows = $query->countQuery()->execute()->fetchField();
      cache_set($cache_name, $num_rows, 'cache');
    }
  }
  return $num_rows;
}
