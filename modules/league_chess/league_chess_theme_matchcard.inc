<?php
// $Id$
/**
 * @file
 * Functions for theme matchcard
 */

function theme_league_chess_matchcard_header($variables) {
  $element = $variables['element'];
  $data = $element['#data'];

  $output = '<div class=league-frame>';
  $etitle = l($data['eventName'],'league_comp/' . $data['event'] . '/efixtures');
  $date = date( 'D jS M Y',($data['date']));
  if(strlen($date) < 2 || $data['date_option'] == 'N') {
    $date = 'not set';
  }
  $output .= "<strong>ENTER MATCH RESULT - EVENT:</strong> ".$etitle." <strong>DATE:</strong> " . $date;
  $output .= "<table border>";
  $output .= "<tr><th>Board</th>";
  if($data['admin']) {
    $output .= "<th>Game Ref</th>";
  }
  $output .= "<th>Colour</th><th>Home</th><th>";
  $home = $data['homeTeamName'];
  $away = $data['awayTeamName'];
  $output .= l($home,'node/' . $data['homeList']);
  $output .= "</th><th align=center>V</th><th>Away</th><th>";
  $output .= l($away,'node/' . $data['awayList']);
  $output .= "</th>";
  if($data['admin']) {
    $output .= "<th></th>";
  }
  $output .= "</tr>\n";

  return $output;
}

function theme_league_chess_results_header($variables) {
  $element = $variables['element'];
  $data = $element['#data'];
  $date = date( 'D jS M Y',($data['date']));
  if(strlen($date) < 2 || $data['date_option'] == 'N') {
    $date = 'not set';
  }

  $output = '<div class=league-frame>';
  $etitle = l($data['eventName'],'league_comp/' . $data['event']);
  $output .= "<strong>ENTER MATCH RESULT - EVENT:</strong> ".$etitle." <strong>DATE:</strong> " . $date;
  $output .= "<table border>";
  $output .= "<tr><th>Board</th><th>Date</th><th>White</th>";
  $output .= "<th align=center>V</th><th>Black</th>";
  $output .= "</th><th></tr>\n";

  return $output;
}

function theme_league_chess_matchcard_footer($variables) {
  $element = $variables['element'];
  $data = $element['#data'];

  $output = "</table><p>Last updated by " . $data['updater'] . " at " . $data['updateTime'];
  $output .= '</div>';

  return $output;
}

function theme_league_chess_matchcard_view($variables) {
  $element = $variables['element'];
  $data = $element['#data'];
  $results = $element['#results'];
  $date = date( 'D jS M Y',$data['datetime']);
  $time = date('H:i',$data['datetime']);
  if(strlen($date) < 2 || $data['date_option'] == 'N') {
    $date = 'not set';
    $time = ' ';
  }

  $output = '<div class=league-frame>';
  if( league_event_type_individual($data['eventType']) ) {
    $etitle = l($data['eventName'],'league_comp/' . $data['event'] . '/rounds');
    $round = $data['round'];
    $output .= "<strong>MATCH RESULT Round </strong>$round <strong>EVENT:</strong> ".$etitle." <strong>DATE:</strong> " . $date . " <strong>TIME:</strong> " . $time;
  } else {
    $etitle = l($data['eventName'],'league_comp/' . $data['event'] . '/efixtures');
    $output .= "$etitle $date";
    if( $data['homeTeam'] == 0 || $data['awayTeam'] == 0 ) {
      $output .= " No Match Card because match does not have 2 teams";
      return $output;
    }
  }
  // Verify Button
  $options = array (
    'attributes' => array (
         'class' => array('button', 'form-submit'),
         'title' => 'Verify Match Result',
//       'onclick' => "jQuery(this).addClass('ajax-progress-throbber');;",
    ),
    'html' => TRUE,
  );
  $button = l('Verify', 'league_fixture/' . $data['nid'] . '/verify', $options);
  $output .= '&nbsp;&nbsp;&nbsp;' .  $button;


  $pdfLink = l('PDF', 'league_chess_matchcard/' . $data['nid'] . '/pdf');
  $xlLink = l('XL', 'league_chess_matchcard/' . $data['nid'] . '/xl');
  $pgnlink = l('PGN', 'league_chess_matchcard/' . $data['nid'] . '/pgn');
  $fidelink = l('FIDE', 'league/fidefile/' . $data['nid']);
  $output .= '<div class="league-plink">' . "$pdfLink | $xlLink | $pgnlink | $fidelink" . '</div>';
  $visible = $data['visible'];
  
  if($visible || ( $data['all'] && league_check_access($data['event']) ) ) {
    $output .= league_chess_matchcard_html($results,$data);
  } else {
    $reason = $data['visibile_reason'];
    $output .= "<p><b>MATCH PAIRINGS NOT YET VISIBLE</b><p>$reason</p>";
    if(!$data['all']) {
      $output .= l('View Pairings (admin only)', 'league_fixture/' . $data['nid'] . '/all');
      $output .= '<p>';
    }
  }

  // Put comment flag on for admins
  if(league_check_access($data['event'])) {
    if(!$visible) {
      $output .= '<b>MATCH ONLY VISIBLE TO OWNERS</b>';
    }
    $flag_link = flag_create_link('commented_fixtures',$data['nid']);
  } else {
    $flag_link = ' ';
  }
  $updaterLink = l($data['updater'], 'user/' . $data['updaterUid'] . '/contact' );
  $reporterLink = l($data['reporter'], 'user/' . $data['reporterUid'] . '/contact' );
  $verifierLink = l($data['verifier'], 'user/' . $data['verifierUid'] . '/contact' );
  $updateTime = $data['updateTime'];
  $reportTime = $data['reportTime'];
  $verifyTime = $data['verifyTime'];
  $output .= "<p>Last update $updaterLink $updateTime. Reported by $reporterLink $reportTime. Verified By $verifierLink $verifyTime<p> $flag_link";
  $output .= '</div>';
  return $output;
}

function league_chess_matchcard_html_header($data) {
  $output = ' ';
  $home = $data['homeTeamName'];
  $away = $data['awayTeamName'];
  $output .= "<tr><th>Board</th><th>Home</th><th>";
  $output .= l($home,'league_team/' . $data['homeTeam']);
  $output .= "</th><th align=center></th><th>";
  $output .= l($away,'league_team/' . $data['awayTeam']);
  $output .= "</th><th>Away</th>";
  $output .= "</tr>\n";
  return $output;
}

function league_chess_matchcard_swiss_score($pid, $data) {
  $output = '';
  if($data['eventType'] == 'S') {
    $score = '0';
    if(isset($data['swiss_scores'][$pid]) ) {
      $score = league_score2points($data['swiss_scores'][$pid]);
    }
    $output = ' (' . $score . ')';
  }
  return $output;
}

function league_chess_matchcard_html($results,$data) {
  $output = "<table border class=card>";
  if(league_event_type_individual($data['eventType']) ) {
    $output .= "<tr><th class=cardb>Board</th><th>Date</th><th>Grade</th><th>White</th><th align=center>V</th><th>Black</th><th>Grade</th></tr>\n";
  } else {
    $output .= league_chess_matchcard_html_header($data);
  }
  $show = $data['brate'];
  $rate = $data['rate'];
  $date = $data['date'];
  $platform = league_chess_event_value($data['event'], 'platform');

  $homeGrade = 0;
  $awayGrade = 0;
  $odd = 'odd';
  $matchSize = $data['boards'];
  if($data['twomatch'] == 'B' || $data['twomatch'] == 'T') {
    $matchSize = $matchSize / 2;
  }

  for($board=0; $board< $data['boards']; $board++)
  {
    if($odd == 'odd') {
      $odd = 'even';
    } else {
      $odd = 'odd';
    }
    $boardNum = $board+1;
    $boardLabel = $boardNum;
    // The twomatch option implies 2 mini matches, so we need to start
    // again at board 1 and put a gap between them.
    if($data['twomatch'] == 'T' || $data['twomatch'] == 'B') {
      if($boardNum > $matchSize) {
        $boardLabel = $boardNum - $matchSize;
      }
      // Put a gap, then an intermediate totals record
      if($boardNum == $matchSize+1) {
        $output .= league_chess_matchcard_adjustment_html($data['homeAdjustment'], $data['awayAdjustment'], $data['handicap']);
        $output .= league_chess_matchcard_penalty_html($data['homePenalty'], $data['awayPenalty']);
	$output .= league_chess_matchcard_total_html($data,$homeGrade,$awayGrade,$data['homeScore1'],$data['awayScore1']);
	// Someone has created an individual all play all event with two matches!
	// Acomb Chess Club
        if(league_event_type_individual($data['eventType']) ) {
	  $output .= "<tr><th class=cardb>Board</th><th>Date</th><th>Grade</th><th>White</th><th align=center>V</th><th>Black</th><th>Grade</th></tr>\n";
	} else {
	  $output .= league_chess_matchcard_html_header($data);
	}
        $homeGrade=0;
        $awayGrade=0;
      }
    }

    $whiteGrade = $results[$board]['whiteGrade'];
    $blackGrade = $results[$board]['blackGrade'];
    $whiteGradeNumeric = league_chess_strip_star($whiteGrade,$rate);
    $blackGradeNumeric = league_chess_strip_star($blackGrade,$rate);
    if($show != 0) {
      $previousWhiteGrade = $results[$board]['previousWhiteGrade'];
      $previousBlackGrade = $results[$board]['previousBlackGrade'];
    }

    if($rate == 0) {
      $whiteGrade = ' ';
      $blackGrade = ' ';
    } else {
      $homeGrade += $whiteGradeNumeric;
      $awayGrade += $blackGradeNumeric;
    }
    if( $data['eventType'] == 'J' || $data['eventType'] == 'K' || $data['eventType'] == 'L') {
      $colour = '(' . $results[$board]['colour'] . ')';
      $gameLink = ' ';
      if($data['showOnlineID']) {
        $gameLink = league_chess_online_icon($results[$board], $platform, 'game');
      }
      $output .= "<tr class=$odd><td>$boardLabel $colour $gameLink </td><td>";
    } else {
      $rdate = $results[$board]['date'];
      if(!isset($rdate) ) {
        $rdate = $date;
      }
      $output .= "<tr class=$odd><td>$boardLabel </td><td>$rdate</td><td>";
    }
    $output .= $whiteGrade;
    if($show != 0) {
      $output .= ' (' . $previousWhiteGrade . ')';
    }
    $output .= "</td><td>";
    if($results[$board]['whiteUid'] != 0) {
      $user = user_load($results[$board]['whiteUid']);
      $output .= theme('user_picture', array('account' =>$user ,'style_name' => 'thumbnail')); 
    }
    $white = $results[$board]['whiteName'];
    if(isset($white)) {
      $output .= league_chess_matchcard_membership($results[$board]['whiteMemb']);
      if($results[$board]['white'] < 0) {
        if($results[$board]['white'] == LEAGUE_CHESS_NEWPLAYER) {
          $output .= l($white,'league_fixture/' . $data['nid'] . '/hlist');
        } else {
          $output .= $white;
        } 
      } else {
        $output .= l($white,'league_player/' . $results[$board]['white']);
        if($data['showOnlineID']) {
          $output .= league_chess_online_icon($results[$board], $platform, 'whiteLid');
        }
        $output .= league_chess_matchcard_swiss_score($results[$board]['white'], $data);
      }
    } else {
      $output .= "&nbsp;";
    }
    $output .= "</td><td>";
    $result = league_chess_result2score($results[$board]['result']);
    if(isset($result))
      $output .= $result;
    else
      $output .= "&nbsp;";
    $output .= "</td><td>";
    if($results[$board]['blackUid'] != 0) {
      $user = user_load($results[$board]['blackUid']);
      $output .= theme('user_picture', array('account' =>$user ,'style_name' => 'thumbnail' )); 
    }
    $black = $results[$board]['blackName'];
    if(isset($black)) {
      $output .= league_chess_matchcard_membership($results[$board]['blackMemb']);
      if($results[$board]['black'] < 0) {
        if($results[$board]['black'] == LEAGUE_CHESS_NEWPLAYER) {
          $output .= l($black,'league_fixture/' . $data['nid'] . '/alist');
        } else {
          $output .= $black;
        } 
      } else {
        $output .= l($black,'league_player/' . $results[$board]['black']);
        $output .= league_chess_matchcard_swiss_score($results[$board]['black'], $data);
        if($data['showOnlineID']) {
          $output .= league_chess_online_icon($results[$board], $platform, 'blackLid');
        }
      } 
    } else {
      $output .= "&nbsp;";
    }
    $output .= "</td><td>";
    $output .= $blackGrade;
    if($show != 0) {
      $output .= ' (' . $previousBlackGrade . ')';
    }
    $output .= "</td></tr>";
  }
  if( $data['eventType'] == 'J' || $data['eventType'] == 'K' || $data['eventType'] == 'L' ) {
    if($data['twomatch'] == 'B' || $data['twomatch'] == 'T') {
      $output .= league_chess_matchcard_adjustment_html($data['homeAdjustment2'], $data['awayAdjustment2'], $data['handicap']);
      $output .= league_chess_matchcard_penalty_html($data['homePenalty2'], $data['awayPenalty2']);
      $output .= league_chess_matchcard_total_html($data,$homeGrade,$awayGrade,$data['homeScore2'],$data['awayScore2']);
    } else {
      $output .= league_chess_matchcard_adjustment_html($data['homeAdjustment'], $data['awayAdjustment'], $data['handicap']);
      $output .= league_chess_matchcard_penalty_html($data['homePenalty'], $data['awayPenalty']);
      $output .= league_chess_matchcard_total_html($data,$homeGrade,$awayGrade,$data['homeScore'],$data['awayScore']);
    }
  }
  $output .= "</table>";

  return $output;
}

function league_chess_matchcard_adjustment_html($homeAdjustment, $awayAdjustment, $handicap) {
  $output = ' ';
  $adjustment = 'Adjustment';
  if($handicap != 'N') {
    $adjustment = 'Handicap';
  }
  if( $homeAdjustment != 0 || $awayAdjustment != 0 ) {
    $output .= "<tr><td>$adjustment</td><td></td><td></td><td>";
    $output .= league_score2points($homeAdjustment);
    $output .= ' - ';
    $output .= league_score2points($awayAdjustment);
    $output .= '</td><td></td><td></td></tr>';
  }
  return $output;
}

function league_chess_matchcard_penalty_html($homePenalty, $awayPenalty) {
  $output = ' ';
  if( $homePenalty > 0 || $awayPenalty > 0 ) {
    $output .= '<tr><td>Penalty</td><td></td><td></td><td>';
    $output .= league_score2points($homePenalty);
    $output .= ' - ';
    $output .= league_score2points($awayPenalty);
    $output .= '</td><td></td><td></td></tr>';
  }
  return $output;
}

function league_chess_matchcard_total_html($data,$homeGrade,$awayGrade,$homeScore,$awayScore) {
  $label = 'Total';
  $grade_limit_option = league_chess_org_value($data['org'], 'grade_limit');
  if($grade_limit_option == 'A' || $grade_limit_option == 'U') {
    // Round up so its clear that anything above the average exceeds the limit
    $homeGrade = ceil($homeGrade / $data['boards'] );
    $awayGrade = ceil($awayGrade / $data['boards'] );
    $label = 'Average';
  }
  $total = league_score2points($homeScore) . ' - ' . league_score2points($awayScore);
  $output = "<tr><td>$label</td><td>$homeGrade</td><td></td><td>$total</td>" . '<td style="text-align: right;">' . "$label</td><td>$awayGrade</td></tr>";
  return $output;
}

// Show the membership status on the match card.
// TODO  This is ECF specific so would be better in the league_ecf module if
//       possible.

function league_chess_matchcard_membership($cat) {
  $output = '<div class="';
  switch($cat) {
    case 'N' : $output .= 'membership-none';
               break;
    case 'G' : $output .= 'membership-gold';
               break;
    case 'B' : $output .= 'membership-bronze';
               break;
    case 'S' : $output .= 'membership-silver';
               break;
    case 'P' : $output .= 'membership-platinum';
               break;
    case 'E' : $output .= 'membership-supporter';
               break;
    case 'L' : $output .= 'membership-life';
               break;
    default  : $output .= 'membership-none';
               $cat = 'N';
  }
  $output .= '">';
  $output .= "$cat</div> ";
  return $output;
}

function league_chess_online_icon($result, $platform, $target='game') {
  if($target=='game') {
    if(!isset($result['whiteLid']) || !isset($result['blackLid']) ) {
      return '';
    }
    $whiteId = $result['whiteLid'];
    $blackId = $result['blackLid'];
    if($whiteId == ' ' || $blackId == ' ') {
      return '';
    }
    $title = 'game url';
    if($platform == 'C') {
      $tv_url = 'https://www.chess.com/live/#m=' . $whiteId . '%7C' . $blackId;
    } else {
      return '';
    }
  } else {
    if(!isset($result[$target]) || $result[$target] == ' ') {
      return '';
    }
    $onlineId = $result[$target];
    $title = league_chess_platform_option_label($platform) . ' url';
    $external_url = 'https://lichess.org/@/' . $onlineId;
    $tv_url = 'https://lichess.org/@/' . $onlineId . '/tv';
    if($platform == 'C') {
      $external_url = 'https://www.chess.com/member/' . $onlineId;
      $tv_url = 'https://www.chess.com/member/' . $onlineId;
    }
    if($platform == 'T') {
      $tv_url = 'https://tornelo.com/chess/orgs/' . $onlineId;
    }
  }
  $options = array('absolute' => TRUE, 'attributes' => array('title' => $title,'target'=>'_blank'), 'html' => TRUE,);
  $tv_image = file_create_url(drupal_get_path('module', 'league_chess') . '/lichesstv.png');
  $link = l('<img src = "' . $tv_image . '" />', $tv_url, $options);
  if($target!='game' && $platform != 'T') {
    $options['attributes']['html'] = FALSE;
    $link .= l($onlineId, $external_url, $options);
  }
  return $link;
}
