<?php

/**
 * @file
 * Functions for league_chess new player matches option.
 */
  
function league_chess_admin_tab_newpmatch ($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);
  // Set title
  $title = $node->title . ' - New Player matches';
  drupal_set_title($title);

  $content['newp']['#table'] = league_chess_newp_matches($node);
  $content['newp']['#theme'] = 'league_table';
  return $content;
}

function league_chess_newp_matches_query($org) {

  // Results with New Player
  $query = db_select('league_chess_result', 'r');
  $newp = LEAGUE_CHESS_NEWPLAYER;
  $query->where("r.white=$newp OR r.black=$newp");
  $query->innerJoin('league_match', 'm', 'm.mid = r.mid');
  $query->addField('m', 'fid', 'fid');
  $query->innerJoin('node', 'n', 'n.nid = m.fid');
  // Their Fixture/Event
  $query->innerJoin('league_fixture', 'f', 'f.nid = n.nid');
  $query->addField('f', 'date', 'date');
  // Event Title
  $query->innerJoin('node', 'e', 'e.nid = f.cid');
  $query->addField('e', 'title', 'event');
  // Fixture Title
  $query->innerJoin('node', 'ht', 'ht.nid = f.home_team');
  $query->innerJoin('node', 'at', 'at.nid = f.away_team');
  $query->addField('ht', 'title', 'home');
  $query->addField('at', 'title', 'away');
  // Active Seasons
  $query->innerJoin('league_competition', 'comp', 'comp.cid = f.cid');
  $query->innerJoin('league_event', 'le', 'le.eid = comp.eid');
  $query->condition('le.lms', 'A');
  $query->addField('le', 'name', 'seasonName');
  //  events for that organisation
  $query->condition('le.org', $org);
  // Report each event once
  $query->distinct();
  //dsm((string)$query);
 
  return $query;
}

function league_chess_newp_matches($node) {

  $query = league_chess_newp_matches_query($node->nid);

  $header = array('Fixture', 'Event', 'Date' );
  $rows = array();
  $result = $query->execute();

  foreach( $result as $row) {
    $title = $row->home . ' v ' . $row->away;
    $fixture = l($title, "league_fixture/$row->fid");
    $date = date('D jS M Y',$row->date);
    $rows[] = array($fixture, $row->event, $date);
  }

  $table['data'] = $rows;
  $table['header'] = $header;
  return $table;
}

function league_chess_count_new_player_matches($org) {
  $cache_name = "league_chess_count_new_player_matches_$org";
  $num_rows = &drupal_static($cache_name);
  if (!isset($num_rows)) {
    if( $cache = cache_get($cache_name)) {
      $num_rows = $cache->data;
    } else {
      $query = league_chess_newp_matches_query($org);
      //dsm((string)$query);
      $num_rows = $query->countQuery()->execute()->fetchField();
      cache_set($cache_name, $num_rows, 'cache');
    }
  }
  return $num_rows;
}
