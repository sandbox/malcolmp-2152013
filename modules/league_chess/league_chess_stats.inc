<?php

/**
 * @file
 * Functions for league_chess new player matches option.
 */

class LeagueStatsSorter {
  public function __construct($sort,$order,$header) {
    if($sort == 'asc') {
      $this->sign = 1;
    } else {
      $this->sign = -1;
    }
    $field = 0;
    $this->char = FALSE;
    foreach($header as $row) {
      if($row['data'] == $order) {
        $field = $row['field'];
        $this->char = $row['char'];
      }
    }
    $this->order = intval($field);
  }

  public function sort(&$data) {
    if($this->char) {
      uasort( $data, array($this, "sortChar") );
    } else {
      uasort( $data, array($this, "sortNum") );
    }
  }

  public function sortNum($fa, $fb) {
    if ( $fa[$this->order] < $fb[$this->order] ) {
      return $this->sign;
    }
    if ( $fa[$this->order] > $fb[$this->order] ) {
      return -1 * $this->sign;
    }
    return 0;
  }

  public function sortChar($fa, $fb) {
    $cmp = strnatcmp($fa[$this->order],$fb[$this->order]) * $this->sign;
//drupal_set_message('sortChar ' . $fa[$this->order] . ' ' . $fb[$this->order] . ' cmp ' . $cmp);
    return $cmp;
  }
}

  
function league_chess_team_player_stats_tab ($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);
  // Set title
  $title = "$node->title";
  drupal_set_title($title);

  $table = league_chess_team_stats_data($node);
  $content['stats']['#table'] = $table;
  $content['stats']['#theme'] = 'league_table';
  $content['stats']['#pdfLink'] = 'link';

  // Check if file to be create via Xl or PDF
  league_table_check_file($table);

  return $content;
}

/**
 * Event Player Stats Tab.
 */
function league_chess_event_player_stats_tab ($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);
  // Set title
  $title = "$node->title - Player Stats";
  drupal_set_title($title);

  $table = league_chess_event_stats_data($node);
  $content['stats']['#table'] = $table;
  $content['stats']['#theme'] = 'league_table';
  $content['stats']['#pdfLink'] = 'link';

  // Check if file to be create via Xl or PDF
  league_table_check_file($table);

  return $content;
}

/**
 * Player Player Stats Tab.
 */
function league_chess_player_player_stats_tab ($player) {
  // Set breadcrumb
  league_player_set_bread_crumb ($player);

  $table = league_chess_player_stats_data($player);
  $content['stats']['#table'] = $table;
  $content['stats']['#theme'] = 'league_table';
  $content['stats']['#pdfLink'] = 'link';

  // Check if file to be create via Xl or PDF
  league_table_check_file($table);

  return $content;
}


/**
 * Utility function used in stats sort
 */
function statSort($fa, $fb) {
  if ( $fa[3] < $fb[3] ) {
    return 1;
  }
  if ( $fa[3] > $fb[3] ) {
    return -1;
  }
  return 0;
}

/**
 * Event Player Stats Data.
 */
function league_chess_event_stats_data($node) {
  $org = league_event_value($node->nid, 'org');

  $query = league_chess_individual_event_query($node);

  $points = array();
  $defaults = array();
  $games = array();
  $players = array();
  $ratings = array();

  //dsm((string)$query);
  $queryResult = $query->execute();

  // Get result and add up scores and total games for each player

  foreach ( $queryResult as $row ) {
    $white = $row->white;
    $black = $row->black;
    $players[$row->white] = "$row->wlast, $row->wfirst | $row->home";
    $players[$row->black] = "$row->blast, $row->bfirst | $row->away";
    $wdl = $row->result;
    league_initialise($games, $white);
    league_initialise($games, $black);
    league_initialise($points, $white);
    league_initialise($points, $black);
    league_initialise($defaults, $white);
    league_initialise($defaults, $black);
    league_initialise($ratings, $white);
    league_initialise($ratings, $black);
    $games[$white]++;
    $games[$black]++;
    league_chess_stats_points($points, $defaults, $white, $black, $wdl);
    if(isset($row->arating) ) {
      $ratings[$white] += league_chess_stats_rating($row->arating);
    }
    if(isset($row->hrating) ) {
      $ratings[$black] += league_chess_stats_rating($row->hrating);
    }
  }

  // Then work out percentages and sort by it.
  $table = league_chess_data2table($games, $points, $defaults, $players, $ratings, TRUE);
  $table['title'] = $node->title;
  return $table;
}

function league_chess_stats_rating($rating) {
  $rating = league_chess_strip_star($rating);
  if( $rating == 0 ) {
    $rating = 1800;
  }
  return $rating;
}

function league_chess_stats_points(&$points, &$defaults, $white, $black, $wdl) {
  if ( $wdl == "W" ) {
    $points[$white] += 2;
  }
  if ( $wdl == "D" ) {
    $points[$white]++;
    $points[$black]++;
  }
  if ( $wdl == "L" ) {
    $points[$black] += 2;
  }
  if ( $wdl == "+" ) {
    $defaults[$white] += 1;
  }
  if ( $wdl == "-" ) {
    $defaults[$black] += 1;
  }
}

/**
 * Team Player Stats Data.
 *  - as for event stats , but only include those for the given team
 */
function league_chess_team_stats_data($node) {
  $event = league_team_value($node, 'event');
  $org = league_event_value($event, 'org');

  // Fixtures in the event
  $query = db_select('league_fixture', 'f');
  $query->condition('f.cid', $event);
  $query->innerJoin('league_competition', 'comp', 'comp.cid = f.cid');
  $query->condition('comp.type', array('L','K','J'), 'in');
  $query->innerJoin('league_match', 'm', 'm.fid = f.nid');
  // Home team
  $query->addField('f', 'home_team', 'homeTeam');
  // Away team
  $query->addField('f', 'away_team', 'awayTeam');
  
  // Results in the fixtures
  $query->innerJoin('league_chess_result', 'r', 'r.mid = m.mid');
  $query->addField('r', 'result', 'result');
  $query->addField('r', 'white', 'white');
  $query->addField('r', 'black', 'black');
  $query->addField('r', 'whiteGrade', 'hrating');
  $query->addField('r', 'blackGrade', 'arating');
  // Exclude games with no result or defaults
  $query->condition('r.result', array('W','D','L'), 'IN');
  // Don't include games where the player is DEFAULT or NEWPLAYER
  $query->condition('r.white', 0, '>');
  $query->condition('r.black', 0, '>');
  // Home Player
  $query->innerJoin('league_player', 'w', 'w.pid = r.white');
  $query->condition('w.org', $org);
  $query->addField('w', 'firstname', 'wfirst');
  $query->addField('w', 'lastname', 'wlast');
  // Away Player
  $query->innerJoin('league_player', 'b', 'b.pid = r.black');
  $query->condition('b.org', $org);
  $query->addField('b', 'firstname', 'bfirst');
  $query->addField('b', 'lastname', 'blast');

  $points = array();
  $games = array();
  $rated = array();
  $defaults = array();
  $players = array();
  $ratings = array();
  $team = $node->nid;

  //dsm((string)$query);
  $queryResult = $query->execute();

  // Get result and add up scores and total games for each player
  // NOTE: need to exclude games not for that team
  foreach ( $queryResult as $row ) {
    $white = $row->white;
    $black = $row->black;
    $players[$row->white] = "$row->wlast, $row->wfirst";
    $players[$row->black] = "$row->blast, $row->bfirst";
    $wdl = $row->result;
    if ( $row->homeTeam == $team ) {
      league_initialise($rated, $white);
      league_initialise($games, $white);
      league_initialise($points, $white);
      league_initialise($defaults, $white);
      league_initialise($ratings, $white);
      $games[$white]++;
      switch ( $wdl ) {
        case  'W' : $points[$white] += 2;
                    $rated[$white]++;
                    break;
        case  'D' : $points[$white]++;
                    $rated[$white]++;
                    break;
        case  'L' : $rated[$white]++;
                    break;
        case  '+' : $defaults[$white]++;
                    break;
        case  '-' : $defaults[$white]++;
                    break;
      }
      // sum of opponents ratings.
      if(isset($row->arating) ) {
        $ratings[$white] += league_chess_stats_rating($row->arating);
      }
    }
    if ( $row->awayTeam == $team ) {
      league_initialise($rated, $black);
      league_initialise($games, $black);
      league_initialise($points, $black);
      league_initialise($defaults, $black);
      league_initialise($ratings, $black);
      $games[$black]++;
      switch ( $wdl ) {
        case  'W' : $rated[$black]++;
                    break;
        case  'D' : $points[$black]++;
                    $rated[$black]++;
                    break;
        case  'L' : $points[$black] += 2;
                    $rated[$black]++;
                    break;
        case  '+' : $defaults[$black]++;
                    break;
        case  '-' : $defaults[$black]++;
                    break;
      }
      // sum of opponents ratings.
      if(isset($row->hrating) ) {
        $ratings[$black] += league_chess_stats_rating($row->hrating);
      }
    }
  }
  // Then work out percentages and sort by it.
  $table = league_chess_data2table($games, $points, $defaults, $players, $ratings);
  $table['title'] = $node->title;
  return $table;
}

/**
 * Create sorted table of stats.
 */
function league_chess_data2table($games, $points, $defaults, $players, $ratings, $split=FALSE) {
  if($split) {
    $header = array(array('data' => 'Name', 'field' => '0', 'char' => TRUE ),
                    array('data' => 'Performance', 'field' => '1', 'char' => FALSE ),
                    array('data' => 'Team', 'field' => '2', 'char' => TRUE ),
                    array('data' => 'Score', 'field' => '3', 'char' => FALSE ),
                    array('data' => 'Games', 'field' => '4', 'char' => FALSE ),
                    array('data' => 'Including Defaults', 'field' => '5', 'sort'=>'asc', 'char' => FALSE ),
                    array('data' => 'Excluding Defaults', 'field' => '6', 'char' => FALSE ),
    );
  } else {
    $header = array(array('data' => 'Name', 'field' => '0', 'char' => TRUE ),
                    array('data' => 'Performance', 'field' => '1', 'char' => FALSE ),
                    array('data' => 'Score', 'field' => '2', 'char' => FALSE ),
                    array('data' => 'Games', 'field' => '3', 'char' => FALSE ),
                    array('data' => 'Including Defaults', 'field' => '4', 'sort'=>'asc', 'char' => FALSE ),
                    array('data' => 'Excluding Defaults', 'field' => '5', 'char' => FALSE ),
    );
  }
  $sort = 'asc';
  $order = 'Including Defaults';
  if( array_key_exists('sort', $_GET) ) {
    $sort = $_GET['sort'];
  }
  if( array_key_exists('order', $_GET) ) {
    $order = $_GET['order'];
  }
  $sorter = new LeagueStatsSorter($sort, $order, $header);

  $rows = array();

  // Then work out percentages and sort by it.

  foreach ($games as $player => $gp) {
    $npoints = $points[$player];
    $ndefaults = $defaults[$player];
    $score = $npoints / 2;
    $from  = $games[$player];
    // Rating performance via the 400 method (see wikipedia)
    // https://en.wikipedia.org/wiki/Elo_rating_system
    // performance = (sum(opponents ratings) + 400(wins-losses)) / games
    $rating  = round( ( $ratings[$player] + 400 * ($npoints - $from)) / $from);

    $percent = round( ( $score / $from ) * 100.0, 2);
    if($from == $ndefaults) { 
      $pdefault = 0;
    } else {
      $pdefault = round( ( $score / ($from - $ndefaults) ) * 100.0, 2);
    }
    if ($player != ' Default' && $player != ' New Player') {
      $name = $players[$player];
      if($split) {
        $bracket = strpos($name, '|');
        $team = ' ';
        if($bracket) {
          $team = substr($name, $bracket+1);
          $name = substr($name, 0, $bracket);
        }
      }
      if($split) {
        $rows[$player] = array($name, $rating, $team, $npoints, $from, $percent, $pdefault);
      } else {
        $rows[$player] = array($name, $rating, $npoints, $from, $percent, $pdefault);
      }
    }
  }
  $sorter->sort($rows);
  foreach($rows as $key => &$row) {
    $p=1;
    if($split) {
      $p=2;
    }
    $row[1+$p] = league_score2points($row[1+$p]);
    $row[3+$p] = $row[3+$p] . '%';
    $row[4+$p] = $row[4+$p] . '%';
    if( league_table_links() ) {
      $row[0] = l($row[0], "league_player/$key/games");
    } 
  }

  $table['data'] = $rows;
  $table['header'] = $header;
  return $table;
}

/**
 * Player Player Stats Data.
 */
function league_chess_player_stats_data($player) {

  $header = array('Team', 'Event', 'Organisation', 'Score', 'Games', 'Unfinished', 'Excluding Defaults', 'Percentage');
  $org = $player->org;
  $pid = $player->pid;
  $ecfcode = $player->ecf_code;

  // Results for the player
  $query = db_select('league_player', 'lp');
  // Use ecfcode if we have one to include results from other leagues
  if(isset($ecfcode) && strlen($ecfcode) > 3 && $ecfcode != ' ') {
    $query->condition('lp.ecf_code', $ecfcode);
  } else {
    $query->condition('lp.pid', $pid);
  }
  $query->addField('lp', 'pid', 'pid');
  $query->innerJoin('league_chess_result', 'r', 'r.white = lp.pid OR r.black = lp.pid');
  $query->addField('r', 'result', 'result');
  $query->addField('r', 'white', 'white');
  $query->addField('r', 'black', 'black');
  $query->addField('r', 'color', 'color');
  // Fixture
  $query->innerJoin('league_match', 'm', 'm.mid = r.mid');
  $query->innerJoin('league_fixture', 'f', 'f.nid = m.fid');
  // Home team
  $query->addField('f', 'home_team', 'homeTeam');
  $query->innerJoin('node', 'ht', 'ht.nid = f.home_team');
  $query->addField('ht', 'title', 'homeTeamName');
  // Away team
  $query->addField('f', 'away_team', 'awayTeam');
  $query->innerJoin('node', 'at', 'at.nid = f.away_team');
  $query->addField('at', 'title', 'awayTeamName');

  // Event for the fixture
  $query->addField('f', 'cid', 'event');
  $query->innerJoin('node', 'n', 'n.nid = f.cid');
  $query->addField('n', 'title', 'etitle');
  $query->innerJoin('league_competition', 'comp', 'comp.cid = f.cid');
  // Active Seasons
  $query->innerJoin('league_event', 'le', 'le.eid = comp.eid');
  $query->condition('le.lms', 'A');
  $query->addField('le', 'name', 'seasonName');

  // Org for the event
  $query->innerJoin('node', 'orgn', 'orgn.nid = le.org');
  $query->addField('orgn', 'title', 'otitle');
  $query->addField('orgn', 'nid', 'org');

  // Tag query so other modules can modify it (league_ecf)
  $query->addTag('league_chess_player_stats');
  $query->addMetaData('league_chess_player', $player);

  $points = array();
  $teams = array();

//dsm((string)$query);
  $queryResult = $query->execute();

  foreach ( $queryResult as $row ) {
    $white = $row->white;
    $black = $row->black;
    $wdl = $row->result;
    $color = $row->color;
    $pid = $row->pid;
    $score = 0;
    $p_score = 0;
    $p_games = 0;
    if($pid == $white) {
      $team = $row->homeTeam;
      $teamName = $row->homeTeamName;
      if ( $wdl == "W" || $wdl == "+" ) {
          $score = 2;
      }
      if ( $wdl == "W" && $black >0 ) {
          $p_score = 2;
          $p_games = 1;
      }
      if ( $wdl == "L" && $black >0 ) {
          $p_games = 1;
      }
    } else {
      $team = $row->awayTeam;
      $teamName = $row->awayTeamName;
      if ( $wdl == "L" || $wdl == "-" ) {
        $score = 2;
      }
      if ( $wdl == "L" && $white >0 ) {
        $p_score = 2;
        $p_games = 1;
      }
      if ( $wdl == "W" && $white >0 ) {
          $p_games = 1;
      }
    }
    if ( $wdl == "D" ) {
      $score = 1;
      $p_score = 1;
      $p_games = 1;
    }
    if(isset($points[$team]['games']) ) {
      $points[$team]['games']++;
    } else {
      $points[$team]['games'] = 1;
      $points[$team]['incomplete'] = 0;
    }
    if ( $wdl == 'J' || $wdl == 'A' || $wdl == 'N' ) {
      $points[$team]['incomplete']++;
    }
    // Score including defaults
    if(isset($points[$team]['score']) ) {
      $points[$team]['score'] += $score;  
    } else {
      $points[$team]['score'] = $score;
    }
    // Score excluding defaults
    if(isset($points[$team]['pscore']) ) {
      $points[$team]['pscore'] += $p_score;  
      $points[$team]['pgames'] += $p_games;  
    } else {
      $points[$team]['pscore'] = $p_score;
      $points[$team]['pgames'] = $p_games;
    }
    // Teams
    $teams[$team]['team'] = $teamName;
    $teams[$team]['eventName'] = $row->etitle;
    $teams[$team]['event'] = $row->event;
    $teams[$team]['orgName'] = $row->otitle;
    $teams[$team]['org'] = $row->org;
  }

  // Then work out the score for each team.

  $rows = array();
  foreach($teams as $team => $values) {
    $teamLink = league_link($teams[$team]['team'], "league_team/$team");
    $event = $teams[$team]['event'];
    $eventLink = league_link($teams[$team]['eventName'], "league_comp/$event");
    $org = $teams[$team]['org'];
    $orgLink = league_link($teams[$team]['orgName'], "node/$org/home");
    $score = league_score2points($points[$team]['score']);
    $games = $points[$team]['games'];
    $unfinished = $points[$team]['incomplete'];
    // Excluding defaults
    $pscore = league_score2points($points[$team]['pscore']);
    $pgames = $points[$team]['pgames'];
    if( $pgames > 0 ) {
      $percent = $points[$team]['pscore'] * 100.0  / ( 2 * $pgames );
      $percent = floor($percent);
    } else {
      $percent = 0.0;
    }
    $excluding = "$pscore from $pgames";
   
    $rows[] = array( $teamLink, $eventLink, $orgLink, $score, $games, $unfinished, $excluding, "$percent%");
  }

  $table['data'] = $rows;
  $table['header'] = $header;
  $table['title'] = $player->firstname . ' ' . $player->lastname;
  return $table;
}

/**
 * Player stats for all clubs with this ECF club code
 * in all organisations for this season
 */
function league_chess_club_stats_tab($node, $org=0) {
  // Set breadcrumb
  league_set_bread_crumb ($node, $org);
  // Set title
  $title = "$node->title";
  drupal_set_title($title);

  $table = league_chess_club_stats_data($node, $org);
  $content['stats']['#table'] = $table;
  $content['stats']['#theme'] = 'league_table';
  $content['stats']['#pdfLink'] = 'link';

  // Check if file to be create via Xl or PDF
  league_table_check_file($table);
  return $content;
}

/**
 * Club Player Stats Query.
 *  - as for event stats , but do all events in the season and only include those for teams of the given club
 */

function league_chess_club_stats_data($node, $org=0) {

  $header = array();
  $header[] = array('data' => 'First Name', 'field' => 'first' );
  $header[] = array('data' => 'Last Name', 'field' => 'last' );
  $header[] = array('data' => 'Score', 'field' => 'score' );
  $header[] = array('data' => 'Games', 'field' => 'games' );
  $header[] = array('data' => 'Percentage', 'field' => 'percent', 'sort'=>'desc' );
  if($org==0) {
    $header[] = array('data' => 'Organisation', 'field' => 'org' );
  }

  // Use a couple of functions from the league_chess module.
  $path = drupal_get_path('module', 'league_chess');
  require_once "$path/league_chess_stats.inc";

  $club = $node->nid;

  // Get sort order
  $order = tablesort_get_order($header);
  $direction = tablesort_get_sort($header);

  // Get query
  $result = league_chess_stats_query($club, $order['sql'], $direction);

  $rows = array();
  foreach($result as $row) {
    $pid = $row->pid;
    $first = league_link($row->first, "league_player/$pid/stats");
    $last = $row->last;
    $score = league_score2points($row->score);
    $games = $row->games;
    $percent = round($row->percent);
    if($org == 0 || $row->nid == $org) {
      $new_row = array( $first, $last, $score, $games, $percent);
      if($org == 0) {
        $new_row[] = $row->org;
      }
      $rows[] = $new_row;
    }
  }

  $table['data'] = $rows;
  $table['header'] = $header;
  $table['title'] = $node->title . ' - player stats';
  return $table;
}

function league_chess_stats_query($club,$order,$direction) {
  $sql = <<<SQL
SELECT sum(result) as 'score', pid, first, last, count(*) as 'games', 50 * sum(result) / count(*) AS 'percent', org, nid
FROM (
SELECT
 case WHEN r.result='W' THEN 2 WHEN r.result='L' THEN 0 ELSE 1 END AS result,
 r.white AS pid,
 w.firstname AS first,
 w.lastname AS last,
 orgn.title AS org,
 orgn.nid AS nid
FROM 
league_fixture f
INNER JOIN league_competition comp ON comp.cid = f.cid
INNER JOIN league_event le ON le.eid = comp.eid
INNER JOIN field_data_field_club hc ON hc.entity_id = f.home_team
INNER JOIN league_match m ON m.fid = f.nid
INNER JOIN node orgn ON orgn.nid = le.org
INNER JOIN league_chess_result r ON r.mid = m.mid
INNER JOIN league_player w ON w.pid = r.white
WHERE  (le.lms = 'A') AND (comp.type IN  ('L', 'J', 'K')) AND (r.result IN  ('W', 'D', 'L')) AND (r.white > 0) AND (r.black > 0) AND hc.field_club_value=$club
UNION ALL 
SELECT
 case WHEN r.result='W' THEN 0 WHEN r.result='L' THEN 2 ELSE 1 END AS result,
 r.black AS pid,
 b.firstname AS first,
 b.lastname AS last,
 orgn.title AS org,
 orgn.nid AS nid
FROM 
league_fixture f
INNER JOIN league_competition comp ON comp.cid = f.cid
INNER JOIN league_event le ON le.eid = comp.eid
INNER JOIN field_data_field_club ac ON ac.entity_id = f.away_team
INNER JOIN field_data_field_ecf_club_code ace ON ace.entity_id = ac.field_club_value
INNER JOIN league_match m ON m.fid = f.nid
INNER JOIN node orgn ON orgn.nid = le.org
INNER JOIN league_chess_result r ON r.mid = m.mid
INNER JOIN league_player b ON b.pid = r.black
WHERE  (le.lms = 'A') AND (comp.type IN  ('L', 'J', 'K')) AND (r.result IN  ('W', 'D', 'L')) AND (r.white > 0) AND (r.black > 0) AND ac.field_club_value=$club 
) t1 GROUP BY pid, first, last
ORDER BY $order $direction
SQL;
  $result = db_query($sql);
  return $result;
}
