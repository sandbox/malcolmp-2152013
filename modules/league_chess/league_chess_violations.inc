<?php

/**
 * @file
 * Functions for chessdb matchcard options.
 */
  
/**
 * Fixture matchcard edit tab.
 */
function league_chess_violation_tab ($node) {
  // Set bread crumb
  league_set_bread_crumb ($node);
  // Set title
  $title =  league_fixture_title($node);
  drupal_set_title($title);

  $fixture = $node->nid;

  // Create matchcard edit form
  $content['form'] = drupal_get_form('league_chess_violation_form', $node );

  return $content;
}

/**
*  Form for updating congress details.
*/
function league_chess_violation_form($form, &$form_state, $node) {

  $form['#node'] = $node;
  $form_state['node'] = $node;

  $event = league_fixture_value($node->nid, 'event');
  $access = league_check_access($event);
  $violations = league_get_violations($node->nid);
  $nviolations = count($violations);
  $form_state['league_violations'] = $violations;

  $form['violations'] = array(
   '#title' => t("Matchcard Violations: $nviolations"),
   '#type'  => 'fieldset',
   '#weight' => 3,
   '#description' => t('Uncheck to prevent the violation showing on status.'),
  );

  $form['violations']['tabletop'] = array(
     '#markup' => '<table>',
     '#type'  => 'markup',
  );

  $odd = 'even';
  // For each violation
  foreach($violations as $type => $status) {
    if($odd == 'odd') {
      $odd = 'even';
    } else {
      $odd = 'odd';
    }

    $fieldName = 'label' . $type;
    $statusName = league_chess_violation($type);
    $form['violations'][$fieldName] = array(
       '#markup' => "<tr><th>$statusName</th><td>",
       '#type'  => 'markup',
    );

    $fieldName = 'check' . $type;

    $default = 0;
    if($status == 'L') {
      $default = 1;
    }
    $form['violations'][$fieldName] = array(
       '#type'  => 'checkbox',
       '#default_value'  => $default,
       '#suffix' => '</td></tr>',
    );
  }

  $form['violations']['tablebottom'] = array(
     '#markup' => '</table>',
     '#type'  => 'markup',
  );

  if($access) {
    $form['submit'] = array(
       '#value' => t('Save'),
       '#type'  => 'submit',
       '#weight' => 15,
    );
  }

  return $form;
}

/**
*  Action after the form is submitted.
*/
function league_chess_violation_form_submit($form, &$form_state) {
  $violations = $form_state['league_violations'];
  $node = $form_state['node'];
  $event = league_fixture_value($node->nid, 'event');
  $org = league_event_value($event, 'org');
  $access = league_check_access($event);
  if(! $access) {
    drupal_set_message('Access denied');
  }

  $status = FALSE;
  // update violations for match
  foreach($violations as $type => $status) {
    $fieldName = 'check' . $type;
    $setting = $form_state['values'][$fieldName];
    $status = 'S';
    if($setting) {
      $status = 'L';
    }
    $league_fixture_violation = (object) array(
       'fid' => $node->nid,
       'type' => $type,
       'status' => $status,
    );
    $status = drupal_write_record('league_fixture_violation', $league_fixture_violation,array('fid', 'type'));
    if(!$status) {
      drupal_set_message('Update of league_fixture_violation Failed', 'error');
    }
  }
  // Clear the violations count cache
  $cache_name = "league_chess_count_violations_$org";
  cache_clear_all($cache_name, 'cache');
  if($status) {
    drupal_set_message('Violations status saved');
  }
}
