<?php

/**
 * @file
 * Functions for league_chess new player matches option.
 */
  
/**
 * sub-tab on reports for press report.
 */
function league_chess_admin_tab_press ($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);

  $org = $node->nid;
  $content = drupal_get_form('league_chess_pressform', $node);
  return $content;
}

/**
 * Create data for press report given form input.
 */
function league_chess_report_press_form_data( $form_state ) {

  $from = $form_state['values']['from'];
  $from = strtotime($from);
  $to = $form_state['values']['to'];
  $to = strtotime($to);
  $node = $form_state['node'];

  $data = league_chess_report_press_data($from, $to, $node);
  return $data;
}

/**
 * Create data for press report.
 */
function league_chess_report_press_data($from, $to, $node) {
  $data = array();
  // Get org name
  $org = $node->nid;
  $data['title'] = $node->title;
  $data['from'] = date('Y-m-d',$from);
  $data['to'] = date('Y-m-d',$to);

  $events = array();
  $fixtures = league_chess_fixtures_for_press($org, $from, $to);
  foreach ( $fixtures as $event => $list  ) {
    $eventNode = node_load($event);
    $eventTitle = $eventNode->title;
    $fixtureList = array();
    foreach ( $list as $fixture => $date  ) {
      $fixtureNode = node_load($fixture);
      $table = league_chess_matchcard_table($fixtureNode);
      $comments = league_chess_fixture_comments($fixture);
      $fdata = array(
        'table' => $table,
        'title' => $fixtureNode->title,
        'comments' => $comments,
      );
      $fixtureList[] = $fdata;
    }
    $event = array(
      'title' => $eventTitle,
      'fixtures' => $fixtureList,
    );
    $events[] = $event;
  }
  $data['events'] = $events;
  return $data;
}

/**
 * Produce press report in PDF format from the data.
 */
function league_chess_report_press_pdf( $data ) {
  $title = $data['title'];
  $from = $data['from'];
  $to = $data['to'];
  $events = $data['events'];

  $heading = "Press Report from $from to $to";
  $pdf = fpdf_start_page($heading);

  foreach($events as $event) {
    $title = $event['title'];
    fpdf_text($pdf, $title);
    $fixtures = $event['fixtures'];
    foreach($fixtures as $fixture) {

      $title = $fixture['title'];
      $table = $fixture['table'];
      $data = $table['data'];
      $header = $table['header'];
      $pdf->GeneralTable($header, $data);

      $comments =  $fixture['comments'];
      foreach($comments as $comment) {
        fpdf_text($pdf, $comment['subject']);
        fpdf_text($pdf, $comment['name']);
        fpdf_text($pdf, $comment['text']);
      }
    }
  }
  fpdf_end_page($pdf);
}

/**
 *  Form to get press report from to dates.
 */
function league_chess_pressform($form, &$form_state, $node) {

  $form_state['node'] = $node;
  $org = $node->nid;
  $form['org'] = array(
    '#value' => $org,
    '#type' => 'value',
  );

  $form['from'] = array(
    '#type' => 'date_popup',
    '#title' => t('From Date'),
    '#date_format' => 'Y-m-d',
    '#date_label_position' => 'within',
    '#required' => TRUE,
  );
  if(isset($form_state['storage']['from'])) {
    $form['from']['#default_value'] = $form_state['storage']['from'];
  }

  $form['to'] = array(
    '#type' => 'date_popup',
    '#title' => t('To Date'),
    '#date_format' => 'Y-m-d',
    '#date_label_position' => 'within',
    '#required' => TRUE,
  );

  if(isset($form_state['storage']['to'])) {
    $form['to']['#default_value'] = $form_state['storage']['to'];
  }

  $form['submit'] = array(
    '#value' => 'Press Report as Web Page',
    '#type' => 'submit',
  );

  $form['pdf'] = array(
    '#value' => 'Press Report as PDF',
    '#type' => 'submit',
    '#submit' => array('league_chess_pressform_pdf'),
  );
 
  if(isset($form_state['storage']['option'])) {
    $option = $form_state['storage']['option'];
    if($option == 'web') {
      $data = league_chess_report_press_form_data($form_state);
      $form['report'] = array(
        '#data' => $data,
        '#theme' => 'league_chess_press',
        '#weight' => 150,
      );
    }
  }


  return $form;
}

/**
 *  Validate press report form.
 */
function league_chess_pressform_validate($form, &$form_state) {
  $from = $form_state['values']['from'];
  $to = $form_state['values']['to'];
  if ( $to < $from ) {
    form_set_error('to', 'To date must be greater than from date');
  }
}

/**
 *  Action when press report form is submitted.
 */
function league_chess_pressform_submit($form, &$form_state) {
  // Tell the form to include press report in its output
  $form_state['storage']['option'] = 'web';
  $form_state['rebuild'] = TRUE;
}

/**
 *  Function to submit press report form for pdf.
 */
function league_chess_pressform_pdf($form, &$form_state) {
  $form_state['storage']['option'] = 'pdf';
  $data = league_chess_report_press_form_data($form_state);
  league_chess_report_press_pdf($data);
}

/**
 *  Get the comments for a fixture.
 */
function league_chess_fixture_comments($fixture) {
  $data = array();
  // Comments
  $query = db_select('comment', 'c');
  $query->condition('c.nid', $fixture);
  $query->addField('c', 'name', 'name');
  $query->addField('c', 'subject', 'subject');
  $query->addField('c', 'uid', 'uid');
  $query->addField('c', 'cid', 'cid');
  $query->addField('c', 'pid', 'pid');
  $query->addField('c', 'thread', 'thread');
  // Comment Text.
  $query->innerJoin('field_data_comment_body', 'b', 'b.entity_id = c.cid');
  $query->addField('b', 'comment_body_value', 'text');
  $result = $query->execute();
  foreach ( $result as $row ) {
    $comment = array(
      'name' => $row->name,
      'subject' => $row->subject,
      'text' => $row->text,
      'comment_id' => $row->cid,
      'parent' => $row->pid,
      'uid' => $row->uid,
      'thread' => $row->thread,
    );
    $data[] = $comment;
  }
  return $data;
}

/**
 * Get The fixtures for an org. This is used for the press report and hence
 * the fixtures are ordered by event and then by date last updated to avoid
 * missing out on reporting of late fixtures.
 */
function league_chess_fixtures_for_press($org, $from, $to) {
  $data = array();
  // Fixtures updated between the dates specified
  $query = db_select('node', 'f');
  $query->condition('f.type', 'fixture');
  $query->condition('f.changed', $from, '>');
  $query->condition('f.changed', $to, '<');
  $query->addField('f', 'nid', 'fixtureId');
  // Fixture date
  $query->innerJoin('league_fixture', 'd', 'd.nid = f.nid');
  $query->addField('d', 'date', 'date');
  // Event that the fixture belongs to
  $query->addField('d', 'cid', 'eventId');
  // Get event type.
  $query->innerJoin('league_competition', 'comp', 'comp.cid = d.cid');
  $query->condition('comp.type', array('L','J','K'), 'IN');
  // Events in the specified organisation
  $query->innerJoin('league_event', 'le', 'le.eid = comp.eid');
  $query->condition('le.org', $org);
  $result = $query->execute();

  foreach ( $result as $row ) {
    $data[$row->eventId][$row->fixtureId] = $row->date;
  }
  return $data;
}

/**
 * Create a match result as a table.
 */
function league_chess_matchcard_table($node) {
  // Get match card and results data
  $data = league_chess_matchcard_data($node);
  $results = league_chess_matchcard_results($node,$data);
  $data['boards'] = count($results);
  if($data['eventType'] == 'L' || $data['eventType'] == 'J' || $data['eventType'] == 'K') {
    $home = $data['homeTeamName'];
    $away = $data['awayTeamName'];
  } else {
    $home = 'White';
    $away = 'Black';
  }

  $platform = league_chess_event_value($node, 'platform');
  $date = date( 'Y-m-d', $data['date']);
  $header = array('Colour', 'Board', $home, 'Grade', $date, $away, 'Grade', 'Game Ref');

  $homeGrade = 0;
  $awayGrade = 0;
  $rate = $data['rate'];
  $rows = array();
  if($data['visible']) {
    for($board=0; $board< $data['boards']; $board++)
    {
      $boardNum = $board+1;
      $whiteGrade = $results[$board]['whiteGrade'];
      $blackGrade = $results[$board]['blackGrade'];
      $colour = $results[$board]['colour'];
      $homeGrade += league_chess_strip_star($whiteGrade, $rate);
      $awayGrade += league_chess_strip_star($blackGrade, $rate);
      $result = league_chess_result2score($results[$board]['result']);
      $black = $results[$board]['blackName'];
      $white = $results[$board]['whiteName'];
      if($platform != 'O' && $platform != 'R') {
        if( $results[$board]['blackLid'] !=0 ) {
          $black .= ' (' . $results[$board]['blackLid'] . ')';
        }
        if( $results[$board]['whiteLid'] !=0 ) {
          $white .= ' (' . $results[$board]['whiteLid'] . ')';
        }
      }
      $ref = $results[$board]['ref'];
      $rows[] = array($colour, $boardNum, $white, $whiteGrade, $result, $black, $blackGrade, $ref);
    }
  }
  $table['data'] = $rows;
  $table['header'] = $header;

  return $table;
}
