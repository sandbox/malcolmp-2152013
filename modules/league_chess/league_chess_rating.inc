<?php
/**
* @file
* Functions for creating FIDE rating file.
*/

function league_chess_fide_rating_fixture($node) {
  if( $node->type == 'fixture') {
    $event = league_fixture_value($node, 'event');
    $storage = array();
    $storage['city'] = 'City';
    $storage['arbiter'] = 'Arbiter (000000)';

    $data = league_chess_rating_event_data($storage,$node);
    $header = $data['header'];
    $players = $data['players'];
    $results = $data['results'];

    league_chess_fide_rating_file($header, $players, $results);
  }
}

// Page to produce rating file for an event

function league_chess_event_rating($node) {
  // Set Breadcrumb
  league_set_bread_crumb ($node);
  // Set title
  $title = $node->title . ' - Rating';
  drupal_set_title($title);
  return drupal_get_form('league_chess_event_rating_form', $node);
}

function league_chess_event_rating_form($form, &$form_state,$node) {

  $form_state['league_node'] = $node;

  $form['note'] = array(
      '#type' => 'markup',
      '#markup'  => '<b>Note</b> After using one of the create file buttons, select the Manaul Rating tab again, otherwise the buttons stop working<p>',
      '#weight' => 4,
  );

  $form['gfile'] = array(
    '#title' => t('Rating File Details'),
    '#type'  => 'fieldset',
    '#weight' => 10,
  );

  $org = league_event_value($node, 'org');

  $form['gfile']['city'] = array(
   '#type'  => 'textfield',
   '#size'  => 30,
   '#title'  => t('City'),
   '#description'  => t('For FIDE Rating Files'),
   '#weight' => 55,
  );

  $form['gfile']['arbiter'] = array(
   '#type'  => 'textfield',
   '#size'  => 10,
   '#title'  => t('Chief Arbiter FIN'),
   '#description'  => t('For FIDE Rating Files'),
   '#weight' => 57,
   '#element_validate' => array('element_validate_integer_positive'), 
  );

  $form['buttons'] = array();
  $form['buttons']['#weight'] = 100;
  $form['buttons']['view'] = array(
    '#value' => t('View FIDE Data'),
    '#type'  => 'submit',
    '#submit'  => array('league_chess_event_rating_view'),
    '#weight' => 10,
  );

  $form['buttons']['fide'] = array(
    '#value' => t('Create FIDE File'),
    '#type'  => 'submit',
    '#submit'  => array('league_chess_event_rating_file'),
    '#weight' => 20,
  );

  if(isset($form_state['storage']['option'])) {
    $option = $form_state['storage']['option'];
    if($option == 'view') {
      $stuff = league_chess_rating_event_data($form_state['storage'],$form_state['league_node']);
      $form['grade_data'] = array(
        '#data' => $stuff,
        '#theme' => 'league_chess_rating',
        '#weight' => 150,
      );
    }
  }
  return $form;
}

function league_chess_rating_event_data($storage,$node, $batch=FALSE) {
  $city = $storage['city'];
  $arbiter = intval($storage['arbiter']);

  if (!league_check_access($node->nid) ) {
    drupal_set_message("Date of Birth will not be included unless you are logged on", 'warning');
  }
  
  $query = db_select('league_fixture', 'f');
  $query->addField('f', 'date', 'date');
  $query->addField('f', 'home_team', 'homeTeam');
  $query->addField('f', 'away_team', 'awayTeam');
  $query->addField('f', 'round', 'round');
  $query->addField('f', 'nid', 'fnid');
  $query->innerJoin('league_competition', 'comp', 'comp.cid = f.cid');
  $query->addField('comp', 'type', 'eventType');
  $query->innerJoin('node', 'en', 'en.nid = f.cid');
  $query->addField('en', 'title', 'eventTitle');

  if($node->type == 'fixture') {
    $query->condition('f.nid', $node->nid);
    $event = league_fixture_value($node, 'event');
    $ftitle =  league_fixture_title($node);
    $eventNode = node_load($event);
    $eid = league_event_value($eventNode, 'season');
    $query->innerJoin('league_event', 'le', "le.eid = $eid");
    $eventTitle = league_event_value($eventNode, 'title');
    $org = league_event_value($eventNode, 'org');
    $orgTitle = league_org_value($org, 'title');
    $title = "$orgTitle $eventTitle $ftitle";
  } else {
    $query->condition('f.cid', $node->nid);
    $query->innerJoin('league_event', 'le', 'le.eid = comp.eid');
    $eventNode = $node;
    $eventTitle = league_event_value($eventNode, 'title');
    $org = league_event_value($eventNode, 'org');
    $orgTitle = league_org_value($org, 'title');
    $title = $orgTitle . " " . $eventTitle;
  }
  $eventType = league_event_value($eventNode, 'type');

  $query->addField('le', 'name', 'season');
  $query->leftJoin('node', 'ht', 'ht.nid = f.home_team');
  $query->leftJoin('node', 'at', 'at.nid = f.away_team');
  $query->addField('ht', 'title', 'homeTitle');
  $query->addField('at', 'title', 'awayTitle');
  $query->innerJoin('league_match', 'm', 'm.fid = f.nid');
  $query->innerJoin('league_chess_result', 'r', 'r.mid = m.mid');
  // Exclude games with no result or defaults
//$query->condition('r.result', array('W','D','L'), 'IN');
  // Result fields
  $query->addField('r', 'white', 'white');
  $query->addField('r', 'black', 'black');
  $query->addField('r', 'result', 'result');
  $query->addField('r', 'color', 'color');
  $query->addField('r', 'board', 'board');
  $query->addField('r', 'rdate', 'rdate');
  $query->addField('r', 'rid', 'gameid');
  $query->orderBy('f.nid');
  $query->orderBy('board');

  $queryResult = $query->execute();
  $header = array();
  $resultList = array();
  $playerList = array();
  $teamList = array();
  $np_count = 0;
  $truncate_count = 0;
  $header['eventType'] = '?';
  $header['Season'] = 0;
  $header['City'] = $city;
  $fide_arbiter = fidedata_get_player($arbiter);
  if(isset($fide_arbiter['lastname']) ) {
    $arbiter_name = $fide_arbiter['lastname'] . ', ' . $fide_arbiter['firstname'];
  } else {
    $arbiter_name = ' ';
  }
  $header['Arbiter'] = "$arbiter_name ($arbiter)";
  if ($queryResult) {
    $players = array();
    $teams = array();
    $players_points = array();

    // Result List
    $minDate = 0;
    $maxDate = 0;
    // Loop round the results

    $count = 0;
    foreach ( $queryResult as $row) {
      $black = $row->black;
      $white = $row->white;
      $count++;
      $board = $row->board;
      $round = $row->round;
      $home = $row->homeTeam;
      $away = $row->awayTeam;
      $color = $row->color;
      $result = $row->result;
      $date = $row->rdate;
      if( $minDate == 0 ) {
        $minDate = $row->rdate;
        $maxDate = $row->rdate;
      } else {
        $minDate = min($row->rdate, $minDate);
        $maxDate = max($row->rdate, $maxDate);
      }
      $eventType = $row->eventType;
      $header['eventType'] = $row->eventType;
      $header['Season'] = $row->season;
      $eventTitle = $row->eventTitle;
      // Add up their points
      if(! isset($players_points[$white] ) ) {
        $players_points[$white] = 0;
      }
      if(! isset($players_points[$black] ) ) {
        $players_points[$black] = 0;
      }
      if ( $result == "W" || $result == "+" ) {
        $players_points[$white] += 2;
      }
      if ( $result == "L" || $result == "-" ) {
        $players_points[$black] += 2;
      }
      if ( $result == "D" ) {
        $players_points[$white] += 1;
        $players_points[$black] += 1;
      }
      // Add player to player list, if not there

      if ( $white > 0 ) {
        $players[$white] = $home;
      }
      if ( $black > 0 ) {
        $players[$black] = $away;
      }

      // Add team to team list, if not there
      $teams[$home] = 1;
      $teams[$away] = 1;

      // Output Results
      $resultList[$count]['PIN1'] = $white;
      $resultList[$count]['PIN2'] = $black;
      $resultList[$count]['Result'] = $result;
      // Write the colour for individual tournaments.
      if ( $eventType == "S" || $eventType == "U" || $eventType == "A"  ) {
        $resultList[$count]['Colour1'] = 'W';
      } else {
        $resultList[$count]['Colour1'] = $color;
      }
      // date is optional
      $resultList[$count]['Date'] = date('d/m/Y',$date);
      $resultList[$count]['Round'] = $round;
      $resultList[$count]['Home'] = $home;
      $resultList[$count]['Away'] = $away;
      $resultList[$count]['fnid'] = $row->fnid;
      $resultList[$count]['GameID'] = $row->gameid;
      $resultList[$count]['Board'] = $board;
      $ftitle =  league_fixture_title($row->fnid);
      $resultList[$count]['FixtureTitle'] = $ftitle;

      // Count the New Player matches
      if ( $black == LEAGUE_CHESS_NEWPLAYER ||
           $white == LEAGUE_CHESS_NEWPLAYER ) {
        $np_count++;
      }
    }
    // Creating header
    // Event name - up to 100 chars
    $header['Event Name'] = $title;
    // Event date
    $header['Competition'] = $eventTitle;
    $header['Org'] = $org;
    $header['StartDate'] = date('d/m/Y',$minDate);
    $header['EndDate'] = date('d/m/Y',$maxDate);
    if(! league_event_type_individual($eventType) ) {
    // Get the ECF club codes for the teams
      foreach ($teams as $teamId => $value) {
        $teamClub = league_team_value($teamId, 'club');
        $clubEcfCode = league_chess_club_value($teamClub, 'ecfcode');
        $teams[$teamId] = $clubEcfCode;
        $teamName = league_team_value($teamId, 'title');
        $teamList[$teamId] = $teamName;
      }
    }
    // setup the player list

    $count = 0;
    foreach ($players as $playerId => $team) {
      $count++;
      $player = league_player_load($playerId);
      if(!is_object($player)) {
        watchdog('league_ecf',"Failed to load player $playerId for $team");
      }
      // The actual data: -
      // 0 - Players ID, PIN
      $playerList[$count]['PIN'] = $player->pid;
      // 1 - Players Federation
      $settings = league_chess_national_settings();
      if ( isset($player->ecf_code) && $player->ecf_code != '0' ) {
        $rated_player = league_chess_get_player($player->ecf_code);
        if(isset($rated_player['Nation']) && strlen($rated_player['Nation'])>2) {
          $playerList[$count]['Federation'] = $rated_player['Nation'];
        } else {
          $playerList[$count]['Federation'] = $settings['federation'];
        }
      } else {
        $playerList[$count]['Federation'] = $settings['federation'];
      }
      // Players points
      if(isset($players_points[$player->pid]) ) {
        $playerList[$count]['Points'] = $players_points[$player->pid];
      } else {
        $playerList[$count]['Points'] = 0;
      }
      // 2 - Players Name.
      $name = $player->lastname . ', ' . $player->firstname;
      $playerList[$count]['NAME'] = $name;
      // 3 - gender
      if ( isset($player->sex) ) {
        $playerList[$count]['sex'] = $player->sex;
      } else {
        $playerList[$count]['sex'] = ' ';
      }
      // 4 - dob
      $playerList[$count]['dob'] = ' ';
      if ($batch || league_check_access($eventNode->nid) ) {
        if( isset($player->dob) && substr($player->dob,0,4) != '0000' ) {
          $playerList[$count]['dob'] = date('d/m/Y',$player->dob);
        }
      }
      // 5 - Club code, for team events
      if ( $eventType == "L" || $eventType == "K" || $eventType == "J" ) {
        $ecf = $teams[$team];
        $playerList[$count]['Club Code'] = $ecf;
        $playerList[$count]['Team'] = $team;
      } else {
        // For individual events try to do it for new players
        if ( isset($player->chess_club) && strlen($player->chess_club) >3 ) {
          $clubEcfCode = $player->chess_club;
          if (isset($clubEcfCode) ) {
            $playerList[$count]['Club Code'] = $clubEcfCode;
          } else {
            $playerList[$count]['Club Code'] = '';
          }
        }
      }
      // 6 - FIDE code (FIN) and rating
      if ( isset($player->chess_fidecode) && $player->chess_fidecode != NULL && $player->chess_fidecode != ' ' && $player->chess_fidecode >0 ) {
        $playerList[$count]['FIDECode'] = $player->chess_fidecode;
        $ratings = fidedata_get_ratings($player->chess_fidecode);
        $rlid = league_chess_event_value($eventNode, 'rate');
        if(isset($ratings[$rlid]) ) {
          $rating = $ratings[$rlid]['rating'];
        } else {
          $rating = 0;
        }
        $playerList[$count]['Rating'] = $rating;
      }
      else {
        $playerList[$count]['FIDECode'] = ' ';
        $playerList[$count]['Rating'] = 0;
      }
    } // end foreach player loop
  } // end-if the query worked
  $data['header'] = $header;
  $data['players'] = $playerList;
  $data['results'] = $resultList;
  $data['teams'] = $teamList;
  $data['warnings'] = array(
    'nps' => $np_count,
    'truncate' => $truncate_count,
  );

  return $data;
}

function league_chess_event_rating_form_validate($form, &$form_state) {
  $op = $form_state['values']['op'];
  $city = trim($form_state['values']['city']);
  $arbiter = $form_state['values']['arbiter'];
  // Check city and arbiter for FIDE rating files
  if( $op == 'Generate FIDE Rating File' ) {
    if( strlen($city) == 0) {
      form_set_error('city', 'City Required for FIDE rating file');
    }
    if( !isset($arbiter) || $arbiter == 0) {
      form_set_error('arbiter', 'FIDE Arbiter FIN required');
    }
  }
}

// Display grading data to the screen.

function league_chess_event_rating_view($form, &$form_state) {
  $node = $form_state['league_node'];

  $city = $form_state['values']['city'];
  $arbiter = $form_state['values']['arbiter'];

  $form_state['storage']['option'] = 'view';
  $form_state['storage']['city'] = $city;
  $form_state['storage']['arbiter'] = $arbiter;

  $form_state['rebuild'] = TRUE;
}

function league_chess_event_rating_file($form, &$form_state) {
  $node = $form_state['league_node'];

  $city = $form_state['values']['city'];
  $arbiter = $form_state['values']['arbiter'];

  $storage = array();
  $storage['city'] = $city;
  $storage['arbiter'] = $arbiter;

  $data = league_chess_rating_event_data($storage,$node);
  $header = $data['header'];
  $players = $data['players'];
  $results = $data['results'];

  league_chess_fide_rating_file($header, $players, $results);
}

function league_chess_fide_date($date) {
  if(strlen($date) > 9 ) {
    $year = substr($date, 6, 4);
    $month = substr($date, 3, 2);
    $day = substr($date, 0, 2);
    $fide_date = "$year/$month/$day";
  } else {
    $fide_date = '          ';
  }
  return $fide_date;
}

function league_chess_fide_rating_file($header, $players, $results, $handle=FALSE) {

  $fileName = $header['Event Name'] . ".TXT";
  if(!$handle) {
    drupal_add_http_header('Content-Type', 'text/plain');
    drupal_add_http_header('Content-Disposition', 'attachment; filename="' . $fileName . '"');
    drupal_add_http_header('Cache-Control',  'max-age=0');
  }
  // Event Name
  league_fide_rating_field($handle, '012', $header['Event Name']);
  // City
  league_fide_rating_field($handle, '022', $header['City']);
  // Federation
  league_fide_rating_field($handle, '032', 'ENG');
  // Chief Arbiter Name (FIN in brackets)
  league_fide_rating_field($handle, '102', $header['Arbiter']);
  // Start and End
  $date = league_chess_fide_date($header['StartDate']);
  league_fide_rating_field($handle, '042', $date);
  $date = league_chess_fide_date($header['EndDate']);
  league_fide_rating_field($handle, '052', $date);

  // Headers this is in the example, but not in the spec?
  league_fide_rating_field($handle, '123', '456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789', '');
  league_fide_rating_field($handle, 'DDD', 'SSSS sTTT NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN RRRR FFF IIIIIIIIIII BBBB/BB/BB PPPP RRRR  1111 1 1');

  // Sort players by rating to get start rank
  usort($players, "league_chess_fide_rating_sort");
  for($i=0; $i<count($players); $i++ ) {
    $players[$i]['SRank'] = $i + 1;
  }
  // Sort players by points to get Rank
  usort($players, "league_chess_fide_points_sort");

  // Store the sranks
  $sranks = array();
  foreach($players as $player) {
    $sranks[$player['PIN']] = $player['SRank'];
  }
  // For Each Player ....
  $pcount=0;
  foreach($players as $player)
  {
    $pcount++;
    //   1-3   ID ( 000 - 999 )
    $id = str_pad('001',3,'0',STR_PAD_LEFT);
    //   5-8   Starting Rank (1-9999)
    $record = str_pad($player['SRank'],4,'0',STR_PAD_LEFT);
    //   10    Sex (m/w)
    if( $player['sex'] == 'M' ) {
      $sex = 'm';
    } else {
      if( $player['sex'] == 'F' ) {
        $sex = 'f';
      } else {
        $sex = ' ';
      }
    }
    $record .= ' ' . $sex;
    $record .= '    ';
    //   15-47 Lastname, Firstname
    $record .= str_pad($player['NAME'], 34);
    //   49-52 FIDE Rating
    $record .= str_pad($player['Rating'], 4);
    //   54-56 FIDE Federation
    $record .= ' ' . str_pad($player['Federation'],4);
    //   58-68 FIN
    $record .= str_pad($player['FIDECode'],11,' ', STR_PAD_LEFT);
    //   70-79 DOB
    $dob = league_chess_fide_date($player['dob']);
    $record .= " $dob";
    //   81-84 Points
    $points = sprintf("%.1f", $player['Points']/2);
    $record .= str_pad($points, 5, ' ', STR_PAD_LEFT);
    //   86-89 Rank
    $record .= ' ' . str_pad($pcount,4,'0',STR_PAD_LEFT);
    //
    //   For Each Round ...
    //   92-95 Starting Rank No of opponent or 0000 for bye etc.
    //   97    Colour w b (- or blank for bye)
    //   99    Result: 1, =, 0 (win, draw, loss), H, F, Z (bye half, full, zero)
    //                 +- forfeit win,loss
    //
    //  Questions:
    //    Do the rounds matter? for example if a game is in round 1 for play a
    //    does it need to be in round 1 for player b? 
    //    What if players don't play in a round to we need a bye ?
    //    Can I just take the round number from the fixture ?
    //    Or should I just list the results and not worry about round?
    // Loop round the results
    $count=1;
//  drupal_set_message(print_r($player, TRUE));
//
    $p_results = array();
    foreach($results as $result)
    {
      if( $result['PIN1'] == $player['PIN'] || $result['PIN2'] == $player['PIN'] ) {
        $p_result = array('Round' => $result['Round'] );
        // Colour and opponent
        if( $result['PIN1'] == $player['PIN'] ) {
          $p_result['Colour'] = strtolower($result['Colour1']);
          if( $result['PIN2'] > 0 ) {
            $srank = $sranks[$result['PIN2']];
          }
          $p_result['Result'] = league_chess_fide_result($result['Result'], TRUE );
        } else {
          if( $result['PIN1'] > 0 ) {
            $srank = $sranks[$result['PIN1']];
          }
          if( $result['Colour1'] == 'W' ) {
            $p_result['Colour'] = 'b';
          } else {
            $p_result['Colour'] = 'w';
          }
          $p_result['Result'] = league_chess_fide_result($result['Result'], FALSE);
        }
        $p_result['SRank'] = $srank ;
        $p_results[] = $p_result;
      }
    }
    if( count($p_results) > 0 ) {
      usort($p_results, "league_chess_fide_round_sort");
      foreach($p_results as $p_result) {
        $record .= '  ' . str_pad($p_result['SRank'],4,'0',STR_PAD_LEFT);
        $record .= ' ' . $p_result['Colour'];
        $record .= ' ' . $p_result['Result'];
      }
    }
    league_fide_rating_field($handle, $id, $record);
  }
  // Get the teams 
  $teams = array();
  foreach($players as $player)
  {
    if(isset($player['Team'] )) {
      $team = intval($player['Team']);
      if($team > 0) {
        if(!isset($teams[$team]) ) {
          $teams[$team]['name'] = league_team_value($team, 'title');
          $teams[$team]['players'] = array();
        }
        $teams[$team]['players'][$player['PIN']] = $player['SRank'];
      }
    }
  }
  // Output a record for each team
  foreach($teams as $team) {
    $record = str_pad($team['name'], 25);
    foreach($team['players'] as $player) {
      $record .= '  ' . str_pad($player, 3 , '0', STR_PAD_LEFT);
    }
    league_fide_rating_field($handle, '013', $record);
  }
  if(!$handle) {
    exit;
  }
}

function league_chess_fide_result($result, $first) {
  switch($result) {
    case 'W' :
      if($first) {
        $fide_result = '1';
      } else {
        $fide_result = '0';
      }
      break;
    case 'L' :
      if($first) {
        $fide_result = '0';
      } else {
        $fide_result = '1';
      }
      break;
    case 'D' :
      $fide_result = '=';
      break;
    case 'A' :
      $fide_result = 'Z';
      break;
    case 'J' :
       $fide_result = 'Z';
       break;
    case '+' :
      if($first) {
        $fide_result = '+';
      } else {
        $fide_result = '-';
      }
      break;
    case '-' :
      if($first) {
        $fide_result = '-';
      } else {
        $fide_result = '+';
      }
      break;
    case '=' : $fide_result = 'Z';
         break;
    case 'N' : $fide_result = 'Z';
         break;
  }
  return $fide_result;
}

// Output one FIDE format rating file field

function league_fide_rating_field($handle, $id, $value=' ', $gap=' ') {
  // file handle supplied
  if($handle) {
    fprintf($handle, "$id$gap$value\n");
  // STDOUT
  } else {
    print "$id$gap$value\n";
  }
}
function league_chess_fide_rating_sort($fa, $fb) {
  $ra = intval($fa['Rating']);
  $rb = intval($fb['Rating']);
  if ( $ra > $rb ) {
    return -1;
  }
  if ( $ra < $rb ) {
    return 1;
  }
  return 0;
}
function league_chess_fide_points_sort($fa, $fb) {
  $ra = intval($fa['Points']);
  $rb = intval($fb['Points']);
  if ( $ra > $rb ) {
    return -1;
  }
  if ( $ra < $rb ) {
    return 1;
  }
  return 0;
}
function league_chess_fide_round_sort($fa, $fb) {
  $ra = intval($fa['Round']);
  $rb = intval($fb['Round']);
  if ( $ra > $rb ) {
    return -1;
  }
  if ( $ra < $rb ) {
    return 1;
  }
  return 0;
}

