<?php

/**
 * @file
 * Functions for league_chess result popup.
 */

// Called after link in the all play all results table is clicked

function league_chess_result_popup($js=NULL, $event=0, $pid1=0, $pid2=0) {

  if ($js) {
    ctools_include('ajax');
    ctools_include('modal');

    $form_state = array(
      'ajax' => TRUE,
      'title' => t('Result Update'),
      'league_chess_event' => $event,
      'league_chess_pid1' => $pid1,
      'league_chess_pid2' => $pid2,
    );

    // Use ctools to generate ajax instructions for the browser to create
    // a form in a modal popup.
    $output = ctools_modal_form_wrapper('league_chess_result_popup_form', $form_state);

    // If the form has been submitted, there may be additional instructions
    // such as dismissing the modal popup.
    if (!empty($form_state['ajax_commands'])) {
      $output = $form_state['ajax_commands'];
    }

    // Return the ajax instructions to the browser via ajax_render().
    print ajax_render($output);
    drupal_exit();
  }
  else {
//  return drupal_get_form('league_chess_result_popup_form');
    // This only happens if the browser doesn't support javascript
    // in which case it doesn't work
    $content = array('#markup' => "<h2>Error javascript needed</h2>",
                     '#type' => 'markup',
    );
    return $content;
  }
}

/**
*  The actual popup form for updating a result.
*/
function league_chess_result_popup_form($form, &$form_state ) {

  $event = $form_state['league_chess_event'];
  $pid1 = $form_state['league_chess_pid1'];
  $pid2 = $form_state['league_chess_pid2'];

  // Table header
  $form['header'] = array(
     '#markup' => "<div id=popup-result-table><table>",
     '#type'  => 'markup',
  );

  // Results between the 2 players
  $results = league_chess_popup_results($event, $pid1, $pid2);
  $form_state['league_chess_results'] = $results;

  // Selector field for each game
  foreach($results as $result) {

    // Result selector
    $field = 'res' . $result['rid'];
    $form[$field] = league_chess_result_selector($result['result']);
    $form[$field]['#prefix'] = '<tr><td width="50%">' . '(' . $result['board'] . ')' . $result['text'] . '</td><td>';
    $form[$field]['#suffix'] = "</td>";

    // Date selector
    $field = 'dat' . $result['rid'];
    $form[$field] = league_chess_date_selector($result['date']);
    $form[$field]['#prefix'] = '<td>';
    $form[$field]['#suffix'] = "</td></tr>";
  }

  // Table footer
  $form['footer'] = array(
     '#markup' => "</table></div>",
     '#type'  => 'markup',
  );

  // Buttons
  $form['cancel'] = array(
     '#value' => t('Cancel'),
     '#type'  => 'submit',
     '#weight' => 30,
     '#submit' => array('league_chess_result_popup_form_cancel'),
  );

  $form['save'] = array(
     '#value' => t('Save'),
     '#type'  => 'submit',
     '#weight' => 40,
  );

  $eventType = league_event_value($event, 'type');
  if( $eventType == 'I' ) {
    $form['up'] = array(
      '#value' => t('Move Board Up'),
      '#type'  => 'submit',
      '#weight' => 50,
      '#submit' => array('league_chess_result_popup_form_up'),
    );
    $form['down'] = array(
      '#value' => t('Move Board Down'),
      '#type'  => 'submit',
      '#weight' => 60,
      '#submit' => array('league_chess_result_popup_form_down'),
    );
  }

  return $form;
}

// Get the results between 2 players for the event

function league_chess_popup_results($event, $pid1, $pid2) {
  // Fixtures in the event
  $query = db_select('league_fixture', 'f');
  $query->addField('f', 'date', 'date');
  $query->condition('f.cid', $event);
  $query->innerJoin('node', 'n', 'n.nid = f.nid');
  // Results in the fixtures
  $query->innerJoin('league_match', 'm', 'm.fid = n.nid');
  $query->innerJoin('league_chess_result', 'r', 'r.mid = m.mid');
  $query->addField('r', 'rid', 'rid');
  $query->addField('m', 'fid', 'fid');
  $query->addField('m', 'mid', 'mid');
  $query->addField('r', 'result', 'result');
  $query->addField('r', 'rdate', 'rdate');
  $query->addField('r', 'board', 'board');
  // Results for one of the players
  $query->condition('white', array($pid1, $pid2), 'IN');
  $query->condition('black', array($pid1, $pid2), 'IN');
  // White
  $query->innerJoin('league_player', 'w', 'w.pid = r.white');
  $query->addField('w', 'firstname', 'wfirst');
  $query->addField('w', 'lastname', 'wlast');
  // Black
  $query->innerJoin('league_player', 'b', 'b.pid = r.black');
  $query->addField('b', 'firstname', 'bfirst');
  $query->addField('b', 'lastname', 'blast');
  // Order by board
  $query->orderBy('r.board', 'ASC');

  $results = array();
  $rows = $query->execute();
  foreach ( $rows as $row) {
    $white = $row->wfirst . ' ' . $row->wlast;
    $black = $row->bfirst . ' ' . $row->blast;
    $result = array(
      'rid' => $row->rid,
      'fid' => $row->fid,
      'mid' => $row->mid,
      'board' => $row->board,
      'date' => date('Y-m-d',$row->rdate),
      'text' => "$white v $black", 
      'result' => $row->result );
    if(!isset($row->rdate)) {
      $result['date'] = substr($row->date,0,10);
    }
    $results[] = $result;
  }

  return $results;
}

// Cancel button pressed in the popup form

function league_chess_result_popup_form_cancel($form, &$form_state ) {
  // Tell the browser to close the modal.
  $form_state['ajax_commands'][] = ctools_modal_command_dismiss();
}

// Up button pressed in the popup form

function league_chess_result_popup_form_up($form, &$form_state ) {
  // Move the board upwards
  league_chess_result_popup_change_board($form_state, FALSE);
  // Tell the browser to close the modal.
  $form_state['ajax_commands'][] = ctools_modal_command_dismiss();
  // Refresh the page to capture updated scores.
  $form_state['ajax_commands'][] = ctools_ajax_command_reload();
}

// Down button pressed in the popup form

function league_chess_result_popup_form_down($form, &$form_state ) {
  // Move the board downwards
  league_chess_result_popup_change_board($form_state, TRUE);
  // Tell the browser to close the modal.
  $form_state['ajax_commands'][] = ctools_modal_command_dismiss();
  // Refresh the page to capture updated scores.
  $form_state['ajax_commands'][] = ctools_ajax_command_reload();
}

function league_chess_result_popup_change_board($form_state, $down=FALSE) {
  $event = $form_state['league_chess_event'];
  $pid1 = $form_state['league_chess_pid1'];
  $pid2 = $form_state['league_chess_pid2'];
  $results = $form_state['league_chess_results'];

  $order = 'DESC';
  $gl = '<';
  $new_board = null;
  // Moving down, increases the board number
  if($down) {
    $order = 'ASC';
    $gl = '>';
  }

  // For the first result between the 2 players ...
  $result = $results[0];
  $new_rid = 0;
  // get board number and match
  $mid = $result['mid'];
  $rid = $result['rid'];
  $board = $result['board'];
  // find the next (down) / previous(up) board number from the current 
  $query = db_select('league_chess_result', 'r');
  $query->condition('r.mid', $mid);
  $query->condition('r.board', $board, $gl);
  $query->addField('r', 'rid', 'rid');
  $query->addField('r', 'board', 'board');
  // Order by board
  $query->orderBy('r.board', $order);
  $result = $query->execute();
  $row = $result->fetchObject();
  if($row) {
    $new_board = $row->board;
    $new_rid = $row->rid;
  }
  // if there is one (ie not already at bottom / top ) ....
  if(!isset($new_board)) {
    if($down) {
      $new_board = 0;
    } else {
      $new_board = 1;
    }
  }
  // if there is one (ie not already at bottom / top ) ....
  if($new_board>0 && $board>0) {
    //   Update the result database records for the 2 results swapping
    //   the board numbers
      db_update('league_chess_result')
    ->fields(array('board' => $board,))
    ->condition('rid', $new_rid, '=')
    ->execute();
      db_update('league_chess_result')
    ->fields(array('board' => $new_board,))
    ->condition('rid', $rid, '=')
    ->execute();
  }

  // Let other modules respond to a match update.
  $org = league_event_value($event, 'org');
  module_invoke_all('league_chess_match_update', $org, $fids);
}

// Submit button pressed in the popup form

function league_chess_result_popup_form_submit($form, &$form_state ) {
  $event = $form_state['league_chess_event'];
  $pid1 = $form_state['league_chess_pid1'];
  $pid2 = $form_state['league_chess_pid2'];
  $results = $form_state['league_chess_results'];
  global $user;

  $fids = array();
  // For each result between the 2 players ...
  foreach($results as $result) {
    // Get the result
    $field = 'res' . $result['rid'];
    $res = $form_state['values'][$field];
    // Get the date
    $field = 'dat' . $result['rid'];
    $date = $form_state['values'][$field];
    if(isset($date)) {
      $date = strtotime($date);
    }
    // Save the fixture id
    $fids[] = $result['fid'];
    // Update the result database record
    db_update('league_chess_result')
    ->fields(array('result' => $res, 'rdate' => $date))
    ->condition('rid', $result['rid'], '=')
    ->execute();
    // Update the fixture database record
    db_update('league_fixture')
    ->fields(array('reporter' => $user->uid, 'report_time' => time()))
    ->condition('nid', $result['fid'], '=')
    ->execute();

  }

  // Let other modules respond to a match update.
  $org = league_event_value($event, 'org');
  module_invoke_all('league_chess_match_update', $org, $fids);

  // Tell the browser to close the modal.
  $form_state['ajax_commands'][] = ctools_modal_command_dismiss();
  // Refresh the page to capture updated scores.
  $form_state['ajax_commands'][] = ctools_ajax_command_reload();
}
