<?php
/**
 * @file
 * Functions for Generating Individual Event pairings
 * 
 * Note about all play all pairings:
 *   Problems with adding new players to an existing event:
 *     Some matches might have been played already (with wrong colour)
 *     Players could be playing each other more than once.
 *
 * Entering results for an all play all would be easier with javascript
 * direct into the result table with a popup drop down (as per matchcard)
 */

define("LEAGUE_CHESS_MAX_EVAL", 9999999999999999);

/**
 * Event parings generation tab.
 */
function league_event_tab_pairings($node) {

  $type = league_event_value($node, 'type');

  // Form for generating all play all pairings
  $content['heading']['#markup'] = "<h2>Pairing Generation</h2>";
  $content['heading']['#type'] = 'markup';

  if( $type == 'A') {
    $text = "This option will attempt to generate pairings that do not already exist based on the event player list. Any existing pairings may get moved between rounds but will not be deleted. It will use all players in the player list so only include those you want in the draw"; 
  } else {
    // Restrict access to Swiss to testers
//global $user;
//if(!in_array('tester', $user->roles)) {
//
//  drupal_set_message("Generate Pairings for Swiss Option available to testers only", 'warning');
//  drupal_goto('node/' . $node->nid);
//}
    $text = "This option will attempt to generate swiss pairings for the selected round. It will not delete any existing pairings with results. If you do not want to use the whole player list, select the players you want included into round 1 in any order and it will repair them if you select that option. In subsequent rounds it will not repeat any pairings from previous rounds and it will try to maintain colour sequence."; 
  }

  $content['text']['#markup'] = $text;
  $content['text']['#type'] = 'markup';



  $content['form'] = drupal_get_form('league_chess_pairings_form', $node);

  if( $type == 'S') {
    $alg_text = "This option does not use any official FIDE or ECF pairings system. It works by giving each possible pairing a score and chosing the pairing with the lowest score. The score is calculated as ( sum_of_differences_between_players_scores*100 + score_of_player_with_bye*10 + number_of_out_of_sequence_colours) ";
    $content['algorithm']['#markup'] = $alg_text;
    $content['algorithm']['#type'] = 'markup';
  }

  return $content;
}

// Get a list of rounds to display on the drop down for swiss pairing

function league_chess_pairing_get_rounds($node) {
  $rows = array();
  $event = $node->nid;

  $query = db_select('league_fixture', 'f');
  $query->fields('f', array('nid', 'date', 'round', 'date_option'));
  $query->condition('f.cid', $event);

  $result = $query->execute();

  $max_round=0;
  foreach ( $result as $row) {
    $round = $row->round;
    if($round > $max_round) {
      $max_round = $round;
    }
    $rows[$round] = 'Round ' . strval( $round );
  }
  $rows[0] = 'Round ' . strval( $max_round+1 );
  return $rows;
}

function league_chess_pairing_list_rounds($node) {
  $rows = array();
  $event = $node->nid;

  $query = db_select('league_fixture', 'f');
  $query->fields('f', array('nid', 'round'));
  $query->condition('f.cid', $event);

  $result = $query->execute();

  $max_round=0;
  foreach ( $result as $row) {
    $round = $row->round;
    $rows[$round] = $row->nid;
  }
  return $rows;
}

/**
 *  Form to specify fixture generation details.
 */
function league_chess_pairings_form($form, &$form_state, $node) {
  $type = league_event_value($node, 'type');
  $form_state['league_event'] = $node;

  if( $type == 'A') {
    $form['odd'] = array(
      '#title' => t('First player black'),
      '#type' => 'checkbox',
    );

    $form['delete'] = array(
      '#title' => t('Delete Existing Pairings with no results'),
      '#type' => 'checkbox',
    );
  } else {

    // Seed the draw.
    $form['seed'] = array(
      '#title' => t('Do a seeded draw based on rating'),
      '#type' => 'checkbox',
      '#default_value' => 0,
    );

    // Pair from list in first round?
    $form['list'] = array(
      '#title' => t('Pair only the players already selected in the first round instead of the whole player list'),
      '#type' => 'checkbox',
      '#default_value' => 0,
    );

    $rounds = league_chess_pairing_get_rounds($node);
    // Round
    $form['round'] = array(
     '#title' => t('Round to Pair'),
     '#type' => 'select',
     '#options' => $rounds,
     '#weight' => 40,
    );
  }

  $form['submit'] = array(
    '#value' => t('Generate Pairings'),
    '#type' => 'submit',
  );
  return $form;
}

function league_chess_pairings_form_submit($form, &$form_state) {
  $node = $form_state['league_event'];
  $type = league_event_value($node, 'type');

  // Get players in list.
  $lid = league_chess_event_value($node, 'list');
  $rate = league_chess_event_value($node, 'rate');
  $rating_list = rating_list_load($rate);
  $players = league_chess_get_list_players($lid,$rate,'Z');

  // Swiss pairings
  if( $type == 'S') {
    $round = $form_state['values']['round'];
    $seed = $form_state['values']['seed'];
    $list = $form_state['values']['list'];
    $player_limit = 50;
    global $user;
    if( $list && count($players) > $player_limit && !in_array('tester', $user->roles)) {
  
      drupal_set_message("Only certified testers are allowed to generate swiss pairings on more than $player_limit players", 'warning');
    } else {
      league_chess_pairings_swiss_batch($node, $round, $players, $seed, $list);
    }

  // All play all pairings
  } else {
    $odd = $form_state['values']['odd'];
    $delete = $form_state['values']['delete'];

    if($delete) {
      league_chess_delete_results($node->nid);
      league_chess_delete_rounds($node->nid);
    } 

    // Generate pairings for all rounds based on player list
    $pairings = league_chess_pairings_generate($node, $odd, $players);

    // Load any existing results and rounds
    $existing_results = league_chess_pairings_load($node);
    $date = date("Y-m-d") . 'T19:30:00';

    $nps=0;
    // For each generated round ...
    foreach($pairings as $round => $results) {

      // Create it if it doesn't exist
      if(isset($existing_results['rounds'][$round])) {
        $rid = $existing_results['rounds'][$round];
      } else {
        $rid = league_chess_create_round($node->nid,$round,$date);
      }
      $fixtureNode = node_load($rid);
      $mid = $fixtureNode->league_matches[0]->mid;
      // For each result in the round ...
      $board=0;
      foreach($results as $result) {
        // TODO check if danger of same board no here ??
        $board++;
        // Create new result if no existing matches between the players
        if(!isset($existing_results['matches'][$result['white']][$result['black']])) {

          league_chess_create_result($mid,$result['white'],$result['black'], $board,$date,$players,$rate);
          $nps++;
        }
      }
    }
    drupal_set_message("$nps pairings generated");
  } 
}

// Generate all play all pairings.

function league_chess_pairings_generate($node, $odd, $players) {
  $pairings = array();
  $nplayers = count($players);
  $ntimes = league_event_value($node, 'ntimes');
  // Set up array to do fixtures from
  $table = array();
  foreach($players as $pid => $player) {
    $table[] = $pid;
  }
  // If odd number of players, add a bye player
  if ( $nplayers % 2 != 0 ) {
    $table[$nplayers] = 0;
    $nplayers++;
  }
  $nmatches = $nplayers / 2;
  $nrounds = ($nplayers - 1);

  // Check if max fixtures exceeded
  $limits = league_get_limits();
  $max_fixtures = $limits['max_fixtures'];
  $expected_fixtures = $nrounds * $nmatches * $ntimes;
  if( $expected_fixtures > $max_fixtures ) {
    drupal_set_message("You are trying to generate $expected_fixtures fixtures, but you are not allowed to generate more than $max_fixtures", 'error');
    return array();
  }

  // for each round
  $rounds = array();
  for ($r = 0; $r < $nrounds; $r++) {
    // for each match
    for ($m = 0; $m < $nmatches; $m++) {
      // option to swap white and black to make things different between
      // seasons
      if ($odd) {
        $rounds[$r][$m]['white'] = $table[$m];
        $rounds[$r][$m]['black'] = $table[$nplayers -$m -1];
      }
      else {
        $rounds[$r][$m]['black'] = $table[$m];
        $rounds[$r][$m]['white'] = $table[$nplayers -$m -1];
      }
    }
    // rotate the table for the next set of fixtures
    league_chess_rotate($table, 1);
  }
  // Interleave so that white and black games are fairly evenly dispersed.
  $interleaved = array();
  for ($i = 0; $i < $nrounds; $i++) {
    $interleaved[$i] = array();
  }

  $evn = 0;
  $odd = ($nplayers / 2);
  for ($i = 0; $i < $nrounds; $i++) {
    if ($i % 2 == 0) {
      $interleaved[$i] = $rounds[$evn++];
    } else {
      $interleaved[$i] = $rounds[$odd++];
    }
  }

  // flip the white and black of match 0 which contains the last team
  // because it is fixed in the table when generating fixtures
  for ($i = 0; $i < $nrounds; $i += 2) {
    $tempTeam = $interleaved[$i][0]['white'];
    $interleaved[$i][0]['white'] = $interleaved[$i][0]['black'];
    $interleaved[$i][0]['black'] = $tempTeam;
  }

  $rounds = $interleaved;
  $round = 0;

  // For each time the players play ...
  for ($it = 0; $it < $ntimes; $it++) {
    // Transfer to fixtures array
    for ($r = 0; $r < $nrounds; $r++) {
      $round_pairings = array();
      // for each match
      for ($m = 0; $m < $nmatches; $m++) {
        // If its not a fixture with bye, then add it
        if ( $rounds[$r][$m]['white'] != 0 && $rounds[$r][$m]['black'] != 0 ) {
          $pairing = array();
          // Flip for 2nd half of season
          if ($it % 2 == 0) {
            $pairing['white'] = $rounds[$r][$m]['white'];
            $pairing['black'] = $rounds[$r][$m]['black'];
          } else {
            $pairing['white'] = $rounds[$r][$m]['black'];
            $pairing['black'] = $rounds[$r][$m]['white'];
          }
          $round_pairings[] = $pairing;
        }
      }
      $round++;
      $pairings[$round] = $round_pairings;
    }  // rounds
  } // ntimes

  return $pairings;
}

// Load the existing pairings for an all play all event

function league_chess_pairings_load($node) {
  $rounds = array();
  $matches = array();
  $query = league_chess_individual_event_query($node, TRUE);
//drupal_set_message(print_r((string)$query,TRUE));
  $queryResult = $query->execute();
  foreach ( $queryResult as $row ) {
    $white = $row->white;
    $black = $row->black;
    $round = $row->round;
    $rid = $row->rid;

    $rounds[intval($round)] = $rid;
    $matches[$row->white][$row->black]=1;
    $matches[$row->black][$row->white]=1;
  }
  // Return data
  $existing_results = array(
     'rounds' => $rounds,
     'matches' => $matches,
  );
  return $existing_results;
}

// Create a new round in an individual event

function league_chess_create_round($event,$round,$date) {
  $node = league_create_fixture($event, 0, 0, $date, $round);
  node_save($node);
  return $node->nid;
}

/**
 *   Rotate table for next set of pairings in all play all event
 */
function league_chess_rotate(&$table, $ntimes) {
  $nplayers = count($table);
  for ($i = 0; $i < $ntimes; $i++) {
    $first = $table[0];
    for ($t = 0; $t < $nplayers -2; $t++) {
      $table[$t] = $table[$t + 1];
    }
    $table[$nplayers -2] = $first;
  }
}

// Create a record in the results table for one game
//  ( just the pairing, but with no result yet )
//   $fid      - fixture id
//   $white    - pid of white
//   $black    - pid of black
//   $board    - board number
//   $date     - match date
//   $players  - list of players as in matchcard drop down
//   $rate     - rate of play
function league_chess_create_result($mid,$white,$black,$board,$date,$players,$rate) {
//drupal_set_message("league_chess_create_result $mid $white $black $board $rate");
  $whiteGrade = ' ';
  $blackGrade = ' ';
  if($rate > 0 ) {
    // 4 digit
    if($rate >7) {
      if($white >0) {
        $whiteGrade = substr($players[$white],-6,4);
      }
      if($black >0) {
        $blackGrade = substr($players[$black],-6,4);
      }
    } else {
      $whiteGrade = substr($players[$white],-6,3);
      $blackGrade = substr($players[$black],-6,3);
    }
  }
  league_chess_write_result(0, $board, $white, 'N', $black, $mid, 'W', strtotime($date), $whiteGrade, $blackGrade);
}

// Remove results(pairings) in the event with no result (W/D/L) recorded.
function league_chess_delete_results($event) {
  $rids = array();
    // Fixtures in the event
  $query = db_select('league_fixture', 'f');
  $query->condition('f.cid', $event);
  $query->addField('f', 'nid', 'round');
  // Results in the fixtures
  $query->innerJoin('league_match', 'm', 'm.fid = f.nid');
  $query->innerJoin('league_chess_result', 'r', 'r.mid = m.mid');
  $query->addField('r', 'rid', 'rid');
  $query->condition('r.result', array('N',' '), 'IN');
  $result = $query->execute();
  foreach ( $result as $row) {
    $rids[] = intval($row->rid);
  }
  if(count($rids) > 0) {
    $nrecs = db_delete('league_chess_result')
    ->condition('rid' , $rids, 'IN')
    ->execute();
    drupal_set_message("$nrecs existing pairings deleted.");
  }
}

// Delete rounds in event that have no results.
function league_chess_delete_rounds($event) {
  $rounds = array();
    // Fixtures in the event
  $query = db_select('league_fixture', 'f');
  $query->condition('f.cid', $event);
  $query->addField('f', 'nid', 'round');
  $query->leftJoin('league_match', 'm', 'm.fid = f.nid');
  $query->leftJoin('league_chess_result', 'r', 'r.mid = m.mid');
  $query->where("r.rid IS NULL");
  $result = $query->execute();
  foreach ( $result as $row) {
    $rounds[] = intval($row->round);
  }
  foreach($rounds as $nid) {
    node_delete($nid);
  }
}

// Set up the swiss pairings batch operation
// ( this will show progress at various points to the user )
//
function league_chess_pairings_swiss_batch($node, $round, $list_players, $seed, $list) {
  $module_file = drupal_get_path('module', 'league_chess') .'/league_chess_pairings.inc';
  $operations = array(
     array('league_chess_pairings_swiss_start', array($node,$round,$list_players,$seed, $list)),
  );

  $batch = array(
        'operations' => $operations,
        'finished' => 'league_chess_pairings_swiss_finished',
        'title'    => 'Generating Pairings',
        'init_message' => 'Starting ...',
        'file' => $module_file,
    );
  batch_set($batch);

}

// Insert the new Swiss pairings that were found:
//
// $pairings     - the new pairings to insert
// $rounds       - the list of pervious rounds
// $round        - the round number of the current round
// $nid          - the node id of the event
// $board        - the board number to start from
//                 (might not be 1 if some parings already exist)
// $list_players - list of players and ratings

function league_chess_pairings_batch_insert($pairings, $rounds, $round, $nid, $board, $list_players) {
  // Create new round if the round we are pairing does not exist yet.
  $date = date("Y-m-d") . 'T19:30:00';
  $rate = league_chess_event_value($nid, 'rate');
  $nrounds = count($rounds);
  if($round ==0) {
    $rid = league_chess_create_round($nid,$nrounds+1,$date);
  } else {
    $rid = $rounds[$round];
  }
  $fixtureNode = node_load($rid);
  $mid = $fixtureNode->league_matches[0]->mid;
  // get existing pairings with no results for the round.
  $existing = league_chess_get_existing_pairings($rid);
  // get board number from existing pairings
//$board=$results['board'];
  // Insert pairings into the round
  foreach($pairings as $pairing) {
//  drupal_set_message(print_r($pairing, TRUE));
    $board++;
      league_chess_create_result($mid,$pairing[0],$pairing[1], $board,$date,$list_players,$rate);
  }
  // remove the old pairings
  league_chess_remove_existing_pairings($existing);
}

// Convert a list of players into a set of pairings by taking them
// in groups of 2.
function league_chess_pairings_from_list($list) {
  $pairings = array();
  $i=0;
  $n= count($list);
  while($i < $n) {
    $pairing = array($list[$i], $list[$i+1]);
    $pairings[] = $pairing;
    $i+=2;
  }
  return $pairings;
}

// Function called when the pairings batch has finished.
function league_chess_pairings_swiss_finished($success,$results,$operations) {
  if( $success ) {
    if(!isset($results['best_score']) ) {
      return;
    }
    $score = $results['best_score'];
    $list = $results['best_pairings'];
    $pairing_scores = $results['data'];
    $nid = $results['nid'];
    $rounds = $results['rounds'];
    $round = $results['round'];
    $board = $results['board'];
    $players = $results['players'];
    $list_players = $results['list_players'];
    $start_time = $results['start_time'];
    $total_time = time() - $start_time;
    drupal_set_message("Pairings Generation Complete with score $score in $total_time seconds");
//  drupal_set_message(print_r($players, TRUE));
    // Turn the list of players into pairings by taking groups of 2.
    $pairings = league_chess_pairings_from_list($list);

    // Swap white and black round if necessary to get the best score.
    for($ip=0; $ip<count($pairings); $ip++) {
      $white = $pairings[$ip][0];
      $black = $pairings[$ip][1];
      if( $pairing_scores[$black][$white] < $pairing_scores[$white][$black] ) {
        $pairings[$ip][0] = $black;
        $pairings[$ip][1] = $white;
      }
    }

    // Insert the new pairings into the database.
    league_chess_pairings_batch_insert($pairings, $rounds, $round, $nid, $board, $list_players);
  } else {
    drupal_set_message("Pairings Generation Failed");
    reset($operations);
  }
}

// Start the Swiss Pairing batch running.

function league_chess_pairings_swiss_start ($node, $round, $list_players, $seed, $list, &$context) {
  if(!isset($context['sandbox']['current'])) {
    $context['sandbox']['start_time'] = time();
//  drupal_set_message("league_chess_pairings_swiss_start START");
    $data = league_chess_pairings_swiss_data($node, $round, $list_players, $seed, $list);
    if( count($data) == 0 ) {
      return 0;
    }
    $pairing_scores = $data['pairing_scores'];
    $board = $data['board'];
    $rounds = $data['rounds'];
    $context['sandbox']['data'] = $pairing_scores;
    $context['sandbox']['nid'] = $node->nid;
    $context['sandbox']['rounds'] = $rounds;
    $context['sandbox']['round'] = $round;
    $context['sandbox']['board'] = $board;
    // This is a list of players in the player list plus ratings
    $context['sandbox']['list_players'] = $list_players;
    $context['sandbox']['chunk'] = 100;
    // List of players ( with random shuffle )
    $players = array_keys($pairing_scores);
    shuffle($players);
    $context['sandbox']['players'] = $players;
    $n = count($context['sandbox']['players']);
    $context['sandbox']['n'] = $n;
    // Setup
    $npairs = $n/2;
    $context['sandbox']['npairs'] = $npairs;
    // Number of times to rotate the array at each level
    $rotations = array();
    for($i=0; $i<$npairs-1; $i++) {
      $rotations[]=0;
    }
    $nrotates = array($n-1);
    for($i=1; $i<$npairs-1; $i++) {
      $nrotates[] = $nrotates[$i-1]-2;
    }
    $context['sandbox']['rotations'] = $rotations;
    $context['sandbox']['nrotates'] = $nrotates;
    // Total possible sets pairings to check
    $total = 1;
    for($i=1; $i<=$npairs; $i++) {
      $total = $total * ($i*2 - 1);
    }
    $context['sandbox']['total'] = $total;
    //
    $current = $npairs-1;
    $context['sandbox']['current'] = $current;
    $context['sandbox']['done'] = 0;
    $context['sandbox']['best_score'] = LEAGUE_CHESS_MAX_EVAL;
    $context['sandbox']['best_pairings'] = $context['sandbox']['players'];
//  league_chess_pairings_swiss_update_best($context, $context['sandbox']['players']);

  }
  league_chess_pairings_swiss_process_batch($context);
}

// Update the best set of pairings found so far.

function league_chess_pairings_swiss_update_best(&$context, $players) {
  static $calls=0;
  $calls++;
  $pairing_scores = $context['sandbox']['data'];
  $score=0;
  $count=0;
  while($count < count($players) ) {
    $white = $players[$count];
    $black = $players[$count+1];
    $score += min($pairing_scores[$white][$black], $pairing_scores[$black][$white]);
    $count+=2;
  }
//drupal_set_message("Update best: calls $calls score $score best: " . $context['sandbox']['best_score']);
//drupal_set_message(print_r($players, TRUE));
  if($score < $context['sandbox']['best_score']) {
//  drupal_set_message("Updating best score to $score from ".$context['sandbox']['best_score']); 
//  drupal_set_message(print_r($players, TRUE));
    $context['sandbox']['best_score'] = $score;
    $context['sandbox']['best_pairings'] = $players;
  }
}

// Get the data about the players and previous rounds in order to do the
// pairings.

function league_chess_pairings_swiss_data($node, $round, $list_players, $seed, $list) {
  $event = $node->nid;
  // get all pairings for this and previous rounds.
  // we need to get for each player, their:
  //  score
  //  previous opponents
  //  colour sequence
  $results = league_chess_swiss_pairings_load($node, $round);
  if($list || $round >1) {
    drupal_set_message('Pairing players based on scores in previous round');
    $players_to_pair = $results['players'];
  } else {
    drupal_set_message('Pairing players from player list for first round');
    $lid = league_chess_event_value($node, 'list');
    $players_to_pair = league_player_players_in_lists_export($lid);
  }
  $rounds = $results['rounds'];
  $board = $results['board'];

  // Remove any that have results in the round already
  foreach($results['paired'] as $pid) {
    if (($key = array_search($pid, $players_to_pair)) !== false) {
      unset($players_to_pair[$key]);
    }
  }

  // Check we have some left!
  $num_players = count($players_to_pair);
  if( $num_players == 0) {
    drupal_set_message('There are no players in the first round to pair or the specified player list is empty', 'warning');
    return array();
  }

  // Add a bye pairing if odd number of players
  if( $num_players % 2 !=0) {
    $players_to_pair[] = LEAGUE_CHESS_FULLBYE;
    $num_players++;
  }

  // Calculate the score of each possible pairing.
  $pairing_scores = array();
  for($w=0; $w<$num_players; $w++) {
    $white = $players_to_pair[$w];
    $pairing_scores[$white] = array();
    for($b=0; $b<$num_players; $b++) {
      $black = $players_to_pair[$b];
      $pairing_scores[$white][$black] = league_chess_pairing_evaluation($white, $black, $results['results'], $seed);
    }
  }
   
  $data = array(
    'pairing_scores' => $pairing_scores,
    'rounds' => $rounds,
    'board' => $board,
  );
  return $data;
}

// Process a group of pairings:
//  evaluate them and see if they are better than what we have so far.
//  ( this was copied from an implementation of Heap's algorithm
//    and modified so that it is restartable after updating the status
//    on the web page )

function league_chess_pairings_swiss_process_batch(&$context) {
  $sandbox = $context['sandbox'];
  $data = $sandbox['data'];
  $chunk = $sandbox['chunk'];
  $current = $sandbox['current'];          // Current fixture set
  $players = $sandbox['players'];
  $rotations = $sandbox['rotations'];
  $npairs = $sandbox['npairs'];
  $n = $sandbox['n'];
  $current = $sandbox['current'];
  $chunk = $sandbox['chunk'];
  $nrotates = $sandbox['nrotates'];
  $total = $sandbox['total'];
  $done = $sandbox['done'];
  $best_score = $sandbox['best_score'];
//drupal_set_message("league_chess_pairings_swiss_process_batch n $n chunk $chunk total $total done $done best_score $best_score current $current");

  $finished = $rotations[0]==$nrotates[0];
  $count = 0;
  while($count < $chunk && !$finished) {
//  drupal_set_message("Checking count $count current $current");
    while($rotations[$current-1] == $nrotates[$current-1] && $current>1) {
      $rotations[$current-1] = 0;
      $current--;
      league_chess_pairings_players_rotate($players, $current*2-1);
      $rotations[$current-1]++;
    }
    $current = $npairs-1;
    league_chess_pairings_players_rotate($players, $current*2-1);
    $rotations[$current-1]++;
    $count++;
    $finished = $rotations[0]==$nrotates[0] || $context['sandbox']['best_score'] == 0;
    if(!$finished) {
      league_chess_pairings_swiss_update_best($context, $players);
    }
//  drupal_set_message(print_r($rotations, TRUE));
  }
  $context['sandbox']['current'] = $current;
  $context['sandbox']['rotations'] = $rotations;
  $context['sandbox']['players'] = $players;
  $context['sandbox']['done'] = $sandbox['done'] + $count;
  $context['sandbox']['nrotates'] = $nrotates;

  if($finished || $done == $total) {
    $context['finished'] = 1;
    $context['results'] = array(
      'best_score'    => $context['sandbox']['best_score'],
      'best_pairings' => $context['sandbox']['best_pairings'],
      'data' => $context['sandbox']['data'],
      'nid' =>           $context['sandbox']['nid'],
      'rounds' =>        $context['sandbox']['rounds'],
      'round' =>        $context['sandbox']['round'],
      'board' =>        $context['sandbox']['board'],
      'players' =>        $context['sandbox']['players'],
      'list_players' =>        $context['sandbox']['list_players'],
      'start_time' =>        $context['sandbox']['start_time'],
    );
  } else {
    $context['finished'] = $done / $total;
  }
  $context['message'] = "Processed $done sets from $total";
}

// Load data about swiss event so far including players, colour sequences and
// scores.
function league_chess_swiss_pairings_load($node, $pair_round) {
  $players = array();             # players in round 1
  $paired_already= array();       # players who are alrady paired
  $results = array();             # the results for the event
  $rounds = array();              # the rounds in the event
  
  // Get all the event details
  $query = league_chess_individual_event_query($node, TRUE);
  $queryResult = $query->execute();

  // For each pairing / result in the event ...
  foreach ( $queryResult as $row ) {
    $white = $row->white;
    $black = $row->black;
    $round = $row->round;
    $rid = $row->rid;
    $wdl = $row->result;
    $board = $row->board;
    $white_rating = league_chess_strip_star($row->hrating);
    $black_rating = league_chess_strip_star($row->arating);
    if($round <= $pair_round) {
      // Rounds
      $rounds[$round] = $rid;

      // Get a list of players in round 1
      if($round == 1) {
        if($white>0 ) {
          $players[] = $white;
        }
        if($black>0 ) {
          $players[] = $black;
        }
      }

      $max_board=0;
      if($round == $pair_round) {
        // Are they paired already?
        if($wdl != 'N' && $wdl != ' ' ) {
          if($white>0 && $black>0) {
            // What board is used?
            if($board > $max_board) {
              $max_board=$board;
            }
          }
          if($white>0 ) {
            $paired_already[] = $white;
          }
          if($black>0 ) {
            $paired_already[] = $black;
          }
        }
      }

      league_chess_store_result($results, $white, 'W' , $wdl, $black, $white_rating, $round, $pair_round);
      league_chess_store_result($results, $black, 'B' , $wdl, $white, $black_rating, $round, $pair_round);
    }

  }

  // Check for up or down floats
  foreach( $results as $pid => $data ) {
    $nscores = count($data['scores']);
    for($i=1; $i<$nscores; $i++) {
      $opponent = $data['opponents'][$i];
      $opponent_score = $results[$opponent]['scores'][$i];
      $my_score = $data['scores'][$i];
      if($my_score < $opponent_score) { 
        $results[$pid]['float']++;
      } else {
        if($my_score > $opponent_score) { 
          $results[$pid]['float']--;
        }
      }
    }
  }
  $rounds = league_chess_pairing_list_rounds($node);

//drupal_set_message(print_r($results, TRUE));

  // Collect results
  $existing_results = array(
     'players' => $players,
     'results' => $results,
     'paired' => $paired_already,
     'board' => $max_board,
     'rounds' => $rounds,
  );
  return $existing_results;
}

// Store one result for the pairings
function league_chess_store_result(&$results, $pid, $colour, $result, $opid, $rating, $round, $pair_round) {

  // Initialise the player record if needed.
  if(!isset($results[$pid]) ) {
    $results[$pid] = array('colours' => array(), 'opponents' => array(), 'score' =>0, 'rating' => $rating, 'float' => 0, 'scores' => array() );
  }

  // Only store info for previous rounds
  if($round < $pair_round) {
    $results[$pid]['colours'][] = $colour;
    $results[$pid]['opponents'][] = $opid;
    if($result == 'D') {
      $results[$pid]['score'] += 1;
    } else {
      if($result == 'W' && $colour == 'W' || $result == 'L' && $colour == 'B' ) {
        $results[$pid]['score'] += 2;
      }
    }
    $results[$pid]['scores'][] = $results[$pid]['score'];
  }
}

// Evaluate the score of a possible pairing between two players.
// (lowest score is better)
// $pairing_set - the parings (pairs of player ids, or zero for bye)
// $results     - the colour sequence and score for each player
//       
function league_chess_pairing_evaluation($white, $black, $results, $seed) {

  // Check for a player playing themself
  if( $white == $black ) {
    return LEAGUE_CHESS_MAX_EVAL;
  }
  // Check that if they have played before
  if( $white >0 && $black >0 ) {
    if( isset($results[$black]['opponents']) ) {
    if( in_array($white, $results[$black]['opponents']) ) {
      return LEAGUE_CHESS_MAX_EVAL;
    }
    }
  }

  $eval_score = 0;
  $eval_colours = 0;
  $eval_bye = 0;
  $eval_rating = 0;
  $eval_float = 0;
  if( $white >0 && $black >0 ) {
    $eval_float = abs($results[$white]['float'] + $results[$black]['float']);
  }

  // If pairing does not contain a bye ...
  if( $white >0 && $black >0 ) {
    // Evaluate the score difference - the more the scores differ, the 
    // worse the pairing is.
    $eval_score += abs($results[$white]['score'] - $results[$black]['score'] );

    // Evaluate the colour sequence
    $eval_colours += league_chess_pairing_colour_sequence($results[$white]['colours']);
  }

  // Minimize the score of the player having the bye.
  if( $black < 0 ) {
    $eval_bye += $results[$white]['score'];
  }

  // Seed the draw.
  if($seed) {
    // If pairing does not contain a bye ...
    if( $white >0 && $black >0 ) {
      $rating_diff = abs($results[$black]['rating'] - $results[$white]['rating']);
      $eval_rating += 1 / (1 + $rating_diff);
    }
  }

  $score = $eval_score*1000 + $eval_bye*100 + $eval_colours*10 + $eval_rating + $eval_float;
  return $score;
}

// Calculate a score to represent a players colour sequence

function league_chess_pairing_colour_sequence($sequence) {
  $eval_seq = 0;
  $last = 'N';
  foreach($sequence as $colour) {
    if($last == 'N') {
      $last = $colour;
    } else {
      if($last == $colour) {
        $eval_seq++;
      } 
    } 
  }
  return $eval_seq;
}

// Remove the given list of result ids
function league_chess_remove_existing_pairings($existing) {
  if(count($existing) > 0) {
    $nrecs = db_delete('league_chess_result')
    ->condition('rid' , $existing, 'IN')
    ->execute();
    drupal_set_message("$nrecs existing pairings deleted.");
  }
}

// Get the result ids of the existing pairings in the round with no results
function league_chess_get_existing_pairings($round_id) {
  $existing = array();
    // Fixtures in the event
  $query = db_select('league_chess_result', 'r');
  $query->innerJoin('league_match', 'm', 'm.mid = r.mid');
  $query->condition('m.fid', $round_id);
  $query->addField('r', 'rid', 'id');
  $result = $query->execute();
  foreach ( $result as $row) {
    $existing[] = $row->id;
  }
  return $existing;
}

// Rotate a portion of the given array so that the last element is
// taken out and put in at the start, the other elements being moved
// along.
//  $table  - the array to shift
//  $start  - the element defining the subset of the array to shift.

function league_chess_pairings_players_rotate(&$table, $start) {
  $nplayers = count($table);
//drupal_set_message("rotate from $start to $nplayers");
  $first = $table[$start];
  for ($t = $start; $t < $nplayers-1; $t++) {
    $table[$t] = $table[$t + 1];
  }
  $table[$nplayers-1] = $first;
}

