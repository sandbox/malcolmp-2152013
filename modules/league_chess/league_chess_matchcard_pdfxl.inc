<?php

/**
 * @file
 * Functions for league_chess new player matches option.
 */
  
function league_chess_matchcard_pdfxl ($node,$format,$id=0) {
  // Set default content incase it fails
  $content = "<h3>Matchcard pdf</h3>$node->title $format";

  // PGN Template
  if ( $format == 'pgn' || $format == 'pgnz' || $format == 'pgnt') {
    $path = drupal_get_path('module', 'league_chess');
    require_once("$path/league_pgn_template.inc");
    league_pgn_template($node,$format,$id);
    return;
  }

  // Get the matchcard as a table
  require_once("league_chess_press_report.inc");
  $table = league_chess_matchcard_table($node);
 
  // PDF format
  if ( $format == 'pdf' ) {
    $table['title'] = 'Match Result';
    fpdf_table_page($table);
    return;
  }

  // XL format
  if ( $format == 'xl' ) {
    $path = drupal_get_path('module', 'league');
    require_once("$path/league_xl_table.inc");
    league_xl_table($table);
    return;
  }


  // Invalid format
  $content .= " Invalid format";

  return $content;
}

