<?php

/**
 * @file
 * Functions for displaying individual or swiss table
 */
  
function league_chess_table_swiss_tab ($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);
  // Set title
  drupal_set_title($node->title);

  $type = league_event_value($node, 'type');
  if($type == 'A') {
    $content[$node->nid]['#table'] = league_chess_table_all_play_all($node);
    $content[$node->nid]['#theme'] = 'league_table';
  } else {
    if($type == 'I') {
      $content[$node->nid]['#data'] = league_chess_get_knockout_data($node);
      $content[$node->nid]['#theme'] = 'league_knockout_table';
    } else {
      $content[$node->nid]['#table'] = league_chess_table_individual($node);
      $content[$node->nid]['#theme'] = 'league_table';
    }
  }

  return $content;
}

/**
 * Create table for an individual event.
 */
function league_chess_table_individual($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);

  $title = $node->title;
  $type = league_event_value($node, 'type');

  // Fixtures in the event
  $query = db_select('league_fixture', 'f');
  $query->addField('f', 'nid', 'fid');
  $query->addField('f', 'round', 'round');
  $query->condition('f.cid', $node->nid);
  $query->innerJoin('node', 'n', 'n.nid = f.nid');
  // results
  $query->innerJoin('league_match', 'm', 'm.fid = f.nid');
  $query->innerJoin('league_chess_result', 'r', 'r.mid = m.mid');
  $query->leftJoin('league_player', 'w', 'w.pid = r.white');
  $query->addField('w', 'lastname', 'wlastname');
  $query->addField('w', 'firstname', 'wfirstname');
  $query->leftJoin('league_player', 'b', 'b.pid = r.black');
  $query->addField('b', 'lastname', 'blastname');
  $query->addField('b', 'firstname', 'bfirstname');
  $query->addField('r', 'result', 'result');
//dsm((string)$query);
  $result = $query->execute();

  $data = array();
  $opponent = array();
  $colour = array();
  $rows = array();
  // Initialise totals
  foreach($result as $row) {
    $white = "$row->wfirstname $row->wlastname";
    $black = "$row->bfirstname $row->blastname";  
    $match = array();
    $match['round'] = $row->round;;
    $match['white'] = $white;
    $match['black'] = $black;
    $match['result'] = $row->result;
    $match['fid'] = $row->fid;
    $data[$white][0] = '0';
    $data[$black][0] = '0';
    $rows[] = $match;
  }

  $max_round = 0;
  $rounds = array();
  // Loop round the results putting the total points for each player
  // into round zero.
  foreach($rows as $row) {
    $white = $row['white'];
    $black = $row['black'];
    $round = $row['round'];
    if ( $round > $max_round) {
      $max_round = $round;
    }
    $fid = $row['fid'];
    $rounds[$round] = $fid;
    $opponent[$white][$round] = $black;
    $opponent[$black][$round] = $white;
    $colour[$white][$round] = 'w';
    $colour[$black][$round] = 'b';
    $res = $row['result'];
    // Win or win by default
    if ( $res == 'W' || $res == '+' ) {
      $data[$white][$round] = 2;
      $data[$black][$round] = 0;
      $data[$white][0] += 2;
    }
    else {
      // Loss or loss by default
      if ( $res == 'L' || $res == '-' ) {
        $data[$white][$round] = 0;
        $data[$black][$round] = 2;
        $data[$black][0] += 2;
      }
      else {
        // Draw.
        if ( $res == 'D' ) {
          $data[$white][$round] = 1;
          $data[$black][$round] = 1;
          $data[$white][0] += 1;
          $data[$black][0] += 1;
        }
        // No result.
        else {
          $data[$white][$round] = -1;
          $data[$black][$round] = -1;
        }
      }
    }
  }
 
  // Sort by points.
  uasort( $data, "_league_chess_swiss_sort" );
  // Set up table headers
  $header = array('No', 'Name');
  if ( $type == 'S' ) {
    for ($i = 1; $i <= $max_round; $i++) {
      if(isset($rounds[$i])) {
        $header[] = l('Round ' . $i, 'league_fixture/' . $rounds[$i] . '/match');
      } else {
        $header[] = 'Round ' . $i;
      }
    }
  }
  $header[] = 'Total';
  // Assign player numbers
  $pnums = array();
  $pnum = 0;
  foreach ($data as $player => $values) {
    if ( isset($player) && $player != NULL && strlen($player)>1 ) {
      $pnum++;
      $pnums[$player] = $pnum;
    }
  }
  $pnum = 0;

  // Transfer into array for table display
  $rows = array();
  foreach ($data as $player => $values) {
    // The query brings back NULL players for default or result not
    // entered yet, so we need to exclude this null player from the
    // table. We shouldn't ever get a player called Default, unless
    // the player list contains that player in which case it should
    // be removed.
    if ( isset($player) && $player != NULL && strlen($player)>1 ) {
      $pnum++;
      $row = array($pnum, $player);
      if ( $type == 'S' ) {
        for ($i = 1; $i <= $max_round; $i++) {
          $cell = '<b>';
          if(isset($values[$i]) && $values[$i]!=-1 ){
            $cell .= league_score2points($values[$i]);
          } else {
            $cell .= '-'; // Add zero, to stop blank if no score at all
          }
          $opp = ' ';
          $colr = ' ';
          if(isset($opponent[$player][$i]) ) {
            if(isset($pnums[$opponent[$player][$i]]) ) {
              $opp = $pnums[$opponent[$player][$i]];
            }
            if( isset($colour[$player][$i]) ) {
              $colr = $colour[$player][$i];
            }
          }
          $cell .= '</b>&nbsp;';
          $cell .= ' (' . $colr . $opp . ')';
          $row[] = $cell;
        }
      }
      $row[] = league_score2points($values[0]);
      $rows[] = $row;
    }
  }
  $table['header'] = $header;
  $table['data'] = $rows;

  return $table;
}

// Results table for all play all individual

function league_chess_table_all_play_all($node) {
  // Get the query to obtain all the results
  $query = league_chess_individual_event_query($node, TRUE);
  // Get the player list, so we can include only those players
  // who have results and appear in the list.
  // this enables players who have withdrawn to be removed from the list
  // and not appear.
  $lid = league_chess_event_value($node, 'list');
  if( $lid != 0 ) {
    $list_players = league_player_players_in_lists_export($lid);
    // Check for an empty player list.
    if( count($list_players) >0 ) {
      $query->condition('r.white', $list_players, 'IN');
      $query->condition('r.black', $list_players, 'IN');
    }
  }
//dsm((string)$query);

  $points = array();
  $players = array();
  // execute the query to get all the results
  $queryResult = $query->execute();

  // Get result and add up scores and total games for each player

  foreach ( $queryResult as $row ) {
    $players[$row->white] = "$row->wlast, $row->wfirst";
    $players[$row->black] = "$row->blast, $row->bfirst";
    $points[] = array($row->white, $row->black, $row->result);
  }
  $num_players = count($players);
  // Initialise grid of results
  //  - data[x]['total'] will contain total point for player x
  $data = array();
  // We can't iterate over players twice so set up data first
  foreach ($players as $player => $name) {
    $data[$player][$player] = ' ';
  }
  foreach ($data as $player => $values) {
    foreach ($players as $opponent => $oname) {
      $data[$player][$opponent] = ' ';
    }
    $data[$player]['total'] = 0;
  }
  
  // Loop round the results putting the total points for each player
  // into round zero.
  foreach($points as $row) {
    $white = $row[0];
    $black = $row[1];
    $res = $row[2];
    if ( $res == 'W' || $res == '+'  ) {
      $data[$white][$black] .= league_score2points('2');
      $data[$black][$white] .= '0';
      $data[$white]['total'] += '2';
    }
    else {
      if ( $res == 'L' || $res == '-'  ) {
        $data[$white][$black] .= '0';
        $data[$black][$white] .= league_score2points('2');
        $data[$black]['total'] += '2';
      }
      else {
        if ( $res == 'D' ) {
          $data[$white][$black] .= league_score2points('1');
          $data[$black][$white] .= league_score2points('1');
          $data[$white]['total'] += '1';
          $data[$black]['total'] += '1';
        }
      }
    }
    $data[$white][$black] .= '(w)';
    $data[$black][$white] .= '(b)';
  }
  
  // Sort by points.
  uasort( $data, "league_chess_all_sort" );
  $playerOrder = array();
  // Set up table headers and new player order array
  $header = array('Name');
  foreach ( $data as $player => $values ) {
    $name = $players[$player];
    $comma = strpos($name, ',');
    if($comma+2 < strlen($name)) {
      $name = substr($name,0,$comma+3);
    }
    $header[] = $name;
    $playerOrder[] = $player;
  }
  $header[] = 'Total';

  // Transfer into array for table display
  $rows = array();
  foreach ($data as $player => $values) {
    $row = array($players[$player]);
    foreach ($playerOrder as $opponent) {
      if ( $player == $opponent ) {
        $row[] = 'x';
      }
      else {
        if(league_check_access($node->nid) && league_table_links() ) {
          $row[] = l($data[$player][$opponent],'league_chess/nojs/' . $node->nid . "/$player/$opponent", array('attributes' => array('class' => 'ctools-use-modal ctools-modal-league-chess-display-modal-dialog-style')));

        } else {
          $row[] = $data[$player][$opponent];
        }
      }
    }
    $row[] = league_score2points($data[$player]['total']);
    $rows[] = $row;
  }
  $table['header'] = $header;
  $table['data'] = $rows;

  // Load the modal library and add the modal javascript.
  ctools_include('modal');
  ctools_modal_add_js();
  $custom_style = array(
    'league-chess-display-modal-dialog-style' => array(
      'modalSize' => array(
        'type' => 'fixed',
        'width' => 400,
        'height' => 430,
      ),
      'animation' => 'fadeIn',
    ),
  );
  drupal_add_js($custom_style, 'setting');

  return $table;
}

// Sort a swiss table
function _league_chess_swiss_sort($fa, $fb) {
  // First sort by points
  if ( $fa[0] > $fb[0] ) {
    return -1;
  }
  if ( $fa[0] < $fb[0] ) {
    return 1;
  }
  return 0;
}
// Sort all play all
function league_chess_all_sort($fa, $fb) {
  // First sort by points
  if ( $fa['total'] > $fb['total'] ) {
    return -1;
  }
  if ( $fa['total'] < $fb['total'] ) {
    return 1;
  }
  return 0;
}

// Get data for an individual knockout

function league_chess_get_knockout_data($node) {
  $event = $node->nid;
  $eventType = league_event_value($node, 'type');
  $output = league_chess_knockout_scores($node);
  $scores = $output['data'];
//drupal_set_message('<pre>' . print_r($scores, TRUE) . '</pre>');
  $players = $output['players'];
  $teams = array();
  foreach($players as $id => $player) {
    $teams[$id][0] = $player;
  }
  $nrounds = count($scores);
  $homeCounts[0] = 0;
  $awayCounts[0] = 0;
  $lastTeam = array();

  if ($nrounds >0) {
    $path = drupal_get_path('module', 'league');
    require_once("$path/league_table.inc");
    $nteams = count($scores[0]);
    for ($r = $nrounds -1; $r > -1; $r--) {
      league_knockout_round_sort_table($scores, $r, $nteams);
    }
    for ($r = 0; $r < $nrounds; $r++) {
      $homeCounts[$r] = 0;
      $awayCounts[$r] = 0;
      $lastTeam[$r] = 'A';
    }
  }

  // Load the modal library and add the modal javascript.
  ctools_include('modal');
  ctools_modal_add_js();
  $custom_style = array(
    'league-chess-display-modal-dialog-style' => array(
      'modalSize' => array(
        'type' => 'fixed',
        'width' => 400,
        'height' => 430,
      ),
      'animation' => 'fadeIn',
    ),
  );
  drupal_add_js($custom_style, 'setting');

  $data = array(
    'scores' => $scores,
    'teams' => $teams,
    'nrounds' => $nrounds,
    'homeCounts' => $homeCounts,
    'awayCounts' => $awayCounts,
    'lastTeam' => $lastTeam,
    'title' => $node->title,
    'event' => $event,
  );
  return $data;

}

/**
 * Get the player scores for a knockout event.
 */
function league_chess_knockout_scores($node) {

  $players = array();
  $matches = array();
  $query = league_chess_individual_event_query($node, TRUE);
  $query->orderBy('f.round', 'ASC')->orderBy('r.board', 'ASC');
  $queryResult = $query->execute();
  $large_negative = -99999999;
  foreach ( $queryResult as $row) {
    // Store players names
    // if the pid is < 0 it means a default or bye but to draw the 
    // table properly they need to be different so negate the other player
    // NB: This would not work for double defaults
    if($row->white < 0 && $row->black < 0) {
      $row->white = $large_negative--;
      $row->black = $large_negative--;
      $players[$row->black] = "Bye";
      $players[$row->white] = "Bye";
    } else {
      if($row->white < 0) {
        $row->white = - $row->black;
        $players[$row->white] = "Bye";
      } else {
        $players[$row->white] = "$row->wlast, $row->wfirst";
      }
      if($row->black < 0) {
        $row->black = - $row->white;
        $players[$row->black] = "Bye";
      } else {
        $players[$row->black] = "$row->blast, $row->bfirst";
      }
    }
    // Calculate points depending on results
    $res = $row->result;
    $gamePoints = array( $row->white => 0, $row->black => 0);
    if ( $res == 'W' || $res == '+'  ) {
      $gamePoints[$row->white] = 2;
    } else {
      if ( $res == 'L' || $res == '-'  ) {
        $gamePoints[$row->black] = 2;
      } else {
        if ( $res == 'D' ) {
          $gamePoints[$row->white] = 1;
          $gamePoints[$row->black] = 1;
        }
      }
    }
    // Calculate the id of the match
    $p_home = max($row->white, $row->black);
    $p_away = min($row->white, $row->black);
    $mid = "$p_home v $p_away";
    if(isset($matches[$mid]) ) {
      if( $matches[$mid]['white'] == $row->white ) {
        $white = 'whiteScore';
        $black = 'blackScore';
      } else {
        $black = 'whiteScore';
        $white = 'blackScore';
      } 
      $matches[$mid][$white] += $gamePoints[$row->white];
      $matches[$mid][$black] += $gamePoints[$row->black];
      $matches[$mid]['board'] = min($matches[$mid]['board'], $row->board);
    } else {
      $white = 'whiteScore';
      $black = 'blackScore';
      $matches[$mid] = array(
        $white => $gamePoints[$row->white],
        $black => $gamePoints[$row->black],
        'white' => $row->white,
        'black' => $row->black,
        'board' => $row->board,
        'round' => $row->round,
      );
    }
  }

  // Remap the board numbers
  $mcount=array();
  foreach ($matches as $mid => $match) {
    $mcount[$match['round']] = 0;
  }
  $board_mapping = array();
  foreach ($matches as $mid => $match) {
    $mcount[$match['round']]++;
    $board_mapping[$mid][$match['board']] = $mcount[$match['round']];
  }
      
  $data = array();
  $counts = array();
  $rounds = array();    // virtual round mapping
  $current_round = 0;

  foreach ( $matches as $key => $match) {
//  drupal_set_message($key . ' ' . print_r($match, TRUE));

    if($match['whiteScore'] > $match['blackScore']) {
      $winner = $match['white'];
    } else {
      $winner = $match['black'];
    }
    $homeScore = league_score2points($match['whiteScore']);
    $awayScore = league_score2points($match['blackScore']);
    $round = $match['round'];
    // Create a virtual round mapping in case there are fixtures for say
    // round 1 and round 3 which messes up otherwise.
    if(!isset($rounds[$round]) ) {
      $current_round++;
      $rounds[$round] = $current_round;
    }
//  drupal_set_message("knockout scores current $current_round round $round virtual " . $rounds[$round]);
    $round = $rounds[$round];
    if(!isset($counts[$round]) ) {
      $counts[$round] = 0;
    }
    $counts[$round]++;
    $fcount = $counts[$round] -1;
    $data[$round -1][$fcount]['homeTeam'] = $match['white'];
    $data[$round -1][$fcount]['awayTeam'] = $match['black'];
    $data[$round -1][$fcount]['fixture'] = $key;
    $data[$round -1][$fcount]['tableNo'] = $board_mapping[$key][$match['board']];
    $data[$round -1][$fcount]['winner'] = $winner;
    if ( $homeScore == 0 && $awayScore == 0) {
      $data[$round -1][$fcount]['homeScore'] = ' ';
      $data[$round -1][$fcount]['awayScore'] = ' ';
    }
    else {
      $data[$round -1][$fcount]['homeScore'] = $homeScore;
      $data[$round -1][$fcount]['awayScore'] = $awayScore;
    }
  }
  $output = array('players' => $players, 'data' => $data);

  return $output;
}
