<?php
/**
* @file
* Functions for adding a clubs player to a list
*/

function league_chess_add_clubs_players_list_page($list) {
  $lid = $list->lid;
  $redirect = "league_player_list/$lid";

  return drupal_get_form('league_chess_club_select_form', $lid,$redirect);
}

function league_chess_club_select_form($form, &$form_state,$lid,$redirect) {
  $form_state['league_chess_lid'] = $lid;
  $form_state['league_chess_redirect'] = $redirect;

  $clubs = league_chess_clublist();
  $form['club'] = array(
   '#type'  => 'select',
   '#options'  => $clubs,
  );
  $form['submit'] = array(
   '#value' => t('Load Players For Club'),
   '#type'  => 'submit',
  );

  return $form;
}

function league_chess_club_select_form_submit($form, &$form_state) {
  $clubCode = $form_state['values']['club'];
  $lid = $form_state['league_chess_lid'];
  $redirect = $form_state['league_chess_redirect'];
  drupal_set_message("Adding players for club $clubCode to list $lid");

  league_chess_add_missing_club_players($lid, $clubCode);

  // Clear players with no grade cache
  $list = league_player_list_load($lid);
  $org = $list->org;
  $cache_name = "league_chess_count_no_grade_players_$org";
  cache_clear_all($cache_name, 'cache');

  $form_state['redirect'] = $redirect;
}
