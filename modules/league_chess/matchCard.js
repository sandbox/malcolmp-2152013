// Javascript for the matchcard
//

// when the document is loaded ..
(function($){
$(document).ready(function() {

// Convert a score stored as an integer double the value into
// something containing half pionts.
function score2points(score) {
  if(score % 2 == 0) {
    points = score / 2;
  } else {
    if(score > 0) {
      if(score > 1) {
        points = (score -1) / 2;
      } else {
        points = ' ';
      }
    } else {
      if(score < -1) {
        points = (score +1) / 2;
      } else {
        points = '-';
      }
    }
    points += ' ½';
  }
  return points;
}

// Function to sort out the score from match card selectors
function totalScore(set_winner) {
  if (set_winner === undefined) {
    set_winner = true;
    alert("Setting Winner undefined");
  }
//if(set_winner==true) {
//  alert("You Setting Winner");
//}
  // Use JQuery to find the form
  form = $('#league-chess-matchcard-form').get(0);

  homeScore = 0;
  awayScore = 0;
  nboards = 0;
  nfinished = 0;
  matchno = 0;

  // Use JQuery to loop round everything with a class of form-select
  $(".form-select").each(function(i) {

    // For those with names starting with "result" ...
    if( this.name.substr(0,6) == "result" ) {
      // Store intermediate totals if 2 mini matches
      if( nboards == msize )
      {
        // Add in adjustments
        // ( options range between minus(-) and plus(+) number of boards
        //   so must subtract msize*2 )
        homeAdjustField = $('#edit-home-adjustment0');
        if(homeAdjustField.length == 1) {
          homeAdjustment = homeAdjustField.get(0).selectedIndex - msize*2;
          homeScore += homeAdjustment;
        }
        awayAdjustField = $('#edit-away-adjustment0');
        if(awayAdjustField.length == 1) {
          awayAdjustment = awayAdjustField.get(0).selectedIndex - msize*2;
          awayScore += awayAdjustment;
        }
        // Add in penalties
        homePenaltyField = $('#edit-home-penalty0');
        if(homePenaltyField.length == 1) {
          homePenalty = homePenaltyField.get(0).selectedIndex;
          homeScore -= homePenalty;
        }
        awayPenaltyField = $('#edit-away-penalty0');
        if(awayPenaltyField.length == 1) {
          awayPenalty = awayPenaltyField.get(0).selectedIndex;
          awayScore -= awayPenalty;
        }
        homeTotal = score2points(homeScore);
        awayTotal = score2points(awayScore);
        if( eventType == 'L' || eventType == 'J' || eventType == 'K' ) {
          form.htotal0.value = homeTotal;
          form.atotal0.value = awayTotal;
        }
        homeScore = 0;
        awayScore = 0;
        matchno++;
      }
      nboards++;
      // Work out the score from the value of the result selector
      score = this.value;
      if( score != "N" && score != "A" && score != "J"  ) {
        nfinished++;
      }
      if( score == "W" || score == "+" )
      {
        homeScore+=2;
      }
      if( score == "D" )
      {
        homeScore+=1;
        awayScore+=1;
      }
      if( score == "L" || score == "-" )
      {
        awayScore+=2;
      }
      if( eventType == 'L' ) {
        if( unfinished_games == 'D' ) {
          if( score == "A" || score == "J" || score == " " ) {
            homeScore+=1;
            awayScore+=1;
          }
        }
      }
    }
  });

  homeAdjustField = $('#edit-home-adjustment0');
  awayAdjustField = $('#edit-away-adjustment0');
  homePenaltyField = $('#edit-home-penalty0');
  awayPenaltyField = $('#edit-away-penalty0');
  if( matchno == 1 ) {
    homeAdjustField = $('#edit-home-adjustment1');
    awayAdjustField = $('#edit-away-adjustment1');
    homePenaltyField = $('#edit-home-penalty1');
    awayPenaltyField = $('#edit-away-penalty1');
  }
  if(homeAdjustField.length == 1) {
    homeAdjustment = homeAdjustField.get(0).selectedIndex - msize*2;
    homeScore += homeAdjustment;
  }
  if(awayAdjustField.length == 1) {
    awayAdjustment = awayAdjustField.get(0).selectedIndex - msize*2;
    awayScore += awayAdjustment;
  }
  // Add in penalties
  if(homePenaltyField.length == 1) {
    homePenalty = homePenaltyField.get(0).selectedIndex;
    homeScore -= homePenalty;
  }
  if(awayPenaltyField.length == 1) {
    awayPenalty = awayPenaltyField.get(0).selectedIndex;
    awayScore -= awayPenalty;
  }
  homeTotal = score2points(homeScore);
  awayTotal = score2points(awayScore);

  if( eventType == 'L' || eventType == 'J' || eventType == 'K' ) {
    if( matchno == 0 )
    {
      form.htotal0.value = homeTotal;
      form.atotal0.value = awayTotal;
    } else {
      form.htotal1.value = homeTotal;
      form.atotal1.value = awayTotal;
    }
    if(set_winner==true) {
      // Update the winning team
      //  None  - No result
      //  Home  - Home win
      //  Home? - Home team at least a draw on threshold
      //  Away  - Away win
      //  Away? - Away team at least a draw on threshold
      //  Draw  - Draw
      //
      //  Note: homeScore (based on 2 for a win) is double the homeTotal
      winner = 'N';
      // If some unfinished games and threshold option
      if( eventType == 'L' && unfinished_games == 'T' && nfinished!=nboards ) {
        if( homeScore > nboards ) {
          winner = 'H';
        }
        if( homeScore == nboards ) {
          winner = 'I';
        }
        if( awayScore > nboards ) {
          winner = 'A';
        }
        if( awayScore == nboards ) {
          winner = 'B';
        }
      // Not threshold
      } else {
        if( unfinished_games != 'M' ) {
          if( homeScore == awayScore ) {
            winner = 'D';
          } else {
            if( homeScore > awayScore ) {
              winner = 'H';
            } else {
              winner = 'A';
            }
          }
        }
      }
      $('#edit-winner').get(0).value = winner;
    }
  }
}

function totalScoreNoWinner() {
  totalScore(false);
}

function totalScoreWinner() {
  totalScore(true);
}

//Function to flip colours when the board 1 is changed
function flipColors() {
  defaultColor = "?";
  // Use JQuery to loop round everything with a class of form-select
  $(".form-select").each(function(i) {

    // For those with names starting with "result" ...
    if( this.name.substr(0,5) == "color" ) {
      if( this.name == "color1" ) {
        defaultColor = this.value;
      } else {
        if( defaultColor != "?" ) {
          if( defaultColor == "W" ) {
            defaultColor = "B";
          } else {
            defaultColor = "W";
          }
        }
        this.value = defaultColor;
      }
    }
  });
}
// Function to add up the grades from match card player selectors
function totalGrade() {
  // Use JQuery to find the form.
  form = $('#league-chess-matchcard-form').get(0);

  homeGrade = 0;
  awayGrade = 0;
  nboards = 0;
  matchno = 0;

  // Use JQuery to loop round everything with a class of form-select
  $(".form-select").each(function(i) {

    // For the home team players ...
    if( this.name.substr(0,4) == "home" && this.name.substr(4,1) != '_' ) {

      //  Sum up grades
      nameText = this.options[this.selectedIndex].text;
      // ungraded?
      if(rate == 0) {
        fullGrade = '';
      } else {
        // 4 digit grade ( could be a cat on the end)
        gradeText = nameText.substr(nameText.length-5,4);
        grade = parseInt(gradeText);
        homeGrade+=grade;
        // include the cat in the grade
        fullGrade = nameText.substr(nameText.length-5,4);
      }
      nboards++;

      // Put grade into hidden field to pass to server
      wgrade = $("input[name='wgrade"+nboards+"']").get(0);
      wgrade.value = fullGrade;
    }
    // For the away team players ...
    if( this.name.substr(0,4) == "away" && this.name.substr(4,1) != '_' ) {

      //  Sum up grades
      if(rate == 0) {
        fullGrade = '';
      } else {
        nameText = this.options[this.selectedIndex].text;
        // 4 digit grade, but bracket on the end
        gradeText = nameText.substr(nameText.length-5,4);
        grade = parseInt(gradeText);
        awayGrade+=grade;
        fullGrade = nameText.substr(nameText.length-5,4);
      }

      // Put grade into hidden field to pass to server
      bgrade = $("input[name='bgrade"+nboards+"']").get(0);
      bgrade.value = fullGrade;

      // Store totals if 2 mini matches
      if( eventType == 'L' || eventType == 'J' || eventType == 'K' ) {
        if(nboards == msize) {
          if( grade_limit_option != 'T'  && nboards > 0 ) {
            // Round up so its clear anything exceeding the average is
            // over the limit
            homeGrade = Math.ceil(homeGrade / nboards);
            awayGrade = Math.ceil(awayGrade / nboards);
          }
          form.gradeh0.value = homeGrade;
          form.gradea0.value = awayGrade;
          homeGrade = 0;
          awayGrade = 0;
          matchno++;
        }
      }
    }
  });

  if(nboards > msize) {
    if( grade_limit_option != 'T' && nboards > 0 ) {
      homeGrade = Math.ceil(homeGrade / msize);
      awayGrade = Math.ceil(awayGrade / msize);
    }
    form.gradeh1.value = homeGrade;
    form.gradea1.value = awayGrade;
  }
//alert("Selected:"+option);
}
// Code to run after page load
winner = $('#edit-winner').get(0);
if( eventType == 'L' ) {
  winner.setAttribute("readonly", "readonly");
}

// Use JQuery to loop round everything with a class of form-select
$(".form-select").each(function(i) {

  // For those with names starting with "result" set score handler
  if( this.name.substr(0,6) == "result" ) {
    this.onchange = totalScoreWinner;
  }
  // For board one colour, set the flip colours handler
  if( this.name == "color1" ) {
    this.onchange = flipColors;
  }
  // For the player drop downs, set grade handler
  if( this.name.substr(0,4) == "home" || this.name.substr(0,4) == "away" ) {
    this.onchange = totalGrade;
  }
});
  // Recalculate total score when adjustment fields changed
  homeAdjustField = $('#edit-home-adjustment0');
  if(homeAdjustField.length == 1) {
    homeAdjustField.get(0).onchange = totalScoreWinner;
  }
  awayAdjustField = $('#edit-away-adjustment0');
  if(awayAdjustField.length == 1) {
    awayAdjustField.get(0).onchange = totalScoreWinner;
  }
  homeAdjustField = $('#edit-home-adjustment1');
  if(homeAdjustField.length == 1) {
    homeAdjustField.get(0).onchange = totalScoreWinner;
  }
  awayAdjustField = $('#edit-away-adjustment1');
  if(awayAdjustField.length == 1) {
    awayAdjustField.get(0).onchange = totalScoreWinner;
  }
  // Recalculate total score when penalty fields changed
  homePenaltyField = $('#edit-home-penalty0');
  if(homePenaltyField.length == 1) {
    homePenaltyField.get(0).onchange = totalScoreWinner;
  }
  awayPenaltyField = $('#edit-away-penalty0');
  if(awayPenaltyField.length == 1) {
    awayPenaltyField.get(0).onchange = totalScoreWinner;
  }
  homePenaltyField = $('#edit-home-penalty1');
  if(homePenaltyField.length == 1) {
    homePenaltyField.get(0).onchange = totalScoreWinner;
  }
  awayPenaltyField = $('#edit-away-penalty1');
  if(awayPenaltyField.length == 1) {
    awayPenaltyField.get(0).onchange = totalScoreWinner;
  }

  $('#league-chess-matchcard-form').submit(function() {
    // Update total score so winner is not updated.
    // (otherwise user can't set it)
    totalScoreNoWinner();
    totalGrade();
    return true;
  });

    totalScoreWinner();
    totalGrade();
  });
})(jQuery);
