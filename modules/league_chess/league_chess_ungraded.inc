<?php
/**
 * @file
 * Games played by ungraded players
 */

/**
 * Ungraded player games.
 */
function league_chess_reports_tab_ungraded($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);
  // Set title
  $title = $node->title . ' - Games By Ungraded players';
  drupal_set_title($title);

  $content['ungraded']['#table'] = league_chess_ungraded_table($node->nid);
  $content['ungraded']['#theme'] = 'league_table';

  // Ensure if a player is picked on, the Back to list tab works.
  $_SESSION['league_player_list_url'] = $_GET['q'];

  return $content;
}

function league_chess_ungraded_table($org) {
  $whites = league_chess_get_none_graded_game_counts($org,'white');
  $blacks = league_chess_get_none_graded_game_counts($org,'black');

  $pids = array_merge(array_keys($whites), array_keys($blacks));

  $players = array();
  foreach($pids as $pid) {
    if(isset($whites[$pid]) ) {
      $players[$pid]['name'] = $whites[$pid]['name'];
      $players[$pid]['club'] = $whites[$pid]['club'];
      $players[$pid]['ecfcode'] = $whites[$pid]['ecfcode'];
      $players[$pid]['grade'] = $whites[$pid]['grade'];
      if(isset($blacks[$pid]) ) {
        $players[$pid]['count'] = $whites[$pid]['count'] + $blacks[$pid]['count'];
      } else {
        $players[$pid]['count'] = $whites[$pid]['count'];
      }
    } else {
      $players[$pid]['name'] = $blacks[$pid]['name'];
      $players[$pid]['club'] = $blacks[$pid]['club'];
      $players[$pid]['ecfcode'] = $blacks[$pid]['ecfcode'];
      $players[$pid]['grade'] = $blacks[$pid]['grade'];
      $players[$pid]['count'] = $blacks[$pid]['count'];
    }
  }

  $header = array('Name', 'Club', 'ECF Code', 'Local Grading', 'Number of games');
  $rows = array();

  foreach($players as $pid => $data) {
    $name = l($data['name'], 'league_player/' . $pid);
    $club = $data['club'];
    $ecfcode = $data['ecfcode'];
    $count = $data['count'];
    $grade = $data['grade'];

    $rows[] = array($name, $club, $ecfcode, $grade, $count);
  }
  $table['data'] = $rows;
  $table['header'] = $header;
  return $table;
}

function league_chess_get_none_graded_game_counts($org,$player='white') {
  $settings = league_chess_national_settings();
  $rlidj = $settings['standard_rlid'];

  // Get all fixture for events in the current season.
  $query = db_select('league_fixture', 'f');
  $query->innerJoin('league_competition', 'comp', 'comp.cid = f.cid');
  // Active Seasons
  $query->innerJoin('league_event', 'le', 'le.eid = comp.eid');
  $query->condition('le.lms', 'A');
  $query->addField('le', 'name', 'seasonName');
  //  events for that organisation
  $query->condition('le.org', $org);
  //  node for the fixture to get vid.
  $query->innerJoin('league_match', 'm', 'm.fid = f.nid');
  //  results for the player
  $query->innerJoin('league_chess_result', 'r', 'r.mid = m.mid');
  $query->addField('r', $player, 'player');
  $query->addExpression('COUNT(*)', 'pcount');
  //  not byes or defaults
  $query->condition("r.white", 0, '>');
  $query->condition("r.black", 0, '>');
  //  player name
  $query->innerJoin('league_player', 'p', "p.pid = r.$player");
  $query->addField('p', 'firstname');
  $query->addField('p', 'lastname');
  $query->addField('p', 'chess_rating', 'grade');
  $query->addField('p', 'ecf_code', 'ecfcode');
  //  players team
  if($player == 'white') {
    $query->innerJoin('field_data_field_club', 'c', "c.entity_id = f.home_team");
  } else {
    $query->innerJoin('field_data_field_club', 'c', "c.entity_id = f.away_team");
  }
  //  players club
  $query->innerJoin('node', 'cn', "cn.nid = c.field_club_value");
  $query->addField('cn', 'title', 'club');
  //  players ecf grading data     
//$query->leftJoin('player_list_player', 'master', "master.natcode = p.ecf_code");
  $query->leftjoin('rating_player', 'rp', "rp.pid = p.ecf_code AND rp.rlid=$rlidj");
  $query->where('p.ecf_code is null or rp.pid is null or rp.rating=0 or rp.cat=:cat', array(':cat' => '*') );
  $query->groupBy($player);
//dsm((string)$query);
//dsm($query->arguments());

  $queryResult = $query->execute();
  
  $players = array();
  foreach ( $queryResult as $row) {
      $players[$row->player]['count'] = $row->pcount;
      $players[$row->player]['name'] = $row->firstname . ' ' . $row->lastname;
      $players[$row->player]['grade'] = $row->grade;
      $players[$row->player]['ecfcode'] = $row->ecfcode;
      $players[$row->player]['club'] = $row->club;
  }
  return $players;
}
