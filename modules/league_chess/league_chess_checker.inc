<?php
/**
 * @file
 * Functions to replace the ecf checker.
 */

/**
 * Players with no ecf codes
 */
function league_chess_reports_no_ecfcode($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);
  $settings = league_chess_national_settings();
  $nat_name = $settings['national_rating_code'];
  // Set title
  $title = "No $nat_name, but similar player exists";
  drupal_set_title($title);

  $content['newly']['#table'] = league_chess_no_ecf_code_players($node);
  $content['newly']['#theme'] = 'league_table';

  // Ensure if a player is picked on, the Back to list tab works.
  $_SESSION['league_player_list_url'] = $_GET['q'];

  return $content;
}

function league_chess_no_ecf_code_players_query($org) {
  $query = db_query("select p.pid, p.firstname, p.lastname, m.firstname as mfirstname, m.lastname as mlastname, m.pid as natcode, cd.field_ecf_club_code_value as lmsclub, clm.name as lmsclubd, m.club AS ecfclub, c.name as ecfclubd
FROM league_player p
JOIN player_list_player m ON m.lastname=p.lastname AND SUBSTRING(m.firstname,1,1) = SUBSTRING(p.firstname,1,1)
JOIN league_player_player lpp ON lpp.pid=p.pid
LEFT JOIN league_club_org lc ON lc.list=lpp.lid and lc.org=p.org
LEFT JOIN field_data_field_ecf_club_code cd ON cd.entity_id=lc.cid
LEFT JOIN rating_list_clubs clm ON clm.club=cd.field_ecf_club_code_value
LEFT JOIN rating_list_clubs c ON c.club=m.club
WHERE p.pid IN (
select p2.pid
FROM league_player p2
WHERE p2.org=$org AND p2.ecf_code=''
UNION  
select p2.pid
FROM league_player p2
LEFT JOIN player_list_player m2 ON m2.pid=p2.ecf_code
WHERE  p2.org=$org AND p2.ecf_code !='' AND m2.pid IS NULL
)");

  return $query;
}

/**
 * Get players with no grades.
 */

function league_chess_no_ecf_code_players($node) {

  $org = $node->nid;
  $header = array();
  $header[] = array('data' => 'First Name', 'field' => 'firstname' );
  $header[] = array('data' => 'Last Name', 'field' => 'lastname' );
  $header[] = array('data' => 'LMS Club', 'field' => 'lmsclub' );
  $header[] = array('data' => 'ECF Code', 'field' => 'pid', 'sort' => 'desc' );
  $header[] = array('data' => 'ECF First Name', 'field' => 'mfirstname' );
  $header[] = array('data' => 'ECF Last Name', 'field' => 'mlastname' );
  $header[] = array('data' => 'Club', 'field' => 'CLUB1' );
  $header[] = array('data' => 'Percent', 'field' => 'percent' );

  $rows = array();

  $queryResult = league_chess_no_ecf_code_players_query($org);

  $maxp = array();
  foreach ( $queryResult as $row ) {
    $pid = $row->pid;
    $firstname = l("$row->firstname","node/$org/league/newplayers/search/$pid");
    $lastname = $row->lastname;
    $mfirstname = $row->mfirstname;
    $mlastname = $row->mlastname;
    $ecfcode = $row->natcode;
    $lmsclub = $row->lmsclub . ' ' . $row->lmsclubd;
    $ecfclub = $row->ecfclub . ' ' . $row->ecfclubd;
    $sim = similar_text($row->firstname, $row->mfirstname, $percent);
    $percent = $percent / 2.0;
    if($row->lmsclub == $row->ecfclub) {
      $percent += 50;
    }
    if($percent > 25) {
      $rows[] = array($firstname, $lastname, $lmsclub, $ecfcode, $mfirstname, $mlastname, $ecfclub, round($percent,2) );
    }
  }

  $table['data'] = $rows;
  $table['header'] = $header;

  return $table;
}

/**
 * Players with differring data in the master list.
 */
function league_chess_reports_ecf_diff($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);
  $settings = league_chess_national_settings();
  $nat_name = $settings['national_rating_name'];
  // Set title
  $title = "Players that differ from the $nat_name list";
  drupal_set_title($title);

  $content['newly']['#table'] = league_chess_diff_players($node, $settings['national_rating_code']);
  $content['newly']['#theme'] = 'league_table';

  // Ensure if a player is picked on, the Back to list tab works.
  $_SESSION['league_player_list_url'] = $_GET['q'];

  return $content;
}

function league_chess_diff_players_query($org) {
  $query = db_query("select p.pid, p.firstname as lmsfirst, p.lastname as lmslast, m.firstname ecffirst, m.lastname as ecflast, p.ecf_code, from_unixtime(p.dob,'%Y-%m-%d') as lmsdob, from_unixtime(m.dob,'%Y-%m-%d') as ecfdob, p.sex as lmssex, m.sex ecfsex, lpp.lid, cd.field_ecf_club_code_value as lmsclub, GROUP_CONCAT( pcc.club SEPARATOR ',' ) as ecfclubs, rlc.name as clubname
FROM league_player p
LEFT JOIN player_list_player m ON m.pid=p.ecf_code
LEFT JOIN league_player_player lpp ON lpp.pid=p.pid
LEFT JOIN league_club_org lc ON lc.list=lpp.lid and lc.org=p.org
LEFT JOIN field_data_field_ecf_club_code cd ON cd.entity_id=lc.cid
LEFT JOIN rating_list_player_clubs pcc ON pcc.pid=m.pid
LEFT JOIN rating_list_clubs rlc ON rlc.club=cd.field_ecf_club_code_value
WHERE p.org=$org AND p.ecf_code IS NOT NULL AND p.ecf_code!=' ' 
AND CHAR_LENGTH(p.ecf_code) > 1
AND ( p.lastname != m.lastname OR p.firstname != m.firstname OR
      ( p.sex !=' ' AND p.sex is not null and p.sex != m.sex AND m.sex IS NOT NULL AND m.sex!=' ') OR
      ( cd.field_ecf_club_code_value NOT IN (SELECT club FROM rating_list_player_clubs pc WHERE pc.pid=m.pid ) ) OR
      ( from_unixtime(p.dob,'%Y-%m-%d')!= from_unixtime(m.dob,'%Y-%m-%d') AND m.dob IS NOT NULL ) OR m.pid IS NULL ) GROUP BY m.pid ORDER BY m.lastname");

  return $query;
}

/**
 * Get players with no grades.
 */

function league_chess_diff_players($node,$code_name='Rating Code') {

  $org = $node->nid;
  $access = league_check_access($org);
  $header = array('First Name', 'Last Name', $code_name, 'Warnings', 'Name Difference', 'DOB Difference', 'Sex Difference', 'New Club');

  $rows = array();

  $queryResult = league_chess_diff_players_query($org);

  foreach ( $queryResult as $row ) {
    $errors = 0;
    $pid = $row->pid;
    $firstname = $row->lmsfirst;
    $lastname = $row->lmslast;
    $ecffirst = $row->ecffirst;
    $ecflast = $row->ecflast;
    $namediff = ' ';
    if($lastname != $ecflast || $firstname != $ecffirst) {
      $namediff = "$ecffirst $ecflast";
      $errors++;
    }
    $firstname = l("$row->lmsfirst","league_player/$pid");
    $pid = $row->ecf_code;
    $lmsclub = $row->lmsclub;
    $clubname = $row->clubname;
    $ecfclubs = $row->ecfclubs;
    $clubdiff = ' ';
    if(isset($lmsclub) && isset($ecfclubs) && !strpos($ecfclubs, $lmsclub)) {
      $errors++;
      $clubdiff = "$clubname ($lmsclub)";
    }
    $ecfdob = $row->ecfdob;
    $lmsdob = $row->lmsdob;
    $dobdiff = ' ';
    if(isset($ecfdob) && $ecfdob!=$lmsdob) {
      if($access) {
        $dobdiff = $ecfdob;
      } else {
        $dobdiff = '****';
      }
      $errors++;
    }
    $ecfsex = $row->ecfsex;
    $lmssex = $row->lmssex;
    $sexdiff = ' ';
    if(isset($lmssex) && $lmssex!=' ' && strlen($lmssex)==1 && $ecfsex!=$lmssex) {
      $sexdiff = $ecfsex;
      $errors++;
    }
    $rows[] = array($firstname, $lastname, $pid, $errors, $namediff, $dobdiff, $sexdiff, $clubdiff);
  }

  $table['data'] = $rows;
  $table['header'] = $header;

  return $table;
}
