<?php

/**
 * @file
 * Functions for league_chess matches with violations.
 */
  
function league_chess_grade_limit_tab ($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);
  // Set title
  $title = "$node->title - Violations";
  drupal_set_title($title);

  $table = league_chess_violation_data($node);
  $content['stats']['#table'] = $table;
  $content['stats']['#theme'] = 'league_table';

  return $content;
}

/**
 * Matches with violations.
 */
function league_chess_violation_data($node) {

  $header = array('Fixture', 'Violation', 'Status' );
  $rows = array();

  $query = league_chess_violations_query($node->nid,TRUE);

  $queryResult = $query->execute();

  // Get result 

  $matches = array();
  foreach ( $queryResult as $row ) {
    $type = league_chess_violation($row->type);
    $status = league_violation_status($row->status);
    $fixture = $row->home . ' v ' . $row->away;
    $fnid = $row->entity_id;
    $link = l($fixture, 'league_fixture/' . $fnid . '/match' );
    $rows[] = array($link, $type, $status );
  }

  $table['data'] = $rows;
  $table['header'] = $header;
  return $table;
}

function league_chess_count_violations($org) {
  $cache_name = "league_chess_count_violations_$org";
  $num_rows = &drupal_static($cache_name);
  if (!isset($num_rows)) {
    if( $cache = cache_get($cache_name)) {
      $num_rows = $cache->data;
    } else {
      $query = league_chess_violations_query($org);
      //dsm((string)$query);
      $num_rows = $query->countQuery()->execute()->fetchField();
      cache_set($cache_name, $num_rows, 'cache');
    }
  }
  return $num_rows;
}

function league_chess_violations_query($org,$all=FALSE) {
  // Active Seasons in the org
  $query = db_select('league_event', 'le');
  $query->condition('le.org', $org);
  $query->condition('le.lms', 'A');
  $query->addField('le', 'name', 'seasonName');
  //  events for those seasons.
  $query->innerJoin('league_competition', 'comp', 'comp.eid = le.eid');
  // fixtures in the event
  $query->innerJoin('league_fixture', 'f', 'f.cid = comp.cid');
  $query->addField('f', 'nid', 'entity_id');
  // event title
  $query->innerJoin('node', 'et', 'et.nid = comp.cid');
  $query->addField('et', 'title', 'event');
  // Fixture title
  $query->innerJoin('node', 'ht', 'ht.nid = f.home_team');
  $query->innerJoin('node', 'at', 'at.nid = f.away_team');
  $query->addField('ht', 'title', 'home');
  $query->addField('at', 'title', 'away');
  // Violation
  $query->innerJoin('league_fixture_violation', 'v', 'v.fid = f.nid');
  $query->addField('v', 'type', 'type');
  $query->addField('v', 'status', 'status');
  $query->orderBy('v.status', 'ASC');
  if(!$all) {
    $query->condition('v.status', 'L');
  }

  return $query;
}
