// Javascript for the team selection tab
//

// when the document is loaded ..
(function($){
$(document).ready(function() {

// Function to add up the grades from player selectors
function totalGrade() {
  // Use JQuery to find the form.
  form = $('#league-chess-team-selection-form').get(0);

  total = 0;
  nboards = 0;
  matchno = 0;

  // Use JQuery to loop round everything with a class of form-select
  $(".form-select").each(function(i) {

    // For the player selectors ...
    if( this.name.substr(0,4) == "team" && this.name.substr(4,1) != '_' ) {

      //  Sum up grades
      nameText = this.options[this.selectedIndex].text;
      if(rate == 0) {
        fullGrade = '';
      } else {
        // 3 digit grade - ignore the CAT
        gradeText = nameText.substr(nameText.length-5,3);
        // 4 digit grade
        if(rate>7) {
          gradeText = nameText.substr(nameText.length-5,4);
        }
        grade = parseInt(gradeText);
        total+=grade;
        // This includes the CAT for 3 digit grades.
        fullGrade = nameText.substr(nameText.length-5,4);
      }
      nboards++;

      // Put grade into hidden field to pass to server
      wgrade = $("input[name='grade"+nboards+"']").get(0);
      wgrade.value = fullGrade;
    }
  });
  // Change to average if requested ..
  if( grade_limit_option != 'T'  && nboards > 0 ) {
    // Round up so its clear anything exceeding the average is
    // over the limit
    total = Math.ceil(total / nboards);
  }
  form.gradet.value = total;

//alert("Selected:"+option);
}
// Code to run after page load

  // Use JQuery to loop round everything with a class of form-select
  $(".form-select").each(function(i) {

    // For the player drop downs, set grade handler
    if( this.name.substr(0,4) == "team" ) {
      this.onchange = totalGrade;
    }
  });

  $('#league-chess-team-select-form').submit(function() {
    totalGrade();
    return true;
  });

  totalGrade();
});
})(jQuery);
