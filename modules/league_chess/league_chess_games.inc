<?php

/**
 * @file
 * Functions for league_chess - player games option.
 */
  
function league_chess_games_tab ($player) {
  $org = $player->org;
  $pid = $player->pid;
  $node = node_load($org);
  // Set breadcrumb
  league_player_set_bread_crumb ($player);

  $table = league_chess_seasons_games($pid,$org);
  $table['title'] = "Games for $player->firstname $player->lastname";
  // Check if file to be create via Xl or PDF
  league_table_check_file($table);

  $content['games']['#pdfLink'] = 'link';
  $content['games']['#table'] = $table;
  $content['games']['#theme'] = 'league_table';
  return $content;
}


/**
 * Games for the player in the org for the current season
 */
function league_chess_seasons_games($pid,$org) {

  $rows = array();
  $header = array('Opponent', 'Result', 'Date', 'Fixture', 'Event' );

  // Results for the player
  $query = db_select('league_chess_result', 'r');
  $query->fields('r', array('white', 'black', 'result', 'whiteGrade', 'blackGrade', 'rdate'));
  $query->where("r.white = $pid or r.black = $pid");
  // Exclude games with no result or defaults
  $query->condition('r.result', array('W','D','L'), 'IN');
  // The event
  $query->innerJoin('league_match', 'm', 'm.mid = r.mid');
  $query->innerJoin('league_fixture', 'f', 'f.nid = m.fid');
  // Event title
  $query->innerJoin('node', 'e', 'e.nid = f.cid');
  $query->addField('e', 'title', 'event');
  $query->innerJoin('league_competition', 'comp', 'comp.cid = f.cid');
  $query->addField('comp', 'type', 'eventType');
  // Active Seasons
  $query->innerJoin('league_event', 'le', 'le.eid = comp.eid');
  $query->condition('le.lms', 'A');
  $query->addField('le', 'name', 'seasonName');
  // The fixture
  $query->innerJoin('node', 'n', 'n.nid = f.nid');
  $query->addField('n', 'nid', 'nid');
  $query->addField('f', 'date', 'date');
  // Fixture title info
  $query->leftJoin('node', 'ht', 'ht.nid = f.home_team');
  $query->leftJoin('node', 'at', 'at.nid = f.away_team');
  $query->addField('ht', 'title', 'home');
  $query->addField('at', 'title', 'away');
  $query->addField('f', 'round', 'round');
  // Opponents
  $query->innerJoin('league_player', 'w', 'w.pid = r.white');
  $query->addField('w', 'firstname', 'wfirst');
  $query->addField('w', 'lastname', 'wlast');
  $query->innerJoin('league_player', 'b', 'b.pid = r.black');
  $query->addField('b', 'firstname', 'bfirst');
  $query->addField('b', 'lastname', 'blast');
  // Sort by date
  $query->orderBy('f.date', 'ASC');

  //dsm((string)$query);
  $queryResult = $query->execute();

  foreach ( $queryResult as $row ) {
    $result = $row->result;
    if( league_event_type_individual($row->eventType)) {
      $fixture = league_link('Round ' . $row->round, "league_fixture/$row->nid");
    } else {
      $fixture = league_link($row->home . ' v ' . $row->away, "league_fixture/$row->nid");
    }
    if(isset($row->rdate) ) {
      $date = date('Y-m-d',$row->rdate);
      // Use fixture date if invalid date because of bug introduced
      // when adding rdate to league_chess_result table.
      if(substr($date,0,4) == '1970') {
        $date = $row->date;
      }
    } else {
      $date = $row->date;
    }
    $date = substr($date,0,10);

    if($row->white == $pid) {
      $opponent = league_link($row->bfirst . ' ' . $row->blast,"league_player/$row->black");
    } else {
      $opponent = league_link($row->wfirst . ' ' . $row->wlast,"league_player/$row->white");
      // If playing black, flip the result - draws stay the same!
      if($result == 'W') {
        $result = 'L';
      } else {
        if($result == 'L') {
          $result = 'W';
        }
      }
    }
    $rows[] = array($opponent, $result, $date, $fixture, $row->event);
  }

  $table['data'] = $rows;
  $table['header'] = $header;
  return $table;
}
