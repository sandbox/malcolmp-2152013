<?php
/**
 * @file
 * PGN template for a matchcard
 */

/**
 * Create PGN file skeleton for an event or a fixture.
 */
function league_pgn_template($node, $format, $round=0) {

  if($format == 'pgnt') {
    // Get list of fixtures for a season
    $query = db_select('league_fixture', 'f');
    $query->addField('f', 'nid', 'nid');
    $query->innerJoin('league_competition', 'comp', 'comp.cid = f.cid');
    $query->condition('comp.eid', $round);
    // Today's
    $query->where('f.date >=UNIX_TIMESTAMP(CURRENT_DATE()) AND f.date < UNIX_TIMESTAMP(DATE_ADD(CURDATE(), INTERVAL 1 DAY) )');
    $result = $query->execute();
    $fixtures = array();
    foreach ( $result as $row) {
      $fixtures[]= $row->nid;
    }
  } else {
    // Get list of fixtures for event
    if($node->type == 'event') {
      $query = db_select('league_fixture', 'f');
      $query->addField('f', 'nid', 'nid');
      $query->condition('f.cid', $node->nid);
      if($round > 0) {
        $query->condition('f.round', $round);
      } else {
        if($round == -1) {
          $query->where('f.date >UNIX_TIMESTAMP(CURRENT_DATE()) AND f.date < UNIX_TIMESTAMP(DATE_ADD(CURDATE(), INTERVAL 1 DAY) )');
        }
      }
      $result = $query->execute();
      $fixtures = array();
      foreach ( $result as $row) {
        $fixtures[]= $row->nid;
      }
      $compid = $node->league_compid;
      $rlid2 = $node->league_rating_type;
    } else {
      $fixtures = array($node->nid);
      $event = league_fixture_value($node->nid, 'event');
      $platform = league_chess_event_value($event, 'platform');
      $eventNode = node_load($event);
      $compid = $eventNode->league_compid;
      $rlid2 = $eventNode->league_rating_type;
    } 
  } 
  if( count($fixtures) == 0) {
    drupal_set_message('No fixtures in event', 'error');
    return;
  }
  if($format == 'pgnz') {
    $fileName = "PGN$compid.ZIP";
    $path = 'public://league_chess';
    if (!file_prepare_directory($path, FILE_CREATE_DIRECTORY)) {
      drupal_set_message(t('Could not create file @file', array('@file' => $path)), 'error');
      return;
    }
    // create directory files/league_chess/pgn (if it doesn't exist)
    $pgn_path = 'public://league_chess/pgn';
    if (!file_prepare_directory($pgn_path, FILE_CREATE_DIRECTORY)) {
      drupal_set_message(t('Could not create file @file', array('@file' => $pgn_path)), 'error');
      return;
    }
    $realpath = drupal_realpath($pgn_path);
    league_remove_files($realpath, 'pgn');
  } else {
    $fileName = "match.pgn";
  }
  // send HTTP headers
  if($format == 'pgnz') {
    drupal_add_http_header('Content-Type', 'application/zip');
  } else {
    drupal_add_http_header('Content-Type', 'text/plain;charset=utf-8');
  }
  drupal_add_http_header('Content-Disposition', 'attachment; filename="' . $fileName . '"');
  drupal_add_http_header('Cache-Control',  'max-age=0');

  $match = 0;
  $game_count = 0;
  foreach($fixtures as $nid) {
    $match++;
    $fixtureNode = node_load($nid);
    // Get match card and results data
    $data = league_chess_matchcard_data($fixtureNode);
    // Check fixture is visible
    if($data['visible']) {
      $round = 1;
      if(isset($data['round'])) {
        $round = $data['round'];
      }
      // TODO need to pass new arg into this to get FIDE ratings.
      $results = league_chess_matchcard_results($fixtureNode,$data);

      $event = $data['event'];
      $platform = league_chess_event_value($event, 'platform');
      $venue = league_chess_platform_option_label($platform);
      $date = date('Y.m.d', $data['date']);
      $sid = league_event_value($event, 'season');
      $season = league_event_load($sid);
      $title = $season->name . ' ' . $data['eventName'];

      $nboards = count($results);
      for($board=0; $board< $nboards; $board++)
      {
        $game_count++;
        $boardNum = $board+1;
        $whiteGrade = '';
        if(isset($results[$board]['whiteGrade'])) {
          $whiteGrade = $results[$board]['whiteGrade'];
        }
        if(isset($results[$board]['blackGrade'])) {
          $blackGrade = $results[$board]['blackGrade'];
        }
        $home = ' ';
        if(isset($data['homeTeamName']) ) {
          $home = $data['homeTeamName'];
        }
        $away = ' ';
        if(isset($data['awayTeamName']) ) {
          $away = $data['awayTeamName'];
        }
        $white = ' ';
        if(isset($results[$board]['whiteName']) ) {
          $white = $results[$board]['whiteName'];
        }
        $black = ' ';
        if(isset($results[$board]['blackName']) ) {
          $black = $results[$board]['blackName'];
        }
        $whitelid = ' ';
        if(isset($results[$board]['whiteLid']) ) {
          $whitelid = $results[$board]['whiteLid'];
        }
        $blacklid = ' ';
        if(isset($results[$board]['blackLid']) ) {
          $blacklid = $results[$board]['blackLid'];
        }
        // result
        $result = league_chess_pgn_result($results[$board]);
        $rid = $results[$board]['rid'];
        // PGN
        if($results[$board]['colour'] == 'W') {
          $pgn_data = league_pgn_game($title, $white, $black, $whiteGrade, $blackGrade, $result, $date, $home, $away, $boardNum, $venue, $round, $whitelid, $blacklid, $format, $match);
        } else {
          $pgn_data = league_pgn_game($title, $black, $white, $blackGrade, $whiteGrade, $result, $date, $away, $home, $boardNum, $venue, $round, $blacklid, $whitelid, $format, $match);
        }
        if($format == 'pgnz') {
          if($results[$board]['colour'] == 'W') {
            $filename = "$realpath/$whitelid" . '_vs_' . "$blacklid" . '_____.__.__.pgn';
          } else {
            $filename = "$realpath/$blacklid" . '_vs_' . "$whitelid" . '_____.__.__.pgn';
          }
          if (!($fpgn = fopen($filename, "w"))) {
            drupal_set_message(t('Could not write to file @file', array('@file' => $filename)), 'error');
            return;
          }
          fprintf($fpgn, $pgn_data);
          fclose($fpgn);
        } else {
          print $pgn_data;
        }
      }  // each board
    }  // fixture visible
  }   // each fixture

  if($game_count == 0) {
    drupal_set_message('No games in event', 'error');
    return;
  }

  if($format == 'pgnz') {
    $zip = new ZipArchive();
    $zipName = "$realpath/$fileName";
    if(file_exists($zipName)) {
      unlink($zipName);
    }
    if ($zip->open($zipName, ZipArchive::CREATE)!==TRUE) {
      drupal_set_message("cannot open <$zipName>");
    } else {
      $files_to_zip = glob($realpath . '/*.pgn');
      foreach($files_to_zip as $file_to_zip) {
        $local_file = basename($file_to_zip);
        if(!$zip->addFile($file_to_zip, $local_file)) {
          drupal_set_message("failed to add <$file_to_zip>", 'error');
        }
      }
      $numfiles = $zip->numFiles;
      $zipstatus = $zip->status;
      if($zip->close() ) {
        drupal_set_message("Created $zipName with $numfiles files status=$zipstatus");
        readfile($zipName);
      } else {
        drupal_set_message("Failed to Create $zipName with $numfiles files status=$zipstatus");
      }
    }
  }
  exit;
}

function league_chess_pgn_result($results) {
  $result ='*';
  if(isset($results['result']) ) {
    $res = $results['result'];
    $col = $results['colour'];
    if($res == 'W') {
      if($col == 'W' ) {
        $result = '1-0';
      } else {
        $result = '0-1';
      }
    } else {
      $result = '1/2-1/2';
      if($res == 'L') {
        if($col == 'W' ) {
          $result = '0-1';
        } else {
          $result = '1-0';
        }
      }
    }
  }
  return $result;
}

function league_pgn_game($title, $white, $black, $whiteGrade, $blackGrade, $result, $date, $homeTeam, $awayTeam, $board, $venue, $round, $whitelid, $blacklid, $format, $match=1) {
  $game = <<<MATCH
[Event "$title"]
[Site "$venue"]
[Date "$date"]
[Round "$round.$match.$board"]
[White "$white"]
[WhiteTeam "$homeTeam"]
[Black "$black"]
[BlackTeam "$awayTeam"]
[Result "$result"]
[ECO "A00"]
[WhiteElo "$whiteGrade"]
[BlackElo "$blackGrade"]
[PlyCount "0"]
[EventDate "$date"]
MATCH;
  if(strlen($whitelid) > 1) {
    $game .= "\n" . '[WhiteOlid "' . $whitelid . '"]';
    $game .= "\n" . '[BlackOlid "' . $blacklid . '"]';
  }
  if($format == 'pgnz') {
    $game .= "\n";
  } else {
    $game .= "\n\n*\n";
  }
  return $game;
}
