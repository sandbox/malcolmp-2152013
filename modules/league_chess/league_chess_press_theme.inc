<?php
/**
 * @file
 * Theme functions for displaying press report as a web page
 */

/**
 * Theme press report as a web page
 */
function theme_league_chess_press($variables) {
  $element = $variables['element'];
  $data = $element['#data'];

  $title = $data['title'];
  $from = $data['from'];
  $to = $data['to'];
  $events = $data['events'];

  $output = "<h3>Press Report from $from to $to</h3>";
 
  foreach($events as $event) {
    $title = $event['title'];
    $output .= "<h3>$title</h3>";
    $fixtures = $event['fixtures'];
    foreach($fixtures as $fixture) {
      $table = array(
        '#theme' => 'league_table',
        '#table' => $fixture['table'],
      );
      $output .= drupal_render($table);
      $comments =  $fixture['comments'];
      foreach($comments as $comment) {
        $output .= "<h4>";
        $output .= $comment['subject'];
        $output .= $comment['name'];
        $output .= "</h4>";
        $output .= $comment['text'];
      }
    }
  }
  return $output;
}
