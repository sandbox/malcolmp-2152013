<?php
/**
 * @file
 * Games played by players with local grades
 */

/**
 * Players with local grades
 */
function league_chess_reports_local($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);
  // Set title
  $title = $node->title . ' - Players with cat L - Local grades';
  drupal_set_title($title);

  $content['newly']['#table'] = league_chess_local_graded_players($node);
  $content['newly']['#theme'] = 'league_table';

  // Ensure if a player is picked on, the Back to list tab works.
  $_SESSION['league_player_list_url'] = $_GET['q'];

  return $content;
}

/**
 * Get players with L grades.
 */

function league_chess_local_graded_players($node) {
  $org = $node->nid;

  $header = array();
  $header[] = array('data' => 'First Name', 'field' => 'firstname' );
  $header[] = array('data' => 'Last Name', 'field' => 'lastname' );
  $header[] = array('data' => 'Local Grade', 'field' => 'chess_rating' );
  $header[] = array('data' => 'ECF Code', 'field' => 'ecf_code' );
  $header[] = array('data' => 'ECF Grade', 'field' => 'grade' );
  $header[] = array('data' => 'ECF Cat', 'field' => 'cat', 'sort'=>'desc' );

  $rows = array();
  $query = db_select('league_player', 'p');
  $query->fields('p', array('pid', 'firstname', 'lastname', 'chess_rating', 'ecf_code'));
  // In a list in the organisaton
  $query->innerJoin('league_player_player', 'lpp', 'lpp.pid = p.pid');
  $query->innerJoin('league_player_list', 'lpl', 'lpl.lid = lpp.lid');
  $query->condition('lpl.org', $org);
  // players with local grades
  $query->condition('p.chess_rating', 0, '>');
  // Check the grading list
  $query->leftJoin('rating_player', 'm', 'm.pid = p.ecf_code AND m.rlid=1');
  $query->addField('m', 'rating', 'grade');
  $query->addField('m', 'cat', 'cat');

  // Make table sortable
  $query = $query->extend('TableSort')->orderByHeader($header);

  //dsm((string)$query);
  $queryResult = $query->execute();

  foreach ( $queryResult as $row ) {
    $pid = $row->pid;
    $firstname = l("$row->firstname","league_player/$pid");
    $lastname = $row->lastname;
    $lgrade = $row->chess_rating;
    $ecfcode = $row->ecf_code;
    $grade = $row->grade;
    $cat = $row->cat;
    $rows[] = array($firstname, $lastname, $lgrade, $ecfcode, $grade, $cat);
  }

  $table['data'] = $rows;
  $table['header'] = $header;

  return $table;
}
