<?php

/**
 * @file
 * league_features.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function league_features_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_admin|node|organisation|default';
  $field_group->group_name = 'group_admin';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organisation';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'children' => array(
      0 => 'field_auto_locking',
      1 => 'field_verify_emails',
      2 => 'field_unfinished_games',
      3 => 'field_default_named',
      4 => 'field_show_current_grade',
      5 => 'field_grade_limit_option',
      6 => 'field_team_designation',
      7 => 'field_visibility',
      8 => 'field_white_default',
      9 => 'field_treasurer',
      10 => 'field_player_list_access',
      11 => 'field_flag_match_comments',
      12 => 'field_grader',
      13 => 'field_organisation_type',
    ),
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'classes' => ' group-admin field-group-fieldset',
        'description' => '',
      ),
    ),
    'format_type' => 'fieldset',
    'label' => 'Admin Details',
    'weight' => '1',
  );
  $field_groups['group_admin|node|organisation|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_admin|node|organisation|form';
  $field_group->group_name = 'group_admin';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organisation';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'children' => array(
      0 => 'field_auto_locking',
      1 => 'field_users',
      2 => 'field_verify_emails',
      3 => 'field_unfinished_games',
      4 => 'field_default_named',
      5 => 'field_show_current_grade',
      6 => 'field_grade_limit_option',
      7 => 'field_team_designation',
      8 => 'field_visibility',
      9 => 'field_white_default',
      10 => 'field_treasurer',
      11 => 'field_owners',
      12 => 'field_player_list_access',
      13 => 'field_flag_match_comments',
      14 => 'field_grader',
      15 => 'field_organisation_type',
      16 => 'title',
    ),
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'classes' => ' group-admin field-group-tab',
        'description' => '',
        'required_fields' => 1,
      ),
    ),
    'format_type' => 'tab',
    'label' => 'Admin Details',
    'weight' => '0',
  );
  $field_groups['group_admin|node|organisation|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_admin|node|organisation|teaser';
  $field_group->group_name = 'group_admin';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organisation';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'children' => array(
      0 => 'field_auto_locking',
      1 => 'field_fixture_revisions',
      2 => 'field_season',
      3 => 'field_verify_emails',
      4 => 'field_website',
    ),
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'classes' => ' group-admin field-group-fieldset',
      ),
    ),
    'format_type' => 'fieldset',
    'label' => 'Admin Details',
    'weight' => '-1',
  );
  $field_groups['group_admin|node|organisation|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_club|node|club|form';
  $field_group->group_name = 'group_club';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'club';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'children' => array(
      0 => 'field_org',
      1 => 'field_venue_match_limit',
      2 => 'field_player_list',
      3 => 'field_ecf_club_code',
      4 => 'field_match_time',
      5 => 'field_contact_user',
      6 => 'title',
      7 => 'field_club_status',
    ),
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'classes' => ' group-club field-group-tab',
        'description' => '',
        'required_fields' => 1,
      ),
    ),
    'format_type' => 'tab',
    'label' => 'Club Details',
    'weight' => '0',
  );
  $field_groups['group_club|node|club|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_display|node|club|form';
  $field_group->group_name = 'group_display';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'club';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'children' => array(
      0 => 'body',
      1 => 'field_google_map',
    ),
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'classes' => ' group-display field-group-tab',
        'description' => '',
        'required_fields' => 1,
      ),
    ),
    'format_type' => 'tab',
    'label' => 'Display',
    'weight' => '3',
  );
  $field_groups['group_display|node|club|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_display|node|event|form';
  $field_group->group_name = 'group_display';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'event';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'children' => array(
      0 => 'body',
    ),
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'classes' => ' group-display field-group-tab',
        'description' => '',
        'required_fields' => 1,
      ),
    ),
    'format_type' => 'tab',
    'label' => 'Display',
    'weight' => '10',
  );
  $field_groups['group_display|node|event|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_display|node|organisation|default';
  $field_group->group_name = 'group_display';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organisation';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'children' => array(
      0 => 'body',
    ),
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'classes' => ' group-display field-group-tab',
        'description' => '',
      ),
    ),
    'format_type' => 'tab',
    'label' => 'Display',
    'weight' => '0',
  );
  $field_groups['group_display|node|organisation|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_display|node|organisation|form';
  $field_group->group_name = 'group_display';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organisation';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'children' => array(
      0 => 'body',
    ),
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'classes' => ' group-display field-group-tab',
        'description' => '',
        'required_fields' => 1,
      ),
    ),
    'format_type' => 'tab',
    'label' => 'Display',
    'weight' => '3',
  );
  $field_groups['group_display|node|organisation|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_event|node|event|form';
  $field_group->group_name = 'group_event';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'event';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'children' => array(
      0 => 'field_ntimes',
      1 => 'field_org',
      2 => 'field_type',
      3 => 'field_boards',
      4 => 'field_rate',
      5 => 'field_grading_limit',
      6 => 'field_event_player_list',
      7 => 'field_below_default',
      8 => 'field_default_grade',
      9 => 'field_registered_event',
      10 => 'field_event_order',
      11 => 'field_two_matches',
      12 => 'title',
      13 => 'field_handicap',
    ),
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'classes' => ' group-event field-group-tab',
        'description' => '',
        'required_fields' => 1,
      ),
    ),
    'format_type' => 'tab',
    'label' => 'Event Details',
    'weight' => '0',
  );
  $field_groups['group_event|node|event|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_files|node|organisation|form';
  $field_group->group_name = 'group_files';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organisation';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'children' => array(
      0 => 'field_files',
    ),
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
        'required_fields' => 1,
      ),
    ),
    'format_type' => 'tab',
    'label' => 'Files',
    'weight' => '4',
  );
  $field_groups['group_files|node|organisation|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_fixture|node|fixture|form';
  $field_group->group_name = 'group_fixture';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'fixture';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'children' => array(
      0 => 'field_away_team',
      1 => 'field_date',
      2 => 'field_home_team',
      3 => 'field_round',
      4 => 'field_table_number',
    ),
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'classes' => ' group-fixture field-group-tab',
        'description' => '',
        'required_fields' => 1,
      ),
    ),
    'format_type' => 'tab',
    'label' => 'Fixture Details',
    'weight' => '0',
  );
  $field_groups['group_fixture|node|fixture|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_generate|node|organisation|default';
  $field_group->group_name = 'group_generate';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organisation';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'children' => array(
      0 => 'field_avoid_date',
    ),
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'classes' => ' group-generate field-group-fieldset',
        'description' => '',
      ),
    ),
    'format_type' => 'fieldset',
    'label' => 'Fixture Generation',
    'weight' => '3',
  );
  $field_groups['group_generate|node|organisation|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_generate|node|organisation|form';
  $field_group->group_name = 'group_generate';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organisation';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'children' => array(
      0 => 'field_avoid_date',
    ),
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'classes' => ' group-generate field-group-tab',
        'description' => '',
        'required_fields' => 1,
      ),
    ),
    'format_type' => 'tab',
    'label' => 'Fixture Generation',
    'weight' => '9',
  );
  $field_groups['group_generate|node|organisation|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_match_avoid|node|team|default';
  $field_group->group_name = 'group_match_avoid';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'team';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'children' => array(
      0 => 'field_avoid_date',
    ),
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'classes' => ' group-match-avoid field-group-fieldset',
      ),
    ),
    'format_type' => 'fieldset',
    'label' => 'Match dates to avoid',
    'weight' => '1',
  );
  $field_groups['group_match_avoid|node|team|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_match_avoid|node|team|form';
  $field_group->group_name = 'group_match_avoid';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'team';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'children' => array(
      0 => 'field_avoid_date',
    ),
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'classes' => ' group-match-avoid field-group-tab',
        'description' => '',
        'required_fields' => 1,
      ),
    ),
    'format_type' => 'tab',
    'label' => 'Match dates to avoid',
    'weight' => '1',
  );
  $field_groups['group_match_avoid|node|team|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_match_avoid|node|team|teaser';
  $field_group->group_name = 'group_match_avoid';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'team';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'children' => array(
      0 => 'field_avoid_date',
      1 => 'field_avoid_date',
    ),
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'classes' => ' group-match-avoid field-group-fieldset',
      ),
    ),
    'format_type' => 'fieldset',
    'label' => 'Match dates to avoid',
    'weight' => '1',
  );
  $field_groups['group_match_avoid|node|team|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_result|node|fixture|form';
  $field_group->group_name = 'group_result';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'fixture';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'children' => array(
      0 => 'field_away_score',
      1 => 'field_event',
      2 => 'field_home_score',
      3 => 'field_locked',
      4 => 'field_verifier',
      5 => 'field_home_penalty',
      6 => 'field_away_penalty',
      7 => 'field_violation',
      8 => 'field_winning_team',
      9 => 'field_home_adjustment',
      10 => 'title',
      11 => 'field_away_adjustment',
    ),
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'classes' => ' group-result field-group-tab',
        'description' => '',
        'required_fields' => 1,
      ),
    ),
    'format_type' => 'tab',
    'label' => 'Fixture Result',
    'weight' => '1',
  );
  $field_groups['group_result|node|fixture|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_team_avoid|node|team|default';
  $field_group->group_name = 'group_team_avoid';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'team';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'children' => array(
      0 => 'field_teams',
    ),
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'classes' => ' group-team-avoid field-group-fieldset',
      ),
    ),
    'format_type' => 'fieldset',
    'label' => 'Teams to avoid fixture clashes',
    'weight' => '2',
  );
  $field_groups['group_team_avoid|node|team|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_team_avoid|node|team|form';
  $field_group->group_name = 'group_team_avoid';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'team';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'children' => array(
      0 => 'field_teams',
    ),
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'classes' => ' group-team-avoid field-group-tab',
        'description' => '',
        'required_fields' => 1,
      ),
    ),
    'format_type' => 'tab',
    'label' => 'Teams to avoid fixture clashes',
    'weight' => '2',
  );
  $field_groups['group_team_avoid|node|team|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_team_avoid|node|team|teaser';
  $field_group->group_name = 'group_team_avoid';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'team';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'children' => array(
      0 => 'field_team_avoid',
    ),
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'classes' => ' group-team-avoid field-group-fieldset',
      ),
    ),
    'format_type' => 'fieldset',
    'label' => 'Teams to avoid fixture clashes',
    'weight' => '2',
  );
  $field_groups['group_team_avoid|node|team|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_team|node|team|form';
  $field_group->group_name = 'group_team';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'team';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'children' => array(
      0 => 'body',
      1 => 'field_captain',
      2 => 'field_club',
      3 => 'field_event',
      4 => 'field_night',
      5 => 'field_team_order',
      6 => 'field_team_penalty',
      7 => 'title',
      8 => 'field_night2',
    ),
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'classes' => ' group-team field-group-tab',
        'description' => '',
        'required_fields' => 1,
      ),
    ),
    'format_type' => 'tab',
    'label' => 'Team Details',
    'weight' => '0',
  );
  $field_groups['group_team|node|team|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_users|node|club|form';
  $field_group->group_name = 'group_users';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'club';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'children' => array(
      0 => 'field_user',
    ),
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'classes' => ' group-users field-group-tab',
        'description' => '',
        'required_fields' => 1,
      ),
      'label' => 'Owners',
    ),
    'format_type' => 'tab',
    'label' => 'Owners',
    'weight' => '1',
  );
  $field_groups['group_users|node|club|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_users|node|event|form';
  $field_group->group_name = 'group_users';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'event';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'children' => array(
      0 => 'field_user',
    ),
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'classes' => ' group-users field-group-tab',
        'description' => '',
        'required_fields' => 1,
      ),
      'label' => 'Owners',
    ),
    'format_type' => 'tab',
    'label' => 'Owners',
    'weight' => '9',
  );
  $field_groups['group_users|node|event|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_users|node|organisation|default';
  $field_group->group_name = 'group_users';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organisation';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'children' => array(
      0 => 'field_owners',
    ),
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'classes' => ' group-users field-group-fieldset',
        'description' => '',
        'id' => '',
      ),
      'label' => 'Owners',
    ),
    'format_type' => 'fieldset',
    'label' => 'Owners',
    'weight' => '2',
  );
  $field_groups['group_users|node|organisation|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_users|node|organisation|form';
  $field_group->group_name = 'group_users';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organisation';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'children' => array(
      0 => 'field_users',
      1 => 'field_owners',
    ),
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'classes' => ' group-users field-group-tab',
        'description' => '',
        'required_fields' => 1,
      ),
      'label' => 'Owners',
    ),
    'format_type' => 'tab',
    'label' => 'Owners',
    'weight' => '1',
  );
  $field_groups['group_users|node|organisation|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_users|node|organisation|teaser';
  $field_group->group_name = 'group_users';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'organisation';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'children' => array(
      0 => 'field_users',
      1 => 'field_users',
      2 => 'field_users',
    ),
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'classes' => ' group-users field-group-fieldset',
      ),
    ),
    'format_type' => 'fieldset',
    'label' => 'Users',
    'weight' => '2',
  );
  $field_groups['group_users|node|organisation|teaser'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Admin Details');
  t('Club Details');
  t('Display');
  t('Event Details');
  t('Files');
  t('Fixture Details');
  t('Fixture Generation');
  t('Fixture Result');
  t('Match dates to avoid');
  t('Owners');
  t('Team Details');
  t('Teams to avoid fixture clashes');
  t('Users');

  return $field_groups;
}
