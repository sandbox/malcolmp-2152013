<?php

/**
 * @file
 * league_features.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function league_features_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function league_features_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function league_features_flag_default_flags() {
  $flags = array();
  // Exported flag: "Commented Fixtures".
  $flags['commented_fixtures'] = array(
    'entity_type' => 'node',
    'title' => 'Commented Fixtures',
    'global' => 1,
    'types' => array(
      0 => 'fixture',
    ),
    'flag_short' => 'Mark comments as not seen',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Mark comments as seen',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 0,
      'teaser' => 0,
      'rss' => 0,
      'diff_standard' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => FALSE,
    'i18n' => 0,
    'module' => 'league_features',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  // Exported flag: "All match updates".
  $flags['league_fixture_updates'] = array(
    'entity_type' => 'node',
    'title' => 'All match updates',
    'global' => 0,
    'types' => array(
      0 => 'organisation',
    ),
    'flag_short' => 'Email me match updates for [node:title]',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Stop emailing me match updates',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'normal',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 'teaser',
      'rss' => 0,
      'diff_standard' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => FALSE,
    'i18n' => 0,
    'module' => 'league_features',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  // Exported flag: "league_status_updates".
  $flags['league_status_updates'] = array(
    'entity_type' => 'node',
    'title' => 'league_status_updates',
    'global' => 0,
    'types' => array(
      0 => 'organisation',
    ),
    'flag_short' => 'Subscribe to status emails',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Unsubscribe from status emails',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 'teaser',
      'rss' => 0,
      'diff_standard' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => FALSE,
    'i18n' => 0,
    'module' => 'league_features',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  // Exported flag: "Postponed Fixtures".
  $flags['postponed_fixtures'] = array(
    'entity_type' => 'node',
    'title' => 'Postponed Fixtures',
    'global' => 1,
    'types' => array(
      0 => 'fixture',
    ),
    'flag_short' => 'Mark Fixture as Postponed',
    'flag_long' => '',
    'flag_message' => 'Fixture Postponed',
    'unflag_short' => 'Set Fixture not postponed',
    'unflag_long' => '',
    'unflag_message' => 'Fixture not postponed',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 0,
      'teaser' => 0,
      'rss' => 0,
      'diff_standard' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => FALSE,
    'i18n' => 0,
    'module' => 'league_features',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
    'status' => FALSE,
  );
  return $flags;

}

/**
 * Implements hook_node_info().
 */
function league_features_node_info() {
  $items = array(
    'club' => array(
      'name' => t('Club'),
      'base' => 'node_content',
      'description' => t('Chess Club that belongs to an organisation'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
    'event' => array(
      'name' => t('Event'),
      'base' => 'node_content',
      'description' => t('Event'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'fixture' => array(
      'name' => t('Fixture'),
      'base' => 'node_content',
      'description' => t('A fixture is a match between 2 teams for a team event, or a round in an individual event.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'organisation' => array(
      'name' => t('Organisation'),
      'base' => 'node_content',
      'description' => t('Chess Organisation, e.g. Blackpool and Fylde League'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
    'team' => array(
      'name' => t('Team'),
      'base' => 'node_content',
      'description' => t('A team of players in a league'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
