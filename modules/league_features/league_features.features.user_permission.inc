<?php
/**
 * @file
 * league_features.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function league_features_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create field_contact'.
  $permissions['create field_contact'] = array(
    'name' => 'create field_contact',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_contact'.
  $permissions['edit field_contact'] = array(
    'name' => 'edit field_contact',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_contact'.
  $permissions['edit own field_contact'] = array(
    'name' => 'edit own field_contact',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_contact'.
  $permissions['view field_contact'] = array(
    'name' => 'view field_contact',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_contact'.
  $permissions['view own field_contact'] = array(
    'name' => 'view own field_contact',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
