<?php
/**
 * @file
 * Games played by none ecf members players
 */

/**
 * none ecf members player games
 */
function league_ecf_reports_tab_noneecf($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);
  // Set title
  $title = $node->title . ' - Games By Non-ECF Members';
  drupal_set_title($title);

  $content['header']['#markup'] = "OTB Games played in active seasons by players  who are not ECF members or whose membership expires before June 30th or the game was played after their membership expired. Note that although not members now, they could have been members when the games were played.<p>";
  $content['header']['#type'] = 'markup';

  $content['noneecf']['#table'] = league_ecf_noneecf_table($node->nid);
  $content['noneecf']['#theme'] = 'league_table';

  $updated = ecfdata_grade_update_date();

  $content['footer']['#markup'] = "<p>Based on membership information from the rating system updated on $updated .";
  $content['footer']['#type'] = 'markup';

  // Ensure if a player is picked on, the Back to list tab works.
  $_SESSION['league_player_list_url'] = $_GET['q'];

  return $content;
}

function league_ecf_count_game_fee_players($org) {
  $cache_name = "league_ecf_count_game_fee_players_$org";
  $num_rows = &drupal_static($cache_name);
  if (!isset($num_rows)) {
    if( $cache = cache_get($cache_name)) {
      $num_rows = $cache->data;
    } else {
      $num_rows = 0;
      $table = league_ecf_noneecf_table($org, FALSE);
      $rows = $table['data'];
      foreach($rows as $row) {
        if($row[4] > 3) {
          $num_rows++;
        }
      }
      cache_set($cache_name, $num_rows, 'cache');
    }
  }
  return $num_rows;
}

function league_ecf_none_member_game_union($org,$player='white',$rate='S') {

  // Set rapid or standard play rates
  if ( $rate == 'S' ) {
    $rates = array('S','D','H');
  } else {
    $rates = array('R','E','I');
  }
  // Get all fixture for events in the current season.
  $query = db_select('league_fixture', 'f');

  // Active Seasons
  $query->innerJoin('league_competition', 'comp', 'comp.cid = f.cid');
  $query->condition('comp.ecf_auto', $rates, 'IN');
  $query->innerJoin('league_event', 'le', 'le.eid = comp.eid');
  $query->condition('le.lms', 'A');
  $query->condition('le.org', $org);
  //  match
  $query->innerJoin('league_match', 'm', 'm.fid = f.nid');
  $query->innerJoin('node', 'n', 'n.nid = m.fid');
  //  results for the player
  $query->innerJoin('league_chess_result', 'r', 'r.mid = m.mid');
  $query->addField('r', $player, 'player');
  if ( $rate == 'S' ) {
    $query->addExpression('2 * COUNT(*)', 'pcount');
  } else {
    $query->addExpression('COUNT(*)', 'pcount');
  }
  //  not byes or defaults
  $query->condition("r.white", 0, '>');
  $query->condition("r.black", 0, '>');
  $query->condition('r.result', array('W','D','L'), 'IN');
  //  player name
  $query->innerJoin('league_player', 'p', "p.pid = r.$player");
  $query->addField('p', 'firstname');
  $query->addField('p', 'lastname');
  $query->addField('p', 'ecf_code', 'ecfcode');
  //  players team
  if($player == 'white') {
    $query->leftJoin('field_data_field_club', 'c', "c.entity_id = f.home_team");
  } else {
    $query->leftJoin('field_data_field_club', 'c', "c.entity_id = f.away_team");
  }
  //  players club
  $query->leftJoin('node', 'cn', "cn.nid = c.field_club_value");
  $query->addField('cn', 'title', 'club');
  // Exclude decease players (D)
  $query->leftJoin('player_list_player', 'plp', "plp.pid = p.ecf_code");
  $query->where("plp.status!='D' OR plp.status IS NULL");
  //  None members or game played after expiry or expiry before June 30th
  $year = date("Y");
  $valid_time = mktime(0, 0, 0, 6, 30, $year);
  $query->where("plp.mdate IS NULL OR plp.status='E' OR plp.status=' ' OR plp.mdate < r.rdate OR plp.mdate < $valid_time");
  $query->addField('plp', 'mid', 'ecfmem');
  $query->addExpression("CASE WHEN plp.status='E' THEN 'Supporter' WHEN plp.mdate < r.rdate THEN 'Expiry before game' WHEN plp.mdate < $valid_time THEN 'Expiry before June 30th' ELSE 'Non Member' END", 'reason');
  $query->groupBy('player');
  $query->groupBy('reason');

  return $query;
}

function league_ecf_noneecf_table($org,$html=TRUE) {
  $table1 = league_ecf_none_member_game_union($org,'white','S');
  $table2 = league_ecf_none_member_game_union($org,'black','S');
  $table3 = league_ecf_none_member_game_union($org,'white','R');
  $table4 = league_ecf_none_member_game_union($org,'black','R');
  $query = Database::getConnection()
  ->select($table1->union($table2, 'ALL')->union($table3, 'ALL')->union($table4, 'ALL'))
  ->fields(NULL, array('player', 'firstname', 'lastname', 'ecfcode', 'ecfmem', 'club', 'reason'))
  ->groupBy('player')
  ->groupBy('reason')
  ->orderBy('total', 'DESC');
  $query->addExpression('sum(pcount)', 'total');

//drupal_set_message((string)$query);

  $queryResult = $query->execute();

  $rows = array();
  foreach ( $queryResult as $row) {
    $pid = $row->player;
    $name = $row->firstname . ' ' . $row->lastname;
    $name_link = l($name, 'league_player/' . $pid);
    $club = $row->club;
    $ecfcode = $row->ecfcode;
    $count = $row->total * 0.5;
    if($count > 3 && $html) {
      $count = "<b>$count</b>";
    }
    $mem = $row->ecfmem;
    $reason = $row->reason;
    $rows[] = array($name_link, $club, $ecfcode, $mem, $count, $reason);
  }
  $header = array('Name', 'Club', 'ECF Code', 'Membership No.', 'Number of games', 'Reason');
  $table['data'] = $rows;
  $table['header'] = $header;
  return $table;
}
