<?php
/**
 * @file
 * Games and grading for players with matching ECF Code
 */

/**
 * Player stats for all clubs with this ECF club code
 * in all organisations for this season
 */
function league_ecf_player_grade_tab($player,$rate='S') {
  // Set breadcrumb
  league_player_set_bread_crumb ($player);
  // Set title
  $name = "$player->firstname $player->lastname";
  // 
  if($rate == 'R') {
    $title = "Games (Rapid Rate)";
  } else {
    $title = "Games (Standard Rate)";
  }

  // Get ECF Code, if its set
  if($player->ecf_code >0) {
    $ecfcode = $player->ecf_code;
    $content['title']['#markup'] = "<h3>$title</h3>";
    $content['title']['#type'] = 'markup';
    // Get games for that ECF code
    $table = league_ecf_seasons_games($ecfcode,$rate);
    $content['table']['#table'] = $table;
    $content['table']['#theme'] = 'league_table';
    // Estimate grade based on them
//  $grade = league_ecf_seasons_grade($player,$table);
//  $content['grade']['#markup'] = $grade;
//  $content['grade']['#type'] = 'markup';
  } else {
    $content['title']['#markup'] = "<h3>$title</h3>Note: Only shows games from current organisation as ECF Grade Code Not Available</p>";
    $content['title']['#type'] = 'markup';
    // Get games for that ECF code
    $table = league_ecf_seasons_games_pid($player->pid,$rate);
    $content['table']['#table'] = $table;
    $content['table']['#theme'] = 'league_table';
    // Estimate grade based on them
 // $grade = league_ecf_seasons_grade($player,$table);
 // $content['grade']['#markup'] = $grade;
 // $content['grade']['#type'] = 'markup';
  }
  return $content;
}

function league_ecf_seasons_games_data($colour, $ecfcode,$rate='S') {
  $rows = array();
  $opponent_colour = 'white';
  if($colour == 'white') {
    $opponent_colour = 'black';
  }

  $lorg = ecfdata_get_player_org();
  if($rate == 'S') {
    $auto_types = array('S', 'D', 'H');
  } else {
    $auto_types = array('R', 'E', 'I');
  }
  // Results for player with given ecfcode
  $query = db_select('league_player', 'p');
  $query->innerJoin('league_chess_result', 'r', "r.$colour=p.pid");
  $query->addField('r', $opponent_colour, 'opponent');
  $query->addField('r', 'result', 'result');
  $query->addField('r', $opponent_colour.'Grade', 'grade');
  // Exclude games with no result or defaults
  $query->condition('r.result', array('W','D','L'), 'IN');
  $query->where("p.ecf_code = $ecfcode");
  // The event
  $query->innerJoin('league_match', 'm', 'm.mid = r.mid');
  $query->innerJoin('league_fixture', 'f', 'f.nid = m.fid');
  // Event title
  $query->innerJoin('node', 'e', 'e.nid = f.cid');
  $query->addField('e', 'title', 'event');
  // Rate of play
  $query->innerJoin('league_competition', 'comp', 'comp.cid = f.cid');
  $query->condition('comp.ecf_auto', $auto_types, 'IN');
  // Active seasons
  $query->innerJoin('league_event', 'le', 'le.eid = comp.eid');
  $query->condition('le.lms', 'A');
  $query->addField('le', 'name', 'seasonName');
  // The fixture
  $query->innerJoin('node', 'n', 'n.nid = f.nid');
  $query->addField('n', 'nid', 'nid');
  // Fixture title
  $query->leftJoin('node', 'ht', 'ht.nid = f.home_team');
  $query->leftJoin('node', 'at', 'at.nid = f.away_team');
  $query->addField('ht', 'title', 'home');
  $query->addField('at', 'title', 'away');
  $query->addField('comp', 'type', 'eventType');
  $query->addField('f', 'round', 'round');
  // Opponent
  $query->innerJoin('league_player', 'lp', "lp.pid = r.$opponent_colour");
  $query->addField('lp', 'firstname', 'first');
  $query->addField('lp', 'lastname', 'last');
  $query->addField('lp', 'chess_rating', 'rating');
  $query->addField('lp', 'ecf_code', 'code');
  // Organisation
  $query->innerJoin('node', 'o', 'o.nid = le.org');
  $query->addField('o', 'title', 'org');
  // exclude test organisations
  $query->innerJoin('field_data_field_organisation_type', 'orgtype', 'orgtype.entity_id = o.nid');
  $query->condition('orgtype.field_organisation_type_value', array('N','C'), 'IN');

//drupal_set_message(print_r((string)$query, TRUE));
  $queryResult = $query->execute();
  foreach ( $queryResult as $row ) {
    $result = $row->result;
    if( league_event_type_individual($row->eventType)) {
      $fixture = l('Round ' . $row->round, "league_fixture/$row->nid");
    } else {
      $fixture = l($row->home . ' v ' . $row->away, "league_fixture/$row->nid");
    }
    $opponent = l($row->first . ' ' . $row->last,"league_player/$row->opponent");
    if($row->rating > 0) {
      $ograde = $row->rating;
    } else {
      $ograde = $row->grade;
    }
    if($colour == 'black') {
      // Playing black, show flip the result (draws unchanged! )
      if($result == 'W') {
        $result = 'L';
      } else {
        if($result == 'L') {
          $result = 'W';
        }
      }
    }
    $rows[] = array($opponent, $result, $ograde, $fixture, $row->event, $row->org);
  }
  return $rows;
}

/**
 * Games for the player(ecfcode) in all orgs for the current season
 */
function league_ecf_seasons_games($ecfcode,$rate='S') {

  $header = array('Opponent', 'Result', 'Grade', 'Fixture', 'Event', 'Organisation' );

  $whites = league_ecf_seasons_games_data('white', $ecfcode,$rate);
  $blacks = league_ecf_seasons_games_data('black', $ecfcode,$rate);
  $rows = array_merge($whites, $blacks);
  $table['data'] = $rows;
  $table['header'] = $header;
  return $table;
}

/**
 * Games for the player(pid) in all orgs for the current season
 */
function league_ecf_seasons_games_pid($pid,$rate='S') {
  if($rate == 'S') {
    $rlid = ecfdata_get_rating_list_id('standard_jan');
  } else {
    $rlid = ecfdata_get_rating_list_id('rapid_aug');
  }
  $rows = array();
  $header = array('Opponent', 'Result', 'Grade', 'Fixture', 'Event', 'Organisation' );

  // Results for player with given pid
  $query = db_select('league_chess_result', 'r');
  $query->fields('r', array('white', 'black', 'result', 'whiteGrade', 'blackGrade'));
  $query->innerJoin('league_match', 'm', 'm.mid = r.mid');
  $query->addField('m', 'fid', 'fid');
  $query->where("r.white = $pid or r.black = $pid");
  // Exclude games with no result or defaults
  $query->condition('r.result', array('W','D','L'), 'IN');
  // The event
  $query->innerJoin('league_fixture', 'f', 'f.nid = m.fid');
  // Event title
  $query->innerJoin('node', 'e', 'e.nid = f.cid');
  $query->addField('e', 'title', 'event');
  // Rate of play
  $query->innerJoin('league_competition', 'comp', 'comp.cid = f.cid');
  $query->innerJoin('rating_list', 'rl', 'rl.rlid = comp.rating_type');
  $query->condition('rl.rate', $rate);
  // Active Seasons
  $query->innerJoin('league_event', 'le', 'le.eid = comp.eid');
  $query->condition('le.lms', 'A');
  $query->addField('le', 'name', 'seasonName');
  // The fixture
  $query->innerJoin('node', 'n', 'n.nid = f.nid');
  $query->addField('n', 'title', 'fixture');
  $query->addField('n', 'nid', 'nid');
  // Opponents
  $query->innerJoin('league_player', 'w', 'w.pid = r.white');
  $query->addField('w', 'firstname', 'wfirst');
  $query->addField('w', 'lastname', 'wlast');
  $query->addField('w', 'chess_rating', 'wrating');
  $query->innerJoin('league_player', 'b', 'b.pid = r.black');
  $query->addField('b', 'firstname', 'bfirst');
  $query->addField('b', 'lastname', 'blast');
  $query->addField('b', 'chess_rating', 'brating');
  // ECF Grades
  $query->leftjoin('rating_player', 'rpw', "rpw.pid = w.ecf_code AND rpw.rlid=$rlid");
  $query->leftjoin('rating_player', 'rpb', "rpb.pid = b.ecf_code AND rpb.rlid=$rlid");
  $query->addField('rpw', 'rating', 'wgrade');
  $query->addField('rpb', 'rating', 'bgrade');
  // Organisation
  $query->innerJoin('node', 'o', 'o.nid = le.org');
  $query->addField('o', 'title', 'org');
  // Sort by date
  $query->orderBy('f.date', 'ASC');

//dsm((string)$query);
  $queryResult = $query->execute();
  foreach ( $queryResult as $row ) {
    $result = $row->result;
    $fixture = l($row->fixture, "node/$row->nid");
    if($row->white == $pid) {
      $colour = 'white';
      $opponent = l($row->bfirst . ' ' . $row->blast,"league_player/$row->black");
      if($row->brating > 0) {
        $ograde = $row->brating;
      } else {
        $ograde = $row->bgrade;
      }
    } else {
      $colour = 'black';
      $opponent = l($row->wfirst . ' ' . $row->wlast,"league_player/$row->white");
      $ograde = $row->wrating;
      if($row->wrating > 0) {
        $ograde = $row->wrating;
      } else {
        $ograde = $row->wgrade;
      }
      // Playing black, show flip the result (draws unchanged! )
      if($result == 'W') {
        $result = 'L';
      } else {
        if($result == 'L') {
          $result = 'W';
        }
      }
    }
    $rows[] = array($opponent, $result, $ograde, $fixture, $row->event, $row->org);
  }
  $table['data'] = $rows;
  $table['header'] = $header;
  return $table;
}

// TODO this doesn't make any sense as its doing an ECF grading calculation
// on possibly the wrong 4 digit grades.
function league_ecf_seasons_grade($player,$table) {
  $results = $table['data'];
  $nresults = count($results);
  $ecf_player = ecfdata_get_player($player->ecf_code);
  $grade = 0;
  if(isset($ecf_player['ratings'][1])) {
    $rating = $ecf_player['ratings'][1];
    $grade = $rating['rating'];
  }
  if($nresults == 0) {
    $newGrade = $grade;
  } else {
    $total = 0;
    for($i=0; $i<$nresults; $i++) {
      $opponentGrade = $results[$i][2];
      $result = $results[$i][1];
      if( $grade != 0) {
        if($opponentGrade > $grade + 40)
          $opponentGrade = $grade + 40;
        if($opponentGrade < $grade - 40)
          $opponentGrade = $grade - 40;
      }
      if($result == 'D') {
        $score = $opponentGrade;
      } else {
        if($result == 'W')
          $score = $opponentGrade + 50;
        else
          $score = $opponentGrade - 50;
      }
      $total += $score;
    }
    $newGrade = round($total / $nresults, 1);
  }
  return "Current Grade = $grade, Estimated performance on these games = $newGrade. NOTE: This estimate is not from the grading system and should not be used as a grade!";
}
