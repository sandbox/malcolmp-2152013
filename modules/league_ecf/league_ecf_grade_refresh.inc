<?php
// $Id$

// Event admin tab
function league_ecf_grade_refresh_tab($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);

  $org = $node->nid;
  $content = array();

  // Form for specifying ECF grade refresh.
  $content['form'] = drupal_get_form('league_ecf_grade_refresh_form', $node);

  return $content;
}

/**
 *  Form for admin tab.
 */
function league_ecf_grade_refresh_form($form, &$form_state, $node) {

  $form_state['league_node'] = $node;

  $form['grades'] = array(
    '#title' => t('Zero Local Ratings'),
    '#type'  => 'fieldset',
    '#weight'  => 100,
  );

  $grade_text = league_ecf_last_refresh_text($node);

  $form['grades']['text'] = array(
   '#type'  => 'markup',
   '#markup'  => $grade_text,
  );

  $form['grades']['refresh'] = array(
    '#value' => t('Set Local Ratings'),
    '#type'  => 'submit',
    '#submit' => array('league_ecf_event_grades_form_submit'),
    '#validate' => array('league_ecf_event_grades_form_validate'),
  );

  // Which ones to set
  $typeOptions = array(
   'all' => t('All players'),
   'rated' => t('All rated players'),
   'notp' => t('All players with rating not category P'),
  );
  $form['grades']['types'] = array(
   '#title'  => t('Ratings to update'),
   '#type'  => 'radios',
   '#options'  => $typeOptions,
   '#required'  => TRUE,
  );

  // What to set them to
  $typeOptions = array(
   'standard' => t('Zero the standard rating'),
   'rapid' => t('Zero the rapid rating'),
   'list' => t('Set to rating from the list'),
  );
  $form['grades']['what'] = array(
   '#title'  => t('What to set the ratings to'),
   '#type'  => 'radios',
   '#required'  => TRUE,
   '#options'  => $typeOptions,
  );

  // Rating list to set local values from.
  $lists = rating_list_list('None');
  $form['grades']['rlid'] = array(
   '#title' => t('Rating list to use'),
   '#type' => 'select',
   '#options' => $lists,
   '#default_value'  => 0,
   '#description'  => t('Set local ratings to this list or zero'),
  );

  $status_text = league_ecf_status_text($node);
  $form['status'] = array(
   '#type'  => 'markup',
   '#markup'  => $status_text,
   '#weight'  => 300,
  );

  return $form;
}

function league_ecf_event_grades_form_submit($form, &$form_state) {
  $node = $form_state['league_node'];
  $type = $form_state['values']['types'];
  $rlid = $form_state['values']['rlid'];
  $what = $form_state['values']['what'];
  $org = $node->nid;
  if (!league_check_access($org)) {
    drupal_set_message('You do not have access to do this','warning');
    return;
  }

  league_ecf_refresh_org_grades_local($org, $type, $rlid, $what);
  drupal_set_message("Local grades refreshed for $node->title ");

  // Clear field cache to changes are seen when view/edit league_player
  cache_clear_all("*", 'cache_field', TRUE);
}

function league_ecf_event_grades_form_validate($form, &$form_state) {
  $type = $form_state['values']['types'];
  $rlid = $form_state['values']['rlid'];
  $what = $form_state['values']['what'];
  if( $rlid == 0 && $what == 'list' ) {
    form_set_error('rlid', 'Must select a rating list');
  }
}

function league_ecf_refresh_org_grades_local($org, $type, $rlid, $what) {
  // Build the SQL 
  $sql = "update league_player p ";
  // If there is a rating list selected that can reset conditional on
  // rating and category
  if( $rlid > 0 ) {
    $sql .= "join rating_player rpa ON rpa.pid=p.ecf_code AND rpa.rlid=$rlid";
    // Rated players
    if($type == 'rated') {
      $sql .= " and rpa.rating!=0 ";
    } else {
      if( $type == 'all' ) {
      // All players
        $sql .= " ";
      } else {
      // Players without a CAT P rating
        $sql .= " and rpa.cat != 'P' ";
      }
    }
  }
  // Determine which rating to reset: rapid or standard.
  $field = 'p.chess_rating';
  if( $rlid > 0 ) {
    $list = rating_list_load($rlid);
    if( $list->rate == 'R' ) {
      $field = 'p.chess_rapid';
    }
  } else {
    if( $what == 'rapid' ) {
      $field = 'p.chess_rapid';
    }
  }
  // Determine what to set it to: zero or a value from the rating list.
  $sql .= "set $field=";
  if( $what == 'list' ) {
    $sql .= "rpa.rating ";
  } else {
    $sql .= "0 ";
  }
  $sql .= "WHERE p.org=$org ";
//if( $rlid > 0 ) {
//  $sql .= " AND $field>0";
//}
  // Update local grades
//drupal_set_message("$sql");
  $result = db_query($sql, array() );
  $rows = $result->rowCount();
  drupal_set_message("$rows grades updated");
  league_ecf_set_last_refresh($org);
}
