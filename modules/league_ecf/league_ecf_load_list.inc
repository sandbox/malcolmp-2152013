<?php
// $Id$
/**
 * @file
 * Functions for loading player list from file.
 */

// Library for reading XL files
//$exlib = libraries_get_path('vendor');
//require_once "$exlib/autoload.php";
use \PhpOffice\PhpSpreadsheet\Spreadsheet;
// use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use \PhpOffice\PhpSpreadsheet\Style\NumberFormat;

function league_ecf_load_player_list_page($list) {
  $lid = $list->lid;

  return drupal_get_form('league_ecf_load_list_form', $list);
}

/**
 * Form for specifying player list file to load.
 */
function league_ecf_load_list_form($form, &$form_state, $list) {

  $form['list'] = array(
      '#type' => 'value',
      '#value' => $list,    // player list
  );

  // Required to do file uploads
  $form['#attributes'] = array('enctype' => "multipart/form-data");

  // File upload button
  $form['listfile'] = array(
    '#type' => 'file',
    '#title' => t('Player list file to upload'),
    '#description' => t('A file in the Excel format used to submit an event for ECF grading including a Player_List tab. The first 3 columns in the player list tab must be PIN, BCFCode, Name. Such a file can be created in the Event/Grading tab by generating an Excel Grading file. This feature was used to grade a congress which had been done with manual pariing cards. The PIN appears in the dropdown when selecting pairings.'),
    '#size' => 64,
  );

  // Submit Button
  $form['submit'] = array(
    '#value' => t('Load Player List File'),
    '#type' => 'submit',
  );

  return $form;
}

/**
 *  Action after the button to load a player list file is pressed.
 */

function league_ecf_load_list_form_submit($form, &$form_state) {

  $list = $form_state['values']['list'];

  // Limit to files with the .xls suffix
  $validators = array(
   'file_validate_extensions' => array('xls XLS'),
   'file_validate_size' => array(2000000, 0),
  );

  // Create league files subdirectory if it doesn't exist
  $path = 'public://league';
  if (!file_prepare_directory($path, FILE_CREATE_DIRECTORY)) {
    drupal_set_message(t('Could not create file @file', array('@file' => $path)), 'error');
    return;
  }

  // Upload the file
  if ($listfile = file_save_upload('listfile', $validators, $path, FILE_EXISTS_REPLACE)) {
    $filename = $listfile->destination;
    drupal_set_message(t('Uploaded @file', array('@file' => $filename)));
    $module_file = drupal_get_path('module', 'league_ecf') .'/league_ecf_load_list.inc';
    $operations = array(
        array('league_ecf_load_list_file', array($filename,$list) )
    );

    $batch = array(
        'operations' => $operations,
        'finished' => 'league_ecf_load_list_finished',
        'title'    => 'Loading ECF Player List File',
        'init_message' => 'Load Starting ...',
        'file' => $module_file,
    );
    batch_set($batch);
    // Clear players with no grade cache
    $org = $list->org;
    $cache_name = "league_ecf_count_no_grade_players_$org";
    cache_clear_all($cache_name, 'cache');
  } else {
    drupal_set_message(t('File Upload failed'));
  }

}

function league_ecf_load_list_file($filename, $list, &$context) {
  watchdog('league_ecf',"Loading Player List file $filename");
  if(!isset($context['sandbox']['current'])) {
    $context['sandbox']['current'] = 2;
    $context['sandbox']['endrec'] = 1;
    $header = league_ecf_load_list_header($filename);
    $context['sandbox']['nrecords'] = $header['nrecords'];
    $context['sandbox']['filename'] = $header['filename'];
    // Read records in batches of chunk
    $context['sandbox']['chunk'] = 600;
    $context['sandbox']['list'] = $list;
    watchdog('league_ecf','Number of records ' . $header['nrecords'] );
  }

  $current = $context['sandbox']['current'];
  $nrecords = $context['sandbox']['nrecords'];
  $current = league_ecf_load_file_batch($context);
  $context['sandbox']['current'] = $current;
}

//
// Define a Read Filter class implementing PHPExcel_Reader_IReadFilter
//
class chunkReadFilter implements \PhpOffice\PhpSpreadsheet\Reader\IReadFilter
{
  private $_startRow = 0;
  private $_endRow = 0;

  public function setRows($startRow, $chunkSize) {
    $this->_startRow = $startRow;
    $this->_endRow = $startRow + $chunkSize;
  }

  public function readCell(string $column, int $row, string $worksheetName = ''): bool {
    // Only read the heading row, and the configured rows
    if (($row == 1) ||
        ($row >= $this->_startRow && $row < $this->_endRow)) {
      return true;
    }
    return false;
  }
}

function league_ecf_load_list_header($filename) {
  $realname = drupal_realpath($filename);

  // Just estimate the number of rows from the file size

  $size = filesize($realname);
  $nrecords = (int)$size/168;

  // Check its a spreadsheet with correct number of columns


  $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xls');
  $objReader->setReadDataOnly(true);

  // Create a new Instance of our Read Filter
  $chunkFilter = new chunkReadFilter();
  // Tell the Reader that we want to use the Read Filter
  $objReader->setReadFilter($chunkFilter);

  $startRow = 0;
  $chunkSize = 1;
  $chunkFilter->setRows($startRow,$chunkSize);

  $objPHPExcel = $objReader->load("$realname");
  $objPHPExcel->setActiveSheetIndexByName('Player_List');
  $objWorksheet = $objPHPExcel->getActiveSheet();

  $highestRow = $objWorksheet->getHighestRow(); 
  $highestColumn = $objWorksheet->getHighestColumn(); 

  $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);

  $row = 1;
  $rowData = $objWorksheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                    NULL,
                                    TRUE,
                                    FALSE);
  $file_header = array(
    'nrecords' => $nrecords,
    'filename' => $realname,
  );

  return $file_header;
}

function league_ecf_load_file_batch(&$context) {
  $sandbox = $context['sandbox'];
  $nrecords = $sandbox['nrecords'];
  $chunk = $sandbox['chunk'];
  $current = $sandbox['current'];
  $filename = $sandbox['filename'];
  $list = $sandbox['list'];

  // Open spreadsheet

  $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xls');
  $objReader->setReadDataOnly(true);

  // Create a new Instance of our Read Filter
  $chunkFilter = new chunkReadFilter();
  // Tell the Reader that we want to use the Read Filter
  $objReader->setReadFilter($chunkFilter);

  $startRow = $current;
  $chunkSize = $chunk;
  $chunkFilter->setRows($startRow,$chunkSize);

  // Read next chunk
  $objPHPExcel = $objReader->load("$filename");
  $objPHPExcel->setActiveSheetIndexByName('Player_List');
  $objWorksheet = $objPHPExcel->getActiveSheet();

  $highestRow = $objWorksheet->getHighestRow(); 
  $highestColumn = $objWorksheet->getHighestColumn(); 

  $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);
  
  $bcount=0;
  $query=0;
  // Loop round the records , and update the database record
  for($row = $current; $row<= $highestRow; $row++) {
    $dataRange = $objWorksheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                    NULL,
                                    TRUE,
                                    FALSE);
    $rowData = $dataRange[0];

    $PIN = intval($rowData[0]);
    $BCFCode = $rowData[1];
    $NAME  = $rowData[2];
    $Gender  = $rowData[3];
    $DOB  = $rowData[4];
    $ClubCode  = $rowData[5];

    $org = $list->org;
    $lid = $list->lid;
//  drupal_set_message(" List PIN $PIN BCFCode $BCFCode NAME $NAME Gender $Gender DOB $DOB ClubCode $ClubCode org $org lid $lid");

    $pid = 0;
    if(isset($BCFCode) && strlen($BCFCode) > 3 ) {
      $pid = league_chess_player_for_ecf_code($org, $BCFCode);
    }
    if( $pid == 0 ) {
      $player = entity_get_controller('league_player')->create($org);
      $names = ecfdata_splitName($NAME);
      $player->firstname = $names['firstname'];
      $player->lastname = $names['surname'];
      // If the first name isn't set then use the name from the 
      // ECF list (because probably its not got a comma )
      if(!isset($player->firstname) || strlen($player->firstname)==0 ) {
        $ecfplayer = ecfdata_get_player($BCFCode);
        if(isset($ecfplayer['ecfCode']) ) {
          $player->firstname = $ecfplayer['firstname'];
          $player->lastname = $ecfplayer['surname'];
        }
      }
      if(strlen($player->lastname)==0 || strlen($player->firstname)==0 ) {
        drupal_set_message("Ignored new player $pid CODE $BCFCode because name was blank");
      } else {
        $player->ecf_code = $BCFCode;
        // Add player
        league_player_save($player);
        $pid = $player->pid;
        drupal_set_message("Added new player $pid CODE $BCFCode NAME $NAME name " . $player->firstname . " " . $player->lastname);
      }
    }
    if( !league_player_in_list($pid, $lid) ) {
      drupal_set_message("Player $pid not in $lid, adding ... ");
      league_player_add_to_list($pid, $lid, $PIN);
    }

  }

  $nrecords = max($highestRow, $nrecords);
  $endrec = $context['sandbox']['endrec'];
  if( $highestRow > $endrec ) {
    $context['sandbox']['endrec'] = $highestRow ;
  }
  $endrec = $context['sandbox']['endrec'];

  // set finished flag in sandbox
  if( $current >= $highestRow ) {
    $context['finished'] = 1;
    $context['message'] = "Finished reading $highestRow";
    drupal_set_message("Read $endrec records");
  } else {
    $context['finished'] = $current / $nrecords;
    $context['message'] = "Read $current records from about $nrecords";
  }

  $current += $chunk;
  return $current;
}

function league_ecf_load_list_finished($success,$results,$operations) {
  watchdog('league_ecf', "league_ecf_load_list_finished");
  if( $success ) {
    watchdog('league_ecf', "read list succeeded");
    drupal_set_message("File Loaded Successfully");
  } else {
    watchdog('ecfdata', "read list failed");
    reset($operations);
  }
}
