<?php
/**
* @file
* Functions for league player options.
*/

function league_ecf_add_to_list($list, $ecfcode, $nid=0) {
  $lid = $list->lid;
  $org = $list->org;
  if(league_ecf_add_player_to_list($list, $ecfcode) ) {
    drupal_set_message(t("Player $ecfcode added to list"));
    // Clear players with no grade cache
    $cache_name = "league_chess_count_no_grade_players_$org";
    cache_clear_all($cache_name, 'cache');
  }
  // Return to a different place if we came from a player list on a node tab
  if(isset($_SESSION['league_player_list_url'])) {
    $url = $_SESSION['league_player_list_url'];
    drupal_goto($url);
  } else {

    if($nid == 0) {
      drupal_goto('league_player_list/' . $list->lid);
    } else {
      drupal_goto("league/club/$nid/$org/plist");
    }
  }
}

function league_ecf_add_player_to_list($list, $ecfcode) {
  $org = $list->org;
  // Check if player in list already
  $pid = league_chess_player_in_list($ecfcode, $list->lid);
  if($pid !=0) {
    drupal_set_message("A player with ECF Code $ecfcode already exists in the player list");
    return FALSE;
  }
  // If no existing player in the org with that ECF code, then create.
  $pid = league_chess_player_for_ecf_code($org, $ecfcode);
  if($pid == 0) {
    $ecfPlayer = ecfdata_get_player($ecfcode);
    $player = entity_get_controller('league_player')->create($org);
    $player->firstname = $ecfPlayer['firstname'];
    $player->lastname = $ecfPlayer['lastname'];
    $dob = $ecfPlayer['dob'];
    if(isset($dob)) {
      $player->dob = $ecfPlayer['dob'];
    } else {
      $player->dob = null;
    }
    $player->sex = $ecfPlayer['sex'];
    $player->ecf_code = $ecfPlayer['ecfCode'];
    $player->chess_fidecode = $ecfPlayer['fidecode'];
    $player->ecf_member = $ecfPlayer['mid'];
  
    // Set club by looking for a club to which this list belongs
    $club = league_chess_club_for_list($list);
    if($club != 0) {
      $player->club = $club;
      $club_code = league_chess_club_value($club, 'ecfcode');
      $player->chess_club = $club_code;
    } else {
      $player->chess_club = $ecfPlayer['clubCode'];
    }

    // Save player
    league_player_save($player);
    $pid = $player->pid;
  }

  // Add player to list
  league_player_add_to_list($pid, $list->lid);
  return TRUE;
}
