<?php
/**
 * @file
 * Functions for theming the View Grading Data button under grading
 */

function theme_league_ecf_grading($variables) {
  $element = $variables['element'];
  $data = $element['#data'];

  $header = $data['header'];
  $players = $data['players'];
  $results = $data['results'];

  $output = "<h3>Grading Results</h3>";
  $output .= league_ecf_grade_array2table1($header);
  $output .= "<h3>Player List</h3>";
  if (count($players) > 0) {
    $output .= league_ecf_grade_array2table2($players);
  }
  else {
    $output .= "No Player Data Found";
  }
  $output .= "<h3>Results List</h3>";
  if (count($results) > 0) {
    $output .= league_ecf_grade_array2table2($results);
  }
  else {
    $output .= "No Results Data Found";
  }
  return $output;
}

// Function to output an array as HTML table

function league_ecf_grade_array2table1($array) {
  $output = "<table>";
  foreach ($array as $item => $value) {
    $output .= "<tr><th>" . $item . "</th><td>" . $value . "</td></tr>\n";
  }
  $output .= "</table>";
  return $output;
}

// Function to output a 2d array as HTML table

function league_ecf_grade_array2table2($array) {
  $output = "<table><tr>";
  $headings = current($array);
  foreach ($headings as $heading => $value) {
    $output .= "<th>" . $heading . "</th>";
  }
  $output .= "</tr>\n";

  foreach ($array as $row) {
    $output .= "<tr>";
    foreach ($row as $item => $value) {
      $output .= "<td>" . $value . "</td>";
    }
    $output .= "</tr>\n";
  }
  $output .= "</table>";
  return $output;
}

