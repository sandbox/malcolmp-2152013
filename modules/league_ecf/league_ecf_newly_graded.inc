<?php
/**
 * @file
 * Games played by none ecf members players
 */

/**
 * none ecf members player games
 */
function league_ecf_reports_tab_newly($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);
  // Set title
  $title = $node->title . ' - Newly Graded Players';
  drupal_set_title($title);

  $link = l('this option', 'node/' . $node->nid . '/league/admin/ecfgrades');
  $content['header']['#markup'] = "To update the grades of these players use $link <p>";
  $content['header']['#type'] = 'markup';

  $content['newly']['#table'] = league_ecf_newly_graded_players($node->nid);
  $content['newly']['#theme'] = 'league_table';

  // Ensure if a player is picked on, the Back to list tab works.
  $_SESSION['league_player_list_url'] = $_GET['q'];

  return $content;
}


// List players that refresh_org_grade_new will update.

function league_ecf_newly_graded_players($org) {
  $rlida = ecfdata_get_rating_list_id('standard_live');
  $rlido = ecfdata_get_rating_list_id('standard_rating');
  $result = db_query("select p.ecf_code AS ecfcode, p.firstname, p.lastname, rpo.rating AS GRADE1, rpa.cat AS CAT, rpa.rating AS GRADE, p.pid FROM league_player p INNER JOIN rating_player rpa ON rpa.rlid=$rlida AND rpa.pid=p.ecf_code AND rpa.cat!='*' AND rpa.rating!=0 INNER JOIN rating_player rpo ON rpo.rlid=$rlido AND rpo.pid=p.ecf_code and rpo.cat!='*' and rpo.rating=0 WHERE p.org=$org", array() );

  $header = array('ECF Code', 'Name', 'Old ECF Grade', 'Cat', 'New ECF Grade');
  $rows = array();

  foreach ( $result as $row) {
    $ecfcode = $row->ecfcode;
    $name = l($row->firstname . ' ' . $row->lastname,"league_player/$row->pid");
    $old_grade = $row->GRADE1;
    $cat = $row->CAT;
    $new_grade = $row->GRADE;
    $rows[] = array($ecfcode, $name, $old_grade, $cat, $new_grade);
  }
  $table['data'] = $rows;
  $table['header'] = $header;
  return $table;
}

