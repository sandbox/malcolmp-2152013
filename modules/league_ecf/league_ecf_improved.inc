<?php
/**
 * @file
 * Most Improved Players.
 */

/**
 * Most Improved Players.
 */
function league_ecf_reports_tab_improved($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);
  // Set title
  $title = $node->title . ' - Most Improved Players';
  drupal_set_title($title);

  $content['header']['#markup'] = "This report lists those players whose standard play September rating has improved most since the previous year<p>";
  $content['header']['#type'] = 'markup';

  $content['improved']['#table'] = league_ecf_table_improved($node);
  $content['improved']['#theme'] = 'league_table';

  return $content;
}

/**
 * table of Most Improved Players ( between September and January )
 */
function league_ecf_table_improved($node) {
  $org = $node->nid;
  // September rating.
  $rlida = ecfdata_get_rating_list_id('standard_rating');
  // Previous year rating.
  $rlido = ecfdata_get_rating_list_id('standard_sep_old');

  $header = array('Grade Improvement', 'Name', 'Club', 'Grade', 'Previous Grade');
  $rows = array();
  $result = db_query("select distinct rpo.rating-rpa.rating AS diff, p.firstname, p.lastname, lp.name AS CLUB, rpa.rating as GRADE, rpo.rating as GRADE1 from league_player p INNER JOIN league_player_player lpp ON lpp.pid=p.pid INNER JOIN league_player_list lp ON lp.lid=lpp.lid INNER JOIN league_club_org fpl ON fpl.list=lp.lid AND fpl.status='A' INNER JOIN rating_player rpa ON rpa.rlid= :rlida AND rpa.pid=p.ecf_code INNER JOIN rating_player rpo ON rpo.rlid= :rlido AND rpo.pid=p.ecf_code where p.org= :org AND rpo.rating<>0 AND rpa.cat!='*' AND rpo.cat!='*' ORDER BY diff LIMIT 20", array(':org' => $org, ':rlida' => $rlida, ':rlido' => $rlido));

  foreach ( $result as $row) {
    $diff = -($row->diff);
    $name = $row->firstname . ' ' . $row->lastname;
    $club = $row->CLUB;
    $grade = $row->GRADE;
    $grade1 = $row->GRADE1;

    $rows[] = array($diff, $name, $club, $grade, $grade1);
  }
  $table['data'] = $rows;
  $table['header'] = $header;
  return $table;
}


