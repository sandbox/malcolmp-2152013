<?php

/**
 * @file
 * Functions for league_chess new player matches option.
 */
  
function league_ecf_reports_dupe_players_tab ($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);
  // Set title
  $title = $node->title . ' - Duplicate Players';
  drupal_set_title($title);

  $content['title']['#markup'] = "<h3>These players have two LMS records in the organisation but the same ECF rating code. Pick the appropriate Keep button for the one you want to keep and it will take you to a confirm screen to merge the records, so don't worry if you aren't sure yet which to keep. You can also pick on the player and use your browser back button to get back here to investiage which to keep. Also it doesn't matter unless there is some differnce between the player details</h3>";
  $content['title']['#type'] = 'markup';

  $content['dupes']['#table'] = league_ecf_dupe_players($node);
  $content['dupes']['#theme'] = 'league_table';
  return $content;
}

function league_ecf_dupe_players_query($org) {

  // Results with New Player
  $query = db_select('league_player', 'lp1');
  $query->innerJoin('league_player', 'lp2', 'lp1.ecf_code = lp2.ecf_code AND lp1.ecf_code != 0 AND lp1.pid < lp2.pid');
  $query->condition('lp1.org', $org);
  $query->condition('lp2.org', $org);
  $query->addField('lp1', 'pid', 'pid1');
  $query->addField('lp2', 'pid', 'pid2');
  $query->addField('lp1', 'ecf_code', 'ecf_code');
  $query->addField('lp1', 'firstname', 'firstname1');
  $query->addField('lp1', 'lastname', 'lastname1');
  $query->addField('lp2', 'firstname', 'firstname2');
  $query->addField('lp2', 'lastname', 'lastname2');
  $query->orderBy('lastname1','ASC');
//drupal_set_message(print_r((string)$query, TRUE));
 
  return $query;
}

function league_ecf_dupe_players($node) {

  $org = $node->nid;
  $url_base = "node/$org/league/newplayers/confirm";
  $query = league_ecf_dupe_players_query($org);

  $header = array('Rating Code', 'Player 1', 'Player 2' );
  $rows = array();

  $options = array (
    'attributes' => array (
       'class' => array('button', 'form-submit'),
       'title' => 'Keep',
    ),
    'html' => TRUE,
  );

  $result = $query->execute();

  foreach( $result as $row) {
    $ecf_code = $row->ecf_code;
    $pid1 = $row->pid1;
    $pid2 = $row->pid2;
    $firstname1 = $row->firstname1;
    $lastname1 = $row->lastname1;
    $firstname2 = $row->firstname2;
    $lastname2 = $row->lastname2;
    $url1 = "$url_base/$pid2/$ecf_code/dupe";
    $url2 = "$url_base/$pid1/$ecf_code/dupe";
    $name1 = "$firstname1 $lastname1";
    $name2 = "$firstname2 $lastname2";
    $link1 = l($name1, "league_player/$pid1");
    $link2 = l($name2, "league_player/$pid2");
    $button1 = l('Keep 1', $url1, $options);
    $button2 = l('Keep 2', $url2, $options);
    $col1 = $button1 . ' ' . $link1;
    $col2 = $button2 . ' ' . $link2;
    $rows[] = array(ecfdata_add_check_digit($ecf_code), $col1, $col2);
  }

  $table['data'] = $rows;
  $table['header'] = $header;
  return $table;
}

function league_ecf_count_dupe_players($org) {
  $cache_name = "league_ecf_count_dupe_players_$org";
  $num_rows = &drupal_static($cache_name);
  if (!isset($num_rows)) {
    if( $cache = cache_get($cache_name)) {
      $num_rows = $cache->data;
    } else {
      $query = $query = league_ecf_dupe_players_query($org);
      //dsm((string)$query);
      $num_rows = $query->countQuery()->execute()->fetchField();
      cache_set($cache_name, $num_rows, 'cache');
    }
  }
  return $num_rows;
}
