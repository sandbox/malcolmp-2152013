<?php
// $Id$

// Class for sorting a league table

class LeagueTableSorter {
  public function __construct($sorts,$head2head) {
    $this->sort_functions = array();
    $this->head2head = $head2head;
    $this->head = FALSE;
    $sort_functions = array(
      'M' => 'compare_match',
      'G' => 'compare_game',
      'D' => 'compare_diff',
      'H' => 'compare_head',
      'T' => 'compare_order',
    );
    foreach($sorts as $key => $setting) {
      if($setting['enabled'] == 1) {
        $this->sort_functions[] = $sort_functions[$key];
        if($key == 'H') {
          $this->head = TRUE;
        }
      }
    }
  }
  // Check is head 2 head sort is active
  public function head() {
    return $this->head;
  }
  // Compare 2 teams according to match points (M)
  public function compare_match($fa, $fb) {
    if ( $fa['points'] > $fb['points'] ) {
      return -1;
    }
    if ( $fa['points'] < $fb['points'] ) {
      return 1;
    }
    return 0;
  }
  // Compare 2 teams according to game points (G)
  public function compare_game($fa, $fb) {
    if ( $fa['for'] > $fb['for'] ) {
      return -1;
    }
    if ( $fa['for'] < $fb['for'] ) {
      return 1;
    }
    return 0;
  }
  // Compare 2 teams according to points difference (D)
  public function compare_diff($fa, $fb) {
    if ( $fa['diff'] > $fb['diff'] ) {
      return -1;
    }
    if ( $fa['diff'] < $fb['diff'] ) {
      return 1;
    }
    return 0;
  }
  // Compare 2 teams according to head to head match (H)
  public function compare_head($fa, $fb) {
    if($this->head2head[$fa['id']][$fb['id']] > $this->head2head[$fb['id']][$fa['id']]) {
      return -1;
    }
    if($this->head2head[$fa['id']][$fb['id']] < $this->head2head[$fb['id']][$fa['id']]) {
      return 1;
    }
    return 0;
  }
  // Compare 2 teams according to team order field (T)
  public function compare_order($fa, $fb) {
    if ( $fa['order'] > $fb['order'] ) {
      return -1;
    }
    if ( $fa['order'] < $fb['order'] ) {
      return 1;
    }
    return 0;
  }
  public function compare($fa, $fb) {
    $val = 0;
    foreach($this->sort_functions as $func) {
      $val = call_user_func(array($this,$func), $fa, $fb);
      if($val != 0) {
        break;
      }
    }
    return $val;
  }
  public function sort(&$data) {
    uasort( $data, array($this, "compare") );
  }
}
  

/**
 * League table tab for an organisation.
 * NOTE: Individual events don't appear here because supplied by league_chess module.
 */
function league_table_org_tab($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);

  $content = array();
  $seasons = league_events_for_org($node->nid);
  foreach ( $seasons as $eid => $events ) {
    foreach($events['events'] as $nid => $event) {
      $eventNode = node_load($nid);
      if($event['type'] == 'L' || $event['type'] == 'J' || $event['type'] == 'K') {
        $content['title'.$nid]['#markup'] = '<h3>' . $eventNode->title . '</h3>';
        $content['title'.$nid]['#type'] = 'markup';
      }
      switch($event['type']) {
        case 'L' : $data = league_event_league_table($eventNode);
                   $content[$nid]['#table'] = $data['table'];
                   $content[$nid]['#theme'] = 'league_table';
                   $content['key'.$nid]['#markup'] = '<div>' . $data['text'] . '</div>';
                   $content['key'.$nid]['#type'] = 'markup';
                   break;
        case 'J' : $content[$nid]['#data'] = league_get_knockout_data($eventNode);
                   $content[$nid]['#theme'] = 'league_knockout_table';
                   break;
        case 'K' : $content[$nid]['#data'] = league_get_knockout_data($eventNode);
                   $content[$nid]['#theme'] = 'league_knockout_table';
                   break;
        default  : break;
      }
    }
  }
  return $content;
}

/**
 * League table tab for an event node.
 */
function league_table_league_tab($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);
  // Set title
  drupal_set_title($node->title);
  // Get league table
  $data = league_event_league_table($node);
  $table = $data['table'];
  // Check if file to be create via Xl or PDF
  league_table_check_file($table);

  $content['ltable']['#table'] = $table;
  $content['ltable']['#theme'] = 'league_table';
  $content['ltable']['#pdfLink'] = 'link';
  $content['key']['#markup'] = '<div>' . $data['text'] . '</div>';
  $content['key']['#type'] = 'markup';

  return $content;
}

/**
 * League cross table tab for an event node.
 */
function league_ctable_league_tab($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);
  // Set title
  drupal_set_title($node->title);

  $content['ctable']['#data'] = league_cross_table_data($node);
  $content['ctable']['#theme'] = 'league_cross_table';

  return $content;
}

/**
 * Knockout table tab for an event node.
 */
function league_table_k_tab($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);
  // Set title
  drupal_set_title($node->title);

  $content['knockout']['#data'] = league_get_knockout_data($node);
  $content['knockout']['#theme'] = 'league_knockout_table';
  return $content;
}

/**
 * Knockout table tab for an event node.
 */
function league_table_j_tab($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);
  // Set title
  drupal_set_title($node->title);

  $content['knockout']['#data'] = league_get_knockout_data($node);
  $content['knockout']['#theme'] = 'league_knockout_table';
  return $content;
}

/**
 * Get the data to create a knockout table.
 */
function league_get_knockout_data($node) {
  $event = $node->nid;
  $teams = league_teams_in_event($event);
  $eventType = league_event_value($node, 'type');
  $scores = league_knockout_scores($node);
  $nrounds = count($scores);
//drupal_set_message("league_get_knockout_data $nrounds");
  $homeCounts[0] = 0;
  $awayCounts[0] = 0;
  $lastTeam = array();

  if ($nrounds >0) {
    if ($eventType == 'K') {
      for ($r = $nrounds -2; $r > -1; $r--) {
        league_knockout_round_sort($scores, $r);
      }
    }
    else {
      $nteams = count($teams);
      for ($r = $nrounds -1; $r > -1; $r--) {
        league_knockout_round_sort_table($scores, $r, $nteams);
      }
    }
    for ($r = 0; $r < $nrounds; $r++) {
      $homeCounts[$r] = 0;
      $awayCounts[$r] = 0;
      $lastTeam[$r] = 'A';
    }
  }

  $data = array(
    'scores' => $scores,
    'nrounds' => $nrounds,
    'homeCounts' => $homeCounts,
    'awayCounts' => $awayCounts,
    'teams' => $teams,
    'lastTeam' => $lastTeam,
    'title' => $node->title,
    'event' => $event,
  );
  return $data;

}

/**
 * Get the fixture scores for a knockout event.
 */
function league_knockout_scores($node) {
  $data = array();

  $counts = array();
  $result = league_fixtures_event_query($node);
  $rounds = array();    // virtual round mapping
  $current_round = 0;

  foreach ( $result as $row) {

    $winner = $row->winner;
    $homeTeam = $row->homeTeamNid;
    $awayTeam = $row->awayTeamNid;
    $homeScore = league_score2points($row->home_score);
    $awayScore = league_score2points($row->away_score);
    $round = $row->round;
    // Create a virtual round mapping in case there are fixtures for say
    // round 1 and round 3 which messes up otherwise.
    if(!isset($rounds[$round]) ) {
      $current_round++;
      $rounds[$round] = $current_round;
    }
//  drupal_set_message("knockout scores current $current_round round $round virtual " . $rounds[$round]);
    $round = $rounds[$round];
    $tableNo = $row->table_num;
    if(!isset($counts[$round]) ) {
      $counts[$round] = 0;
    }
    $counts[$round]++;
    $fcount = $counts[$round] -1;
    $fixture = $row->nid;
    $data[$round -1][$fcount]['homeTeam'] = $homeTeam;
    $data[$round -1][$fcount]['awayTeam'] = $awayTeam;
    $data[$round -1][$fcount]['fixture'] = $fixture;
    $data[$round -1][$fcount]['tableNo'] = $tableNo;
    $data[$round -1][$fcount]['winner'] = $winner;
    if ( $homeScore == 0 && $awayScore == 0) {
      $data[$round -1][$fcount]['homeScore'] = ' ';
      $data[$round -1][$fcount]['awayScore'] = ' ';
    }
    else {
      $data[$round -1][$fcount]['homeScore'] = $homeScore;
      $data[$round -1][$fcount]['awayScore'] = $awayScore;
    }
  }

  return $data;
}

/**
 * Add in a child fixture to make up earlier rounds in the table
 */
function league_knockout_create_child($id, &$data, $parent, $venue, $home, $away) {
  $next = count($data);
//  drupal_set_message("child id $id parent $parent venue $venue home $home away $away next $next ");
  $data[$next]['fixture'] = $id;
  $data[$next]['parent'] = $parent;
  $data[$next]['parentVenue'] = $venue;
  $data[$next]['homeTeam'] = $home;
  $data[$next]['awayTeam'] = $away;
}

/**
 * Sort a kncokout table to get the teams displayed in the corect order.
 */
function league_knockout_round_sort_table(&$data, $round, $nteams) {
  // Work out number of tables that we should have in this round
  // This is the power of 2 greater than the number of teams
  // (causes a problem if number of teams in event doesn't match
  //  number of fixtures in first round? )
  // There will be some byes in the first round if these numbers aren't equal
  $nloop = ($nteams / 2);
  $ntables = 2;
  while ( $ntables < $nloop ) {
    $ntables *= 2;
  }
  // There is half the number of fixtures in the table each round
  $ntables = $ntables / pow(2, $round);
//drupal_set_message("round $round nteams $nteams nloop $nloop ntables $ntables");
  // Work out which slots in the table don't have a fixture in
  $tables = array();
  foreach ($data[$round] as $f => $fixture) {
    $t = $fixture['tableNo'];
    $tables[$fixture['tableNo']] = $f;
  }
  // Add in dummy fixtures so that the table looks right
  static $newid = 0;
  for ($table = 1; $table <= $ntables; $table++) {
    if (!isset($tables[$table])) {
      $newid--;
      $next = count($data[$round]);
      $data[$round][$next]['tableNo'] = $table;
      $data[$round][$next]['fixture'] = $newid;
    }
  }
  // Sort by table no (postition in the table)
  usort( $data[$round], "league_knockout_tsort" );
}

/**
 * Sort function used above.
 */
function league_knockout_tsort($fa, $fb) {
  if ( $fa['tableNo'] > $fb['tableNo'] ) {
    return 1;
  }
  if ( $fa['tableNo'] < $fb['tableNo'] ) {
    return -1;
  }
  return 0;
}

/**
 * Sort the rounds so we can display the oldest round to the left
 */
function league_knockout_round_sort(&$data, $round) {
  // Set fixtures parents
  // (2 child fixtures in round 1 have a common parent in round 2)
  foreach ($data[$round] as $f => $fixture) {
    $data[$round][$f]['parent'] = -1;
    foreach ($data[$round + 1] as $pf => $pfixture) {
      if ( $pfixture['homeTeam'] != NULL) {
        if ( $fixture['homeTeam'] == $pfixture['homeTeam'] ||
            $fixture['awayTeam'] == $pfixture['homeTeam'] ) {
          // set the childs parent
          $data[$round][$f]['parent'] = $pf;
          // whoever won the child fixture is playing at home in the parent fixture
          $data[$round][$f]['parentVenue'] = 'H';
          // set the parents home child fixture
          $data[$round + 1][$pf]['homeChild'] = $fixture['fixture'];
        }
      } else {
        $data[$round + 1][$pf]['homeChild'] = NULL;
      }
      if ( $pfixture['awayTeam'] != NULL) {
        if ( $fixture['homeTeam'] == $pfixture['awayTeam'] ||
            $fixture['awayTeam'] == $pfixture['awayTeam'] ) {
          $data[$round][$f]['parent'] = $pf;
          // whoever won the child fixture is playing away in the parent fixture
          $data[$round][$f]['parentVenue'] = 'A';
          $data[$round + 1][$pf]['awayChild'] = $fixture['fixture'];
        }
      } else {
        $data[$round + 1][$pf]['awayChild'] = NULL;
      }
    }
  }
  static $newid = 0;
  $lastParentVenue = 'H';
  $lastParent = -1;
  // Create parents for orphans
  foreach ($data[$round] as $f => $fixture) {
    if( $data[$round][$f]['parent'] == -1) {
//    drupal_set_message("Fixture $f has no parent");
      if($lastParent == -1) {
        $lastParent = count($data[$round+1]);
        $data[$round][$f]['parent'] = $lastParent;
        $newid--;
        league_knockout_create_child($newid, $data[$round+1], -2, $lastParentVenue, 0, 0);
 //     drupal_set_message("Round $round Setting child of $lastParent to $f");
        $data[$round + 1][$lastParent]['homeChild'] = $f;
        if($lastParentVenue == 'H') {
          $lastParentVenue = 'A';
        } else {
          $lastParentVenue = 'H';
        }
      } else {
        $data[$round][$f]['parent'] = $lastParent;
//      drupal_set_message("Round $round Setting child of $lastParent to $f");
        $data[$round + 1][$lastParent]['awayChild'] = $f;
        $lastParent = -1;
      }
    }
  }
  // Create child fixtures for childless parents
  reset($data[$round + 1]);
//drupal_set_message("Round $round, count " . count($data[$round+1]));
  foreach ($data[$round + 1] as $pf => $pfixture) {
    $fixt = $pf;
//drupal_set_message("round $round pf $pf home $home away $away");
    if (isset($data[$round + 1][$pf]['homeChild'])) {
      // If no away child fixture, create it
      if (!isset($data[$round + 1][$pf]['awayChild'])) {
        $newid--;
        league_knockout_create_child($newid, $data[$round], $fixt, 'A', 0, 0);
//    }
 //   else {
 //    drupal_set_message("Both set $home $away");
      }
      // If no home child fixture, create it
    }
    else {
      if (isset($data[$round + 1][$pf]['awayChild'])) {
        $newid--;
        league_knockout_create_child($newid, $data[$round], $fixt, 'H', 0, 0);
        // If neither child fixture exists, create it
      }
      else {
        $newid--;
        league_knockout_create_child($newid, $data[$round], $fixt, 'H', $data[$round + 1][$pf]['homeTeam'], 0);
        $newid--;
        league_knockout_create_child($newid, $data[$round], $fixt, 'A', $data[$round + 1][$pf]['awayTeam'], 0);
      }
    }

  }

  // Sort by postition and Home / Away
  usort( $data[$round], "league_knockout_fsort" );
}

function league_knockout_fsort($fa, $fb) {
  if ( $fa['parent'] > $fb['parent'] ) {
    return 1;
  }
  if ( $fa['parent'] < $fb['parent'] ) {
    return -1;
  }
  if ( isset($fa['parentVenue']) ) {
    if ( $fa['parentVenue'] == "H" ) {
      return -1;
    }
    if ( $fa['parentVenue'] == "A" ) {
      return 1;
    }
  }
  return 0;
}

function league_event_league_table($node) {
  $title = $node->title;
  $text = '';
  // Standard columns
  $header = array(
    'Team',
    'Play',
    'Won',
    'Draw',
    'Lost',
  );
  // Configurable columns
  $cols = str_split($node->league_cols);
  foreach($cols as $col) {
    if($col != '0') {
      $header[] = league_table_column_label($col);
      switch($col) {
        case 'S' : $text .= 'SP = Season Penalties, ';
                   break;
        case 'M' : $text .= 'MP = Match Penalties, ';
                   break;
        case 'I' : $text .= 'IM = Incomplete Matches, ';
                   break;
        case 'B' : $text .= 'Def = Defaulted Boards, ';
                   break;
      }
    }
  }

  $aggregate_score = array();

  // Initialise table with zeros for each team
  $hteams = array();
  $data = array();
  $teams = league_teams_in_event($node->nid);
  foreach( $teams as $team => $teamData ) {
    $data[$team]['points'] = 0.0;
    $data[$team]['won'] = 0.0;
    $data[$team]['lost'] = 0.0;
    $data[$team]['drawn'] = 0.0;
    $data[$team]['points'] = 0.0;
    $data[$team]['played'] = 0.0;
    $data[$team]['for'] = 0.0;
    $data[$team]['against'] = 0.0;
    $data[$team]['mp'] = 0.0;
    $data[$team]['im'] = 0.0;
    $data[$team]['name'] = $teamData[0];
    $data[$team]['order'] = $teamData[1];
    $data[$team]['defaults'] = 0;
    $hteams[$team] = 0;
//  drupal_set_message("Setting hteams $team $teamData[0] $teamData[1]");
  }

  $head2head = array();
  foreach( $teams as $team => $teamName ) {
    $head2head[$team] = $hteams;
  }

  // Get the fixture details
  $org = league_event_value($node, 'org');
  $twomatch = league_event_value($node, 'twomatch');
  $aggregate = ($twomatch == 'B');
  $unfinished = league_org_value($org, 'unfinished');
  $result = league_fixtures_event_query($node);
  $lastHomeScore = 0;
  $lastAwayScore = 0;
  $lastFid = 0;
  foreach ( $result as $row) {
    $homeTeam = $row->homeTeamNid;
    $awayTeam = $row->awayTeamNid;
    // Protect against fixtures with no teams
    // (which can happen if you set an event up as a knockout and change it)
    if(!isset($homeTeam) || !isset($awayTeam) ) {
      continue;
    }
    $homePenalty = 0;
    $awayPenalty = 0;
    // Get Penalty
    if(isset($row->home_penalty) ) {
      $homePenalty = $row->home_penalty;
    }
    if(isset($row->away_penalty) ) {
      $awayPenalty = $row->away_penalty;
    }
    $homeScore = $row->home_score;
    $awayScore = $row->away_score;
    $head2head[$homeTeam][$awayTeam] += $homeScore;
    $head2head[$awayTeam][$homeTeam] += $awayScore;
    $homeOrder = $row->homeOrder;
    $awayOrder = $row->awayOrder;
//  drupal_set_message("homeScore $homeScore awayScore $awayScore awayTeam $awayTeam homeTeam $homeTeam homePenalty $homePenalty awayPenalty $awayPenalty"); 
    if ( $homeScore > 0 || $awayScore > 0) {
      // Somehow league fixture teams get unset, so pick this up here.
      if(!isset($data[$homeTeam]) || !isset($data[$awayTeam]) ) {
        $elink = l('this fixture', 'league_fixture/' . $row->entity_id . '/edit');
        drupal_set_message("Error: team not set in $elink ", 'error');
        continue;
      }
      // Defaulted boards
      $data[$homeTeam]['defaults'] += $row->wdef;
      $data[$awayTeam]['defaults'] += $row->bdef;
      // 
      // (I) - Ignore unfinished games, just add up points as normal
      // (D) - Treat unfinished games as draws, so if not unfinished
      //       add up points as normal (Note this is done in the matchcard
      //       processing so no need to do anything here)
      // (T) - Use threshold method, so if not unfinished add up
      //       points as normal
      // (M) - If unfinished, don't add up any points
      $winner = $row->winner;
      if ( $unfinished == 'I' || $unfinished == 'D' ||
           ($unfinished == 'M' && $winner != 'N' ) ||
           ($unfinished == 'T' &&
            ( $winner == 'H' || $winner == 'A' || $winner == 'D' )
            )
         ) {
        if ( $homeScore == $awayScore ) {
          $data[$homeTeam]['points'] += 1;
          $data[$homeTeam]['drawn'] += 1;
          $data[$awayTeam]['points'] += 1;
          $data[$awayTeam]['drawn'] += 1;
        }
        else {
          if ( $homeScore > $awayScore ) {
            $data[$homeTeam]['points'] += 2;
            $data[$homeTeam]['won'] += 1;
            $data[$awayTeam]['lost'] += 1;
          }
          else {
            $data[$awayTeam]['points'] += 2;
            $data[$awayTeam]['won'] += 1;
            $data[$homeTeam]['lost'] += 1;
          }
        }
      } else {
        if ( $unfinished == 'T' ) {
          if ($winner == 'I' ) {
            $data[$homeTeam]['points'] += 1;
          }
          if ($winner == 'B' ) {
            $data[$awayTeam]['points'] += 1;
          }
        }
      }
      $data[$homeTeam]['played']++;
      $data[$awayTeam]['played']++;
      $data[$awayTeam]['for'] += $awayScore;
      $data[$awayTeam]['against'] += $homeScore;
      $data[$homeTeam]['for'] += $homeScore;
      $data[$homeTeam]['against'] += $awayScore;
      $data[$homeTeam]['mp'] += $homePenalty;
      $data[$awayTeam]['mp'] += $awayPenalty;
      $data[$homeTeam]['im'] += $row->incomp;
      $data[$awayTeam]['im'] += $row->incomp;
      // Team order (repeated for every team row)
      $data[$homeTeam]['order'] = intval($homeOrder);
      $data[$awayTeam]['order'] = intval($awayOrder);
      // Team winning on aggregate of home and away fixtures gets bonus
      // point (if that option is set)
      if($aggregate && $lastFid==$row->nid) {
        $aggregate_home = $lastHomeScore + $homeScore;
        $aggregate_away = $lastAwayScore + $awayScore;
        if( ($lastHomeScore + $homeScore) > ($lastAwayScore + $awayScore) ) {
            // Bonus to home team
            $data[$homeTeam]['points'] += 1;
        //  drupal_set_message("Bonus to home team");
        } else {
          if( ($lastHomeScore + $homeScore) < ($lastAwayScore + $awayScore) ) {
            // Bonus to away team
            $data[$awayTeam]['points'] += 1;
        //  drupal_set_message("Bonus to away team");
          }
        }
      }
      $lastHomeScore = $homeScore;
      $lastAwayScore = $awayScore;
    }

    $lastFid=$row->nid;
  }
  foreach ($data as $team => $values) {
    $data[$team]['order'] = $values['order'];
    $data[$team]['for'] = $values['for'];
    $data[$team]['against'] = $values['against'];
    $data[$team]['diff'] = $values['for'] - $values['against'];
    $data[$team]['id'] = $team;
    $data[$team]['penalty'] = league_team_value($team,'penalty');
    if($twomatch == 'M') {
      $data[$team]['points'] = $values['for'] + ($values['won'] * 2);
    } else {
      $data[$team]['points'] = $data[$team]['points'] - $data[$team]['penalty'];
    }
    $data[$team]['mp'] = league_score2points($values['mp']);
  }
  // Sort league table
  $sorts = league_get_competition_table_settings($node);
  $sorter = new LeagueTableSorter($sorts, $head2head);
  $sorter->sort($data);

  // Transfer into array for table display
  $rows = array();
  foreach ($data as $team => $values) {
    if (league_table_links()) {
      $name = l($values['name'], 'league_team/' . $values['id']);
    }
    else {
      $name = $values['name'];
    }
    if($twomatch == 'M') {
      $points = league_score2points($values['points']);
    } else {
      $points = $values['points'];
    }
    $row = array(
      $name,
      $values['played'],
      $values['won'],
      $values['drawn'],
      $values['lost'],
    );
    foreach($cols as $col) {
      if($col != '0') {
        switch($col) {
          case 'F' : $row[] = league_score2points($values['for']);
                     break;
          case 'A' : $row[] = league_score2points($values['against']);
                     break;
          case 'P' : $points_label = $points;
                     if (league_table_links()) {
                       $points_label = '<b>' . $points . '</b>';
                     }
                     $row[] = $points_label;
                     break;
          case 'S' : $row[] = $values['penalty'];
                     break;
          case 'M' : $row[] = $values['mp'];
                     break;
          case 'I' : $row[] = $values['im'];
                     break;
          case 'D' : $row[] = league_score2points($values['diff']);
                     break;
          case 'B' : $row[] = $values['defaults'];
                     break;
        }
      }
    }
    $rows[] = $row;
        
  }
  $table['header'] = $header;
  $table['data'] = $rows;
  $table['title'] = "League table for $title";


  $data = array('table' => $table, 'text' => $text);
  return $data;
}

function league_teams_in_event($event) {
  $teams = array();

  $query = db_select('field_data_field_event', 'f');
  $query->fields('f', array('entity_id'));
  $n = $query->innerJoin('node', 'n', '%alias.nid = f.entity_id');
  $query->addField($n, 'title', 'teamName');
  $query->condition('f.entity_type', 'node');
  $query->condition('f.bundle', 'team');
  $query->condition('field_event_nid', $event);
  $query->leftJoin('field_data_field_team_order', 'ord', 'ord.entity_id = f.entity_id');
  $query->addField('ord', 'field_team_order_value', 'team_order');

  $result = $query->execute();
  foreach ( $result as $row) {
    $teams[$row->entity_id] = array($row->teamName, $row->team_order);
  }
  return $teams;
}

//
// Get data for cross table
//
function league_cross_table_data($node) {

  $rows = array();
  $event = $node->nid;

  // Teams in the event
  $query = db_select('field_data_field_event', 't');
  $query->condition('t.entity_type', 'node');
  $query->condition('t.bundle', 'team');
  $query->condition('t.field_event_nid', $event);
  $query->leftJoin('node', 'hn', 'hn.nid = t.entity_id');
  $query->addField('hn', 'title', 'home');
  $query->addField('hn', 'nid', 'homenid');

  // Fixtures for the home team
  $query->leftJoin('league_fixture', 'f', 'f.home_team = t.entity_id');
  $query->addField('f', 'nid', 'fixture');
  $query->addField('f', 'date', 'date');

  // home team score
  $query->leftJoin('league_match', 'm', 'm.fid = f.nid');
  $query->addField('m', 'home_score', 'homeScore');
  // away team and score
  $query->leftJoin('node', 'an', 'an.nid = f.away_team');
  $query->addField('an', 'title', 'away');
  $query->addField('an', 'nid', 'awaynid');
  $query->addField('m', 'away_score', 'awayScore');

  $query->orderBy('homenid', 'ASC')->orderBy('awaynid', 'ASC');

  $result = $query->execute();

  foreach ( $result as $row) {
    $away =  $row->away;
    $home =  $row->home;

    if(!isset($rows[$row->homenid]['results'][$row->awaynid]) ) {
      $rows[$row->homenid]['results'][$row->awaynid] = array();
    }
    $homeScore = league_score2points($row->homeScore);
    $awayScore = league_score2points($row->awayScore);
    $score = "$homeScore - $awayScore";
    $match = array( 'score' => $score, 'fid' => $row->fixture );
    $rows[$row->homenid]['results'][$row->awaynid][] = $match;
    $rows[$row->homenid]['team'] = $home;
//  drupal_set_message(" score $score home $home away $away ");
  }
  return $rows;
}
