<?php
/**
* @file
* Functions for creating the event settings form.
*/

// Event Settings page.
function league_event_settings_tab($node) {
  // Set Breadcrumb
  league_set_bread_crumb ($node);
  // Set title
  drupal_set_title($node->title);

  return drupal_get_form('league_event_settings_form', $node);
}

// Event add page.
function league_event_settings_add($node) {
  // Set Breadcrumb
  league_set_bread_crumb ($node);
  $event = league_create_event($node->nid);

  return drupal_get_form('league_event_settings_form', $event);
}

// Event settings form
function league_event_settings_form($form, &$form_state,$node) {

  $form_state['league_node'] = $node;
  $org = league_event_value($node, 'org');
  $new = !isset($node->nid);

  // Javascript object for fields only applicable to some event types.
  $js = "var conditional_fields=new Array();\n";

  // Field Set
  $form['settings'] = array(
    '#type' => 'fieldset',
    '#weight' => 10,
    '#title'  => t('Event Settings'),
  );

  // Event Name
  $form['settings']['name'] = array(
   '#type'  => 'textfield',
   '#size'  => 20,
   '#title'  => t('Event Name'),
   '#default_value'  => league_event_value($node, 'title'),
   '#required' => true,
   '#weight' => 10,
  );

  // Event Type
  $form['settings']['type'] = array(
   '#title' => t('Event Type'),
   '#type' => 'select',
   '#options' => league_event_types(),
   '#default_value'  => league_event_value($node, 'type'),
   '#required' => true,
   '#weight' => 20,
  );

  // Season
  if($new) {
    $seasons = league_event_list_for_org($org, array('A', 'N'));
  } else {
    $seasons = league_event_list_for_org($org);
  }
  $form['settings']['season'] = array(
   '#title' => t('Season'),
   '#type' => 'select',
   '#options' => $seasons,
   '#default_value'  => league_event_value($node, 'season'),
   '#required' => true,
   '#weight' => 20,
  );

  // Event Order
  $form['settings']['order'] = array(
   '#type'  => 'textfield',
   '#size'  => 20,
   '#title'  => t('Event Order'),
   '#description'  => t('order listed on left hand side'),
   '#default_value'  => league_event_value($node, 'order'),
   '#weight' => 50,
   '#maxlength' => 3,
   '#required' => true,
  );

  // Time teams play
  $form['settings']['ntimes'] = array(
   '#type'  => 'textfield',
   '#size'  => 20,
   '#title'  => t('Times teams play'),
   '#description'  => t('Used in fixture generation for the number of time teams or individuals play each other'),
   '#default_value'  => league_event_value($node, 'ntimes'),
   '#weight' => 60,
   '#maxlength' => 1,
  );
  $js .= "conditional_fields.push({field:'.form-item-ntimes', types:['L', 'A']});\n";

  // Handicap
  $handicaps = league_handicaps();

  $handicap = league_event_value($node, 'handicap');
  $form['settings']['handicap'] = array(
   '#type'  => 'radios',
   '#options'  => $handicaps,
   '#title'  => t('Handicap'),
   '#default_value'  => $handicap,
   '#weight' => 70,
  );
  $js .= "conditional_fields.push({field:'.form-item-handicap', types:['L', 'J', 'K']});\n";

  // Special Fixture options

  $twomatches = league_two_match_options();

  $twomatch = league_event_value($node, 'twomatch');
  $form['settings']['twomatch'] = array(
   '#type'  => 'radios',
   '#options'  => $twomatches,
   '#title'  => t('Special Fixture Options'),
   '#default_value'  => $twomatch,
   '#weight' => 80,
  );
  $js .= "conditional_fields.push({field:'.form-item-twomatch', types:['L']});\n";

  // Display 
  $display = league_event_value($node, 'display');
  $form['settings']['display'] = array(
   '#type'  => 'textarea',
   '#cols'  => 60,
   '#rows'  => 5,
   '#title'  => t('Display Text'),
   '#default_value'  => $display,
   '#weight' => 90,
  );

  // League table sort order

  $form['sorts'] = array(
      '#title' => t('League Table Sort Order'),
      '#type' => 'fieldset',
      '#weight' => 100,
      '#attributes' => array('class' => array('competition-fieldset')),
      '#theme' => 'league_competition_table_edit',
      '#tree' => TRUE,
  );
  $js .= "conditional_fields.push({field:'.competition-fieldset', types:['L']});\n";

  $nid = 0;
  if(!$new) {
    $nid = $node->nid;
  }
  $settings = league_get_competition_table_settings($node, $new);
  $form_state['league_comp_sorts'] = array();
  foreach($settings as $setting) {
    $weight = $setting['weight'];
    $label = $setting['label'];
    $enabled = $setting['enabled'];
    $col = $setting['col'];
    $form_state['league_comp_sorts'][] = $col;
    $form['sorts']['COL' . $col]['label'] = array(
        '#type' => 'item',
        '#markup' => $label,
    );
    // This field is invisible, but contains sort info (weights).
    $form['sorts']['COL' . $col]['weight'] = array(
      '#type' => 'weight',
      '#title' => t('Weight'),
      '#title_display' => 'invisible',
      '#default_value' => $weight,
    );
    // Checkbox to enable
    $form['sorts']['COL' . $col]['enabled'] = array(
        '#type' => 'checkbox',
        '#default_value' => $enabled,
    );
  }

  // Field Set
  $form['columns'] = array(
    '#type' => 'fieldset',
    '#weight' => 150,
    '#attributes' => array('class' => array('columns-fieldset')),
    '#title'  => t('League Table Columns'),
  );

  // Columns
  $cols = league_table_column_explanations();
  $league_cols = $node->league_cols;
  while(strlen($league_cols) < count($cols) ) {
    $league_cols .= '0';
  }
  $colstring = str_split($league_cols);
  $default_cols = array();
  $c=0;
  foreach($cols as $col => $name) {
    $default_cols[$col] = $colstring[$c];
    $c++;
  }
  $form['columns']['cols'] = array(
    '#title' => t('Columns'),
    '#type' => 'checkboxes',
    '#options' => $cols,
    '#default_value' => $default_cols,
  );
  $js .= "conditional_fields.push({field:'.columns-fieldset', types:['L']});\n";

  // Buttons

  $form['buttons'] = array();
  $form['buttons']['#weight'] = 200;
  $form['buttons']['submit'] = array(
    '#value' => t('Save'),
    '#type' => 'submit',
    '#submit'  => array('league_event_settings_save'),
  );

  $form['buttons']['delete'] = array(
    '#value' => t('Delete'),
    '#type' => 'submit',
    '#submit'  => array('league_event_settings_delete'),
  );

  // Javascript

  drupal_add_js($js, 'inline');
  drupal_add_js(drupal_get_path('module', 'league').'/event_update.js');

  return $form;
}

/**
*  Validate event settings.
*/
function league_event_settings_form_validate($form, &$form_state) {
  $order = $form_state['values']['order'];
  $ntimes = $form_state['values']['ntimes'];
  $node = $form_state['league_node'];
  $new = !isset($node->nid);
  $type = $form_state['values']['type'];
  if(!$new && league_event_type_individual($type) != league_event_type_individual($node->league_type) ) {
    // Check that there are no fixtures
    $org = league_event_value($node, 'org');
    require_once ('league_fixture.inc');
    $table = league_fixtures_by_org($org, 'event', $node->nid);
    if(count($table['data']) > 0) {
      $message = "You can't change the event type from team to individual if fixtures already exist";
      form_set_error('type', $message);
    }
  }


  if(!ctype_digit($order) || $ntimes < 0 || $ntimes >999 ) {
    form_set_error('order', "Error: event order must be an integer");
  }
  if(!ctype_digit($ntimes) || $ntimes < 1 || $ntimes >5 ) {
    form_set_error('ntimes', "Error: number of times teams play must be greater than zero and less than 5");
  }
  $cols = $form_state['league_comp_sorts'];
  $sorts = FALSE;
  foreach($cols as $col) {
    $col_vals = $form_state['values']['sorts']['COL'. $col];
    $enabled = $col_vals['enabled'];
    if($enabled == 1) {
      $sorts = TRUE;
    }
  }
  if( !$sorts ) {
    form_set_error('sorts', "Error: League table sort order must have at least one sort");
  }
}

// Save changes made to event settings

function league_event_settings_save($form, &$form_state) {
  $node = $form_state['league_node'];
  $new = !isset($node->nid);
  $name = $form_state['values']['name'];
  $season = $form_state['values']['season'];
  $type = $form_state['values']['type'];
  $order = $form_state['values']['order'];
  $twomatch = $form_state['values']['twomatch'];
  $handicap = $form_state['values']['handicap'];
  $ntimes = $form_state['values']['ntimes'];
  $display = $form_state['values']['display'];
  $cols = $form_state['values']['cols'];
  $node->title = $name;
  $node->league_type = $type;
  $node->league_eid = $season;
  $node->league_event_order = $order;
  $node->league_handicap = $handicap;
  $node->league_two_matches = $twomatch;
  $node->league_ntimes = $ntimes;
  $cols = implode($cols);
  $node->league_cols = $cols;
  $node->body['und'][0]['value'] = $display;

  // Save league table and sort order
  league_event_form_table_handler($form, $form_state, $node, $new);
  node_save($node);
  drupal_set_message('Settings saved');
  // Moved to league_chess_event_player_list_save as that is called later
  // but should really be here, perhaps via a hook when event is an entity.
  // drupal_goto("node/$node->nid");
}

// Event Delete button

function league_event_settings_delete($form, &$form_state) {
  $node = $form_state['league_node'];
  $nid = $node->nid;
  drupal_goto("node/$nid/delete");
}

function league_event_get_owners($node) {
  $owners = $node->league_owners;
  return $owners;
}

// Submit handler on event node for saving the league table sort order

function league_event_form_table_handler($form, $form_state, $node, $new) {
  $cols = $form_state['league_comp_sorts'];
  $sorts = array();
  foreach($cols as $col) {
    $col_vals = $form_state['values']['sorts']['COL'. $col];
    $weight = $col_vals['weight'];
    $enabled = $col_vals['enabled'];
    $sorts[$col] = array( 'weight' => $weight, 'enabled' => $enabled );
  }
  uasort($sorts, 'drupal_sort_weight');
  $new_weight = 0;
  $new_sorts = array_fill(0,8,' ');
  $count=0;
  foreach($sorts as $key => $value ) {
    if( $value['enabled'] == 1 ) {
      $new_sorts[$count] = $key;
      $count++;
    }
  }
  // Convert to character string
  $new_sorts = implode($new_sorts);
  $node->league_sorts = $new_sorts;
}

// Update league table sort order

function league_competition_update_sorts($event, $sorts) {
  $query = db_update('league_competition');
  $query->condition('cid', intval($event));
  $query->fields(array('sorts' =>$sorts,));
  //dsm((string)$query);
  $rows = $query->execute();
}
