<?php
// $Id$

// Club admin tab
function league_admin_tab_clubs($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);

  $org = $node->nid;
  // Provide a form for adding clubs for authorised users
  if(league_check_access($org)) {
    $content['form'] = drupal_get_form('league_club_admin_form', $org);
  } else {
    $content['form']['#type'] = 'markup';
    $content['form']['#markup'] = '<h3>You do not have access to add clubs</h3>';
  }

  return $content;
}

/**
 *  Form for adding a new club.
 */
function league_club_admin_form($form, &$form_state, $org) {

  $form['org'] = array(
      '#type' => 'value',
      '#value' => $org,
  );

  $form['div']['#type'] = 'markup';
  $form['div']['#markup'] = '<div id="home-clubform">';

  $form['club'] = array(
      '#title' => t('Select club to add'),
      '#type' => 'fieldset',
      '#attributes' => array('class' => array('clubs-fieldset') ),
  );

  $form['club']['button'] = array(
      '#value' => t('Add Club'),
      '#type' => 'submit',
      '#submit' => array('league_club_admin_form_add_club'),
  );

  $clubs = league_club_node_list();
  $form['club']['id'] = array(
      '#type' => 'select',
      '#options' => $clubs,
      '#attributes' => array('class' => array('club-selector')) ,
  );

  $weight = 5;

  $form['divend']['#type'] = 'markup';
  $form['divend']['#markup'] = '</div>';
  $form['divend']['#weight'] = $weight++;

  $form['header']['#type'] = 'markup';
  $form['header']['#markup'] = '<div id="home-clubs"><table><tr><th>Club</th><th>Status</th></tr>';
  $form['header']['#weight'] = $weight++;

  $form['save'] = array(
     '#value' => t('Save Club Status'),
     '#type'  => 'submit',
     '#weight' => $weight++,
  );


  $org_clubs = league_club_status($org);
  $options = Array('A' => 'Active', 'F'=> 'Friendly', 'N' => 'Inactive');
  foreach ( $org_clubs as $nid => $club ) {
    $title = $club['name'];
    $default = $club['status'];
// drupal_set_message(" title $title default $default");
    $link = '<tr><td>' . l($title, "league/club/$nid/0") . '</td><td>';
    $form["oclub$nid"] = array(
      '#prefix' => $link,
      '#suffix' => '</td></tr>',
      '#type' => 'select',
      '#options' => $options,
      '#weight' => $weight++,
      '#default_value' => $default,
      '#attributes' => array('class' => array('club-status')  ),
    );
  }

  $form['clubs']['#type'] = 'markup';
  $form['clubs']['#markup'] = '</table>';
  $form['clubs']['#weight'] = $weight++;

  $form['footer']['#type'] = 'markup';
  $form['footer']['#markup'] = '</div>';
  $form['footer']['#weight'] = $weight++;

  return $form;
}
/**
 *  Action after the form is submitted.
 */
function league_club_admin_form_submit($form, &$form_state) {
  $org = $form_state['values']['org'];
  $values = $form_state['values'];
  foreach( $values as $key => $value ) {
    if(strlen($key)> '5' && substr($key,0,5) == 'oclub') {
      $nid = substr($key,5);
      $node = node_load($nid);
      $query = db_update('league_club_org');
      $query->condition('org', $org);
      $query->condition('cid', $node->nid);
      $query->fields(array('status' => $value));
      $rows = $query->execute();
    }
  }
  drupal_set_message("Club status saved");
}

/**
 *  Action after the form is submitted.
 */
function league_club_admin_form_add_club($form, &$form_state) {
  $org = $form_state['values']['org'];
  $id = $form_state['values']['id'];
  if($id == 0) {
    drupal_set_message("You must select a club", 'error');
  } else {
    league_club_admin_add_to_org($id, $org);
  }
}

function league_club_admin_add_to_org($id, $org) {
  $club = node_load($id);
  drupal_set_message('Adding club ' . $club->title);
  node_save($club);
  $league_club_org = (object) array(
         'cid' => $club->nid,
         'org' => $org,
         'status' => 'A',
         'list' => 0,
  );
  $status = drupal_write_record('league_club_org', $league_club_org);
  if(!$status) {
    drupal_set_message('Update of league_org_club Failed', 'error');
  }
}
