// Javascript for fixture list date picker
//
// function called when date link is clicked
function pickFixtureDate(e) {
  ep=e.parentElement;
  // Re-format date for jquery
  dateBits=e.text.split(" ");
  dateDay=dateBits[1];
  dateDay=dateDay.substring(0,1);
  if(dateBits[1].length == 4) {
    dateDay=dateBits[1].substring(0,2);
  }
  dateMonth=dateBits[2];
  dateYear=dateBits[3];
  dateDefault=dateDay+' '+dateMonth+' '+dateYear;
//alert('Date Clicked');
  (function($){
  jep = $( ep );
    var $dp = $("<input type='text' />").hide().datepicker({
    dateFormat: 'd M yy',
    defaultDate: dateDefault,
    onSelect: function(dateText, inst) {
//    Function called when a date is selected
//     alert('Date:'+dateText);
       // Add twerly progress thingy (throbber)
       jep.append('<div id="date-progress" class="ajax-progress"><div class="throbber">&nbsp;</div></div>');
       url = Drupal.settings.basePath + 'league/fdate/';
       // Send an AJAX request to the server
       $.ajax({
           url: url, // This is the AjAX URL set by the custom Module
           method: "GET",
           data: { fid : e.id, fdate : dateText },
           dataType: "json",          // Type of content expected response
           success: function(data) {
             fid = data.fid;
             date = data.date;
//           alert('Ajax returned '+date+' fid '+fid);
             link=$('#'+fid).get(0);
             link.text=date;
             // Remove twerly progress thingy (throbber)
             $('#date-progress').remove();
//           link.focus();
           }
       });

    }
}).appendTo(jep);
    if ($dp.datepicker('widget').is(':hidden')) {
        $dp.show().datepicker('show').hide();
        $dp.datepicker("widget").position({
            my: "left top",
            at: "right top",
            of: this
        });
        $dp.focus();
    } else {
        $dp.hide();
    }
    
  })(jQuery);
  e.preventDefault();
}
// function called when swap link is clicked
function swapTeams(e) {
  ep=e.parentElement;
  (function($){
    jep = $( ep );
    // Add twerly progress thingy (throbber)
    jep.append('<div id="swap-progress" class="ajax-progress"><div class="throbber">&nbsp;</div></div>');
    url = Drupal.settings.basePath + 'league/fswap/';
    // Send an AJAX request to the server
    $.ajax({
        url: url,               // URL to send the request to
        method: "GET",
        data: { fid : e.id },   // Data to pass
        dataType: "json",       // Type of content expected response
        success: function(data) {
          fid = data.fid;
          fid = fid.substring(4);
        //alert('Ajax returned fid '+fid);
          homelink=$('#home'+fid).get(0);
          awaylink=$('#away'+fid).get(0);
          homeTeam=homelink.text;
          awayTeam=awaylink.text;
          homelink.text=awayTeam;
          awaylink.text=homeTeam;
          // Remove twerly progress thingy (throbber)
          $('#swap-progress').remove();
           }
       });
    
  })(jQuery);
  e.preventDefault();
}
