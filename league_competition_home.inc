<?php
// $Id$
/**
 * @file
 * Functions for league event tab
 */

/**
 * display content of league event tab
 */
function league_competition_view_tab($node) {
  // Set bread crumb
  league_set_bread_crumb ($node);
  // Set title
  drupal_set_title($node->title);
  
  $content = node_view($node);
  return $content;
}
