<?php
/**
* @file
* Functions for fixture settings tab.
*/

function league_fixture_settings_tab($node) {
  // Set Breadcrumb
  league_set_bread_crumb ($node);
  // Set title
  $title =  league_fixture_title($node);
  drupal_set_title($title);

  return drupal_get_form('league_fixture_settings_form', $node);
}

// fixture add page.

function league_fixture_settings_add($node) {
  // Set Breadcrumb
  league_set_bread_crumb ($node);

  // Get event type
  $type = league_event_value($node, 'type');

  // Set title
  if( league_event_type_individual($type) ) {
    $title =  "$node->title - Add Round";
  } else {
    $title =  "$node->title - Add Fixture";
  }
  drupal_set_title($title);

  // Create new fixture
  $date = date( 'Y-m-d', time() ) . 'T19:30:00';
  $node = league_create_fixture($node->nid, 0, 0, $date, 0);

  // Ensure default start of 19:30
  $date = substr($date,0,11) . '19:30:00';
  $node->league_date = league_date_to_time($date);

  // Show settings form
  return drupal_get_form('league_fixture_settings_form', $node);
}

function league_fixture_settings_form($form, &$form_state,$node) {

  $event = league_fixture_value($node, 'event');
  $type = league_event_value($event, 'type');
  $teams = league_event_teams($event);
  $nteams = count($teams);
  if( $nteams < 2 && ( $type == 'L' || $type == 'J' || $type == 'K')) {
    $link = l('Add Teams', "node/$event/teams");
    drupal_set_message(" You must $link before creating fixtures", 'error');
  }

  $form_state['league_node'] = $node;

  if(!league_event_type_individual($type)) {
    if($type!='L') {
      $teams = array(0 => ' ') + $teams;
    }

    $default = league_fixture_value($node, 'homeTeam');
    $form['hometeam'] = array(
        '#title' => t('Home Team'),
        '#type' => 'select',
        '#options' => $teams,
        '#default_value'  => $default,
        '#attributes' => array('class' => array('team-selector')) ,
    );

    $default = league_fixture_value($node, 'awayTeam');
    $form['awayteam'] = array(
        '#title' => t('Away Team'),
        '#type' => 'select',
        '#options' => $teams,
        '#default_value'  => $default,
        '#attributes' => array('class' => array('team-selector')) ,
    );
  }

  $date = league_fixture_value($node, 'datetime');
  if($date!=0) {
    $default  = date('Y-m-d H:i:00', $date);
  } else {
    $default  = date('Y-m-d 19:30:00');
  }
  $form['date'] = array(
   '#type'  => 'date_popup',
   '#title'  => t('Date'),
   '#date_format'  => 'Y-m-d H:i',
   '#date_label_position'  => 'within',
   '#date_year_range' => '-1:+3',
   '#default_value'  => $default,
   '#required' => TRUE,
   // Use 'timepicker' option provided by the Date Popup Timepicker module.
   // #timepicker property itself is provided by the Date Popup module.
   '#timepicker' => 'timepicker',
   // New #timepicker_options property handled by the Date Popup Timepicker
   // module and lets to customize timepicker options.
   '#timepicker_options' => array(
      'rows' => 6,
      'minutes' => array(
        'starts' => 0,
        'ends' => 55,
        'interval' => 5,
      ),
      'showCloseButton' => TRUE,
      'closeButtonText' => t('Close'),
    ),
  );

  $dateOptions = array(
   'D' => t('Normal'),
   'P' => t('Postponed'),
   'N' => t('Not Set'),
   'W' => t('Week Of'),
   'F' => t('Not Shown On Fixture List'),
  );

  $default = league_fixture_value($node, 'date_option');
  $form['doption'] = array(
   '#title'  => t('Date Option'),
   '#type'  => 'radios',
   '#options'  => $dateOptions,
   '#default_value'  => $default,
  );

  if(isset($node->nid)) {
    $round = league_fixture_value($node, 'round');
  } else {
    if($type == 'S' || $type == 'U' || $type == 'A' ) {
      $round = league_next_round($event);
    } else {
      $round = 1;
    }
  }
  $form['round'] = array(
   '#type'  => 'textfield',
   '#size'  => 20,
   '#title'  => t('Round'),
   '#default_value'  => $round,
   '#required' => TRUE,
   '#element_validate' => array('element_validate_integer_positive'), 
  );

  if($type=='J') {
    $table = league_fixture_value($node, 'table');
    $form['table'] = array(
     '#type'  => 'textfield',
     '#size'  => 20,
     '#title'  => t('Table'),
     '#default_value'  => $table,
     '#required' => TRUE,
     '#element_validate' => array('element_validate_integer_positive'), 
    );
  }
  
  $form['buttons'] = array();
  $form['buttons']['#weight'] = 100;
  $form['buttons']['save'] = array(
    '#value' => t('Save'),
    '#type'  => 'submit',
    '#submit'  => array('league_fixture_settings_save'),
    '#validate'  => array('league_fixture_settings_validate'),
    '#weight' => 15,
  );

  $form['buttons']['cancel'] = array(
    '#value' => t('Cancel'),
    '#type'  => 'submit',
    '#submit'  => array('league_fixture_settings_cancel'),
    '#weight' => 15,
  );

  $form['buttons']['delete'] = array(
    '#value' => t('Delete'),
    '#type'  => 'submit',
    '#submit'  => array('league_fixture_settings_delete'),
    '#validate'  => array('league_fixture_settings_delete_validate'),
    '#weight' => 15,
  );

  return $form;
}

function league_fixture_settings_validate($form, &$form_state) {
  $node = $form_state['league_node'];

  // Round must be 99 or less for the ECF rating system
  $round = $form_state['values']['round'];
  if( $round > 99 ) {
    form_set_error('round', 'Round must be 99 or less');
  }

}

function league_fixture_settings_save($form, &$form_state) {
  $node = $form_state['league_node'];
  $event = league_fixture_value($node, 'event');
  $type = league_event_value($event, 'type');

  $round = $form_state['values']['round'];
  $node->league_round = $round;

  if($type=='J') {
    $table = $form_state['values']['table'];
    $node->league_table_num = $table;
  }

  if(league_event_type_individual($type)) {
      $node->title = "Round $round";
  } else {
    $hometeam = $form_state['values']['hometeam'];
    $awayteam = $form_state['values']['awayteam'];
    $node->league_home_team = $hometeam;
    if($hometeam==0) {
      $homeName = 'unknown';
    } else {
      $homeNode = node_load($hometeam);
      $homeName = $homeNode->title;
    }
    $node->league_away_team = $awayteam;
    if($awayteam==0) {
      $awayName = 'unknown';
    } else {
      $awayNode = node_load($awayteam);
      $awayName = $awayNode->title;
    }
    $node->title = "$homeName v $awayName";
  }

  $doption = $form_state['values']['doption'];
  $node->league_date_option = $doption;

  $date = $form_state['values']['date'];
  $timestamp = league_date_to_time($date, TRUE);
  $node->league_date = $timestamp;

  node_save($node);
  drupal_set_message("Fixture settings saved");
  // Clear the cached fixture query
  $cache_name = "league_fixtures_event_query_$event";
  cache_clear_all($cache_name, 'cache');

  if(league_event_type_individual($type)) {
    drupal_goto("league_comp/$event/rounds");
  } else {
    drupal_goto("league_comp/$event/xfixtures");
  }
}

function league_fixture_settings_delete_validate ($form, &$form_state) {
  $node = $form_state['league_node'];
  $locked = league_fixture_value($node, 'locked');
  if($locked == 1) {
    $message = "Can't delete locked fixture";
    form_set_error('title', $message);
  }
}

function league_fixture_settings_delete($form, &$form_state) {
  $node = $form_state['league_node'];
  $event = league_fixture_value($node, 'event');

  $url = 'league/delfixture/' . $node->nid;
  drupal_goto($url);
}

function league_fixture_settings_cancel($form, &$form_state) {
  $node = $form_state['league_node'];
  $event = league_fixture_value($node, 'event');
  $type = league_event_value($event, 'type');

  drupal_set_message("Change cancelled");
  if(league_event_type_individual($type)) {
    drupal_goto("league_comp/$event/rounds");
  } else {
    drupal_goto("league_comp/$event/xfixtures");
  }
}

function league_fixture_entity_delete($fid) {
  $node = node_load($fid);
  $locked = league_fixture_value($node, 'locked');
  if($locked == 1) {
    drupal_set_message("Can't delete locked fixture");
    $event = league_fixture_value($node, 'event');
    $type = league_event_value($event, 'type');
    if(league_event_type_individual($type)) {
      drupal_goto("league_comp/$event/rounds");
    } else {
      drupal_goto("league_comp/$event/xfixtures");
    }
  } else {
    return drupal_get_form('league_fixture_entity_delete_form',$node);
  }
}

function league_fixture_entity_delete_form($form, &$form_state, $node) {
  $event = league_fixture_value($node, 'event');
  $type = league_event_value($event, 'type');
  $title = league_fixture_title($node);
  if(league_event_type_individual($type)) {
    $url = "node/$event/rounds";
  } else {
    $url = "node/$event/xfixtures";
  }
  $form['fid'] = array(
    '#type'  => 'value',
    '#value' => $node,
  );

  $form = confirm_form($form,
      "Are you sure, Do you want to delete $title ?",
      $url,
      'The action cannot be undone.',
      'Delete Fixture',
      'Cancel'
  );
  return $form;
}

function league_fixture_entity_delete_form_submit($form, &$form_state) {
  $node = $form_state['values']['fid'];
  node_delete($node->nid);
  drupal_set_message("Fixture $node->title deleted");
  $event = league_fixture_value($node, 'event');
  $type = league_event_value($event, 'type');
  if(league_event_type_individual($type)) {
    drupal_goto("league_comp/$event/rounds");
  } else {
    drupal_goto("league_comp/$event/xfixtures");
  }
}
