<?php
/**
 * @file
 * Previous Seasons
 */

/**
 * Previous Seasons tab
 */
function league_admin_tab_seasons($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);

  $table = league_events_all($node->nid);
  $content['fixtures']['#table'] = $table;
  $content['fixtures']['#theme'] = 'league_table';
  return $content;
}

/** 
 * return a list of events for the specified organisation
 * by season.
 * Parameters:
 *  org  - organisation of interest.
 */
function league_events_all($org) {

  $header = array(
    'Season',
    'Event',
    'Type',
  );

  $data = array();

  $query = db_select('league_event', 'le');
  $query->condition('le.org', $org);
  $query->innerJoin('league_competition', 'comp', 'comp.eid = le.eid');
  $query->addField('comp', 'cid', 'entity_id');
  $query->innerJoin('node', 'n', 'n.nid = comp.cid');
  $query->addField('n', 'title', 'event');
  $query->addField('n', 'nid', 'nid');
  // Old Seasons
  $query->condition('le.lms', 'O');
  $query->addField('le', 'name', 'season');
  $query->addField('le', 'eid', 'eid');
  $query->addField('comp', 'type', 'type');
  $query->orderBy('season', 'DESC');
  $query->orderBy('comp.event_order', 'ASC');
  $result = $query->execute();
  foreach ( $result as $row) {
    $type = league_event_type_string($row->type);
    $event = l($row->event, league_table_link($row->nid,$row->type) );
    $data[] = array($row->season, $event, $type);
  }

  $table['header'] = $header;
  $table['data'] = $data;
  $table['title'] = "Previous Seasons";

  return $table;
}


