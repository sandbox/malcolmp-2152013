<?php
           
/**
 * @file
 * Drush League Module.
 */

/**
 * Implementation of hook_drush_command().
 */
function league_drush_command() {
  $items['league-export'] = array(
    'description' => 'Drush command to export league data.',
    'aliases' => array('lex'),  // alias of command
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'drupal dependencies' => array('league', 'file'),
    'examples' => array(        // Listed when user types : drush help lecf
      'league-export all' => 'Export all organisations',
      'league-export org 27 mangle' => 'Export organisation 27 and mangle data',
      'league-export season 7 mangle' => 'Export season 7 and mangle data',
    ),
  );
  $items['league-status'] = array(
    'description' => 'Drush command to output league status.',
    'aliases' => array('lestat'),  // alias of command
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'drupal dependencies' => array('league', 'file'),
    'examples' => array(        // Listed when user types : drush help lecf
      'league-status counts' => 'Output counts of events',
    ),
  );
  return $items;
}

/*
 * Callback function for hook_drush_command().
 */
function drush_league_export() {
  $mangle = FALSE;
  $target = 0;
  $dirname = $_SERVER['HOME'] . '/export';
  drush_print("Writing to $dirname ");
  if(!is_dir($dirname) ) {
    drush_print("Trying to create $dirname ");
    mkdir($dirname);
  }
  // Get arguments passed in command
  $args = func_get_args();
  if(count($args) == 0 ) {
    drush_print("ERROR: no arguments");
    return;
  } else {
    $option = $args[0];
    if( $option == 'all' ) {
      drush_print('Export all organisations');
      if(count($args) > 1 ) {
        if( $args[1] == 'mangle' ) {
          drush_print('Mangling ');
          $mangle = TRUE;
        }
      }
    } else {
      if( $option == 'org' || $option == 'season' ) {
        if(count($args) > 1 ) {
          $target = intval($args[1]);
          if( $target == 0 ) {
            drush_print('ERROR org or season not an int');
            return;
          }
          if(count($args) > 2 ) {
            if( $args[2] == 'mangle' ) {
              drush_print('Mangling ');
              $mangle = TRUE;
            }
          }
        }
      } else {
        drush_print('ERROR: option must be all, org or season');
        return;
      }
    }
  } // no args

  $mangle_state = 'off';
  if( $mangle ) {
    $mangle_state = 'on';
  }

  $start_time=time();
  require_once("league_admin_admin.inc");
  switch($option) { 
    case 'season' :
      $filename = $dirname . '/export.json';
      $season = league_event_load($target);
      drush_print("Exporting season $target mangling $mangle_state to $filename");
      league_export_season($season, $mangle, $filename);
      break;
    case 'org' :
      $filename = $dirname . '/export.json';
      $node = node_load($target);
      drush_print("Exporting organisation $target mangling $mangle_state to $filename");
      league_export_whole_organisation($node, $mangle, $filename);
      break;
    case 'all' :
      $orgs = league_list_orgs();
      $count = 0;
      $total = count($orgs);
      foreach($orgs as $org) {
        $count++;
        $node = node_load($org);
        $filename = $dirname . "/export$org.json";
        drush_print("Exporting $count of $total: organisation $org mangling $mangle_state to $filename");
        league_export_whole_organisation($node, $mangle, $filename);
      }
      break;
  }
  $time = ( time() - $start_time) / 60;
  drush_print("Export finished in $time minutes");
}

function drush_league_status() {
  // Get arguments passed in command
  $args = func_get_args();
  if(count($args) == 0 ) {
    drush_print("ERROR: no arguments");
    return;
  } else {
    $option = $args[0];
  }
  if( $option == 'counts') {
    drush_print("organisation, season, event, nfixtures, nresults");
    $sql = "select org.title as org, s.name as season, e.title as event, (select count(*) from league_fixture f where f.cid=c.cid) as nfixtures, (select count(*) from league_chess_result r join league_match m on m.mid=r.mid join league_fixture ff on ff.nid=m.fid where ff.cid=c.cid) as nresults from node e join league_competition c on c.cid=e.nid join league_event s on s.eid=c.eid join node org on org.nid = s.org where e.type='event'";
  }
  $result = db_query($sql);
  foreach($result as $row) {
    $organisation = trim($row->org);
    $season = trim($row->season);
    $event = trim($row->event);
    $nfixtures = $row->nfixtures;
    $nresults = $row->nresults;
    $line = "$organisation, $season, $event, $nfixtures, $nresults";
    drush_print($line);
  }


}
