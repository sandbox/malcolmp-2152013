<?php
/**
 * @file
 * Functions for update fixture date
 */

/**
 * Update the date of a fixture
 */
function league_fixture_date() {
  $fid = (isset($_GET['fid']) ? $_GET['fid'] : 0);
  $fdate = (isset($_GET['fdate']) ? $_GET['fdate'] : 'none');
  // NOTE: doing with a query and node cache doesn't seem to get reset
  //   (dates in match edit wrong)
  $node = node_load($fid);
  // If there is a date already, then try and keep the existing time.
  if($node->league_date > 0) {
    $existing_time = date('H:i:00',$node->league_date);
    $fdate .= ' ' . $existing_time;
//  watchdog('league', "league_fixture_date: $fid fdate $fdate existing $existing_time");
  }
  $time = strtotime($fdate);
  $node->league_date = $time;
  node_save($node);
  $outdate = date( 'D jS M Y', $time);
  $data = array('fid' => $fid, 'date' => $outdate);
  drupal_json_output($data);
  exit;
}
