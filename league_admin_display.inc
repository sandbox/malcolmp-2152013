<?php
// $Id$

// Admin Display tab
function league_admin_tab_display($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);

  // Show display form
  return drupal_get_form('league_display_settings_form', $node);
}

// Admin Display form
function league_display_settings_form($form, &$form_state,$node) {

  $form_state['league_node'] = $node;

  // Text Format
  if(isset($node->body['und'][0]) ) {
    $default = $node->body['und'][0]['format'];
  } else {
    $default = '';
  }
  $formats = array('plain_text' => 'Plain text', 'filtered_html' => 'Filtered HTML');
  $form['format'] = array(
    '#title' => t('Format'),
    '#type' => 'select',
    '#options' => $formats,
    '#default_value'  => $default,
    '#required' => true,
    '#weight' => 20,
  );

  // Display 
  if(isset($node->body['und'][0]) ) {
    $display = $node->body['und'][0]['value'];
  } else {
    $display = '';
  }
  $form['display'] = array(
    '#type'  => 'textarea',
    '#cols'  => 60,
    '#rows'  => 25,
    '#title'  => t('Display Text'),
    '#default_value'  => $display,
    '#weight' => 30,
  );

  // Save button
  $form['submit'] = array(
    '#value' => t('Save'),
    '#type' => 'submit',
    '#weight' => 40,
  );

  return $form;
}

// Action when the form is submitted.

function league_display_settings_form_submit($form, &$form_state) {
  $node = $form_state['league_node'];
  $display = $form_state['values']['display'];
  $format = $form_state['values']['format'];

  // Set the text and format
  $node->body['und'][0]['value'] = $display;
  $node->body['und'][0]['format'] = $format;

  // Notify field widgets
  field_attach_submit('node', $node, $form, $form_state);

  // Save the settings
  node_save($node);
  drupal_set_message('Settings saved');
}
