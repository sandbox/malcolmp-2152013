<?php
/**
 * @file
 * Functions for swap fixture teams
 */

/**
 * Swap the teams of a fixture
 */
function league_fixture_team_swap() {
  $fid = (isset($_GET['fid']) ? $_GET['fid'] : 0);
  $fixture = substr($fid, 4);

  $node = node_load($fixture);
  $home = $node->league_home_team;
  $away = $node->league_away_team;
  //watchdog('league', "league_fixture_team_swap: $fid $fixture $home $away " );
  $node->league_home_team = $away;
  $node->league_away_team = $home;
  $title = league_fixture_create_title($away, $home);
  $node->title = $title;
  node_save($node);
  // Send the id back to javascript to update the page
  $data = array('fid' => $fid );
  drupal_json_output($data);
  exit;
}
