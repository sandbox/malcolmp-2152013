<?php
/**
 * @file
 * Functions for locking fixtures
 */

/**
 * Lock a fixture
 */
function league_fixture_lock($fixture) {
  $rows = league_fixture_set_lock($fixture, TRUE);
  // Clear cache so change is picked up
  cache_clear_all("field:node:$fixture", 'cache_field', TRUE);
  $event = league_fixture_value($fixture, 'event');
  $type = league_event_value($event, 'type');
  if($type == 'S' || $type == 'U' || $type == 'A' ) {
    drupal_goto("league_comp/$event/rounds");
  } else {
    drupal_goto("league_comp/$event/xfixtures");
  }
}

/**
 * Unlock a fixture
 */
function league_fixture_unlock($fixture) {
  $rows = league_fixture_set_lock($fixture, FALSE);
  // Clear cache so change is picked up
  cache_clear_all("field:node:$fixture", 'cache_field', TRUE);
  $event = league_fixture_value($fixture, 'event');
  $type = league_event_value($event, 'type');
  if($type == 'S' || $type == 'U' || $type == 'A' ) {
    drupal_goto("league_comp/$event/rounds");
  } else {
    drupal_goto("league_comp/$event/xfixtures");
  }
}

/**
 * Mark fixture as Locked / Unlocked in the database.
 */
function league_fixture_set_lock($fixture, $lock) {
  if ($lock) {
    $update = 1;
  }
  else {
    $update = 0;
  }
  // Don't do it this way because it changes the date the fixture
  // was updated and so it appears on the latest fixtures again.
  //$node = node_load($fixture);
  //$node->league_locked = $update;
  //node_save($node);
  // Update using the database instead
  $query = db_update('league_fixture');
  $query->condition('nid', $fixture);
  $query->expression('locked', $update);
  //dsm((string)$query);
  $rows = $query->execute();
  return $rows;
}

