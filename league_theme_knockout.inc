<?php
/**
 * @file
 * Function to Theme a team knockout table
 */

/**
 * Theme a knockout table
 */
function theme_league_knockout_table($variables) {
  $element = $variables['element'];
  $data = $element['#data'];

  $output = '<div class=league-lframe>';
  $scores = $data['scores'];
  $teams = $data['teams'];
  $event = $data['event'];
  $nrounds = count($scores);

  $output .= "<table border class=ktable>";
  if($nrounds > 0 && isset($scores[0]) ) {
    // number of fixtures = number of items in round 1.
    $nfixtures = count($scores[0]);
    $row = 0;
    // idea is to loop round the rows and columns in the table, pulling in
    // teams from the scores array as needed. Problem is we need to take in
    //  a home team and then an away team alternately from each round list
    for ($fc = 0; $fc < $nfixtures; $fc++) {
      $row++;
      $output .= "<tr class=odd>";
      $output .= league_table_knockout_cell($scores, $teams, $nrounds, $row, $data['homeCounts'], $data['awayCounts'], $data['lastTeam'], $event);
      $output .= "</tr>\n";
      $row++;
      $output .= "<tr class=odd>";
      $output .= league_table_knockout_cell($scores, $teams, $nrounds, $row, $data['homeCounts'], $data['awayCounts'], $data['lastTeam'], $event);
      $output .= "</tr>\n";
    }
  }
  $output .= "</table>";
  $output .= "</div>";

  return $output;
}

function league_table_knockout_cell($scores, $teams, $nrounds, $row, &$homeCounts, &$awayCounts, &$lastTeam, $event ) {
  $output = " ";
  for ($r = 0; $r < $nrounds; $r++) {
    if ( ($row -1) % pow(2, $r) == 0 ) {
      $rowSpan = pow(2, $r);
      if ( $rowSpan == 1) {
        $output .= "<td class=ktable>";
      }
      else {
        $output .= "<td class=ktable rowspan=$rowSpan>";
      }
      $fixture = 0;
      if ($lastTeam[$r] == 'H') {
        $team = "awayTeam";
        if(isset($scores[$r][$awayCounts[$r]][$team]) ) {
          $teamId = $scores[$r][$awayCounts[$r]][$team];
          if(isset($scores[$r][$awayCounts[$r]]['awayScore'])) {
            $points = $scores[$r][$awayCounts[$r]]['awayScore'];
          } else {
            $points = ' ';
          }
        } else {
          $teamId = 0;
          $points = ' ';
        }
        $lastTeam[$r] = 'A';
        if(isset($scores[$r][$awayCounts[$r]]) ) {
          $fixture = $scores[$r][$awayCounts[$r]]['fixture'];
        } else {
          $fixture = 0;
        }
        $awayCounts[$r]++;
      } else {
        $team = "homeTeam";
        if(isset($scores[$r][$homeCounts[$r]][$team]) ) {
          $teamId = $scores[$r][$homeCounts[$r]][$team];
          if(isset($scores[$r][$homeCounts[$r]]['homeScore'])) {
            $points = $scores[$r][$homeCounts[$r]]['homeScore'];
          } else {
            $points = ' ';
          }
        } else {
          $teamId = 0;
          $points = ' ';
        }
        $lastTeam[$r] = 'H';
        if(isset($scores[$r][$homeCounts[$r]]) ) {
          $fixture = $scores[$r][$homeCounts[$r]]['fixture'];
        } else {
          $fixture = 0;
        }
        $homeCounts[$r]++;
      }
      if (isset($teamId) && $teamId != 0) {
        $teamName = $teams[$teamId][0];
        if(is_string($fixture) && strpos($fixture, 'v') ) {
          $pids = explode('v', $fixture);
          $white = trim($pids[0]);
          $black = trim($pids[1]);
          $teamName = l($teamName, "league_chess/nojs/$event/$white/$black", array('attributes' => array('class' => 'ctools-use-modal ctools-modal-league-chess-display-modal-dialog-style')));
        } else {
          if($fixture > 0) {
            $teamName = l($teamName, "league_fixture/$fixture");
          }
        }
      }
      else {
        $teamName = '*';
      }
      if ( $rowSpan == 1) {
        $output .= "$teamName </td><td class=ktable> $points";
      }
      else {
        $output .= "$teamName </td><td class=ktable rowspan=$rowSpan> $points";
      }
      $output .= "</td>";
    }
  }
  return $output;
}
