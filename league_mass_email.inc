<?php
/**
* @file
* Functions for mass email options.
*/

/**
*  Tab for organisation owner mass email option.
*/
function league_mass_email_page($node, $group) {

  $org = $node->nid;

  switch($group) {
    case 'U' : $title = "Email Organisation Users";
               $heading = 'Organisation';
               break;
    case 'S' : $title = "Email Club Secretaries";
               $heading = 'Club';
               break;
    case 'T' : $title = "Email Team Captains";
               $heading = 'Team';
               break;
  }

  $content['form'] = user_tools_mass_email_page($title, $heading, 'league', 'html');

  return $content;
}

/**
*  Tab for admin option to email all organisation owners.
*/

function league_admin_mass_email_page() {
  $title = 'Email organisation owners';

  $content['form'] = user_tools_mass_email_page($title, 'Organisation', 'league', 'html');

  return $content;
}
