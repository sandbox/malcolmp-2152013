<?php
// $Id$
/**
 * @file
 * Functions for League fixture load.
 */

// Library for reading XL files
//$exlib = libraries_get_path('vendor');
//require_once "$exlib/autoload.php";
use \PhpOffice\PhpSpreadsheet\Spreadsheet;
use \PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use \PhpOffice\PhpSpreadsheet\Style\NumberFormat;

function league_admin_tab_fixture_load($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);

  $content = array();
  $content['form'] = drupal_get_form('league_fixtures_load_form', $node->nid);

  return $content;
}
  
// Form to specify fixtures file to load
function league_fixtures_load_form($form, &$form_state, $org) {
  
  $form_state['org'] = $org;

  // Get list of seasons for the organisation
  $seasons = league_season_list($org);

  // Season selector
  $form['season'] = array(
      '#type' => 'select',
      '#options'  => $seasons,
      '#required'  => TRUE,
      '#title' => t('Season'),
      '#description' => t('Season to import fixtures into'),
  );

  // Delete checkbox
  $form['delete'] = array(
   '#type'  => 'checkbox',
   '#title'  => t('Delete Existing Fixtures For Season'),
   '#description' => t('Any existing fixtures that do not have results entered for them in events in the current season that are specified in this file will be deleted.'),
  );

  // Required to do file uploads
  $form['#attributes'] = array('enctype' => "multipart/form-data");

  // File upload button
  $form['fixtfile'] = array(
    '#type' => 'file',
    '#title' => t('Fixtures File to Upload'),
    '#description' => t('Format should be event name, home team, away team, date. The file type can be either an old style MS Excel file (.xls) or comma seperated (.csv) file. If using a .csv file then the date should be in the form yyyy-mm-dd, for a .xls file it should be a date format cell'),
    '#size' => 64,
  );

  // Import button.
  $form['submit'] = array(
     '#value' => t('Import Fixtures'),
     '#type'  => 'submit',
  );

  // Delete button.
  $form['delbut'] = array(
     '#value' => t('Delete Fixtures'),
     '#type'  => 'submit',
     '#submit'  => array('league_fixture_load_delete_button'),
  );

  return $form;
}

// Get eventid for event with given name.
function league_event_named($name, $org, $season, $row, &$context) {
  $events=$context['sandbox']['events'];
  $event = 0;
  if(isset($events[$name]) ) {
    $event = $events[$name];
  } else {
    $nevents = league_get_event_named($name, $org, $season);
    if(count($nevents) < 1) {
      drupal_set_message("No event named $name in selected season on row $row", 'error');
    }
    if(count($nevents) > 1) {
      drupal_set_message("More than one event named $name in selected season on row $row", 'error');
    }
    if(count($nevents) == 1) {
      $event = $nevents[0];
      $events[$name] = $event;
    }
  }
  $context['sandbox']['events'] = $events;
  return $event;
}

//
// Get event by name.
//
function league_get_event_named($name, $org, $season) {
//drupal_set_message("league_get_event_named $name $org $season");
  $rows = array();

  // Nodes of type event with a matching title
  $query = db_select('node', 'n');
  $query->condition('n.type', 'event');
  $query->condition('n.title', $name);
  $query->fields('n', array('nid'));
  $query->innerJoin('league_competition', 'comp', 'comp.cid = n.nid');
  // In the correct season.
  $query->condition('comp.eid', $season);
  $query->innerJoin('league_event', 's', 's.eid = comp.eid');
  // In the correct organisation.
  $query->condition('s.org', $org);
//drupal_set_message((string)$query);
  $result = $query->execute();

  foreach ( $result as $row) {
    $nid =  $row->nid;
    $rows[] = $nid;
  }
  return $rows;
}

/**
 *  Display the confirm delete form.
 */

function league_fixture_load_delete($form, &$form_state, $org, $season) {

  $form = array();
   
  $form['org'] = array(
	'#type' => 'value',
	'#value' => $org,
  );

  $form['season'] = array(
	'#type' => 'value',
	'#value' => $season,
  );

  $orgNode = node_load($org);
  $orgTitle = $orgNode->title;
  $league_event = league_event_load($season);
  $seasonName = $league_event->name;

  $form = confirm_form(
	$form,
	t('Are you sure you want to delete all fixtures for this season?'),
	"node/$org/league/fixtures",
	t("All fixtures for $seasonName in $orgTitle that do not have match results will be deleted. This action cannot be undone."),
	t('Delete'),
	t('Cancel')
  );
  return $form;
}

/**
 *  Action after the button to delete fixtures is pressed.
 */

function league_fixture_load_delete_button($form, &$form_state) {
  $org = $form_state['org'];
  $season = $form_state['values']['season'];
  // Check for access
  if (!league_check_access($org)) {
    drupal_set_message('You do not have access to do this','warning');
    return;
  }

  drupal_goto("league/delfixt/$org/$season");
}
/**
 *  Action after the button to delete fixtures is pressed.
 */

function league_fixture_load_delete_submit($form, &$form_state) {
  $org = $form_state['values']['org'];
  $season = $form_state['values']['season'];
  // Check for access
  if (!league_check_access($org)) {
    drupal_set_message('You do not have access to do this','warning');
    return;
  }
  $events = league_season_events($season);
  foreach($events as $nid) {
    $node = node_load($nid);
    $event = $node->title;
    $count = league_delete_old_fixtures($nid, time());
    drupal_set_message("Deleted $count old fixtures for event $event");
    // Clear the league_fixtures_event_query cache
    $cache_name = "league_fixtures_event_query_$event";
    cache_clear_all($cache_name, 'cache');
  }
  drupal_goto("node/$org/league/fixtures");
}

/**
 *  Action after the button to load a fixture file is pressed.
 */

function league_fixtures_load_form_submit($form, &$form_state) {

  $season = $form_state['values']['season'];
  $delete = $form_state['values']['delete'];
  $org = $form_state['org'];

  // Check for access
  // TODO should event owners be able to import fixtures.
  if (!league_check_access($org)) {
    drupal_set_message('You do not have access to do this','warning');
    return;
  }
//drupal_set_message(" Fixture load: season $season org $org delete $delete");

  // Limit to files with the .xls suffix
  $validators = array(
   'file_validate_extensions' => array('xls XLS csv CSV'),
   'file_validate_size' => array(2000000, 0),
  );

  // Create league files subdirectory if it doesn't exist
  $path = 'public://league';
  if (!file_prepare_directory($path, FILE_CREATE_DIRECTORY)) {
    drupal_set_message(t('Could not create file @file', array('@file' => $path)), 'error');
    return;
  }

  // Upload the file
  if ($xlsfile = file_save_upload('fixtfile', $validators, $path, FILE_EXISTS_REPLACE)) {
    $filename = $xlsfile->destination;
    $filetype = 'c';
    if(substr($filename,-3) == 'xls' || substr($filename,-3) == 'XLS') {
      $filetype = 'x';
    }
    drupal_set_message(t('Uploaded @file of type @type', array('@file' => $filename, '@type' => $filetype)));
    $module_file = drupal_get_path('module', 'league') .'/league_fixture_load.inc';
    $operations = array(
        array('league_fixture_load_file', array($filename,$filetype,$org,$season,$delete)),
    );

    $batch = array(
        'operations' => $operations,
        'finished' => 'league_fixture_load_finished',
        'title'    => 'Loading Fixture File',
        'init_message' => 'Load Starting. Uploading file ...',
        'file' => $module_file,
    );
    batch_set($batch);
  } else {
    drupal_set_message(t('File missing or Upload failed'));
  }
}

function league_fixture_load_file($filename,$filetype,$org,$season,$delete, &$context) {
  watchdog('league',"Loading fixtures file $filename");
  if(!isset($context['sandbox']['current'])) {
    $context['sandbox']['filetype'] = $filetype;
    $context['sandbox']['current'] = 1;
    $context['sandbox']['created'] = 0;
    $context['sandbox']['endrec'] = 1;
    $header = league_fixture_load_header($filename,$filetype);
    $context['sandbox']['nrecords'] = $header['nrecords'];
    $context['sandbox']['filename'] = $header['filename'];
    $context['sandbox']['org'] = $org;
    $context['sandbox']['season'] = $season;
    $context['sandbox']['stime'] = time();
    $context['sandbox']['delete'] = $delete;
    $context['sandbox']['events'] = array();
    $context['sandbox']['errors'] = 0;
    // Read records in batches of chunk
    $context['sandbox']['chunk'] = 50;
    watchdog('league','Number of records ' . $header['nrecords'] );
  }

  $current = $context['sandbox']['current'];
  $nrecords = $context['sandbox']['nrecords'];
  $current = league_fixture_load_batch($context);
  $context['sandbox']['current'] = $current;
}

//
// Define a Read Filter class implementing IReadFilter
// This allows reading part of spreadsheet at a time
//
class chunkReadFilter implements \PhpOffice\PhpSpreadsheet\Reader\IReadFilter
{
  private $_startRow = 0;
  private $_endRow = 0;

  public function setRows($startRow, $chunkSize) {
    $this->_startRow = $startRow;
    $this->_endRow = $startRow + $chunkSize;
  }

  public function readCell(string $column, int $row, string $worksheetName = ''): bool {
    // Only read the heading row, and the configured rows
    if (($row == 1) ||
        ($row >= $this->_startRow && $row < $this->_endRow)) {
      return true;
    }
    return false;
  }
}

// Work out file size

function league_fixture_load_header($filename,$filetype) {
  $realname = drupal_realpath($filename);
  $highestColumnIndex = 0;

  // Get number of rows from the file.
  $nrecords = count( file($realname) );

  // Check its a spreadsheet with correct number of columns
  if($filetype == 'x') {

    $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xls');
    $objReader->setReadDataOnly(true);

    $startRow = 0;

    $objPHPExcel = $objReader->load("$realname");
    $objWorksheet = $objPHPExcel->getActiveSheet();

    $highestRow = $objWorksheet->getHighestRow(); 
    $highestColumn = $objWorksheet->getHighestColumn(); 

    $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); 

    $nrecords = $highestRow;
  }

  $file_header = array(
    'nrecords' => $nrecords,
    'filename' => $realname,
    'ncols' => $highestColumnIndex,
  );

  return $file_header;
}

function league_fixture_load_batch(&$context) {
  $sandbox = $context['sandbox'];
  $nrecords = $sandbox['nrecords'];
  $chunk = $sandbox['chunk'];
  $current = $sandbox['current'];
  $filename = $sandbox['filename'];
  $filetype = $sandbox['filetype'];
  $org = $sandbox['org'];
  $season = $sandbox['season'];
  $delete = $sandbox['delete'];
  $errors = 0;
  $created = 0;
  $finished = FALSE;

//drupal_set_message("league_fixture_load_batch chunk $chunk current $current nrecords $nrecords filename $filename filetype $filetype org $org season $season ");

  // XL format
  if($filetype == 'x') {
    // Open spreadsheet

    $objReader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xls');
    $objReader->setReadDataOnly(true);

    // Create a new Instance of our Read Filter
    $chunkFilter = new chunkReadFilter();
    // Tell the Reader that we want to use the Read Filter
    $objReader->setReadFilter($chunkFilter);

    $startRow = $current;
    $chunkSize = $chunk;
    $chunkFilter->setRows($startRow,$chunkSize);

    // Read next chunk
    $objPHPExcel = $objReader->load("$filename");
    $objWorksheet = $objPHPExcel->getActiveSheet();

    $highestRow = $objWorksheet->getHighestRow(); 
    $highestColumn = $objWorksheet->getHighestColumn(); 

    $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); 
  
    $start = $current;
    if($start == 1) {
      $start = 2;
    }
    // Loop round the records , and update the database record
    for($row = $start; $row<= $highestRow; $row++) {
      $dataRange = $objWorksheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                    NULL,
                                    TRUE,
                                    FALSE);
      $rowData = $dataRange[0];

      $eventName = $rowData[0];
      $homeTeamName = $rowData[1];
      $awayTeamName = $rowData[2];
      $excelDate  = $rowData[3];
        $dateString = NumberFormat::toFormattedString($excelDate, 'YYYY-MM-DD hh:mm');
      $nid = league_fixture_load_process_record($org, $season, $row, $context, $eventName, $homeTeamName, $awayTeamName, $dateString);
      if($nid>0) {
        $created++;
      } else {
        $errors++;
      }
    }
    if( $highestRow >= $nrecords ) {
      $finished = TRUE;
    }
  } else {
    // Type CSV
    $recount=0;
    $row=0;
    // Open CSV file
    $file = fopen($filename, 'r');
    // read till we have read chunk records or reached the end
    while (($rowData = fgetcsv($file)) !== FALSE && $recount<$chunk) {
      $row++;
      // skip header to current record
      if($row>1 && $row>$current) {
        $recount++;
        $eventName = $rowData[0];
        $homeTeamName = $rowData[1];
        $awayTeamName = $rowData[2];
        $dateString = $rowData[3];
        // Create the fixture
        $nid = league_fixture_load_process_record($org, $season, $row, $context, $eventName, $homeTeamName, $awayTeamName, $dateString);
        if($nid>0) {
          $created++;
        } else {
          $errors++;
        }
      }
    }
    fclose($file);
//  $highestRow = $current-1;
    if($recount<$chunk) {
      $finished = TRUE;
    }
  }

//$nrecords = max($highestRow, $nrecords);
  $endrec = $context['sandbox']['endrec'];
  if( $nrecords > $endrec ) {
    $context['sandbox']['endrec'] = $nrecords ;
  }
  $endrec = $context['sandbox']['endrec'];
  $context['sandbox']['created'] += $created;
  $context['sandbox']['errors'] += $errors;
  $current += $chunk;

  // set finished flag in sandbox
  if( $finished ) {
    $context['finished'] = 1;
    $context['message'] = "Finished reading $nrecords records";
    drupal_set_message("Read $endrec records");
    $events = $context['sandbox']['events'];
    // Delete old fixtures
    if($delete) {
      drupal_set_message("Deleting old fixtures");
      foreach($events as $name => $event) {
        // pass in a time -4, so we don't delete the fixtures just created.
        $count = league_delete_old_fixtures($event, $context['sandbox']['stime'] - 4);
        drupal_set_message("Deleted $count old fixtures for event $name");
      }
    }
    $created = $context['sandbox']['created'];
    $errors = $context['sandbox']['errors'];
    drupal_set_message("Created $created fixtures $errors errors");
  } else {
    $context['finished'] = $current / $nrecords;
    $context['message'] = "Read $current records from $nrecords";
  }

  return $current;
}

// Process a fixture record

function league_fixture_load_process_record($org, $season, $row, $context, $eventName, $homeTeamName, $awayTeamName, $dateString) {
//drupal_set_message("league_fixture_load_process_record $org, $season, $row, $context, $eventName, $homeTeamName, $awayTeamName, $dateString");
  $nid=0;
  // get event (from DB if not in static cache)
  $event = league_event_named($eventName, $org, $season, $row, $context);
  if($event>0) {
    // get home team (from DB if not in static cache)
    $home = league_team_named($homeTeamName, $event, $eventName, $row);
    // get away team (from DB if not in static cache)
    $away = league_team_named($awayTeamName, $event, $eventName, $row);
    // Check date
    $date = league_fixture_load_check_date($dateString, $row);
    // create fixture. allow a team name of Bye to create a fixture with no team
    if( ($home>0 || $homeTeamName == 'Bye') && ($away>0 || $awayTeamName == 'Bye') && $date != 'ERROR') {
      $node =  league_create_fixture($event, $home, $away, $date, 1, 0, FALSE);
      node_save($node);
      $nid = $node->nid;
    }
  }
  return $nid;
}

function league_fixture_load_check_date($dateString, $row) {
  if(strlen($dateString) < 10 || strlen($dateString) > 16) {
    drupal_set_message('Date field >' . $dateString . "< on row $row not valid: Must have 10 characters YYYY-MM-DD", 'error');
    return 'ERROR';
  } else {
    $year = intval(substr($dateString, 0, 4));
    $month =  intval(substr($dateString, 5, 2));
    $day =  intval(substr($dateString, 8, 2));
    $hour = 0;
    $minute = 0;
    if(strlen($dateString > 10) ) {
      $hour =  intval(substr($dateString, 11, 2));
      $minute =  intval(substr($dateString, 14, 2));
    }
    if(checkdate ( $month, $day, $year ) ) {
      $date = sprintf("%04d-%02d-%02dT%02d:%02d:00", $year, $month, $day, $hour, $minute);
      //drupal_set_message("league_fixture_load_check_date $date");
      return $date;
    } else {
      drupal_set_message('Date field >' . $dateString . "< on row $row not valid:. Should be of form YYYY-MM-DD year $year month $month day $day", 'error');
      return 'ERROR';
    }
  }
}

function league_fixture_load_finished($success,$results,$operations) {
  if( $success ) {
    drupal_set_message("File Load Complete");
  } else {
    drupal_set_message("Fixture Load Failed");
    reset($operations);
  }
}

function league_team_named($name, $event, $eventName, $row) {
  static $teams=array();
  $team = 0;
  if($name == 'Bye') {
    return $team;
  }
  if(isset($teams[$name][$event]) ) {
    $team = $teams[$name][$event];
  } else {
    $nteams = league_get_team_named($name, $event);
    if(count($nteams) < 1) {
      drupal_set_message("No team named $name in event $eventName on row $row", 'error');
    }
    if(count($nteams) > 1) {
      drupal_set_message("More than one team named $name in event $eventName on row $row", 'error');
    }
    if(count($nteams) == 1) {
      $team = $nteams[0];
      $teams[$name][$event] = $team;
    }
  }
  return $team;
}

//
// Get event by name.
//
function league_get_team_named($name, $event) {
  $rows = array();

  // Nodes of type team with a matching title
  $query = db_select('node', 'n');
  $query->condition('n.type', 'team');
  $query->condition('n.title', $name);
  $query->fields('n', array('nid'));
  // In the correct event.
  $query->innerJoin('field_data_field_event', 'e', 'e.entity_id = n.nid');
  $query->condition('field_event_nid', $event);
//dsm((string)$query);

  $result = $query->execute();

  foreach ( $result as $row) {
    $nid =  $row->nid;
    $rows[] = $nid;
  }
  return $rows;
}

function league_delete_old_fixtures($event, $time) {
  // Fixtures in the event
  $query = db_select('league_fixture', 'f');
  $query->fields('f', array('nid'));
  $query->condition('f.cid', $event);
  // Fixtures created before we loaded the new ones
  $query->innerJoin('node', 'n', 'n.nid = f.nid');
  $query->condition('n.created', $time, '<');
  // Fixtures with no results
  $query->leftJoin('league_match', 'm', 'm.fid = f.nid');
  $query->leftJoin('league_chess_result', 'r', 'r.mid = m.mid');
  $query->where('r.rid is null');
//dsm((string)$query);

  $count=0;
  $result = $query->execute();
  foreach ( $result as $row) {
    $nid = $row->nid;
    node_delete($nid);
    $count++;
  }
  return $count;
}
