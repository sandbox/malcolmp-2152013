<?php
/**
 * @file
 * Functions for late matches tab.
 */

/**
 * Late Matches report tab
 */
function league_admin_tab_late($node) {
  // Set title
  $title = $node->title . ' - Late Matches';
  drupal_set_title($title);

  // Get table 
  $content['late']['#table'] = league_table_late_matches($node);
  $content['late']['#theme'] = 'league_table';

  return $content;
}

/**
 * Create query showing late matches
 */
function league_table_late_matches_query($org) {

  // Seasons for that organisation
  $query = db_select('league_event', 'le');
  $query->condition('le.lms', 'A');
  $query->addField('le', 'name', 'seasonName');
  $query->condition('le.org', $org);
  $query->innerJoin('league_competition', 'comp', 'comp.eid = le.eid');
  // fixtures by event
  $query->innerJoin('league_fixture', 'f', 'f.cid = comp.cid');
  $query->addField('f', 'nid', 'nid');
  $query->addField('f', 'date', 'date');
  $query->addField('f', 'date_option', 'date_option');
  $query->innerJoin('league_match', 'm', 'm.fid = f.nid');
  // event title
  $query->innerJoin('node', 'et', 'et.nid = f.cid');
  $query->addField('et', 'title', 'event');
  // home team 
  $query->innerJoin('node', 'hn', 'hn.nid = f.home_team');
  $query->addField('hn', 'title', 'home');
  // away team 
  $query->innerJoin('node', 'an', 'an.nid = f.away_team');
  $query->addField('an', 'title', 'away');
  // zero scores and date before today
  $query->condition('m.away_score', 0);
  $query->condition('m.home_score', 0);
  $time = time() - 24*60*60;
  $query->where("f.date <$time AND f.date_option = 'D'");
  //dsm((string)$query);

  return $query;
}

/**
 * Create table showing late matches
 */
function league_table_late_matches($node) {

  $query = league_table_late_matches_query($node->nid);
  $header = array('Event', 'Home Team', 'Away Team', 'Date');
  $rows = array();

  $result = $query->execute();
  foreach($result as $row) {
    $event = l($row->event, 'league_fixture/' . $row->nid . '/match' );
    $home = $row->home;
    $away = $row->away;
    $date = date('D jS M Y',$row->date);
    $rows[] = array($event, $home, $away, $date);
  }
  $table['data'] = $rows;
  $table['header'] = $header;
  return $table;

}
/**
 * Get number of late matches
 */
function league_count_late_matches($org) {

  $cache_name = "league_count_late_matches_$org";
  $num_rows = &drupal_static($cache_name);
  if (!isset($num_rows)) {
    $use_cache = FALSE;
    if( $cache = cache_get($cache_name)) {
      $num_rows = $cache->data;
      // Do this check because cron may not be running.
      if(time() > $cache->expire) {
        $use_cache = TRUE;
      }
    }
    if(!$use_cache) {
  
      $query = league_table_late_matches_query($org);
      // fixtures by event
      $num_rows = $query->countQuery()->execute()->fetchField();
      // cache for a day because more matches might be late then
      cache_set($cache_name, $num_rows, 'cache', time()+360*24);
    }
  }
  return $num_rows;
}
