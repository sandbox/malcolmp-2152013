<?php
/**
 * @file
 * Functions for no date matches tab.
 */

/**
 * Late Matches report tab
 */
function league_admin_tab_no_date($node) {
  // Set title
  $title = $node->title . ' - No Date Matches';
  drupal_set_title($title);

  // Get table 
  $content['late']['#table'] = league_table_no_date_matches($node);
  $content['late']['#theme'] = 'league_table';

  return $content;
}

/**
 * Create query showing late matches
 */
function league_table_no_date_matches_query($org) {


  // seasons for the org
  $query = db_select('league_event', 'le');
  $query->condition('le.org', $org);
  // Active Seasons
  $query->condition('le.lms', 'A');
  // events for that season
  $query->innerJoin('league_competition', 'comp', 'comp.eid = le.eid');
  // fixtures by event
  $query->innerJoin('league_fixture', 'f', 'f.cid = comp.cid');
  $query->fields('f', array('nid', 'date', 'date_option', 'home_team', 'away_team'));
  // event title
  $query->innerJoin('node', 'et', 'et.nid = f.cid');
  $query->addField('et', 'title', 'event');
  // home team 
  $query->innerJoin('node', 'hn', 'hn.nid = f.home_team');
  $query->addField('hn', 'title', 'home');
  // away team 
  $query->innerJoin('node', 'an', 'an.nid = f.away_team');
  $query->addField('an', 'title', 'away');
  // un-dated matches
  $query->where("f.date = 0 OR f.date_option = 'N' OR f.date_option = 'P'");

  return $query;
}

/**
 * Create table showing un-dated matches
 */
function league_table_no_date_matches($node) {

  $query = league_table_no_date_matches_query($node->nid);
  $header = array('Event', 'Home Team', 'Away Team', 'Date');
  $rows = array();

  $result = $query->execute();
  foreach($result as $row) {
    $event = l($row->event, 'league_fixture/' . $row->nid . '/fsettings' );
    $home = $row->home;
    $away = $row->away;
    if(isset($row->date) ) {
      $date = date('D jS M Y',$row->date);
    } else {
      $date = ' ';
    }
    // Modify the date if the date_option field is set. 
    if(isset($row->date_option) ) {
      switch($row->date_option) {
        case 'N' : $date = 'Not Set';
        break;
        case 'P' : $date = 'Postponed';
        break;
        case 'W' : $date = "Week $date";
        break;
      }
    }

    $rows[] = array($event, $home, $away, $date);
  }
  $table['data'] = $rows;
  $table['header'] = $header;
  return $table;

}
