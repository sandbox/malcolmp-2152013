<?php
/**
 * @file
 * Function to Theme a league cross table
 */

/**
 * Theme a cross table
 */
function theme_league_cross_table($variables) {
  $element = $variables['element'];
  $data = $element['#data'];

  $output = '<div class=league-lframe>';

  $output .= "<table border class=ktable>";

  // Column Headings
  $teams = array();
  $output .= "<tr><th>Team</th>";
  foreach($data as $team => $values) {
    $name = $values['team'];
    $name = league_cross_table_short_name($name);
    $output .= "<th>$name</th>";
    $teams[] = $team;
  }
  $output .= "</tr>";

  // Rows
  foreach($data as $home => $values) {
    $team = $values['team'];
    $output .= "<tr>";
    $output .= "<th>$team</th>";
    foreach($teams as $away) {
      if($home == $away) {
        $output .= "<th></th>";
      } else {
        if(isset($values['results'][$away]) ) {
          $links = array();
          foreach($values['results'][$away] as $match) {
            $nid = $match['fid'];
            $links[] = l($match['score'],"league_fixture/$nid");
          }
          $scores = implode('<br>', $links);
        } else {
          $scores = ' ';
        }
        $output .= "<td>$scores</td>";
      }
    }
    $output .= "</tr>";
  }
  
  $output .= "</table>";
  $output .= "</div>";

  return $output;
}

function league_cross_table_short_name($name) {
  $output = '';
  $parts = explode(' ', $name);
  foreach($parts as $part) {
    $output .= substr($part, 0, 5);
    $output .= ' ';
  }
  return $output;
}
