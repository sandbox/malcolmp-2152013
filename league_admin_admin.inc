<?php
// $Id$

// Option to export a season
function league_export_season($season, $mangle=FALSE, $fileName ="export.json") {
  $data = league_export_create();
  league_export_season_data($data, $season, $mangle);
  module_invoke_all('league_export', $data, $mangle);
  league_export_json($data, $fileName);
}

// create empty object to store the data in
function league_export_create() {
  $data = new StdClass();
  $data->version = "7.0";
  $data->settings = array();
  $data->seasons = array();
  $data->users = array();
  $data->players = array();
  $data->lists = array();
  $data->clubs = array();
  $data->rlists = array();
  return $data;
}

// Event admin tab
function league_export_organisation($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);

  $org = $node->nid;
  $content = array();

  // Provide a form for creating an xml export of the data
  $content['form'] = drupal_get_form('league_export_form', $node);

  return $content;
}

/**
 *  Form for admin tab.
 */
function league_export_form($form, &$form_state, $node) {

  $form_state['league_node'] = $node;

  $form['export'] = array(
      '#title' => t('Export'),
      '#type' => 'fieldset',
      '#attributes' => array('class' => array('export-fieldset')),
  );

  // Mangle checkbox
  $form['export']['mangle'] = array(
   '#type'  => 'checkbox',
   '#title'  => t('Mangle For Prviacy'),
   '#description' => t('Change DOB and emails for data protection.'),
  );

  $form['export']['button'] = array(
      '#value' => t('Export Organisation Data'),
      '#type' => 'submit',
      '#submit' => array('league_export_form_submit'),
  );

  $form['import'] = array(
      '#title' => t('Import'),
      '#type' => 'fieldset',
      '#attributes' => array('class' => array('export-fieldset')),
  );

  // Required to do file uploads
  $form['#attributes'] = array('enctype' => "multipart/form-data");

  // File upload button
  $form['import']['impfile'] = array(
    '#type' => 'file',
    '#title' => t('LMS JSON File to Upload'),
    '#description' => t('Format should be as exported via the Export option or via the Export button in Admin/Season.'),
    '#size' => 64,
  );

  // Import button.
  $form['import']['button'] = array(
     '#value' => t('Import Seasons'),
     '#type'  => 'submit',
     '#submit' => array('league_export_form_import'),
  );

  return $form;
}

function league_export_whole_organisation($node, $mangle=FALSE, $fileName ="export.json") {
  $data = league_export_create();
  // Export the organisation data
  league_export_organisation_data($data, $node, $mangle);
  // Get list of seasons for the organisation
  $seasons = league_season_list($node->nid, array('A','N','O') );
  foreach ( $seasons as $sid => $name ) {
    $season = league_event_load($sid);
    league_export_season_data($data, $season, $mangle);
  }
  // Export all the clubs
  $clubs = league_club_status($node->nid, Array('A','F','N') );
  foreach ( $clubs as $nid => $sdata ) {
    league_export_add_club($data, $nid, $node->nid, $sdata['status'], $mangle);
  }

  // Export rating list mappings
  $rlists = rating_list_list_details('ecf');
  foreach($rlists as $rlid => $ldata) {
    league_export_add_rlist($ldata, $rlid, $data);
  }
  $rlists = rating_list_list_details('fide');
  foreach($rlists as $rlid => $ldata) {
    league_export_add_rlist($ldata, $rlid, $data);
  }

  // Let other modules react
  module_invoke_all('league_export', $data, $mangle);
  league_export_json($data, $fileName);
}

/**
 *  Action after the form is submitted.
 */
function league_export_form_submit($form, &$form_state) {
  $node = $form_state['league_node'];
  $mangle = $form_state['values']['mangle'];

  if(league_check_access($node->nid)) {
    // Export the organisation data
    league_export_whole_organisation($node, $mangle);

  } else {
    drupal_set_message("You do not have access to this option");
  }
}

function league_export_json($data, $fileName ="export.json") {
  if(drupal_is_cli() ) {
    file_put_contents($fileName, json_encode( $data ) );
  } else {
    drupal_add_http_header('Content-Type', 'application/json');
    drupal_add_http_header('Content-Disposition', 'attachment; filename="' . $fileName . '"');
    drupal_add_http_header('Cache-Control',  'max-age=0');
    echo json_encode( $data );
    exit;
  }
}

function league_export_club_data($data, $club, $org, $status, $mangle) {
  $node = node_load($club);
  $text = '';
  if(isset($node->body['und'][0]['value']) ) {
    $text = $node->body['und'][0]['value'];
  }
  $code = 'XXXX';
  if(isset($node->field_ecf_club_code) ) {
    $code = $node->field_ecf_club_code['und'][0]['value'];
  }
  $gmap = '';
  if(isset($node->field_google_map['und']) ) {
    $gmap = $node->field_google_map['und'][0]['value'];
  }

  $club_data = array(
    'id' => $node->nid, 
    'name' => league_club_value($node, 'title'), 
    'contact' => league_club_value($node, 'contact'), 
    'limit' => league_club_value($node, 'limit'), 
    'time' => league_club_value($node, 'time'), 
    'code' => $code,
    'text' => $text,
    'status' => $status,
    'list' => league_player_object_list($node->nid, $org),
    'gmap' => $gmap,
  );

  // If there is a list, then add it to the list of lists 
  // ( there is one list per org )
  $list = league_player_object_list($club, $org);
  if( $list != 0 ) {
    league_export_add_list($data, $list, $mangle);
  }
  $club_data['list'] = $list;

  if( !isset($club_data['contact']) || strlen($club_data['contact'])==0 ) {
    $club_data['contact'] = 0;
  }
  // Add contact to users list
  if( $club_data['contact'] != 0 ) {
    league_export_add_user($data, $club_data['contact'], $mangle);
  }

  // Add any club owners to the users list and the event json
  $owners = user_tools_owners_list('B', $node->nid, array('O'), 'name');
  $club_data['owners'] = $owners;
  foreach($owners as $uid => $name) {
    league_export_add_user($data, $uid, $mangle);
  }

  return $club_data;
}

function league_export_list_data($data, $lid, $mangle) {
  $list = league_player_list_load($lid);
  if( is_object($list) ) {
    $list_data = array(
      'name'   => $list->name, 
      'lid'    => $list->lid, 
      'players' => league_player_players_in_lists_export($lid),
    );
  } else {
    $list_data = array(
      'name'   => 'unknown', 
      'lid'    => 0, 
      'players' => [],
    );
  }
  foreach($list_data['players'] as $pid) {
    league_export_add_player($data, $pid, $mangle);
  }
  return $list_data;
}

function league_export_rlist_data($data, $rlid) {
  $list_data = array(
    'name'   => $data['name'], 
    'rate'   => $data['rate'], 
    'list'   => $data['list'], 
    'rlid'    => $rlid, 
  );
  return $list_data;
}

function league_export_load_player($pid) {
  $result = db_query("SELECT * from league_player WHERE pid=$pid");
  $player = $result->fetch();
  return $player;
}

function league_export_set_if_not_set($object, $property, $default) {
  if( !isset($object->$property) ) {
    $object->$property = $default;
  }
}

function league_export_player_data($pid, $mangle) {
  $player = (object)league_export_load_player($pid);

  // Mangle the dob
  $dob = 0;
  if( isset($player->dob) ) {
    $dob = $player->dob;
  }
  // Add or subtract a random number of days.
  if( isset($dob) && $dob != 0 && $mangle ) {
    $dob += ( 60 - rand(0,120) ) * 60*60;
  }

  // Set sex ( some of no value )
  $sex = 'U';
  if( isset($player->sex) ) {
    $sex = $player->sex;
  }

  league_export_set_if_not_set($player, 'lastname', 'unknown');
  league_export_set_if_not_set($player, 'firstname', 'unknown');
  league_export_set_if_not_set($player, 'chess_fidecode', 0);
  league_export_set_if_not_set($player, 'chess_rating', 0);
  league_export_set_if_not_set($player, 'chess_rapid', 0);
  league_export_set_if_not_set($player, 'chess_lichess', '');
  league_export_set_if_not_set($player, 'chess_comchess', '');
  league_export_set_if_not_set($player, 'chess_club', '');
  league_export_set_if_not_set($player, 'ecf_code', 0);
  league_export_set_if_not_set($player, 'chess_federation', '');
  
  $player_data = array(
    'dob'   => $dob, 
    'sex'   => $sex, 
    'lastname'   => $player->lastname, 
    'firstname'   => $player->firstname, 
    'club'   => $player->chess_club, 
    'ecf_code'   => $player->ecf_code, 
    'chess_fidecode'   => $player->chess_fidecode, 
    'chess_rating'   => $player->chess_rating, 
    'chess_rapid'   => $player->chess_rapid, 
    'chess_federation'   => $player->chess_federation, 
    'chess_lichess'   => $player->chess_lichess, 
    'chess_comchess'   => $player->chess_comchess, 
  );
  return $player_data;
}

function league_export_season_data($data, $season, $mangle) {
  $season_data = array(
    'name'   => $season->name, 
    'sid'    => $season->eid, 
    'status' => $season->lms,
    'display_order' => $season->display_order,
    'events' => array()
  );
  $event_list = league_season_events($season->eid);
  foreach( $event_list as $cid ) {
    $event_data = league_export_event_data($data, $cid, $mangle);
    $season_data['events'][$cid] = $event_data;
  }
  $data->seasons[$season->eid] = $season_data;
}

function league_export_event_data($data, $cid, $mangle) {
  $node = node_load($cid);
  $text = '';
  if(isset($node->body['und'][0]['value']) ) {
    $text = $node->body['und'][0]['value'];
  }
  $event_data = array(
    'name' => league_event_value($cid, 'title'),
    'type' => league_event_value($cid, 'type'), 
    'ntimes' => league_event_value($cid, 'ntimes'), 
    'handicap' => league_event_value($cid, 'handicap'), 
    'two_matches' => league_event_value($cid, 'twomatch'), 
    'order' => league_event_value($cid, 'order'), 
    'graded_games' => $node->league_ecf_games,
    'sorts' => $node->league_sorts,
    'cols' => $node->league_cols,
    'compid' => $node->league_compid,
    'updated' => $node->changed,
    'updater' => $node->uid,
    'text' => $text,
    'list' => league_player_event_value($cid, 'list'),
    'nboards' => league_chess_event_value($cid, 'boards'),
    'grade_limit' => league_chess_event_value($cid, 'limit'),
    'default_grade' => league_chess_event_value($cid, 'default_grade'),
    'below_default' => league_chess_event_value($node, 'below_default'),
    'rating_type' => league_chess_event_value($node, 'rate'),
    'rating_type2' => league_chess_event_value($node, 'brate'),
    'chess_platform' => league_chess_event_value($node, 'platform'),
    'ecf_treasurer' => league_ecf_event_value($node, 'treasurer'),
    'ecf_auto' => league_ecf_event_value($node, 'auto'),
    'ecf_mem_only' => league_ecf_event_value($node, 'ecf_mem_only'),
    'ecf_grader' => league_ecf_event_value($node, 'grader'),
    'ecf_subno' => $node->league_ecf_subno,
    'teams' => array(),
    'fixtures' => array(),
  );
  $fixture_list = league_export_fixtures_list($cid);
  foreach( $fixture_list  as $fixture) {
    $fixture_data = league_export_fixture_data($data, $fixture, $mangle);
    $event_data['fixtures'][$fixture->nid] = $fixture_data;
  }
  // Add the teams
  $teams = league_export_teams_list($cid);
  foreach($teams as $team) {
    $event_data['teams'][$team->nid] = league_export_team_data($data, $team, $mangle);
  }
  // If there is a list, then add it to the list of lists 
  if( $event_data['list'] != 0 ) {
    league_export_add_list($data, $event_data['list'], $mangle);
  }
  // Add any event owners to the users list and the event json
  $owners = user_tools_owners_list('E', $cid, array('O'), 'name');
  $event_data['owners'] = $owners;
  foreach($owners as $uid => $name) {
    league_export_add_user($data, $uid, $mangle);
  }
  return $event_data;
}

function league_export_team_data($data, $node, $mangle) {
  $club = league_team_value($node, 'club');
  $code = league_chess_club_value($club, 'ecfcode');
  $team = array(
    'title' => league_team_value($node, 'title'), 
    'id' => $node->nid, 
    'night' => league_team_value($node, 'night'), 
    'night2' => league_team_value($node, 'night2'), 
    'order' => league_team_value($node, 'order'), 
    'club' => $code,
    'captain' => league_team_value($node, 'captain'), 
    'penalty' => league_team_value($node, 'penalty'), 
  );
  // Add team captain to users in $data.
  if( !isset( $team['captain'] ) ) {
    $team['captain'] = 0;
  }
  if( $team['captain'] !=0 ) {
    league_export_add_user($data, $team['captain'], $mangle);
  }
  return $team;
}

function league_export_add_user($data, $uid, $mangle) {
  if( !array_key_exists($uid, $data->users) ) {
    $user_data = league_export_user_data($uid, $mangle);
    if( $user_data ) {
      $data->users[$uid] = $user_data;
    }
  }
}

function league_export_add_rlist($ldata, $rlid, $data) {
  if( !array_key_exists($rlid, $data->rlists) ) {
    $data->rlists[$rlid] = league_export_rlist_data($ldata, $rlid);
  }
}

function league_export_add_club($data, $nid, $org, $status, $mangle) {
  $club_code = league_chess_club_value($nid, 'ecfcode');
  if( !array_key_exists($club_code, $data->clubs) ) {
    $data->clubs[$club_code] = league_export_club_data($data, $nid, $org, $status, $mangle);
  }
}

function league_export_add_list($data, $lid, $mangle) {
  if( !array_key_exists($lid, $data->lists) ) {
    $data->lists[$lid] = league_export_list_data($data, $lid, $mangle);
  }
}

function league_export_add_player($data, $pid, $mangle) {
  if($pid > 0 ) {
    if( !array_key_exists($pid, $data->players) ) {
      $data->players[$pid] = league_export_player_data($pid, $mangle);
    }
  }
}

function league_export_fixture_data($data, $node, $mangle) {
  if(isset($node->revision_uid)) {
    $updaterUid = $node->revision_uid;
  } else {
    $updaterUid = $node->uid;
  }


  $fixture_data = array(
    'title' => league_fixture_value($node, 'title'), 
    'homeTeam' => league_fixture_value($node, 'homeTeam'), 
    'awayTeam' => league_fixture_value($node, 'awayTeam'), 
    'date' => $node->league_date,
    'date_option' => league_fixture_value($node, 'date_option'), 
    'locked' => league_fixture_value($node, 'locked'), 
    'winner' => league_fixture_value($node, 'winner'), 
    'round' => league_fixture_value($node, 'round'), 
    'table' => league_fixture_value($node, 'table'), 
    'verifier' => league_fixture_value($node, 'verifier'), 
    'verify_time' => league_fixture_value($node, 'verify_time'), 
    'reporter' => league_fixture_value($node, 'reporter'), 
    'report_time' => league_fixture_value($node, 'report_time'), 
    'updater' => $updaterUid,
    'update_time' => $node->changed,
    'violations' => league_get_violations($node->nid),
  );
 
  // Add the reporter and verifier to the users list
  $fixture_data['verifier'] = league_export_check_add_user($data, $fixture_data['verifier'], $mangle);
  $fixture_data['reporter'] = league_export_check_add_user($data, $fixture_data['reporter'], $mangle);
  
  // Get Comments
  $path = drupal_get_path('module', 'league_chess');
  require_once("$path/league_chess_press_report.inc");
  $comments = league_chess_fixture_comments($node->nid);
  // Add comments authors
  foreach($comments as &$comment) {
    $comment['uid'] = league_export_check_add_user($data, $comment['uid'], $mangle);
  }
  // Save comments
  $fixture_data['comments'] = $comments;

  return $fixture_data;
}

function league_export_check_add_user($data, $uid, $mangle) {
  if( !isset($uid) || strlen($uid)==0 ) {
    $uid = 0;
  }
  // Add contact to users list
  if( $uid != 0 ) {
    league_export_add_user($data, $uid, $mangle);
  }
  return $uid;
}

function league_random_string($length = 10) {
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}

function league_export_user_data($uid, $mangle) {
  $user = user_load($uid);
  if( !is_object($user) ) {
    return FALSE;
  }
  $user_data = array(
    'uname' => $user->name, 
    'mail' => $user->mail, 
    'pass' => $user->pass, 
  );

  if($mangle) {
    $user_data['mail'] = league_random_string() . '@something.com';
  }

  if(isset($user->field_name['und']) ) {
    $user_data['name'] = $user->field_name['und'][0]['value'];
  }
  if(isset($user->field_phone['und']) ) {
    $user_data['phone'] = $user->field_phone['und'][0]['value']; 
  }
  if(isset($user->field_mobile['und']) ) {
    $user_data['mobile'] = $user->field_mobile['und'][0]['value']; 
  }
  if(isset($user->field_address['und']) ) {
    $user_data['address'] = $user->field_address['und'][0]['value']; 
  }
  return $user_data;
}

/**
 *  Get a list of teams.
 */
function league_export_teams_list($event) {
  $teams = league_event_teams($event);
  $nodes = array();
  foreach ( $teams as $nid => $name) {
    $nodes[] = node_load($nid);
  }
  return $nodes;
}

/**
 *  Get a list of fixtures.
 */
function league_export_fixtures_list($event) {
  $fixtures = array();
  $query = db_select('league_fixture', 'f');
  $query->condition('f.cid', $event);
  $query->fields('f', array('nid'));
  $result = $query->execute();
  foreach ( $result as $row) {
    $nid = $row->nid;
    $fixtures[] = node_load($nid);
  }
  return $fixtures;
}

function league_import_file_validate_name(stdClass $file, $org) {
  $errors = array();
  $file->filename = "Import$org.json";
  $path = 'public://league/';
//drupal_set_message(" league_import_file_validate_name org $org");
  $file->destination = file_destination($path.$file->filename, FILE_EXISTS_REPLACE);
  if ($file->destination === FALSE) {
    $errors[] = t('The file %source could not be uploaded because a file by that name already exists in the destination %directory.',
    array('%source' => $file->source, '%directory' => $path));
  }
  return $errors;
}

/**
 *  Action after the form is submitted.
 */
function league_export_form_import($form, &$form_state) {
  $node = $form_state['league_node'];
  $org = $node->nid;
  // Check for access
  if (!league_check_access($org)) {
    drupal_set_message('You do not have access to do this','warning');
    return;
  }

  // Limit to files with the .xls suffix
  $validators = array(
   'file_validate_extensions' => array('json JSON'),
   'file_validate_size' => array(2000000, 0),
   'league_import_file_validate_name' => array($org),
  );
  // Create league files subdirectory if it doesn't exist
  $path = 'public://league';
  if (!file_prepare_directory($path, FILE_CREATE_DIRECTORY)) {
    drupal_set_message(t('Could not create file @file', array('@file' => $path)), 'error');
    return;
  }
  // Remove the previous log file for this organisation
  $log_file = league_import_log_file_name($org);
  if(file_exists($log_file) ) {
    unlink($log_file);
  }
  $now = date("Y/m/d H:i:s");
  league_import_log($org, "Starting import at $now");
  // Upload the file
  if ($xlsfile = file_save_upload('impfile', $validators, $path, FILE_EXISTS_REPLACE)) {
    $filename = $xlsfile->destination;
    drupal_set_message(t('Uploaded @file', array('@file' => $filename)));
    drupal_goto("league/import/$org/v");
  } else {
    drupal_set_message(t('File missing or Upload failed'));
  }
}

/**
 *  Action after the form is submitted.
 */
function league_import_confirm_form_submit($form, &$form_state) {
  $org = $form['#org'];
  $node = node_load($org);
//drupal_set_message("league_import_confirm_form_submit $org");
  league_import_json($node, 'i');
}

function league_import_json($node, $option) {
  $org = $node->nid;
  // Check for access
  global $user;
  if ( !in_array('administrator', $user->roles) && !in_array('ecf_admin', $user->roles)) {
//if (!league_check_access($org)) {
    drupal_set_message('You do not have access to do this','warning');
    drupal_goto("node/$org/league/admin/export");
    return;
  }
  $path = "public://league/Import$org.json";
  $filename = drupal_realpath($path);
  if($option == 'v') {
    if( league_import_validate_file($option, $filename) ) {
      return drupal_get_form('league_import_confirm_form', $org);
    } else {
      drupal_set_message('Import file contained errors','warning');
      drupal_goto("node/$org/league/admin/export");
      return;
    }
  } else { 
    $json = file_get_contents($filename);
    $data = json_decode($json, true);
    $n_seasons = count($data['seasons']);
    $module_file = drupal_get_path('module', 'league') .'/league_admin_admin.inc';
    $operations = array(
      array('league_export_load_file', array($data,$org)),
    );
    $batch = array(
        'operations' => $operations,
        'finished' => 'league_import_finished',
        'title'    => 'Import JSON to LMS',
        'init_message' => 'Load Starting. Uploading file ...',
        'file' => $module_file,
    );
    batch_set($batch);
  }
}

function league_import_confirm_form ($form, &$form_state, $org) {
  $data = league_import_validate_file('c');
  $message = league_import_file_counts($data);
  $url = "node/$org/league/admin/export";
  $form = confirm_form($form,
      "Are you sure, Do you want to import data into the LMS organisation ?",
      $url,
      $message,
      'Import Data',
      'Cancel'
  );
  $form['#org'] = $org;
  return $form;
}

function league_import_file_counts($data) {
  $nclubs = 0;
  $nplayers = 0;
  $nseasons = count($data['seasons']);
  $nevents = 0;
  $nfixtures = 0;
  $nresults = 0;
  $nusers = 0;
  foreach($data['seasons'] as $sid => $season) {
    foreach($season['events'] as $cid => $event) {
      $nevents++;
      foreach($event['fixtures'] as $fid => $fixture) {
        $nfixtures++;
        foreach($fixture['matches'] as $match) {
          foreach($match['results'] as $result) {
            $nresults++;
          }
        }
      }
    }
  }
  $message = "The action cannot be undone. This will result in the loading of $nclubs clubs, $nplayers players, $nseasons seasons, $nevents events, $nfixtures fixtures, $nresults results $nusers users";
  return $message;
}

// Validate the imported file.
function league_import_validate_file($option, $filename=NULL) {
  static $data = array();
  if($option == 'v') {
    $json = file_get_contents($filename);
    $data = json_decode($json, true);
    if( array_key_exists('seasons',  $data) ) {
//    drupal_set_message('seasons exists in JSON');
      return TRUE;
    } else {
//    drupal_set_message('Seasons not found in file','error');
      return FALSE;
    }
  } else {
    return $data;
  }
}

function league_export_load_file($data,$org, &$context) {
  watchdog('league',"league_export_load_file $org");
  $nseasons = count($data['seasons']);
  watchdog('league',"Loading $nseasons seasons");
  if(!isset($context['sandbox']['current'])) {
    $context['sandbox']['org'] = $org;
    $context['sandbox']['current'] = 0;
    $context['sandbox']['data'] = $data;
    // Create list of objects to process
    $objects = array();
    foreach($data['seasons'] as $sid => $season) {
      $object = array('type' =>'s', 'sid' => $sid);
      $objects[] = $object;
      foreach($season['events'] as $cid => $event) {
        $object = array('type' =>'e', 'sid' => $sid, 'cid' =>$cid);
        $objects[] = $object;
      }
    }
    $context['sandbox']['objects'] = $objects;
    $n_objects = count($context['sandbox']['objects']);
    watchdog('league',"Number of objects to import $n_objects");
    $current = 0;
    $context['finished'] = $current / $n_objects;
    $context['message'] = "Imported $current objects from $n_objects";
  } else {
    $current = league_import_load_batch($context);
  }
  $context['sandbox']['current'] = $current;
}

function league_import_finished($success,$results,$operations) {
  $org = $results['org'];
  if( $success ) {
    drupal_set_message("Import Complete");
    league_import_log($org, "Import Completed OK");
  } else {
    drupal_set_message("Import Failed");
    league_import_log($org, "Import Failed");
    reset($operations);
  }
  $now = date("Y/m/d H:i:s");
  league_import_log($org, "Finished import at $now");
  drupal_goto("node/$org/league/admin/export");
}

function league_import_load_batch(&$context) {
  $sandbox = $context['sandbox'];
  $objects = $sandbox['objects'];
  $data = $sandbox['data'];
  $current = $sandbox['current'];
  $org = $sandbox['org'];
  $errors = 0;
  $created = 0;

  league_import_process_object($objects[$current], $org, $context['sandbox']['data']);
  $n_objects = count($objects);
  $current++;
  $context['results'] = array( 'org'    => $org,);

  // set finished flag
  if( $current >= $n_objects) {
    $context['finished'] = 1;
    $context['message'] = "Finished importing $n_objects objects";

    drupal_set_message("imported $n_objects objects");
} else {
    $context['finished'] = $current / $n_objects;
    $context['message'] = "Imported $current objects from $n_objects";
  }

  return $current;
}

function league_import_process_object($object, $org, &$data) {
  switch ($object['type']) {
    case 's' : $sid = league_import_process_object_season($object, $org, $data);
               $data['seasons'][$object['sid']]['sid'] = $sid;
               break;
    case 'e' : league_import_process_object_event($object, $org, $data);
               break;
  }
}
function league_import_process_object_season($object, $org, $data) {
  $sid = $object['sid'];
  $name = $data['seasons'][$sid]['name'];
  league_import_log($org, "Processing season $sid named $name org $org");
  // Create new season 
  $season = entity_get_controller('league_event')->create($org);
//$season->name = substr('IMPORT ' . $name,0,30);
  $season->name = $name;
  $season->lms = 'A';
  league_event_save($season);
  // create new season and return the sid
  $new_sid = $season->eid;
  league_import_log($org, "Created season sid $new_sid named $name org $org");
  return $new_sid;
}
function league_import_process_object_event($object, $org, $data) {
  $sid = $object['sid'];
  $new_sid = $data['seasons'][$sid]['sid'];
  $cid = $object['cid'];
  $event_data = $data['seasons'][$sid]['events'][$cid];
  $name = $event_data['name'];
  league_import_log($org, "Processing event $cid for org $org named $name");
  // create the event in the season, then all its fixtures/matches/results.
  $node = league_create_event($org);
  $node->title = $name;
  // owners??
  $node->league_type = $event_data['type'];
  $node->league_eid = $new_sid;
  $node->league_event_order = $event_data['order'];
  $node->league_handicap = $event_data['handicap'];
  $node->league_two_matches = $event_data['two_matches'];
  $node->league_ntimes = $event_data['ntimes'];
  $node->league_cols = $event_data['cols'];
  $node->league_sorts = $event_data['sorts'];
  // TODO - test if this works with quotes and stuff in it
  $node->body['und'][0]['value'] = $event_data['text'];
  module_invoke_all('league_import', 'event', $node, $event_data, $org);
  node_save($node);
  foreach( $event_data['fixtures'] as $fid => $fixture ) {
    league_import_process_fixture($node->nid, $fixture, $org);
  }
  // The compid is set on creation, so we need to reset it if it does not eixst
  // it should be zero for new imports from other systems
  $compid = $event_data['compid'];
  if( $compid != 0 ) {
    if( ! league_competition_exists($compid) ) {
      league_change_event_compid($node, $compid);
    }
  }
  // These fields need to be updated seperately as node_save sets them itself
  league_set_node_updater($node, $event_data['updater'], $event_data['updated']);
}

function league_import_process_fixture($cid, $fixture, $org) {
//$fid = $fixture['fid'];
  $title = $fixture['title'];
  $home = $fixture['homeTeam'];
  $away = $fixture['awayTeam'];
  $date = $fixture['date'];
  $round = $fixture['round'];
  $table = $fixture['table'];
  league_import_log($org, "Processing fixture $title for event $cid");
  $node = league_create_fixture($cid, $home, $away, $date, $round, $table, FALSE);
  $node->league_date_option = $fixture['date_option'];
  $node->league_date = $fixture['date'];
  $node->league_locked = $fixture['locked'];
  $node->league_winner = $fixture['winner'];
  $node->league_verifier = $fixture['verifier'];
  $node->league_reporter =$fixture['reporter'];
  $node->league_report_time =$fixture['report_time'];
  $node->league_verify_time = $fixture['verify_time'];
  // Save node
  node_save($node);
  // These fields need to be updated seperately as node_save sets them itself
  league_set_node_updater($node, $fixture['updater'], $fixture['update_time']);

  // Update the matches (which will have been created in node_save)
  $m=0;
  $fid = $node->nid;
  $node = node_load($fid);
  //TODO - test this with a 2 match fixture
  foreach( $fixture['matches'] as $mid => $match ) {
    
    league_import_process_match($match, $node->league_matches[$m], $org);
    $m++;
  }
}

function league_import_set_fixture_update($node, $updater, $update_time) {
  $nid = $node->nid;
  $query = db_update('node');
  $query->condition('nid', $nid);
  $query->fields(array('uid' => $updater, 'changed' => $update_time));
  $rows = $query->execute();
  drupal_set_message("league_import_set_fixture_update $nid $updater $update_time $nrows");
}

function league_import_process_match($match, $league_match, $org) {
  $league_match->home_score = $match['home_score'];
  $league_match->away_score = $match['away_score'];
  $league_match->home_penalty = $match['home_penalty'];
  $league_match->away_penalty = $match['away_penalty'];
  $league_match->home_adjustment = $match['home_adjustment'];
  $league_match->away_adjustment = $match['away_adjustment'];
  module_invoke_all('league_import', 'match', $league_match, $match['results'], $org);
}

function league_import_log_file_name($org) {
  $path = 'public://league';
  $log_file = drupal_realpath("$path/import$org.log");
  return $log_file;
}

function league_import_log($org, $text) {
  $log_file = league_import_log_file_name($org);
  file_put_contents($log_file, $text. "\n", FILE_APPEND);
}

function league_export_organisation_data($data, $node, $mangle) {
//drupal_set_message(print_r($node, TRUE));
  $unnamed = league_org_value($node,'unnamed');
  if( $unnamed == 'yes' ) {
    $unnamed = 'Y';
  } else {
    $unnamed = 'N';
  }
  $flag = league_org_value($node,'flag_comments');
  $flag_comments = 'Y';
  if($flag == 0) {
    $flag_comments = 'N';
  }
  $text = '';
  if( isset( $node->body['und'] ) ) {
    $text = $node->body['und'][0]['value'];
    $format = $node->body['und'][0]['format'];
    if( $format == 'plain_text' ) {
      $text = check_markup($text, $format);
    }
  }
  $settings = array(
    'name' => league_org_value($node,'title'),
    'users' => user_tools_owners_list('O', $node->nid, array('U'), 'name'),
    'owners' => user_tools_owners_list('O', $node->nid, array('O'), 'name'),
    'team_designation' => league_org_value($node,'designation'),
    'type' => league_org_value($node,'type'),
    'flag_comments' => $flag_comments,
    'unfinished' => league_org_value($node,'unfinished'),
    'unnamed' => $unnamed,
    'visibility' => league_org_value($node,'visibility'),
    'white_on_odds' => league_chess_org_value($node,'white_default'),
    'list_access' => league_org_value($node,'lists'),
    'grade_limit' => league_chess_org_value($node,'grade_limit'),
    'text' => $text,
  );
  $data->settings = $settings;
  foreach($settings['users'] as $uid => $name) {
    league_export_add_user($data, $uid, $mangle);
  }
  foreach($settings['owners'] as $uid => $name) {
    league_export_add_user($data, $uid, $mangle);
  }
}
