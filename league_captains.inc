<?php
/**
 * @file
 * Team Captains.
 */

/**
 * Team Captains.
 */
function league_admin_tab_captains($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);
  // Set title
  $title = 'Team Captains';
  drupal_set_title($title);

  if (!user_is_logged_in()) {
    $content['text']['#type'] = 'markup';
    $content['text']['#markup'] = '(Login to see email and phone.)';
  }
  $content['table']['#table'] = league_team_captains_table($node);
  $content['table']['#theme'] = 'league_table';
  return $content;
}

/** 
 * Get team captains for the season
 */
function league_team_captains_table($node) {
  $org = $node->nid;

  $header = array('Team', 'Event', 'Captain', 'Email', 'Telephone', 'Mobile');
  $rows = array();

  // Teams and their events
  $query = db_select('field_data_field_event', 'f');
  $query->fields('f', array('entity_id'));
  $query->innerJoin('node', 'n', 'n.nid = f.entity_id');
  $query->addField('n', 'title', 'team');
  $query->addField('n', 'nid', 'nid');
  $query->condition('f.entity_type', 'node');
  $query->condition('f.bundle', 'team');
  // Active Seasons for that organisation
  $query->innerJoin('league_competition', 'comp', 'comp.cid = f.field_event_nid');
  $query->innerJoin('league_event', 'le', 'le.eid = comp.eid');
  $query->condition('le.lms', 'A');
  $query->condition('le.org', $node->nid);
  $query->addField('le', 'name', 'seasonName');
  $query->addField('le', 'eid', 'eid');
  // Active Seasons for that organisation
  // event name
  $query->innerJoin('node', 'e', 'e.nid = f.field_event_nid');
  $query->addField('e', 'title', 'event');
  $query->addField('e', 'nid', 'eventid');
  // team captain
  $query->innerJoin('field_data_field_captain', 'cap', 'cap.entity_id = n.nid');
  $query->addField('cap', 'field_captain_value', 'uid');
  // email address
  $query->leftJoin('users', 'u', 'u.uid = cap.field_captain_value');
  $query->addField('u', 'mail', 'mail');
  // mobile
  $query->leftJoin('field_data_field_mobile', 'mob', 'mob.entity_id = u.uid');
  $query->addField('mob', 'field_mobile_value', 'mobile');
  // phone
  $query->leftJoin('field_data_field_phone', 'p', 'p.entity_id = u.uid');
  $query->addField('p', 'field_phone_value', 'phone');
  // name 
  $query->leftJoin('field_data_field_name', 'name', 'name.entity_id = u.uid');
  $query->addField('name', 'field_name_value', 'name');
  // Order by
  $query->orderBy('team', 'ASC');

  $result = $query->execute();
  foreach ( $result as $row) {
    $team = l($row->team, 'league_team/' . $row->entity_id);
    $event = l($row->event, 'node/' . $row->eventid);
    $captain = l($row->name, 'user/' . $row->uid . '/contact');
    if (league_user_in_org($org) ) {
      $phone = $row->phone;
      $mobile = $row->mobile;
      $mail = l($row->mail, 'mailto:' . $row->mail, array('absolute' => TRUE));
    } else {
      $phone = '*****';
      $mobile = '*****';
      $mail = '*****';
    }
    $rows[] = array($team, $event, $captain, $mail, $phone, $mobile);
  }

  $table['data'] = $rows;
  $table['header'] = $header;
  return $table;
}

