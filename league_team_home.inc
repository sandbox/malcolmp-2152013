<?php
// $Id$
/**
 * @file
 * Functions for league home tab
 */

/**
 * display content of team settings tab
 */
function league_team_edit_tab($node) {
  // Set title
  drupal_set_title($node->title);

  module_load_include('inc', 'node', 'node.pages');
  $form = drupal_get_form('team_node_form', $node);
  return $form;
}

/**
 * display content of league home tab
 */
function league_team_view_tab($node) {
  // Set bread crumb
  league_set_bread_crumb ($node);
  // Set title
  drupal_set_title($node->title);

  $rows = array();
  // Event
  $event = league_team_value($node, 'event');
  $eventName = l(league_event_value($event, 'title'), "league_comp/$event");
  $rows[] = array('Event', $eventName);

  // Match night(s)
  $days = league_get_days();
  $night = league_team_value($node, 'night');
  if($night >0) {
    $rows[] = array('Match Night', $days[$night]);
  }
  $night2 = league_team_value($node, 'night2');
  if($night2 >0) {
    $rows[] = array('Match Night 2', $days[$night2]);
  }
  
  // Club
  $club = league_team_value($node, 'club');
  $clubName = league_club_value($club, 'title');
  $rows[] = array('Club', $clubName);

  // Team Captain
  $captain = league_team_value($node, 'captain');
  if($captain > 0) {
    $name = league_user_value($captain, 'name');
    $rows[] = array('Team Captain', l($name, "user/$captain/contact") );
  }

  // Team order
  $order = league_team_value($node, 'order');
  $rows[] = array('Team Order', $order);

  // Team Penalty
  $penalty = league_team_value($node, 'penalty');
  $rows[] = array('Team Season Penalty', $penalty);

  $content = array(
    '#rows' => $rows,
    '#theme' => 'table',
  );

  return $content;
}
