<?php
/**
 * @file
 * Chess Database Module.
 */

/**
 * General Function to do a table
 */
function theme_league_table($variables) {
  $element = $variables['element'];
  $table = $element['#table'];
  $rows = $table['data'];     // Actual table data
  $header = $table['header']; // Headers

  if(isset($element['#pdfLink'])) {
    $pdfLink = TRUE;
  } else {
    $pdfLink = FALSE;
  }

  if(isset($element['#ical'])) {
    $ical = TRUE;
  } else {
    $ical = FALSE;
  }

  if(isset($element['#xurl'])) {
    $xurl = $element['#xurl'];
  } else {
    $xurl = ' ';
  }

  // Table heading
  if (isset($title)) {
    $output = "<h3 class=league-table-heading>$title</h3>";
  }
  else {
    $output = " ";
  }

  // Links for PDF and XL output.
  $output .= " ";
  if ( $pdfLink ) {
    $output .= '<div class=league-links>';
    // Override url if requested (in case of multiple tables per page)
    if ($xurl == ' ') {
      $curr_uri = $_GET['q'];
    }
    else {
      $curr_uri = $xurl;
    }
    $poptions = array('format' => 'pdf');
    $output .= l('PDF', $curr_uri, array('attributes' => array('target' => '_blank'), 'query' => $poptions));
    $output .= ", ";
    $xoptions = array('format' => 'xl');
    $output .= l('XL', $curr_uri,  array('attributes' => array(), 'query' =>$xoptions));
    if($ical) {
      $output .= ", ";
      $xoptions = array('format' => 'ical');
      $output .= l('ICAL', $curr_uri,  array('attributes' => array(), 'query' =>$xoptions));
    }
    $output .= '</div>';
  }

  // Create table as a web page
  $table = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows'  => $rows,
  );
  $output .= drupal_render($table);
  return $output;
}
