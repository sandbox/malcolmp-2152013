<?php
/*
 * Parse import xml file
 *
 */

class LeagueXmlParser {
  var $org;      // New org
  var $users;    // Array of users keyed by xml uid, contains real uid
  var $players;  // Array of players keyed by xml pid
  var $lists;    // Array of lists keyed by xml lid
  var $events;   // Array of events keyed by xml nid
  var $teams;    // Array of teams keyed by xml nid
  var $clubs;    // Array of clubs keyed by xml nid
  var $fixtures; // Array of fixtures keyed by xml nid
  var $results;  // Array of results keyed by xml rid
  var $current_type; // (user / player / list / event )
  var $current_id;   // (uid, pid, lid, event_id )
  var $current_fixture;
  var $current_event;
  var $current_team;
  var $current_result;
  var $debug;
  var $message;
  var $total;
  var $done;

  public function getMessage() {
    return $this->message;
  }

  public function LeagueXmlParser() {
    $this->org = array('attrs' => array(),
                       'users' => array(),
                 );    
    $this->users = array(); 
    $this->players = array();
    $this->lists = array();  
    $this->events = array();
    $this->teams = array();
    $this->clubs = array();
    $this->fixtures = array();
    $this->results = array();
    $this->current_type = 'league'; 
    $this->current_id = 0;
    $this->current_fixture = 0;
    $this->current_event = 0;
    $this->current_team = 0;
    $this->current_result = 0;
    $this->debug = FALSE;
    $this->message = 'OK';
    $this->total = 0;
    $this->done = 0;
  }
 
  public function start_element($parser, $name, $attrs) {
    if($this->debug) {
      echo "start_element $name\n";
    }
    switch($name) {
      case 'Users' : if($this->current_type == 'league') {
                       $this->current_type = 'user';
                     }
                     break;
      case 'User'  : if($this->current_type == 'user') {
                       $this->total++;
                       $this->users[$attrs['uid']] = $attrs;
                     }
                     if($this->current_type == 'org') {
                       $this->org['users'][] = $attrs;
                     }
                     if($this->current_type == 'event') {
                       $this->events[$this->current_id]['users'][] = $attrs;
                     }
                     if($this->current_type == 'club') {
                       $this->clubs[$this->current_id]['users'][] = $attrs;
                     }
                     break;
      case 'Organisation'  : $this->current_type = 'org';
                             $this->org['attrs'] = $attrs;
                       $this->total++;
                       break;
      case 'Players' : $this->current_type = 'player';
                       break;
      case 'Player'  : $this->players[$attrs['pid']] = $attrs;
                       $this->total++;
                       break;
      case 'PlayerLists' : $this->current_type = 'list';
                       break;
      case 'PlayerList' : $this->lists[] = $attrs;
                       $this->total++;
                       break;
      case 'Clubs' : $this->current_type = 'club';
                       break;
      case 'Club'  : $this->current_id = count($this->clubs);
                     $this->clubs[] = array('attrs' => $attrs,
                                            'users' => array() );

                     $this->total++;
                     break;
      case 'Events'  : $this->current_type = 'event';
                     break;
      case 'Event'  :  $this->current_id = count($this->events);
                     $this->events[$this->current_id] = array('attrs' => $attrs,
                                            'users' => array(), );
                     $this->total++;
                     break;
      case 'Fixtures'  : $this->current_type = 'fixture';
                     break;
      case 'Fixture'  :  $this->current_fixture = count($this->fixtures);
                     // Set event id to current event
                     $attrs['event'] = $this->current_id;
                     $this->fixtures[$this->current_fixture] = $attrs;
                     $this->total++;
                     break;
      case 'Teams'  : $this->current_type = 'team';
                     break;
      case 'Team'  :  $this->current_team = count($this->teams);
                     // Set event id to current event
                     $attrs['event'] = $this->current_id;
                     $this->teams[$this->current_team] = $attrs;
                     $this->total++;
                     break;
      case 'Results'  : $this->current_type = 'result';
                     break;
      case 'Result'  :  $this->current_result = count($this->results);
                     // Set fixture id to current fixture
                     $attrs['fixture'] = $this->current_fixture;
                     $this->results[] = $attrs;
                     $this->total++;
                     break;
    }

  }
  public function end_element($parser, $name) {
  }
  public function character_data($parser, $data) {
  }

  public function parse_file($in_file, $debug) {
    $xml_parser = xml_parser_create();
    xml_set_element_handler($xml_parser, array($this,"start_element"), array($this,"end_element") );
//  xml_set_character_data_handler($xml_parser, array($this,"character_data") );
    xml_parser_set_option($xml_parser, XML_OPTION_CASE_FOLDING, 0);
    if (!($fp = fopen($in_file, "r"))) {
      $this->message = "could not open XML input";
      return FALSE;
    }
    while ($data = fread($fp, 4096)) {
      $data = str_replace(array("\n", "\r", "\t"), '', $data);
      if (!xml_parse($xml_parser, $data, feof($fp))) {
        $this->message = sprintf("XML error: %s at line %d",
                      xml_error_string(xml_get_error_code($xml_parser)),
                      xml_get_current_line_number($xml_parser));
        return FALSE;
      }
    }
    xml_parser_free($xml_parser);

    $this->current_type = 'league'; 
    $this->current_id = 0;

    return TRUE;
  }
}

function league_import_file($parser, $debug, &$context) {
//watchdog('league',"Loading org " . print_r($parser, TRUE) );
  if(!isset($context['sandbox']['parser'])) {
    $context['sandbox']['parser'] = $parser;
  }
  league_import_batch($context);
}

function league_import_queues($debug, $total, $queue, &$context) {
//watchdog('league',"Loading org " . print_r($parser, TRUE) );
  if(!isset($context['sandbox']['done'])) {
    $context['sandbox']['done'] = 0;
    $context['sandbox']['total'] = $total;
  }
  $sandbox = $context['sandbox'];
  $done = $sandbox['done'];

  if($item = $queue->claimItem() ) {
    league_import_process_item($item->data, $debug);
    $queue->deleteItem($item);
    $context['finished'] = $done / $total;
    $context['message'] = "Imported $done objects from $total";
  } else {
  // set finished flag in sandbox
    $context['finished'] = 1;
    $context['message'] = "Finished reading";
    drupal_set_message("Imported $total objects");
  }
}

function league_import_org($data) { 
  $node = league_import_create_node('organisation');
  //TODO users ?
  $node->title = $data['attrs']['name'];
  //TODO - replaced need to import seasons
//$node->field_current_season['und'][0]['value'] = $data['attrs']['season'];
  node_save($node);
  $data['attrs']['newid'] = $node->nid;
  if($data['debug']) {
    drupal_set_message("Importing org, created node:" . $node->nid);
  }
  league_import_set_mapping('organisation', 0, $node->nid);
}

function league_import_set_mapping($type, $old, $new) {
  $mapping = (object) array('type' => $type, 'id' => $old, 'newid' => $new);
  drupal_write_record('league_import_mapping', $mapping);
}

function league_import_user($parser) { 
  $user_keys = array_keys($parser->users);
  $new_user = $parser->users[$user_keys[$parser->current_id]];
  // Check if user exists for the email
  $email = $new_user['mail'];
  drupal_set_message("checking user mail $email id " . $parser->current_id);
  $user = user_load_by_mail($new_user['mail']);
  // If user doesn't exist, create one.
  if(! $user ) {
    drupal_set_message("creating user");
//  $password = user_password(8);
    $password = $new_user['pass'];
    $user = array(
       'name' => $new_user['mail'], // use mail incase one with same name exist
       'mail' => $new_user['mail'],
       'pass' => $password,
       'status' => 1,
       'init' => 'email address',
       'roles' => array(), // No other roles than Authenticated
    );
    $user = user_save(NULL, $user);
    if($parser->debug) {
      drupal_set_message("Created user:" . $parser->current_id . ' uid ' . $user->uid);
    }
    $user->field_name['und'][0]['value'] = $new_user['field_name'];
    $user->field_phone['und'][0]['value'] = $new_user['field_phone'];
    $user->field_mobile['und'][0]['value'] = $new_user['field_mobile'];
    $user->field_address['und'][0]['value'] = $new_user['field_address'];
    user_save($user);
  }
  $new_user['newid'] = $user->uid;

  if($parser->debug) {
    drupal_set_message("Importing user:" . $parser->current_id . ' uid ' . $user->uid);
  }
}
function league_import_club($parser) { 
  $club_keys = array_keys($parser->clubs);
  $new_club = $parser->clubs[$club_keys[$parser->current_id]];
  // seach for club by ecfcode and create if needed
  $clubCode = $new_club['attrs']['ecfcode'];
  $club_id = league_ecf_club_for_ecf_code(0, $clubCode);
  if($club_id == 0) {
    drupal_set_message("Creating club:" . $clubCode);
    // create club
    $node = league_import_create_node('club');
    $node->title = $new_club['attrs']['name'];
    $node->field_ecf_club_code['und'][0]['value'] = $new_club['attrs']['ecfcode'];
//  $node->field_player_list['und'][0]['value'] = $new_club['attrs']['list'];
//  $node->field_org['und'][0]['nid'] = $parser->org['attrs']['newid'];
    node_save($node);
    $club_id = $node->nid;
  }
  $new_club['attrs']['newid'] = $club_id;
  // set node fields
  if($parser->debug) {
    drupal_set_message("Importing club:" . $parser->current_id);
  }
}
function league_import_list($parser) { 
  // create new list 
  // add all players to it
  if($parser->debug) {
    drupal_set_message("Importing list:" . $parser->current_id);
  }
}
function league_import_player($parser) { 
  $org = $parser->org['attrs']['newid'];
  $player_keys = array_keys($parser->players);
  $new_player = $parser->players[$player_keys[$parser->current_id]];
  // store players new pid indexed by old pid
  $player = entity_get_controller('league_player')->create($org);
  $player->dob = $new_player['dob'];
  $player->sex = $new_player['sex'];
  $player->lastname = $new_player['lastname'];
  $player->firstname = $new_player['firstname'];
  $player->club = $parser->clubs[$new_player['club']]['newid'];
  $player->rating = $new_player['rating'];
  $player->rapid_rating = $new_player['rapid_rating'];
  $player->ecfcode = $new_player['ecfcode'];
  $player->membership = $new_player['membership'];
  $player->rating_category = $new_player['rating_category'];
  $player->fidecode = $new_player['fidecode'];
  league_player_save($player);
  $new_player['newid'] = $player->pid;
  
  if($parser->debug) {
    drupal_set_message("Importing player:" . $parser->current_id . ' pid ' . $player->pid);
  }
}
function league_import_event($parser) { 
  // store new event ids indexed by old
  if($parser->debug) {
    drupal_set_message("Importing event:" . $parser->current_id);
  }
}
function league_import_team($parser) { 
  // set event id from new index
  // store new team ids indexed by old
  if($parser->debug) {
    drupal_set_message("Importing team:" . $parser->current_id);
  }
}
function league_import_fixture($parser) { 
  // set event id from new index
  // store new fixture ids indexed by old
  // NOTE: don't set a date if its zero (or don't export it?)
  if($parser->debug) {
    drupal_set_message("Importing fixture:" . $parser->current_id);
  }
}
function league_import_result($parser) { 
  // set fixture id from new index
  // set white and black from new index
  if($parser->debug) {
    drupal_set_message("Importing result:" . $parser->current_id);
  }
}

function league_import_batch($context) {
  $sandbox = $context['sandbox'];
  $parser = $sandbox['parser'];
  switch ($parser->current_type) { 
    case 'league' : league_import_org($parser);
                    $parser->done++;
                    $parser->current_type = 'user'; 
                    $parser->current_id = 0;
                    watchdog('league', 'Completed Org ' . $parser->done);
                    break;
    case 'user' :   if($parser->current_id == count($parser->users) ) {
                      $parser->current_type = 'player';
                      $parser->current_id = 0;
                      watchdog('league', 'Completed Users ' . $parser->done);
                    } else {
                      league_import_user($parser);
                      $parser->done++;
                      $parser->current_id++;
                    }
                    break;
    case 'player' : if($parser->current_id == count($parser->players) ) {
                      $parser->current_type = 'list';
                      $parser->current_id = 0;
                      watchdog('league', 'Completed Players ' . $parser->done);
                    } else {
                      league_import_player($parser);
                      $parser->done++;
                      $parser->current_id++;
                    }
                    break;
    case 'list' :   if($parser->current_id == count($parser->lists) ) {
                      $parser->current_type = 'club';
                      $parser->current_id = 0;
                      watchdog('league', 'Completed Lists ' . $parser->done);
                    } else {
                      league_import_list($parser);
                      $parser->done++;
                      $parser->current_id++;
                    }
                    break;
    case 'club' :   if($parser->current_id == count($parser->clubs) ) {
                      $parser->current_type = 'event';
                      $parser->current_id = 0;
                      watchdog('league', 'Completed Clubs ' . $parser->done);
                    } else {
                      league_import_club($parser);
                      $parser->done++;
                      $parser->current_id++;
                    }
                    break;
    case 'event' :   if($parser->current_id == count($parser->events) ) {
                      $parser->current_type = 'team';
                      $parser->current_id = 0;
                      watchdog('league', 'Completed Events ' . $parser->done);
                    } else {
                      league_import_event($parser);
                      $parser->done++;
                      $parser->current_id++;
                    }
                    break;
    case 'team' :   if($parser->current_id == count($parser->teams) ) {
                      $parser->current_type = 'fixture';
                      $parser->current_id = 0;
                      watchdog('league', 'Completed Teams ' . $parser->done);
                    } else {
                      league_import_team($parser);
                      $parser->done++;
                      $parser->current_id++;
                    }
                    break;
    case 'fixture' :   if($parser->current_id == count($parser->fixtures) ) {
                      $parser->current_type = 'result';
                      $parser->current_id = 0;
                      watchdog('league', 'Completed Fixtures ' . $parser->done);
                    } else {
                      league_import_fixture($parser);
                      $parser->done++;
                      $parser->current_id++;
                    }
                    break;
    case 'result' :   if($parser->current_id == count($parser->results) ) {
                      $parser->current_type = 'none';
                      $parser->current_id = 0;
                      watchdog('league', 'Completed Results ' . $parser->done);
                    } else {
                      league_import_result($parser);
                      $parser->done++;
                      $parser->current_id++;
                    }
                    break;
  }
  // set finished flag in sandbox
  if( $parser->done >= $parser->total ) {
    $context['finished'] = 1;
    $context['message'] = "Finished reading";
    if($parser->debug) {
      watchdog('league',"league_import_batch " . print_r($parser, TRUE) );
    }
    drupal_set_message("Imported $parser->total objects");
  } else {
    $context['finished'] = $parser->done / $parser->total;
    $context['message'] = "Imported $parser->done objects from $parser->total";
  }
}

function league_parse_file($filename,$debug) {
  $start_time = time();

  $parser = new LeagueXmlParser();
  $parser->debug = $debug;
  if(!$parser->parse_file($filename,$debug)) {
    watchdog('league', 'Import FAILED in XML Parse: ' . $parser->getMessage() );
    return FALSE;
  } else {
    $end_time = time();
    $seconds = $end_time - $start_time;
    watchdog('league', "File parsed in $seconds seconds");
  }
  return $parser;
} 

function league_import_finished($success,$results,$operations) {
  watchdog('league', "league_import_finished");
  if( $success ) {
    watchdog('league', "import succeeded");
    drupal_set_message("File Loaded Successfully");
  } else {
    watchdog('league', "import failed");
    reset($operations);
  }
}

function league_import_create_node($type) {
  //create bare node
  $node = new StdClass();

  $node->type = $type;

  //give it a published status
  $node->status = 1;

  //set the user
  global $user;
  $node->uid = $user->uid;

  //set comments on for fixtures
  if($type == 'fixtures') {
    $node->comment = 2;
  } else {
    $node->comment = 1;
  }

  return $node;
}

function league_import_process_item($data) {
  watchdog('league', 'Process item ' . print_r($data, TRUE) );
  // create item dependent on type
  switch ($data['type']) { 
    case 'org' : league_import_org($data);
                 break;
//  case 'user' : league_import_user($data);
//                break;
//  case 'player' : league_import_player($data);
//                  break;
//  case 'list' :  league_import_list($data);
//                  break;
//  case 'club' :   league_import_club($data);
//                  break;
//  case 'event' :  league_import_event($data);
//                  break;
//  case 'team' :   league_import_team($data);
//                  break;
//  case 'fixture' : league_import_fixture($data);
//                  break;
//  case 'result' :  league_import_result($data);
//                  break;
  }
  // store mapping of new id
}

function league_import_queue_items($parser, &$total) {
  // Clear down import table
  $result = db_query("DELETE from league_import_mapping");
  // Get queue  
  $queue = DrupalQueue::get('league_import');
  $total = 0;
  // Clear down existing queues
  while($item = $queue->claimItem()) {
    // Load a batch of records.
    $queue->deleteItem($item);
  }

  // Queue users
  foreach( $parser->users as $user ) {
    $data = array( 'type' => 'user',
                   'debug' => $parser->debug,
                   'attrs' => $user,
            );
    $queue->createItem($data);
    $total++;
  }

  // Queue org
  $data = array( 'type' => 'org',
                 'debug' => $parser->debug,
                 'attrs' => $parser->org['attrs'],
                 'users' => $parser->org['users'],
          );
  $queue->createItem($data);
  $total++;

  // Queue Players
  foreach( $parser->players as $player ) {
    $data = array( 'type' => 'player',
                   'debug' => $parser->debug,
                   'attrs' => $player,
            );
    $queue->createItem($data);
    $total++;
  }

  // Queue Lists
  foreach( $parser->lists as $list ) {
    $data = array( 'type' => 'list',
                   'debug' => $parser->debug,
                   'attrs' => $list );
    $queue->createItem($data);
    $total++;
  }

  // Queue Clubs
  foreach( $parser->clubs as $club ) {
    $data = array( 'type' => 'club',
                   'debug' => $parser->debug,
                   'attrs' => $club['attrs'],
                   'users' => $club['users'],
            );
    $queue->createItem($data);
    $total++;
  }

  // Queue Events
  foreach( $parser->events as $event ) {
    $data = array( 'type' => 'event',
                   'debug' => $parser->debug,
                   'attrs' => $event['attrs'],
                   'users' => $event['users'],
            );
    $queue->createItem($data);
    $total++;
  }
                     
  // Queue Teams
  foreach( $parser->teams as $team ) {
    $data = array( 'type' => 'team',
                   'debug' => $parser->debug,
                   'attrs' => $team,
            );
    $queue->createItem($data);
    $total++;
  }

  // Queue Fixtures
  foreach( $parser->fixtures as $fixture ) {
    $data = array( 'type' => 'fixture',
                   'debug' => $parser->debug,
                   'attrs' => $fixture,
            );
    $queue->createItem($data);
    $total++;
  }
 
  // Queue Results
  foreach( $parser->results as $result ) {
    $data = array( 'type' => 'result',
                   'debug' => $parser->debug,
                   'attrs' => $result,
            );
    $queue->createItem($data);
    $total++;
  }
  return $queue;
}
