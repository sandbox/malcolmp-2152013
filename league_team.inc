<?php
// $Id$

// Team admin tab
function league_teams_tab($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);
  // Set title
  drupal_set_title($node->title);

  $event = $node->nid;
  // List of teams in event
  $teams = league_teams_for_event($event);
  // Check access
  if (league_check_access($event)) {
    $team = league_create_team($event);
    $content['form'] = drupal_get_form('league_team_form', $team, $teams);
  }

  $content['teams']['#content'] = league_event_teams_list($event,$teams);
  $content['teams']['#theme'] = 'league_divs_render';
  $content['teams']['#title'] = "Teams for $node->title";
  $content['teams']['#div'] = "teams";
  
  return $content;
}

function league_create_team($event) {
  $node = new StdClass();

  $node->type = 'team';

  //give it a published status
  $node->status = 1;

  //set the user
  global $user;
  $node->uid = $user->uid;

  $lang = LANGUAGE_NONE;
  $node->field_event[$lang][0]['nid'] = $event;

  return $node;
}

/**
 *  Form for adding a new team.
 */
function league_team_form($form, &$form_state, $node, $teams ) {

  $form['node'] = array(
      '#type' => 'value',
      '#value' => $node,    // Team node
  );

  $form['team'] = array(
      '#title' => t('Add Team'),
      '#type' => 'fieldset',
      '#attributes' => array('class' => array('teams-fieldset') ),
  );

  $event = $node->field_event['und'][0]['nid'];
  $org = league_event_value($event, 'org');
  $clubs = league_clubs_in_org($org, Array('A','F') );
  if(count($clubs) == 0) {
    $link = l('Add Clubs', "node/$org/league/clubs");
    drupal_set_message("You must $link before creating teams", 'error');
  } else {
    $form['team']['club'] = array(
        '#title' => t('Club'),
        '#type' => 'select',
        '#options' => $clubs,
        '#attributes' => array('class' => array('club-selector') ),
    );

    $days = league_get_days();
    $form['team']['night'] = array(
        '#title' => t('Match Night'),
        '#type' => 'select',
        '#options' => $days,
        '#attributes' => array('class' => array('night-selector') ),
    );

    $form['team']['name'] = array(
        '#type' => 'textfield',
        '#size' => 20,
        '#title' => t('Team Name'),
        '#required'  => TRUE,
    );

    $form['team']['button'] = array(
        '#value' => t('Add Team To Event'),
        '#type' => 'submit',
    );

    // Add java script to set default team name as club selected
    $js = league_event_next_team_js($clubs,$event);
    drupal_add_js($js, 'inline');
    $path = drupal_get_path('module', 'league');
    drupal_add_js($path . '/team.js');
  }
  return $form;
}

/**
 *  Action after the form is submitted.
 */
function league_team_form_submit($form, &$form_state) {
  $node = $form_state['values']['node'];
  $event = $node->field_event['und'][0]['nid'];
  // Check access
  if (!league_check_access($event)) {
    drupal_set_message('You do not have access to do this','warning');
    return;
  }
  $name = $form_state['values']['name'];
  $club = $form_state['values']['club'];
  $night = $form_state['values']['night'];
  $node->title = $name;
  $node->field_club['und'][0]['value'] = $club;
  $node->field_night['und'][0]['value'] = $night;
  node_save($node);
  drupal_set_message("Team $name added");
}

function league_event_teams_list($event,$data) {
  $links = array();
  foreach ( $data as $nid => $team ) {
    $links[] = l($team['name'], "league_team/$nid") . '(' . league_day2string($team['night']) . ')';
  }

  $content = array(
    '#items' => $links,
    '#theme' => 'item_list',
  );
  return drupal_render($content);
}


/**
 *  Get javascript list of next team names for each club.
 */
function league_event_next_team_js($clubs,$event) {
  $org = league_event_value($event, 'org');
  $type = league_event_value($event, 'type');
  // If its a league event, then we want to assign team names
  // by any division in that season.
  // Otherwise just look at teams in that event only
  if($type == 'L') {
    $season = league_event_value($event, 'season');
    $teams = league_org_teams($org, $season);
  } else {
    $teams = league_event_teams($event);
  }
  $next = array();
  $org_team_designation = league_org_value($org, 'designation');
  foreach($clubs as $key => $club) {
//  drupal_set_message("key $key Club $club $org_team_designation");
    $next[$key] = "$club $org_team_designation";
  }
  $next_teams = array();
// For each proposed new team name ...
  foreach($next as $clubId => &$teamName) {
    $newTeamName = $teamName;
    // Look at any existing teams
    foreach($teams as $id => $team) {
//    $name = $team['name'];
      $name = $team;
      // If same club name
      if(substr($name,0,-2) == substr($newTeamName,0,-2) ) {
        // If designation equal or higher
        if(substr($name,-1) >= substr($newTeamName,-1) ) {
          $designation = substr($name,-1);
          $designation++;
          $newTeamName = substr($name,0,-1) . $designation;
//        drupal_set_message("$name $teamName $clubId $newTeamName");
        }
      }
    }
    $next_teams[$clubId] = $newTeamName;
  }
  $js = "var next_teams=new Object;\n";
//foreach($next as $clubId => $teamName) {
  foreach($next_teams as $clubId => $teamName) {
    $js .= "next_teams['" . $clubId . "']='" . $teamName . "';\n";
  }
  return $js;
}
