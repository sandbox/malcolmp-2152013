<?php
/**
 * @file
 * Functions for League seasons admin.
 */

/**
 * Organisation season tab.
 */
function league_admin_tab_season($node) {

  // Check if its season edit
  $edit = arg(4);
  if(isset($edit)) {
    $season = arg(5);
    if(isset($season)) {
      if (!league_check_access($node->nid)) {
        drupal_set_message('You do not have access to do this','warning');
        drupal_goto('node/' . $node->nid . '/league/season');
      } else {
        // Season Edit form
        if($edit == 'order') {
          return league_season_team_order($node->nid,$season);
        } else {
          if($edit == 'copy') {
            return league_admin_copy_season($node->nid,$season);
          } else {
            return league_admin_edit_season($node->nid,$season);
          }
        }
      }
    } else {
      if($edit == 'add') {
        return league_admin_add_season($node->nid);
      }
    }
  }
  
  $content['link']['#type'] = 'markup';
  $content['link']['#markup'] = l('Add Season','node/' . $node->nid . '/league/season/add');

  // Table of new or active seasons
  $table = league_season_table($node->nid, FALSE);
  $content['new']['#table'] = $table;
  $content['new']['#theme'] = 'league_table';

  // Table of old seasons
  $table = league_season_table($node->nid, TRUE);
  $content['old']['#table'] = $table;
  $content['old']['#theme'] = 'league_table';

  return $content;
}

/**
 *  Action after the form is submitted
 */
function league_season_save_validate($form, &$form_state) {
}

/**
 *  Action after the form is submitted
 */
function league_season_save_submit($form, &$form_state) {
  $season = $form['#league_event'];
  $org = $season->org;
  if (!league_check_access($org)) {
    drupal_set_message('You do not have access to do this','warning');
    return;
  }
  $season->lms = $form_state['values']['status'];
  $season->name = $form_state['values']['name'];
  $season->display_order = $form_state['values']['display_order'];
  league_event_save($season);
  drupal_set_message("Season Saved");
  drupal_goto("node/$org/league/season");
}

function league_season_copy_submit($form, &$form_state) {
  $season = $form['#league_event'];
  $org = $season->org;
  $sid = $season->eid;
  drupal_set_message("Copy season $sid in org $org");
  $url = "node/$org/league/season/copy/$sid";
  drupal_goto($url);
}

/**
 *  Action after the copy form is submitted.
 */
function league_season_copy_form_submit($form, &$form_state) {
  $season = $form['#league_event'];
  $org = $season->org;

  if (!league_check_access($org)) {
    drupal_set_message('You do not have access to do this','warning');
    return;
  }

  $name = $form_state['values']['name'];
  $status = $form_state['values']['status'];
  $teams = $form_state['values']['teams'];

  // Create new season 
  $event = entity_get_controller('league_event')->create($org);
  $event->name = $name;
  $event->lms = 'A';
  league_event_save($event);

  league_season_clone($org, $season->eid, $event->eid);
  $new_season = $event->name;
  drupal_set_message("New season $new_season created with status active");

  // Mark existing season in-active
  if( $status ) {
    $oldevent = league_event_load($season->eid);
    $oldevent->lms = 'O';
    league_event_save($oldevent);
    drupal_set_message('Season ' . $oldevent->name . ' set to status old');
  }

  // Route to team order on new season
  if( $teams ) {
    drupal_goto('node/' . $org . '/league/season/order/' . $event->eid);
  } else {
    drupal_goto('node/' . $org . '/league/season');
  }
}

function league_season_clone($org, $season, $new_season) {
  $lang = LANGUAGE_NONE;

  $data = league_season_data($org, $season);
  $team_mapping = array();

  // Foreach event in the org for the season ...
  $events = $data['#events'];
  foreach($events as $event => $edata) {

    // Create a new event in the new season
    $node = node_load($event);
    unset($node->nid);
    unset($node->vid);
    unset($node->created);
    $node->league_eid = $new_season;
    module_invoke_all('league_event_clone', $node);
    node_save($node);
    $new_event = $node->nid;

    // For each team in the old event ...
    $teams = $edata['#teams'];
    $new_teams = array();
    foreach($teams as $team => $teamName) {

      // Create a team in the new event and store mapping
      $tnode = node_load($team);
      unset($tnode->nid);
      unset($tnode->vid);
      $tnode->field_event['und'][0]['nid'] = $new_event;
      // Set the team order, just in case its a team before this was added
      if(!isset($tnode->field_team_order['und'])) {
        $tnode->field_team_order['und'][0]['value'] = '1';
      }
      // Remove team penalties if there are any.
      if(isset($tnode->field_team_penalty['und'])) {
        $tnode->field_team_penalty['und'][0]['value'] = '0';
      }
      // save the new team
      node_save($tnode);
      $new_team = $tnode->nid;
      $team_mapping[$team] = $new_team;
      $new_teams[] = $tnode;
    }

    // For each team in the new event ...
    foreach($new_teams as $t => $tnode) {
      // Add its team avoids to the new team using the mapping
      if(isset($tnode->field_teams[$lang])) {
        $new_avoids = array();
        foreach ( $tnode->field_teams[$lang] as $av => $avoid) {
          if (isset($tnode->field_teams[$lang][$av]['value']) ) {
            $v_old = $tnode->field_teams[$lang][$av]['value'];
            if( isset($team_mapping[$v_old]) ) {
              $v_new = $team_mapping[$v_old];
              $new_avoids[]['value'] = $v_new;
            }
          }
        }
        $tnode->field_teams[$lang] = $new_avoids;
        node_save($tnode);
      }
    }
  }
}

/**
 *  Action after the form is submitted.
 */
function league_season_order_submit($form, &$form_state) {
  $season = $form_state['league_event'];

  drupal_goto('node/' . $season->org . '/league/season/order/' . $season->eid);
}

/**
 *  Action after the form is submitted.
 */
function league_season_export_submit($form, &$form_state) {
  $season = $form_state['league_event'];
  drupal_goto('/league/exports/' . $season->eid);
}

function league_season_delete_page($season) {
  return drupal_get_form('league_season_delete_confirm_form', $season);
}

function league_season_copy_form($form, &$form_state, $season) {
  $form['#league_event'] = $season;
  $form_state['league_event'] = $season;

  $form['name'] = array(
   '#type'  => 'textfield',
   '#size'  => 20,
   '#maxlength' => 30,
   '#title'  => t('New Season Name'),
   '#required'  => TRUE,
  );

  // Team Edit checkbox
  $form['teams'] = array(
   '#type'  => 'checkbox',
   '#title'  => t('Go into the team order option'),
   '#description' => t('You will be taken to the team order option to move teams between divisions. You can do this later if not now.'),
  );

  // Old status box.
  $form['status'] = array(
   '#type'  => 'checkbox',
   '#title'  => t('Set status to old'),
   '#description' => t('The status of the season you are copying will be set to OLD. You can do this later if not now.'),
  );

  $form['buttons'] = array();
  $form['buttons']['#weight'] = 100;
  $form['buttons']['submit'] = array(
    '#value' => t('Create New Season'),
    '#type' => 'submit',
  );

  // This prevents the cancel button forcing you to specify a season name.
  $form['buttons']['cancel'] = array(
    '#type' => 'button',
    '#value' => t('Cancel'),
    '#attributes' => array('onClick' => 'history.go(-1); event.preventDefault();'),
  );

  return $form;
}


function league_season_delete_confirm_form($form, &$form_state, $season) {
  if(arg(3)=='del') {
    drupal_set_message("delete season");
  }
  $org = $season->org;
  $title = $season->name;
  $url = "node/$org/league/season/edit/$season->eid";
  $events = league_events_for_org($org, ' ', $season->eid, array('A', 'N', 'O'));
  $form['#league_competitions'] = $events;
  $event_text = 'This will remove these ' . count($events) . ' events: ';
  foreach($events as $key => $value) {
    $event_text .= $value['name'] . ', ';
  }

  $form = confirm_form($form,
      "Are you sure, Do you want to delete season $title ?",
      $url,
      'The action cannot be undone. ' . $event_text . ' An alternative is to set the season status to old so it no longer appears',
      'Delete Season',
      'Cancel'
  );
  $form['#league_event'] = $season;
  return $form;
}

function league_season_delete_submit($form, &$form_state) {
  $season = $form['#league_event'];
  $org = $season->org;

  drupal_goto('league/delseason/' . $season->eid);
}


/**
 *  Action after the form is submitted.
 */
function league_season_delete_confirm_form_submit($form, &$form_state) {
  $season = $form['#league_event'];
  $org = $season->org;

  if (!league_check_access($org)) {
    drupal_set_message('You do not have access to do this','warning');
    return;
  }
  $data = league_season_data($org, $season->eid);
  $events = $data['#events'];

  // Check for locked fixtures
  $locked = 0;
  foreach($events as $event => $edata) {
    $locked += league_locked_fixtures($event);
  }
  if ( $locked > 0) {
    drupal_set_message('You must unlock fixtures in this season before you do this because the fixtures will be deleted','warning');
    return;
  }

  league_season_delete($data,$season->eid);
  drupal_goto('node/' . $org . '/league/season');
}

/**
 *  Delete a season.
 */
function league_season_delete($data,$season) {
//drupal_set_message(print_r($data,TRUE));
  // For each event ...
  $events = $data['#events'];
  foreach($events as $event => $edata) {
    drupal_set_message("Deleting event $event from season $season.");
    //   Remove all the fixtures
    league_delete_fixtures($event);
    //   For each team ...
    $teams = $edata['#teams'];
    foreach($teams as $team => $teamName) {
      // Delete the team
      node_delete($team);
    }
    // Delete the event
    node_delete($event);
  }
  league_event_delete($season);
  drupal_set_message("Season $season deleted.");
}

/**
 *  Option to re-order the teams in a season and move them between events.
 *
 *  NOTE: This does not work when an event has no teams. 
 *        Not sure why, Might be a drupal issue.
 */
function league_season_team_order($org, $season) {
  if (!league_check_access($org)) {
    drupal_set_message('You do not have access to do this','warning');
    return;
  }
  $data = league_season_data($org, $season);
  $season_name = $data['#league_event']->name;

  $content = array();
  $content['header']['#type'] = 'markup';
  $content['header']['#markup'] = "<h3>Team order for season: $season_name</h3>";
  $content['form'] = drupal_get_form('league_season_team_order_form', $data);
  return $content;
}

/** 
 *  Get all the data for a season
 * Parameters:
 *  org    - organisation of interest.
 *  season - season
 */
function league_season_data($org, $season) {
//drupal_set_message("league_season_data $org $season");
  $data = array();
  $data['#org'] = $org;
  $data['#season'] = $season;
  $data['#league_event'] = league_event_load($season);

  $query = db_select('league_competition', 'f');
  $query->condition('f.eid', $season);
  $query->addField('f', 'cid', 'entity_id');
  $query->innerJoin('node', 'n', 'n.nid = f.cid');
  $query->addField('n', 'title', 'eventName');
  $query->addField('f', 'type', 'eventType');
  $query->leftJoin('field_data_field_event', 'e', 'e.field_event_nid = f.cid AND e.bundle=:bundle',array(':bundle' => 'team'));
  $query->addField('e', 'entity_id', 'teamNid');
  $query->leftJoin('node', 'en', 'en.nid = e.entity_id');
  $query->addField('en', 'title', 'teamName');
  $query->leftJoin('field_data_field_team_order', 'ord', 'ord.entity_id = e.entity_id');
  $query->orderBy('f.cid', 'ASC')->orderBy('field_team_order_value', 'ASC');

//dsm((string)$query);
//drupal_set_message((string)$query);
  $result = $query->execute();
  $event_teams = array();
  $events = array();
  foreach ( $result as $row) {
    $events[$row->entity_id]['#event'] = $row->eventName;
    $events[$row->entity_id]['#type'] = $row->eventType;
    if(isset($row->teamNid)) {
      $event_teams[$row->entity_id][$row->teamNid] = $row->teamName;
    }
  }
  // Initialise in case no teams
  foreach ( $events as $entity_id => $eventData ) {
    $events[$entity_id]['#teams'] = array();
  }
  // Get teams
  foreach ( $event_teams as $eventNid => $edata ) {
    $teams = array();
    foreach ( $edata as $teamNid => $teamName ) {
      $teams[$teamNid] = $teamName;
    }
    $events[$eventNid]['#teams'] = $teams;
  }
  $data['#events'] = $events;
  return $data;
}

/**
 *  Form to edit season.
 */
function league_season_team_order_form($form, &$form_state, $data) {

  $form_state['league_season'] = $data;
  $form['#tree'] = TRUE;
  $form['#theme'] = 'league_season_edit';

  $events = $data['#events'];
  $org = $data['#org'];
  $season = $data['#season'];
  $ecount = 0;
  foreach($events as $event => $edata) {
    $eventName = $edata['#event'];
    $eventType = $edata['#type'];
    $ecount++;
//  drupal_set_message("Setting up event $event $ecount");
    $form['events'][$event]['event'.$ecount]['label'] = array(
      '#type' => 'item',
      '#markup' => $eventName,
    );
    $form['events'][$event]['event'.$ecount]['tevent'] = array(
      '#type' => 'hidden',
      '#title' => t('Event'),
      '#title_display' => 'invisible',
      '#default_value' => $event,
    );
    $teams = $edata['#teams'];
    $t=0;
    foreach($teams as $team => $teamName) {
      $t++;
      $form['events'][$event][$team]['label'] = array(
        '#type' => 'item',
        '#markup' => $teamName,
      );
      // This field is invisible, but contains sort info (weights).
      // (need to become a field in the team)
      $form['events'][$event][$team]['weight'] = array(
        '#type' => 'weight',
        '#title' => t('Weight'),
        '#title_display' => 'invisible',
        '#default_value' => $t,
      );
      $form['events'][$event][$team]['tevent'] = array(
        '#type' => 'hidden',
        '#title' => t('Event'),
        '#title_display' => 'invisible',
        '#default_value' => $event,
      );
      $form['events'][$event][$team]['tteam'] = array(
        '#type' => 'hidden',
        '#title' => t('Team'),
        '#title_display' => 'invisible',
        '#default_value' => $team,
      );
    }

  }

  $form['buttons'] = array();
  $form['buttons']['#weight'] = 100;
  $form['buttons']['submit'] = array(
    '#value' => t('Save'),
    '#type' => 'submit',
    '#submit'  => array('league_season_edit_save'),
  );

  $form['buttons']['delete'] = array(
    '#value' => t('Cancel'),
    '#type' => 'submit',
    '#submit'  => array('league_season_edit_cancel'),
  );

  return $form;
}

// Theme function for league season edit form.
function theme_league_season_edit($variables) {
  $form = $variables['form'];
 
  // Create an array of table rows
  $rows = array();
  foreach (element_children($form['events']) as $event) {
    foreach (element_children($form['events'][$event]) as $team) {
    // Row for the event (not dragable)
    if( substr($team,0,5) == 'event' ) {
//      drupal_set_message("Setting up event for $team");
        $form['events'][$event][$team]['tevent']['#attributes']['class'] = array('events-order-event','events-order-event-' . $event);
      $indent = theme('indentation', array('size' => '0'));
      $rows[] = array(
        'data' => array(
          array('class' => array('event-cross')),
          $indent . drupal_render($form['events'][$event][$team]['label'])
          . drupal_render($form['events'][$event][$team]['tevent'])
                 , 
          1,               // weight
        ),
        'class' => array('tabledrag-root'),
      );
    } else {
    // Row for the team (indented, dragable)
        $form['events'][$event][$team]['weight']['#attributes']['class'] = array('events-order-weight');
        $form['events'][$event][$team]['tevent']['#attributes']['class'] = array('events-order-event','events-order-event-' . $event);
        $form['events'][$event][$team]['tteam']['#attributes']['class'] = array('events-order-team');
         $indent = theme('indentation', array('size' => '1'));
          $row = array(
            'data' => array(
              array('class' => array('event-cross')),
              $indent . drupal_render($form['events'][$event][$team]['label']).
              drupal_render($form['events'][$event][$team]['tevent']) . 
              drupal_render($form['events'][$event][$team]['tteam']),
              drupal_render($form['events'][$event][$team]['weight']),
            ),
            'class' => array('draggable','tabledrag-leaf',''),
          );
          $rows[] = $row;
        }
      }
    }
 
  // Table header
  $header = array('Pick cross and drag to move team', t('Event / Team'), t('Weight') );
  $output = ' ';
  $output .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'events-order')));
  $output .= drupal_render_children($form);
 
  drupal_add_tabledrag('events-order',         // table id
                       'order',                // operation
                       'sibling',
                       'events-order-weight'); // Class of weights column
  drupal_add_tabledrag('events-order',         // table id
                       'match',                // operation
                       'sibling',              // relationship 
                       'events-order-event',   // class identifying group
                       'events-order-event',   // Field containing sub group
                       'events-order-event',   // Field to update value of?
                       FALSE,                  // Hide column
                       1
                       ); 
 
  return $output;
}

/**
 *  Action after the season edit form is submitted.
 */
function league_season_edit_cancel($form, &$form_state) {
  $org = $form_state['league_season']['#org'];
  $season = $form_state['league_season']['#season'];
  drupal_goto("node/$org/league/season");
}

/**
 *  Action after the season edit form is submitted.
 */
function league_season_edit_form_cancel($form, &$form_state) {
  $season = $form_state['league_event'];
  $org = $season->org;
  drupal_goto("node/$org/league/season");
}

/**
 *  Action after the season edit form is submitted.
 */
function league_season_copy_cancel($form, &$form_state) {
  $org = $form_state['league_season']['#org'];
  $season = $form_state['league_season']['#season'];
  drupal_goto("node/$org/league/season");
}

/**
 *  Action after the season edit form is submitted.
 */
function league_season_edit_save($form, &$form_state) {
  $org = $form_state['league_season']['#org'];
  $season = $form_state['league_season']['#season'];
  $data = $form_state['league_season'];

  if (!league_check_access($org)) {
    drupal_set_message('You do not have access to do this','warning');
    return;
  }
//drupal_set_message(print_r($form_state['values']['events'], TRUE));
  $events = $form_state['values']['events'];
//drupal_set_message('Event count:' . count($events) );

  // Check for locked fixtures
  $locked = 0;
  foreach($events as $event => $edata) {
    $locked += league_locked_fixtures($event);
  }

  if ( $locked > 0 ) {
    drupal_set_message("Ignoring Changes to season because there are locked fixtures",'warning');
  } else {
    // For each event ...
    foreach($events as $event => $edata) {
//    drupal_set_message("Checking event $event");
      // Get the teams and sort by weight
      $teams = array();
      foreach($edata as $team => $data) {
        if( substr($team,0,5) != 'event' ) {
//        drupal_set_message("Checking team $team new event:" . $data['tevent']);
          $teams[$team] = array(
             'weight' => $data['weight'],
             'new_event' => $data['tevent'],
          );
        }
      }
      uasort($teams, 'drupal_sort_weight');
      // Reset the weights and also the event 
      // incase the team has been moved between events
      $new_weight = 0;
      foreach($teams as $team => $tdata) {
        $new_weight++;
        $new_event = $tdata['new_event'];
        $weight = $tdata['weight'];
        // Load team node, set event and weight, and save
        $node = node_load($team);
        $node->field_team_order['und'][0]['value'] = $new_weight;
        // Only change the event if the team had no fixtures.
        if($node->field_event['und'][0]['nid'] != $new_event) {
          require_once ('league_fixture.inc');
          $table = league_fixtures_by_org($org, 'team', $node->nid);
          if(count($table['data']) > 0) {
            $teamName = $node->title;
            drupal_set_message("Movement of team $teamName ignored because it already has fixtures, delete fixtures first.", 'error');
          } else {
            $node->field_event['und'][0]['nid'] = $new_event;
          }
        }
        
        node_save($node);
      }
    }
    drupal_set_message("Changes to season saved");
  }
  drupal_goto("node/$org/league/season");
}

function league_admin_add_season($org) {
  $season = entity_get_controller('league_event')->create($org);
  $season->lms = 'N';
  $season->name = 'New Season';
  $content['form'] = drupal_get_form('league_season_edit_form', $season);

  return $content;
}

function league_admin_edit_season($org,$sid) {
  $season = league_event_load($sid);
  $content['form'] = drupal_get_form('league_season_edit_form', $season);

  return $content;
}

function league_admin_copy_season($org,$sid) {
  $season = league_event_load($sid);
  $season_name = $season->name;

  $events = league_events_for_org($org, ' ', $sid, array('A', 'N', 'O'));
  $event_text = "This option will create a new season by copying $season_name . The new season will contain the same " . count($events) . ' events at the old season, but with no fixtures or results. The events to be copied are: ';
  foreach($events as $key => $value) {
    $event_text .= $value['name'] . ', ';
  }

  $content = array();
  $content['header']['#type'] = 'markup';
  $content['header']['#markup'] = "<h3>Create new season by copying: $season_name</h3>$event_text";
  $content['form'] = drupal_get_form('league_season_copy_form', $season);

  return $content;
}

/**
 *  Form to edit season.
 */
function league_season_edit_form($form, &$form_state, $season) {
  $form['#league_event'] = $season;
  $form_state['league_event'] = $season;

  $form['season']['name'] = array(
   '#type'  => 'textfield',
   '#size'  => 20,
   '#title'  => t('Season Name'),
   '#default_value'  => $season->name,
   '#maxlength' => 30,
  );

  $form['season']['display_order'] = array(
   '#type'  => 'textfield',
   '#size'  => 20,
   '#title'  => t('Season Order'),
   '#description'  => t('Determines order multiple seasons are listed in.'),
   '#default_value'  => $season->display_order,
  );

  $form['season']['status'] = array(
   '#prefix' => '<div id=active-select>',
   '#suffix' => '</div>',
   '#type'  => 'radios',
   '#title'  => t('Season Status'),
   '#description'  => t('A status of NEW is not shown on the lefthand side so the organiser can set up fixtures without the users seeing them. A status of OLD means the events only appear under the Previous Seasons option. Active is the normal status.'),
   '#default_value'  => $season->lms,
   '#options'  => array( 'N' => 'New', 'A' => 'Active', 'O' => 'Old' ),
  );

  $form['buttons'] = array();

  $form['buttons']['save'] = array(
    '#value' => t('Save'),
    '#type' => 'submit',
    '#submit'  => array('league_season_save_submit'),
    '#validate'  => array('league_season_save_validate'),
  );

  if(!empty($season->eid)) {
    $form['buttons']['submit'] = array(
      '#value' => t('Copy'),
      '#type' => 'submit',
      '#submit'  => array('league_season_copy_submit'),
    );

    $form['buttons']['delete'] = array(
      '#value' => t('Delete'),
      '#type' => 'submit',
      '#submit'  => array('league_season_delete_submit'),
    );

    $form['buttons']['order'] = array(
      '#value' => t('Team Order'),
      '#type' => 'submit',
      '#submit'  => array('league_season_order_submit'),
    );

    $form['buttons']['export'] = array(
      '#value' => t('Export'),
      '#type' => 'submit',
      '#submit'  => array('league_season_export_submit'),
    );

    $form['buttons']['cancel'] = array(
      '#value' => t('Cancel'),
      '#type' => 'submit',
      '#submit'  => array('league_season_edit_form_cancel'),
    );

    $form['text'] = array(
    '#markup' => '<p>Use the Copy button to create a complete copy of a season and all its events, when starting a new season. You will get a chance to specificy a name for the new season and to move teams between divisions for promotion and relegation. The Export button saves all the results for the season into a JSON file with the aim of it being able to be reload it into the system in case of accidently deletions without having to reset the whole LMS, although the reload option is not available yet.',
    '#type' => 'markup',
    );
  }

  return $form;
}

// Return a table of seasons

function league_season_table($org, $old=FALSE ) {
  $header = array('Season (Edit)', 'Status', 'Copy', 'Delete', 'Team Order');
  $rows = array();
  $query = db_select('league_event', 'le');
  $query->condition('le.org', $org);
  $query->fields('le', array('eid','name','lms'));
  $query->orderBy('name', 'ASC');
  if( $old ) { 
    $query->condition('le.lms', 'O');
  } else {
    $query->condition('le.lms', array('A', 'N'), 'IN' );
  }
//dsm((string)$query);
  $result = $query->execute();

  $options = array (
    'attributes' => array (
       'class' => array('button', 'form-submit'),
       'title' => 'Title',
    ),
    'html' => TRUE,
  );

  foreach ( $result as $row) {
    $sid = $row->eid;
    $link = l($row->name, "node/$org/league/season/edit/$sid");
    $status = league_event_lms_status($row->lms);
    $copy = l('Copy', "node/$org/league/season/copy/$sid", $options);
    $delete = l('Delete', "league/delseason/$sid", $options);
    $order = l('Team Edit', "node/$org/league/season/order/$sid", $options);
    $rows[] = array($link, $status, $copy, $delete, $order);
  }
  $table['data'] = $rows;
  $table['header'] = $header;
  return $table;
}
