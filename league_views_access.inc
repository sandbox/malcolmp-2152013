<?php
 
/**
 * Access plugin that provides property based access control.
 */
class league_view_object_access_plugin extends views_plugin_access {
 
  function summary_title() {
    return t('League Object Owner');
  } // summary_title()
 
/**
 * Determine if the current user has access or not.
 */
  function access($account) {    
    return league_view_object_access($account);
  }
 
  function get_access_callback() {
    return array('league_view_object_access', array());
  }
 
}
