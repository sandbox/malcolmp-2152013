<?php
/**
 * @file
 * Functions for League cron functions.
 */

// Replace http with https. 
// NOTE: This means that the links won't work in dev or test
// (so might need a temporary hack! )

function league_http2https($string) {
  $link = str_replace('http:', 'https:',$string);
  return $link;
}

// Send emails to subscribed users for status updates

function league_send_status_updates() {
  // Get subscribed users by org
  $flag = 'league_status_updates';
  $orgs = league_subscribed_users($flag);
  watchdog('league', 'Organisations to send status updates for: ' . count($orgs));
  // Get current status
  $league_status = variable_get('league_status',array());
  // For each org ...
  foreach($orgs as $org => $subs) {
//  watchdog('league', "Getting status for $org");
    // Get last status
    $status = league_org_status_block($org );
    if( isset($league_status[$org]) ) {
      // If its changed, send an email
      if( $status != $league_status[$org] ) {
        $orgTitle = league_org_value($org, 'title');
        $text = '<h2>Status Change</h2>There has been a status update to ';
        $text .= league_http2https(l($orgTitle, "node/$org/home", array('absolute' => TRUE,) ) );
        $text .= "</sl>";
        $mailfrom = variable_get('site_mail', NULL);
        $mailto = league_send_get_mail_to($subs);
        watchdog('league', "Send mail updates for $flag $org to $mailto");
        // email data
        $params = array(
              'subject' => "Status Update for $orgTitle",
              'body' => $text,
        );
        // Send the admin email
        drupal_mail('league',           // module implementing hook_mail
                    'html',             // key defining this mail
                    $mailto,            // email addresses to send to
                    language_default(),
                    $params,
                    $mailfrom,          // from email address
                    TRUE);
      } else {
        watchdog('league', "No updates for $flag $org");
      }
    }
    $league_status[$org] = $status;
  }
  variable_set('league_status', $league_status);
}

function league_send_get_mail_to($subs) {
  if( league_setting_mailtest() ) {
    $mailto = variable_get('site_mail', NULL);
  } else {
    $emails = array();
    foreach($subs as $uid) {
      $user = user_load($uid);
      $emails[] = $user->mail;
    }
    $mailto = implode(',', $emails);
  }
  return $mailto;
}

// Send emails to subscribed users for updated fixtures

function league_send_fixture_updates() {
  // Get subscribed users by org
  $flag = 'league_fixture_updates';
  $orgs = league_subscribed_users($flag);
  watchdog('league', 'Organisations to send fixture updates for: ' . count($orgs));

  // Get last time cron ran
  $since = variable_get('cron_last');

  // For each org ...
  foreach($orgs as $org => $subs) {
    watchdog('league', "Getting updates for $org");
    // Get list of updated fixtures
    $fixtures = league_get_updated_fixtures($org, $since);
    // If there are any, then create an email to those users
    if(count($fixtures) > 0) {
      $text = '<h2>Match Updates</h2>Updates have been received for the following matches:<sl>';
      foreach($fixtures as $fixture => $title) {
//      watchdog('league', "Send mail updates for fixture $fixture title $title ");
        $text .= '<li>';
        $text .= league_http2https(l($title, "node/$fixture", array('absolute' => TRUE,) ) );
      }
      $text .= "</sl>";
      $mailfrom = variable_get('site_mail', NULL);
// TODO might have SPAM issues with multiple people in the to,
//   need to send seperate mails...
      $mailto = league_send_get_mail_to($subs);
      $orgTitle = league_org_value($org, 'title');
      watchdog('league', "Send mail updates for $flag $org to $mailto");
      // email data
      $params = array(
            'subject' => "Match Updates for $orgTitle",
            'body' => $text,
      );
      // Send the admin email
      drupal_mail('league',           // module implementing hook_mail
                  'html',             // key defining this mail
                  $mailto,            // email addresses to send to
                  language_default(),
                  $params,
                  $mailfrom,          // from email address
                  TRUE);
    } else {
      watchdog('league', "No updates for $flag $org");
    }
  }
}

// Given a particular flag, return list of users and orgs for that flag
// (flag = type of mail)
function league_subscribed_users($flag) {
  $subs = array();
  $query = db_select('flag', 'f');
  $query->condition('f.name', $flag);
  $query->innerJoin('flagging', 'fl', 'fl.fid = f.fid');
  $query->addField('fl', 'uid', 'uid');
  $query->addField('fl', 'entity_id', 'org');
  $result = $query->execute();
  foreach ( $result as $row) {
    if(isset($subs[$row->org]) ) { 
      $subs[$row->org][] = $row->uid;
    } else {
      $subs[$row->org] = array($row->uid);
    }
  }
//watchdog('league', "Getting users for $flag " . print_r($subs, TRUE));

  return $subs;
}

function league_get_updated_fixtures($org, $since) {
  watchdog('league', "league_get_updated_fixtures $org $since");
  $fixtures = array();
  // Fixtures updated since
  $query = db_select('node', 'f');
  $query->condition('f.type', 'fixture');
  $query->condition('f.changed', $since, '>');
  $query->addField('f', 'nid', 'nid');
  // Fixtures with results
  $query->innerJoin('league_match', 'm', 'm.fid = f.nid');
  $query->leftJoin('league_chess_result', 'r', 'r.mid = m.mid');
  $query->where('r.rid is not null');
  $query->distinct();
  // Event that the fixture belongs to
  $query->innerJoin('league_fixture', 'e', 'e.nid = f.nid');
  $query->addField('e', 'cid', 'eventId');
  // Get event type.
  $query->innerJoin('league_competition', 'comp', 'comp.cid = e.cid');
  $query->condition('comp.type', array('L','J','K'), 'IN');
  // Events in the specified organisation
  $query->innerJoin('league_event', 'le', 'le.eid = comp.eid');
  $query->condition('le.org', $org);
  // Fixture title
  $query->innerJoin('node', 'ht', 'ht.nid = e.home_team');
  $query->innerJoin('node', 'at', 'at.nid = e.away_team');
  $query->addField('ht', 'title', 'home');
  $query->addField('at', 'title', 'away');

  $result = $query->execute();

  foreach ( $result as $row ) {
    $fixtures[$row->nid] = $row->home . ' v ' . $row->away;
  }

  return $fixtures;
}

// Send emails about un-verified fixtures

function _league_fixture_verify_alert() {
  watchdog('league', 'sending verify emails');
  //TODO = this is way out of date
  // Get the fixtures which meet the following criteria:
  // -Orgs that have the option turned on.
  // -Team events in active seasons.
  // -Fixtures that are: - unlocked (lock=0),
  //                     - dated in the past
  //                     - have a complete result
  //                     - are not verified.
  //     ( fid, owners or captains, update uid, away team/club, home team/club )
  // For each of these fixtures ...
  //   Mark the fixture so we don't do this again (set locked=2)
  //   _chessdb_fixture_verify_lock($fixture);
  //   Get the email recpients (depending on option set for org):
  //     - club owners (returns 2 lists home and away)
  //     - team captains (returns 2 lists home and away)
  //   foreach list ...
  //     if it doesn't contain updater_uid add all to the list of recipients .
  //   send one mail for each person to avoid spam blocking

}

// Figure out who to email
//  Options: - No mails, mail clubs, mail team captains only.

function _chessdb_get_email_recipients($homeclub, $awayclub, $updaterUid) {
  $users = array();
  // If the last updater is not in the away club, then mail them
  // If the last updater is not in the home club, then mail them
  // Note: If an adminstrator not in either club updated it,
  // they both get mailed.
  $users = array_unique($users);
  foreach ($users as $uid) {
    // TODO Convert "user_load" to "user_load_multiple" if "$uid" is other than a uid.
    // To return a single user object, wrap "user_load_multiple" with "array_shift" or equivalent.
    // Example: array_shift(user_load_multiple(array(), $uid))
    $user_object = user_load($uid);
    $recipients .= $user_object->mail . ',';
  }
  // remove trailing comma
  return rtrim($recipients, ",");
}

function _chessdb_fixture_verify_lock($fixture) {
  watchdog('league', "marking fixture $fixture as email sent");
  // TODO Please convert this statement to the D7 database API syntax.
  /* db_query("UPDATE {chessdb_fixture} set locked=2 where id=%d", $fixture) */
  $result = NULL;
}

function _league_fixture_lock_lock($lock) {
  // Lock verified after 1 week (lock=2)
  $days = 7;
  // Lock un-verified after 3 weeks (lock=0)
  if ($lock == 0) {
    $days = 21;
  }
  watchdog('league', "locking fixtures: lock=$lock days=$days");
  // Set lock=1 for any fixtures passed the cut off date
  // Where:
  // -Organisation has locking set on
  // -Team events in the current season
  // -Fixtures matching specified lock value.
  // -Fxitures dated after specified days, and have a result
}
