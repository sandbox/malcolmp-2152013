<?php
// $Id$

// Admin Concacts tab
function league_admin_tab_contacts($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);

  // Display instructions message
  $org = $node->nid;
  $content['header'] = array(
    '#type' => 'markup',
    '#markup' => "<div>Pick on one of the organisers links below to access their contact form</div>",
  );

  // Get owners
  // Get user for type 'O'=organsiation, for $org, at level 'O'=owner
  $owners = user_tools_owners_list('O', $org, array('O'), 'name');

  // Display links to owner contact forms
  $links = array();
  foreach($owners as $uid => $owner) {
    $links[] = l($owner, "user/$uid/contact");
  }
  $content['list'] = array(
    '#items' => $links,
    '#theme' => 'item_list',
  );

  return $content;
}
