<?php
// $Id$
/**
 * @file
 * Theme a label value pair
 */

function theme_league_label_value($variables) {
  $element = $variables['element'];
  $field_name = $element['#name'];
  $field_label = $element['#label'];
  $field_value = $element['#value'];
  $output = '<div class="field field-label-inline ';
  $output .= $field_name;
  $output .= '">';
  $output .= '<div class="field-label">';
  $output .= $field_label;
  $output .= ':&nbsp</div>';
  $output .= '<div class="field-item even">';
  $output .= $field_value;
  $output .= '</div>';
  $output .= '</div>';
  return $output;
}
