<?php
// $Id$

// Event admin tab
function league_admin_tab_events($node) {
  // Set breadcrumb
  league_set_bread_crumb ($node);

  $org = $node->nid;

  // Provide a form for adding events for authorised users
  if(league_check_access($org)) {
    $content['form'] = drupal_get_form('league_event_admin_form', $org);
  } else {
    $content['header']['#type'] = 'markup';
    $content['header']['#markup'] = '<h3>You do not have access to add events</h3>';
  }

  $content['events']['#content'] = league_org_events_list($org, ' ', array('A', 'N') );
  $content['events']['#theme'] = 'league_divs_render';
  $content['events']['#div'] = "events";

  return $content;
}

/**
 *  Form for adding a new event.
 */
function league_event_admin_form($form, &$form_state, $org) {

  $form['org'] = array(
      '#type' => 'value',
      '#value' => $org,
  );

  $form['event'] = array(
      '#title' => t('Add Event'),
      '#type' => 'fieldset',
      '#attributes' => array('class' => array('events-fieldset')),
  );

  $form['event']['button'] = array(
      '#value' => t('Add Event To Organisation'),
      '#type' => 'submit',
      '#submit' => array('league_event_admin_form_submit'),
  );

  return $form;
}
/**
 *  Action after the form is submitted.
 */
function league_event_admin_form_validate($form, &$form_state) {
  $org = $form_state['values']['org'];
  $seasons = league_season_list($org);
  if( count($seasons) == 0 ) {
      form_set_error('button', 'You must have either a new or active season to create an event');
  }
}

/**
 *  Action after the form is submitted.
 */
function league_event_admin_form_submit($form, &$form_state) {
  $org = $form_state['values']['org'];
//$form_state['redirect'] = "node/add/event/$org";
  $form_state['redirect'] = "node/$org/addevent";
}



