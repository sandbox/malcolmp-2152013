<?php
/**
 * @file
 * Functions for flagged matches.
 */

/**
 * Flagged Matches report tab
 */
function league_report_tab_flagged($node) {
  // Set title
  $title = $node->title . ' - Flagged Matches';
  drupal_set_title($title);

  // Get table 
  $content['flagged']['#table'] = league_table_flagged_matches($node);
  $content['flagged']['#theme'] = 'league_table';

  return $content;
}

/**
 * Create query showing flagged matches
 */
function league_flagged_matches_query($org) {

  // fixtures by event
  $query = db_select('league_fixture', 'f');
  $query->fields('f', array('nid', 'date'));
  $query->innerJoin('league_competition', 'comp', 'comp.cid = f.cid');
  // Active Seasons
  $query->innerJoin('league_event', 'le', 'le.eid = comp.eid');
  $query->condition('le.lms', 'A');
  $query->addField('le', 'name', 'season');
  //  events for that organisation
  $query->condition('le.org', $org);
  // flag
  $query->innerJoin('flagging', 'fl', 'fl.entity_id = f.nid');
  // flag type
  $query->innerJoin('flag', 'fg', 'fg.fid = fl.fid');
  $query->addField('fg', 'title', 'flag');
  // event title
  $query->innerJoin('node', 'et', 'et.nid = f.cid');
  $query->addField('et', 'title', 'event');
  // fixture node
  $query->innerJoin('node', 'ht', 'ht.nid = f.home_team');
  $query->innerJoin('node', 'at', 'at.nid = f.away_team');
  $query->addField('ht', 'title', 'home');
  $query->addField('at', 'title', 'away');
//dsm((string)$query);

  return $query;
}

/**
 * Create table showing flagged matches
 */
function league_table_flagged_matches($node) {

  $query = league_flagged_matches_query($node->nid);
  $header = array();
  $header[] = array('data' => 'Season', 'field' => 'season' );
  $header[] = array('data' => 'Event', 'field' => 'event', 'sort'=>'asc' );
  $header[] = array('data' => 'Home Team', 'field' => 'home' );
  $header[] = array('data' => 'Away Team', 'field' => 'away' );
  $header[] = array('data' => 'Date', 'field' => 'date' );
  $header[] = array('data' => 'Flag', 'field' => 'flag' );

  // Make table sortable
  $query = $query->extend('TableSort')->orderByHeader($header);

  $rows = array();

  $result = $query->execute();
  foreach($result as $row) {
    $season = $row->season;
    $event = $row->event;
    $home = l($row->home, 'league_fixture/' . $row->nid . '/match' );
    $away = l($row->away, 'league_fixture/' . $row->nid . '/match' );
    $flag = $row->flag;
    $date = date('D jS M Y',$row->date);
    $rows[] = array($season, $event, $home, $away, $date, $flag);
  }
  $table['data'] = $rows;
  $table['header'] = $header;
  return $table;

}
/**
 * Get number of flagged matches
 */
function league_count_flagged_matches($org) {

  $cache_name = "league_count_flagged_matches_$org";
  $num_rows = &drupal_static($cache_name);
  if (!isset($num_rows)) {
    $use_cache = FALSE;
    if( $cache = cache_get($cache_name)) {
      $num_rows = $cache->data;
      // Do this check because cron may not be running.
      if(time() > $cache->expire) {
        $use_cache = TRUE;
      }
    }
    if(!$use_cache) {
  
      $query = league_flagged_matches_query($org);
      // fixtures by event
      $num_rows = $query->countQuery()->execute()->fetchField();
      // cache for a day because more matches might be late then
      cache_set($cache_name, $num_rows, 'cache', time()+360*24);
    }
  }
  return $num_rows;
}
