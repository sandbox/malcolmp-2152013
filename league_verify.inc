<?php
/**
 * @file
 * Verify Fixture Result
 */

/**
 * Verify Fixture Result
 */
function league_fixture_verify_tab($node) {
  if($node->league_locked == 1) {
    drupal_set_message("This fixture has been locked by the organisation owner and cannot be changed",'error');
    drupal_goto("league_fixture/$node->nid");
  }
  if (!user_is_logged_in()) {
    drupal_set_message("You must log in to verify a fixture",'error');
    drupal_goto("league_fixture/$node->nid");
  }
  $reporter = league_fixture_value($node, 'reporter');
  if($reporter == 0) {
    drupal_set_message("Fixtures with no reported result can't be verified.",'error');
    drupal_goto("league_fixture/$node->nid");
  }
  // get the access the verifier has
  global $user;
  $access = league_fixture_verify_access($node,$user->uid);
  if ($access == 'none') {
    drupal_set_message("You do not have access to verify this fixture",'error');
    drupal_goto("league_fixture/$node->nid");
  }
  // Set user who verified the fixture. Use an SQL query so it doesn't 
  // change the updated by user.
  $fixture = $node->nid;
//drupal_set_message("verify fixture $fixture user $user->uid");
  $query = db_update('league_fixture');
  $query->condition('nid', $fixture);
  $query->fields(array('verifier'=>$user->uid,'verify_time'=>time()));
  //dsm((string)$query);
  // NOTE: - the number of rows is zero if the fixture was already
  //         verified by this user.
  $rows = $query->execute();
  // Clear cache so change is picked up
  cache_clear_all("field:node:$fixture", 'cache_field', TRUE);

  // Clear the league_fixtures_event_query cache so the verify
  // appears on the fixture list.
  $event = league_fixture_value($fixture, 'event');
  $cache_name = "league_fixtures_event_query_$event";

  drupal_set_message("fixture verified by $access owner");
  // Go back to the match card view
  drupal_goto("league_fixture/$node->nid");
}

// returns type of access the verifier has.
// 'none' means they can't updated
function league_fixture_verify_access($node,$verifier) {
  $reporter = league_fixture_value($node, 'reporter');
  $lastUpdateUser = user_load($reporter);
  $verifyUser = user_load($verifier);
  $lastUpdaterInHome = FALSE;
  $lastUpdaterInAway = FALSE;
  $verifierInHome = FALSE;
  $verifierInAway = FALSE;
  // owners of the event can verify
  $event = league_fixture_value($node, 'event');
  if( league_check_access( $event, 'C', $verifier ) ) {
    return 'event';
  }
  // Team events
  $type = league_event_value($event, 'type');
  if( league_event_type_individual($type) ) {
    return 'none';
  }
  // get teams
  $homeTeam = league_fixture_value($node, 'homeTeam');
  $awayTeam = league_fixture_value($node, 'awayTeam');
  // get home team club
  $homeClub = league_team_value($homeTeam, 'club');
  $homeClubNode = node_load($homeClub);
  $awayClub = league_team_value($awayTeam, 'club');
  $awayClubNode = node_load($awayClub);
  // check if the last update user is a from one of the clubs
  if ( _league_check_users($homeClubNode,$lastUpdateUser) ) {
    $lastUpdaterInHome = TRUE;
  }
  if ( _league_check_users($awayClubNode,$lastUpdateUser) ) {
    $lastUpdaterInAway = TRUE;
  }
  // if the last updater is an event owner then any of the teams 
  // should be able to verify because it is assumed it was updated
  // in the capacity of owner.
  if(league_check_access( $event, 'C', $reporter ) ) {
    $lastUpdaterInHome = TRUE;
    $lastUpdaterInAway = TRUE;
  }
  // check if the verifier is a from one of the clubs
  if ( _league_check_users($homeClubNode,$verifyUser) ) {
    $verifierInHome = TRUE;
  }
  if ( _league_check_users($awayClubNode,$verifyUser) ) {
    $verifierInAway = TRUE;
  }
  // can verify if owner from the other club.
  if( $lastUpdaterInHome && $verifierInAway ) {
    return 'away team';
  }
  if( $lastUpdaterInAway && $verifierInHome ) {
    return 'home team';
  }
//drupal_set_message(" lastUpdaterInHome $lastUpdaterInHome lastUpdaterInAway $lastUpdaterInAway verifierInAway $verifierInAway verifierInHome $verifierInHome lastUpdateUser $lastUpdateUser->uid verifyUser $verifyUser->uid ");
  // The only case left is that the last updater was not a club owner
  // and so the verifier can be from either club.
  if( !$lastUpdaterInAway && !$lastUpdaterInHome) {
    if( $lastUpdateUser->uid != $verifyUser->uid ) {
      if( $verifierInHome ) {
        return 'home team';
      }
      if( $verifierInAway ) {
        return 'away team';
      }
    }
  }
  return 'none';
}
